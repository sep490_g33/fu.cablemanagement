﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class Supplier : CommonEntity
    {
        public Supplier()
        {
            Cables = new HashSet<Cable>();
            OtherMaterials = new HashSet<OtherMaterial>();
        }

        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string Country { get; set; }
        public Guid? CreatorId { get; set; }
        public string SupplierDescription { get; set; }

        public virtual User Creator { get; set; }
        public virtual ICollection<Cable> Cables { get; set; }
        public virtual ICollection<OtherMaterial> OtherMaterials { get; set; }
    }

    public static class SupplierConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Supplier>(entity =>
            {
                entity.ToTable("Supplier");

                entity.Property(e => e.Country)
                    .HasColumnType("nvarchar(200)");


                entity.Property(e => e.SupplierName)
                    .IsRequired()
                    .HasMaxLength(50);



                entity.HasOne(d => d.Creator)
                    .WithMany(p => p.Suppliers)
                    .HasForeignKey(d => d.CreatorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Supplier__Creato__300424B4");
            });
        }

    }
}
