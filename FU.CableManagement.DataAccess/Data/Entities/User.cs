﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class User : CommonEntity
    {
        public User()
        {
            Cables = new HashSet<Cable>();
            Issues = new HashSet<Issue>();
            RequestApprovers = new HashSet<Request>();
            RequestCreators = new HashSet<Request>();
            Suppliers = new HashSet<Supplier>();
            WarehouseCreators = new HashSet<Warehouse>();
            WarehouseWarehouseKeepers = new HashSet<Warehouse>();
        }

        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int RoleId { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual Role Role { get; set; }
        public virtual ICollection<Cable> Cables { get; set; }
        [NotMapped]
        public virtual ICollection<Issue> Issues { get; set; }
        public virtual ICollection<Request> RequestApprovers { get; set; }
        public virtual ICollection<Request> RequestCreators { get; set; }
        public virtual ICollection<Supplier> Suppliers { get; set; }
        [NotMapped]
        public virtual ICollection<Warehouse> WarehouseCreators { get; set; }
        [NotMapped]
        public virtual ICollection<Warehouse> WarehouseWarehouseKeepers { get; set; }
    }
    public static class UserConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.Property(e => e.UserId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__User__RoleId__276EDEB3");
            });
        }
    }
}
