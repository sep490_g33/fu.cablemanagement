﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class TransactionCable : CommonEntity
    {
        public Guid TransactionId { get; set; }
        public Guid CableId { get; set; }
        public int StartPoint { get; set; }
        public int EndPoint { get; set; }
        public int Length { get; set; }
        public string Note { get; set; }
        public virtual Cable Cable { get; set; }
        public virtual TransactionHistory Transaction { get; set; }
    }

    public static class TransactionCableConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TransactionCable>(entity =>
            {
                entity.HasKey(e => new { e.TransactionId, e.CableId })
                    .HasName("PK__Transact__FC2F10F68675DA26");

                entity.ToTable("TransactionCable");

                entity.Property(e => e.Note).HasMaxLength(255);

                entity.HasOne(d => d.Cable)
                    .WithMany(p => p.TransactionCables)
                    .HasForeignKey(d => d.CableId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Transacti__Cable__47DBAE45");

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.TransactionCables)
                    .HasForeignKey(d => d.TransactionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Transacti__Trans__46E78A0C");
            });
        }
    }
}
