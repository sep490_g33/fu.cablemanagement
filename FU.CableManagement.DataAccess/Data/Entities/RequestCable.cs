﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class RequestCable : CommonEntity
    {
        public Guid RequestId { get; set; }
        public Guid CableId { get; set; }
        public int StartPoint { get; set; }
        public int EndPoint { get; set; }
        public int Length { get; set; }
        public string? Status { get; set; }
        public int? RecoveryDestWarehouseId { get; set; }
        public int? ImportedWarehouseId { get; set; }
        public virtual Warehouse? RecoveryDestWarehouse { get; set; }
        public virtual Warehouse? ImportedWarehouse { get; set; }
        public virtual Cable Cable { get; set; }
        public virtual Request Request { get; set; }
    }

    public static class RequestCableConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RequestCable>(entity =>
            {
                entity.HasKey(e => new { e.RequestId, e.CableId , e.StartPoint, e.EndPoint})
                    .HasName("PK__RequestC__9AC47BE7FD0DAC77");

                entity.ToTable("RequestCable");

                //entity.Property(e => e.RecoveryDestWarehouseId).HasColumnType("int");

                entity.Property(e => e.Status).HasColumnType("nvarchar(15)");

                entity.HasOne(d => d.Cable)
                    .WithMany(p => p.RequestCables)
                    .HasForeignKey(d => d.CableId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__RequestCa__Cable__59063A47");

                entity.HasOne(d => d.Request)
                    .WithMany(p => p.RequestCables)
                    .HasForeignKey(d => d.RequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__RequestCa__Reque__5812160E");

                entity.HasOne(d => d.RecoveryDestWarehouse)
                    .WithMany(p => p.RecoveryWarehosuesCab)
                    .HasForeignKey(d => d.RecoveryDestWarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
                entity.HasOne(d => d.ImportedWarehouse)
                    .WithMany(p => p.ImportedWarehousesCab)
                    .HasForeignKey(d => d.ImportedWarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });
        }
    }
}
