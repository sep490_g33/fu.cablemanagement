﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class OtherMaterialsCategory : CommonEntity
    {
        public OtherMaterialsCategory()
        {
            OtherMaterials = new HashSet<OtherMaterial>();
        }

        public int OtherMaterialsCategoryId { get; set; }
        public string OtherMaterialsCategoryName { get; set; }

        public virtual ICollection<OtherMaterial> OtherMaterials { get; set; }
        public virtual ICollection<NodeMaterialCategory> NodeMaterialCategories { get; set; }
    }

    public static class OtherMaterialsCategoryConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OtherMaterialsCategory>(entity =>
            {
                entity.ToTable("OtherMaterialsCategory");
                entity.HasKey(e => e.OtherMaterialsCategoryId);
                entity.Property(e => e.OtherMaterialsCategoryName).HasMaxLength(100);
            });
        }
    }

}
