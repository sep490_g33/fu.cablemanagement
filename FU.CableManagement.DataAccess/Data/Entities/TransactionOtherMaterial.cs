﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class TransactionOtherMaterial : CommonEntity
    {
        public Guid TransactionId { get; set; }
        public int? OtherMaterialsId { get; set; }
        public int Quantity { get; set; }
        public string Note { get; set; }

        public virtual OtherMaterial OtherMaterials { get; set; }
        public virtual TransactionHistory Transaction { get; set; }
    }

    public static class TransactionOtherMaterialConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TransactionOtherMaterial>(entity =>
            {
                entity.HasKey(e => new { e.TransactionId, e.OtherMaterialsId })
                    .HasName("PK__Transact__040DB8D40D72C562");

                entity.Property(e => e.Note).HasMaxLength(255);

                entity.Property(e => e.OtherMaterialsId).HasDefaultValue(null);

                entity.HasOne(d => d.OtherMaterials)
                    .WithMany(p => p.TransactionOtherMaterials)
                    .HasForeignKey(d => d.OtherMaterialsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Transacti__Other__4BAC3F29");

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.TransactionOtherMaterials)
                    .HasForeignKey(d => d.TransactionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Transacti__Trans__4AB81AF0");
            });
        }

    }
}
