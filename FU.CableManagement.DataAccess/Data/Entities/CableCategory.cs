﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class CableCategory : CommonEntity
    {
        public CableCategory()
        {
            Cables = new HashSet<Cable>();
        }

        public int CableCategoryId { get; set; }
        public string CableCategoryName { get; set; }

        public virtual ICollection<Cable> Cables { get; set; }
    }

    public static class CableCategoryConfiguration
    {
        public static void Config(ModelBuilder builder)
        {
            builder.Entity<CableCategory>(entity =>
            {
                entity.ToTable("CableCategory");

                entity.Property(e => e.CableCategoryName)
                    .IsRequired()
                    .HasMaxLength(100);
            });
        }
    }
}

