﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class TransactionHistory : CommonEntity
    {
        public TransactionHistory()
        {
            TransactionCables = new HashSet<TransactionCable>();
            TransactionOtherMaterials = new HashSet<TransactionOtherMaterial>();
        }

        public Guid TransactionId { get; set; }
        public string TransactionCategoryName { get; set; }
        public string Description { get; set; }
        public Guid RequestId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? WarehouseId { get; set; }
        public int? FromWarehouseId { get; set; }
        public int? ToWarehouseId { get; set; }
        public Guid? IssueId { get; set; }
        public virtual Request Request { get; set; }
        public virtual Warehouse? Warehouse { get; set; }
        public virtual Warehouse? FromWarehouse { get; set; }
        public virtual Warehouse? ToWarehouse { get; set; }
        public virtual Issue? Issue { get; set; }
        public virtual ICollection<TransactionCable> TransactionCables { get; set; }
        public virtual ICollection<TransactionOtherMaterial> TransactionOtherMaterials { get; set; }
    }
    public static class TransactionHistoryConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TransactionHistory>(entity =>
            {
                entity.HasKey(e => e.TransactionId)
                    .HasName("PK__Transact__55433A6B23692FC5");

                entity.ToTable("TransactionHistory");

                entity.Property(e => e.TransactionId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.TransactionCategoryName).HasMaxLength(50);
                //entity.Property(e => e.FromWarehouseName).HasColumnType("nvarchar(50)");
                //entity.Property(e => e.ToWarehouseName).HasColumnType("nvarchar(50)");
                //entity.Property(e => e.IssueCode).HasColumnType("varchar(50)");

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.TransactionHistories)
                    .HasForeignKey(d => d.WarehouseId);
                entity.HasOne(d => d.FromWarehouse)
                    .WithMany(p => p.TransactionFromWarhouse)
                    .HasForeignKey(d => d.FromWarehouseId);
                entity.HasOne(d => d.ToWarehouse)
                    .WithMany(p => p.TransactionToWarehouse)
                    .HasForeignKey(d => d.ToWarehouseId);
                entity.HasOne(d => d.Issue)
                    .WithMany(p => p.TransactionHistories)
                    .HasForeignKey(d => d.IssueId);

                entity.HasOne(d => d.Request)
                    .WithMany(p => p.TransactionHistories)
                    .HasForeignKey(d => d.RequestId);
                
            });
        }
    }
}
