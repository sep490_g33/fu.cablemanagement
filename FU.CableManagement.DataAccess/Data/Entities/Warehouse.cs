﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class Warehouse : CommonEntity
    {
        public Warehouse()
        {
            Cables = new HashSet<Cable>();
            TransactionHistories = new HashSet<TransactionHistory>();
            TransactionFromWarhouse = new HashSet<TransactionHistory>();
            TransactionToWarehouse = new HashSet<TransactionHistory>();
            OtherMaterials = new HashSet<OtherMaterial>();
            RecoveryWarehosuesCab = new HashSet<RequestCable>();
            ImportedWarehousesCab = new HashSet<RequestCable>();
            ImportedWarehousesMat = new HashSet<RequestOtherMaterial>();
            ImportedWarehousesMat = new HashSet<RequestOtherMaterial>();
            Requests = new HashSet<Request>();
        }

        public int WarehouseId { get; set; }
        public string WarehouseName { get; set; }
        public string WarehouseAddress { get; set; }
        public Guid? WarehouseKeeperid { get; set; }
        public Guid CreatorId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public virtual User Creator { get; set; }
        public virtual User WarehouseKeeper { get; set; }
        public virtual ICollection<Cable> Cables { get; set; }
        public virtual ICollection<OtherMaterial> OtherMaterials { get; set; }
        public virtual ICollection<TransactionHistory> TransactionHistories { get; set; }
        public virtual ICollection<TransactionHistory> TransactionFromWarhouse { get; set; }
        public virtual ICollection<TransactionHistory> TransactionToWarehouse { get; set; }
        public virtual ICollection<RequestCable> RecoveryWarehosuesCab { get; set; }
        public virtual ICollection<RequestCable> ImportedWarehousesCab { get; set; }
        public virtual ICollection<RequestOtherMaterial> RecoveryWarehosuesMat { get; set; }
        public virtual ICollection<RequestOtherMaterial> ImportedWarehousesMat { get; set; }
        public virtual ICollection<Request> Requests { get; set; }
    }

    public static class WarehouseConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Warehouse>(entity =>
            {
                entity.ToTable("Warehouse");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.WarehouseName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Creator)
                    .WithMany(p => p.WarehouseCreators)
                    .HasForeignKey(d => d.CreatorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Warehouse__Creat__2D27B809");

                entity.HasOne(d => d.WarehouseKeeper)
                    .WithMany(p => p.WarehouseWarehouseKeepers)
                    .HasForeignKey(d => d.WarehouseKeeperid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Warehouse__Wareh__2C3393D0");
            });
        }
    }
}
