﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public class Node : CommonEntity
    {
        public Node()
        {
            NodeCables = new HashSet<NodeCable>();
            NodeMaterials = new HashSet<NodeMaterial>();
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public string NodeCode { get; set; }
        public string NodeNumberSign { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public string Note { get; set; }
        public Guid? RouteId { get; set; }
        public int NumberOrder { get; set; }
        public Route Route { get; set; }
        public string MaterialCategory { get; set; }
        public virtual ICollection<NodeCable> NodeCables { get; set; }
        public virtual ICollection<NodeMaterial> NodeMaterials { get; set; }
        public virtual ICollection<NodeMaterialCategory> NodeMaterialCategories { get; set; }
    }
    public static class NodeConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Node>(entity =>
            {
                entity.ToTable("Node");
                entity.HasKey(x => x.Id);

                entity.HasOne(x => x.Route)
               .WithMany(x => x.ListNodes)
               .HasForeignKey(x => x.RouteId)
               .OnDelete(DeleteBehavior.ClientSetNull);
            });
        }
    }
}
