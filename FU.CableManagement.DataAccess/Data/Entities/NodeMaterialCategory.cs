﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public class NodeMaterialCategory : CommonEntity
    {
        public Guid Id { get; set; }
        public int OtherMaterialCategoryId { get; set; }
        public Guid NodeId { get; set; }
        public int Quantity { get; set; }
        public Node Node { get; set; }
        public OtherMaterialsCategory MaterialCategory { get; set; }
    }
    public static class NodeMaterialCategoryConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NodeMaterialCategory>(entity =>
            {
                entity.ToTable("NodeMaterialCategory");
                entity.HasKey(x => x.Id);

                entity.HasOne(x => x.Node)
                .WithMany(x => x.NodeMaterialCategories)
                .HasForeignKey(x => x.NodeId)
                .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(x => x.MaterialCategory)
                .WithMany(x => x.NodeMaterialCategories)
                .HasForeignKey(x => x.OtherMaterialCategoryId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            });

        }
    }

}
