﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public class NodeMaterial : CommonEntity
    {
        public Guid Id { get; set; }
        public int OtherMaterialsId { get; set; }
        public Guid NodeId { get; set; }
        public Node Node { get; set; }
        public int Quantity { get; set; }
        public OtherMaterial OtherMaterial { get; set; }
    }
    public static class NodeMaterialConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NodeMaterial>(entity =>
            {
                entity.ToTable("NodeMaterial");
                entity.HasKey(x => x.Id);

                entity.HasOne(x => x.Node)
                .WithMany(x => x.NodeMaterials)
                .HasForeignKey(x => x.NodeId)
                .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(x => x.OtherMaterial)
                .WithMany(x => x.NodeMaterials)
                .HasForeignKey(x => x.OtherMaterialsId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            });

        }
    }
}
