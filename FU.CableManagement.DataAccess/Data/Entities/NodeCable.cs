﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public class NodeCable : CommonEntity
    {
        public Guid Id { get; set; }
        public Guid CableId { get; set; }
        public Guid NodeId { get; set; }
        public int OrderIndex { get; set; }
        public Cable Cable { get; set; }
        public Node Node { get; set; }
    }
    public static class NodeCableConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NodeCable>(entity =>
            {
                entity.ToTable("NodeCable");
                entity.HasKey(x => x.Id);

                entity.HasOne(x => x.Node)
                .WithMany(x => x.NodeCables)
                .HasForeignKey(x => x.NodeId)
                .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(x => x.Cable)
                .WithMany(x => x.NodeCables)
                .HasForeignKey(x => x.CableId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            });

        }
    }

}
