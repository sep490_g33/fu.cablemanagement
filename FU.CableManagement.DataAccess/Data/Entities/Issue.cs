﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class Issue : CommonEntity
    {
        public Issue()
        {
            Requests = new HashSet<Request>();
            TransactionHistories = new HashSet<TransactionHistory>();
        }

        public Guid IssueId { get; set; }
        public string IssueName { get; set; }
        public string IssueCode { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Group { get; set; }
        public string CableRoutingName { get; set; }
        public string Status { get; set; }
        public Guid CreatorId { get; set; }

        public virtual User Creator { get; set; }

        public virtual ICollection<Request> Requests { get; set; }
        public virtual ICollection<TransactionHistory> TransactionHistories { get; set; }
    }

    public static class IssueConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Issue>(entity =>
            {
                entity.ToTable("Issue");

                entity.Property(e => e.IssueId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.Status).HasColumnType("nvarchar(50)");
                entity.Property(e => e.Group).HasColumnType("nvarchar(50)");
                entity.Property(e => e.CableRoutingName).HasColumnType("nvarchar(100)");
                entity.Property(e => e.CreatedDate).HasColumnType("datetime2");

                entity.Property(e => e.IssueCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);
                entity.HasIndex(e => e.IssueCode).IsUnique(true);

                entity.Property(e => e.IssueName).HasMaxLength(50);

                entity.HasOne(d => d.Creator)
                    .WithMany(p => p.Issues)
                    .HasForeignKey(d => d.CreatorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Issue__CreatorId__4F7CD00D");
            });
        }
    }
}
