﻿namespace FU.CableManagement.DataAccess.Data.Entities
{
    public class CommonEntity
    {
        public DateTime CreatedAt { get; set; }
        public DateTime UpdateAt { get; set; }
        public bool IsDeleted { get; set; }
    }
}
