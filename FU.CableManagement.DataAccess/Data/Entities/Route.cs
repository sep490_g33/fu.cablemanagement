﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public class Route : CommonEntity
    {
        public Guid RouteId { get; set; }
        public string RouteName { get; set; }
        public virtual ICollection<Node> ListNodes { get; set; }
    }
    public static class RouteConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Route>(entity =>
            {
                entity.ToTable("Route");
                entity.HasKey(x => x.RouteId);
            });
        }
    }
}
