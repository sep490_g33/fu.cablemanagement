﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class RequestOtherMaterial : CommonEntity
    {
        public Guid RequestId { get; set; }
        public int? OtherMaterialsId { get; set; }
        public string? Status { get; set; }
        public int Quantity { get; set; }
        public int? RecoveryDestWarehouseId { get; set; }
        public int? ImportedWarehouseId { get; set; }
        public virtual Warehouse? RecoveryDestWarehouse { get; set; }
        public virtual Warehouse? ImportedWarehouse { get; set; }
        public virtual OtherMaterial OtherMaterials { get; set; }
        public virtual Request Request { get; set; }
    }

    public static class RequestOtherMaterialConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RequestOtherMaterial>(entity =>
            {
                entity.HasKey(e => new { e.RequestId, e.OtherMaterialsId, e.Quantity })
                    .HasName("PK__RequestO__62E6D3C5FBF475BA");

                entity.Property(e => e.RecoveryDestWarehouseId).HasColumnType("int");

                entity.Property(e => e.OtherMaterialsId).HasDefaultValue(null);

                entity.Property(e => e.Status).HasColumnType("nvarchar(15)");

                entity.HasOne(d => d.OtherMaterials)
                    .WithMany(p => p.RequestOtherMaterials)
                    .HasForeignKey(d => d.OtherMaterialsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__RequestOt__Other__5CD6CB2B");

                entity.HasOne(d => d.Request)
                    .WithMany(p => p.RequestOtherMaterials)
                    .HasForeignKey(d => d.RequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__RequestOt__Reque__5BE2A6F2");
                entity.HasOne(d => d.RecoveryDestWarehouse)
                    .WithMany(p => p.RecoveryWarehosuesMat)
                    .HasForeignKey(d => d.RecoveryDestWarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
                entity.HasOne(d => d.ImportedWarehouse)
                    .WithMany(p => p.ImportedWarehousesMat)
                    .HasForeignKey(d => d.ImportedWarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });
        }
    }
}
