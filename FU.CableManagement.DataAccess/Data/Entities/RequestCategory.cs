﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class RequestCategory : CommonEntity
    {
        public RequestCategory() { 
            Requests = new HashSet<Request>();
        }
        public int RequestCategoryId { get; set; }
        public string RequestCategoryName { get; set; }
        public virtual ICollection<Request> Requests { get; set; }
    }
    public static class RequestCategoryConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RequestCategory>(entity =>
            {
                

                entity.HasKey(e => e.RequestCategoryId);

                entity.Property(e => e.RequestCategoryName)
                .HasColumnType("nvarchar(50)");
            });
        }
    }
}
