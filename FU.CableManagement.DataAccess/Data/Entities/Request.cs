﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class Request : CommonEntity
    {
        public Request()
        {
            RequestCables = new HashSet<RequestCable>();
            RequestOtherMaterials = new HashSet<RequestOtherMaterial>();
            TransactionHistories = new HashSet<TransactionHistory>();
        }

        public Guid RequestId { get; set; }
        public string RequestName { get; set; }
        public string Content { get; set; }
        public int? DeliverWarehouseId { get; set; }
        public Guid CreatorId { get; set; }
        public Guid? ApproverId { get; set; }
        public Guid? IssueId { get; set; }
        public string Status { get; set; }
        public int RequestCategoryId { get; set; }
        public virtual RequestCategory RequestCategory { get; set; }
        public virtual Warehouse? DeliverWarehouse { get; set; }
        public virtual User Approver { get; set; }
        public virtual User Creator { get; set; }
        public virtual Issue Issue { get; set; }
        public virtual ICollection<RequestCable> RequestCables { get; set; }
        public virtual ICollection<RequestOtherMaterial> RequestOtherMaterials { get; set; }
        public virtual ICollection<TransactionHistory> TransactionHistories { get; set; }
    }

    public static class RequestConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Request>(entity =>
            {
                entity.ToTable("Request");

                entity.Property(e => e.RequestId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Content).HasMaxLength(255);

                entity.Property(e => e.RequestName).HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(20);
                entity.Property(e => e.IssueId).HasDefaultValue(null);

                entity.Property(e => e.DeliverWarehouseId).HasColumnType("int").HasDefaultValue(null);

                entity.HasOne(d => d.RequestCategory)
                    .WithMany(p => p.Requests)
                    .HasForeignKey(d => d.RequestCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Approver)
                    .WithMany(p => p.RequestApprovers)
                    .HasForeignKey(d => d.ApproverId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Request__Approve__5441852A");

                entity.HasOne(d => d.Creator)
                    .WithMany(p => p.RequestCreators)
                    .HasForeignKey(d => d.CreatorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Request__Creator__534D60F1");

                entity.HasOne(d => d.Issue)
                    .WithMany(p => p.Requests)
                    .HasForeignKey(d => d.IssueId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Request__IssueId__5535A963");

                entity.HasOne(d => d.DeliverWarehouse)
                    .WithMany(p => p.Requests)
                    .HasForeignKey(d => d.DeliverWarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });
        }
    }
}
