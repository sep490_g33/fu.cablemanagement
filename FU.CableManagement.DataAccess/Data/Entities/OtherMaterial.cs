﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class OtherMaterial : CommonEntity
    {
        public OtherMaterial()
        {
            RequestOtherMaterials = new HashSet<RequestOtherMaterial>();
            TransactionOtherMaterials = new HashSet<TransactionOtherMaterial>();
            NodeMaterials = new HashSet<NodeMaterial>();
        }

        public int OtherMaterialsId { get; set; }
        public int OtherMaterialsCategoryId { get; set; }
        public string Unit { get; set; }
        public string? Code { get; set; }
        public int? Quantity { get; set; }
        public int? WarehouseId { get; set; }
        public int? SupplierId { get; set; }
        public int MinQuantity { get; set; }
        public int MaxQuantity { get; set; }
        public string Status { get; set; }

        public virtual Supplier Supplier { get; set; }
        public virtual Warehouse WareHouse { get; set; }
        public virtual OtherMaterialsCategory OtherMaterialsCategory { get; set; }
        public virtual ICollection<RequestOtherMaterial> RequestOtherMaterials { get; set; }
        public virtual ICollection<TransactionOtherMaterial> TransactionOtherMaterials { get; set; }
        public virtual ICollection<NodeMaterial> NodeMaterials { get; set; }
    }
    public static class OtherMaterialConfiguration
    {
        public static void Config(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OtherMaterial>(entity =>
            {
                entity.HasKey(e => e.OtherMaterialsId)
                    .HasName("PK__OtherMat__14E82BF4B0F17E3B");


                entity.Property(e => e.MinQuantity).HasColumnType("int");

                entity.Property(e => e.MaxQuantity).HasColumnType("int");

                entity.Property(e => e.Code).HasColumnType("nvarchar(50)");

                entity.Property(e => e.Status).HasColumnType("nvarchar(15)");

                entity.Property(e => e.Unit)
                    .HasColumnType("nvarchar(15)")
                    .IsUnicode(false);

                entity.HasOne(d => d.OtherMaterialsCategory)
                    .WithMany(p => p.OtherMaterials)
                    .HasForeignKey(d => d.OtherMaterialsCategoryId);

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.OtherMaterials)
                    .HasForeignKey(d => d.SupplierId)
                    .HasConstraintName("FK__OtherMate__Suppl__33D4B598");
                entity.HasOne(d => d.WareHouse)
                    .WithMany(p => p.OtherMaterials)
                    .HasForeignKey(d => d.WarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OtherMate__Wareh__36B12233");
            });
        }
    }
}
