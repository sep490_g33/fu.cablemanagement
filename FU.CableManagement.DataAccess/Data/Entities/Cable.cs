﻿using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data.Entities
{
    public partial class Cable : CommonEntity
    {
        public Cable()
        {
            RequestCables = new HashSet<RequestCable>();
            TransactionCables = new HashSet<TransactionCable>();
            NodeCables = new HashSet<NodeCable>();
        }

        public Guid CableId { get; set; }
        //public string CableName { get; set; }
        public int CableCategoryId { get; set; }
        public string? Code { get; set; }
        public int? WarehouseId { get; set; }
        public int? SupplierId { get; set; }
        public int StartPoint { get; set; }
        public int EndPoint { get; set; }
        public int Length { get; set; }
        public bool IsExportedToUse { get; set; }
        public bool IsInRequest { get; set; }
        public int? YearOfManufacture { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public Guid CreatorId { get; set; }
        public Guid? CableParentId { get; set; }

        public virtual User Creator { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual Warehouse Warehouse { get; set; }
        public virtual CableCategory CableCategory { get; set; }
        public virtual ICollection<RequestCable> RequestCables { get; set; }
        public virtual ICollection<TransactionCable> TransactionCables { get; set; }
        public virtual ICollection<NodeCable> NodeCables { get; set; }
    }

    public static class CableConfiguration
    {
        public static void Config(ModelBuilder builder)
        {
            builder.Entity<Cable>(entity =>
            {
                entity.ToTable("Cable");

                entity.Property(e => e.CableId).HasDefaultValueSql("(newid())");

                //entity.Property(e => e.CableName)
                //    .IsRequired()
                //    .HasMaxLength(100);

                entity.Property(e => e.Status).HasMaxLength(30);


                entity.Property(e => e.IsExportedToUse).HasColumnType("bit");
                entity.Property(e => e.IsInRequest).HasColumnType("bit");
                entity.Property(e => e.Description).HasColumnType("nvarchar(max)");

                entity.Property(e => e.Code).HasColumnType("nvarchar(50)");

                entity.HasOne(d => d.CableCategory)
                    .WithMany(p => p.Cables)
                    .HasForeignKey(d => d.CableCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Creator)
                    .WithMany(p => p.Cables)
                    .HasForeignKey(d => d.CreatorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Cable__CreatorId__403A8C7D");

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.Cables)
                    .HasForeignKey(d => d.SupplierId)
                    .HasConstraintName("FK__Cable__SupplierI__3E52440B");

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.Cables)
                    .HasForeignKey(d => d.WarehouseId)
                    .HasConstraintName("FK__Cable__Warehouse__3D5E1FD2");
            });
        }
    }
}
