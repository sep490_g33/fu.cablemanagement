﻿using FU.CableManagement.DataAccess.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace FU.CableManagement.DataAccess.Data
{
    public partial class CableManagementContext : DbContext
    {
        public CableManagementContext()
        {
        }

        public CableManagementContext(DbContextOptions<CableManagementContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cable> Cables { get; set; }
        public virtual DbSet<CableCategory> CableCategories { get; set; }
        public virtual DbSet<Issue> Issues { get; set; }
        public virtual DbSet<OtherMaterial> OtherMaterials { get; set; }
        public virtual DbSet<OtherMaterialsCategory> OtherMaterialsCategories { get; set; }
        public virtual DbSet<Request> Requests { get; set; }
        public virtual DbSet<RequestCable> RequestCables { get; set; }
        public virtual DbSet<RequestOtherMaterial> RequestOtherMaterials { get; set; }
        public virtual DbSet<RequestCategory> RequestCategories { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<TransactionCable> TransactionCables { get; set; }
        public virtual DbSet<TransactionHistory> TransactionHistories { get; set; }
        public virtual DbSet<TransactionOtherMaterial> TransactionOtherMaterials { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Warehouse> Warehouses { get; set; }
        public virtual DbSet<Node> Nodes { get; set; }
        public virtual DbSet<NodeCable> NodeCables { get; set; }
        public virtual DbSet<NodeMaterial> NodeMaterials { get; set; }
        public virtual DbSet<NodeMaterialCategory> NodeMaterialCategories { get; set; }
        public virtual DbSet<Route> Routes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            CableConfiguration.Config(modelBuilder);
            CableCategoryConfiguration.Config(modelBuilder);
            IssueConfiguration.Config(modelBuilder);
            OtherMaterialConfiguration.Config(modelBuilder);
            OtherMaterialsCategoryConfiguration.Config(modelBuilder);
            RequestConfiguration.Config(modelBuilder);
            RequestCableConfiguration.Config(modelBuilder);
            RequestCategoryConfiguration.Config(modelBuilder);
            RequestOtherMaterialConfiguration.Config(modelBuilder);
            RoleConfiguration.Config(modelBuilder);
            SupplierConfiguration.Config(modelBuilder);
            TransactionCableConfiguration.Config(modelBuilder);
            TransactionHistoryConfiguration.Config(modelBuilder);
            TransactionOtherMaterialConfiguration.Config(modelBuilder);
            UserConfiguration.Config(modelBuilder);
            NodeConfiguration.Config(modelBuilder);
            NodeCableConfiguration.Config(modelBuilder);
            NodeMaterialConfiguration.Config(modelBuilder);
            NodeMaterialCategoryConfiguration.Config(modelBuilder);
            RouteConfiguration.Config(modelBuilder);
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = new CancellationToken())
        {
            OnBeforeSaving();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        protected virtual void OnBeforeSaving()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.State == EntityState.Added)
                {
                    if (((CommonEntity)entry.Entity).CreatedAt == null) ((CommonEntity)entry.Entity).CreatedAt = DateTime.UtcNow;
                    if (((CommonEntity)entry.Entity).IsDeleted == null) ((CommonEntity)entry.Entity).IsDeleted = false;
                }

                ((CommonEntity)entry.Entity).UpdateAt = DateTime.UtcNow;
            }
        }
    }
}
