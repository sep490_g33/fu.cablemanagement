﻿using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.Common.Enums;
using FU.CableManagement.DataAccess.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace FU.CableManagement.DataAccess.Data
{
    public class SeedData
    {
        public async static Task Seed(CableManagementContext context)
        {
            if (!(await context.Roles.AnyAsync()))
            {
                var listRole = new List<Role>
                {
                    new Role { Rolename = "Admin"},
                    new Role { Rolename = "Leader"},
                    new Role { Rolename = "Staff"},
                    new Role { Rolename = "WareHouse Keeper"}
                };
                await context.AddRangeAsync(listRole);
                await context.SaveChangesAsync();
            }
            var listUser = new List<User>();
            if (!(await context.Users.AnyAsync()))
            {
                //Password: 123456abc, faked email, phone
                listUser = new List<User>
                {
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "Minh Hiếu", Lastname = "Phạm", //0
                                RoleId = 1, Phone="0123456789", Username = "hieupm", Email = "minhhieupham@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "Đức Bình", Lastname = "Hoàng", //1
                                RoleId = 2, Phone="0987654321", Username = "Binhhd", Email = "binhhd@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "Phú Quý", Lastname = "Hà",       //2
                                RoleId = 4, Phone="0111444867", Username = "quyhp", Email = "quyhp@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "Trí Hiền", Lastname = "Vũ",      //3
                                RoleId = 3, Phone="0564738297", Username = "hienvt", Email = "hienvt@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "1", Lastname = "Thủ Kho",        //4
                                RoleId = 4, Phone="0562749375", Username = "thukho1", Email = "thukho1@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "2", Lastname = "Thủ Kho",        //5
                                RoleId = 4, Phone="0435643627", Username = "thukho2", Email = "thukho2@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "3", Lastname = "Thủ Kho",        //6
                                RoleId = 4, Phone="0564729465", Username = "thukho3", Email = "thukho3@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "4", Lastname = "Thủ Kho",        //7
                                RoleId = 4, Phone="0846593946", Username = "thukho4", Email = "thukho4@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "5", Lastname = "Thủ Kho",        //8
                                RoleId = 4, Phone="0347339472", Username = "thukho5", Email = "thukho5@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "6", Lastname = "Thủ Kho",        //9
                                RoleId = 4, Phone="0975739346", Username = "thukho6", Email = "thukho6@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "7", Lastname = "Thủ Kho",        //10
                                RoleId = 4, Phone="0475245738", Username = "thukho7", Email = "thukho7@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "8", Lastname = "Thủ Kho",        //11
                                RoleId = 4, Phone="0475245738", Username = "thukho8", Email = "thukho8@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "9", Lastname = "Thủ Kho",        //12
                                RoleId = 4, Phone="0475245738", Username = "thukho9", Email = "thukho9@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "10", Lastname = "Thủ Kho",        //13
                                RoleId = 4, Phone="0475245738", Username = "thukho10", Email = "thukho10@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                    new User {UserId = Guid.NewGuid(),CreatedDate = DateTime.Now, Firstname = "11", Lastname = "Thủ Kho",        //14
                                RoleId = 4, Phone="0475245738", Username = "thukho11", Email = "thukho11@gmail.com",
                            Password = "931145d4ddd1811be545e4ac88a81f1fdbfaf0779c437efba16b884595274d11", IsDeleted = false},
                };
                await context.AddRangeAsync(listUser);
                await context.SaveChangesAsync();
            }


            if (!(await context.Warehouses.AnyAsync()))
            {
                var listWarehouse = new List<Warehouse>
                {
                    new Warehouse { WarehouseName = "ĐCV Hà Nội", CreatedAt = DateTime.Now, CreatedDate = DateTime.Now,
                                IsDeleted = false, CreatorId = listUser[0].UserId, WarehouseKeeperid = listUser[4].UserId,
                                UpdatedDate = null},
                    new Warehouse { WarehouseName = "ĐCV Nam Định", CreatedAt = DateTime.Now, CreatedDate = DateTime.Now,
                                IsDeleted = false, CreatorId = listUser[0].UserId, WarehouseKeeperid = listUser[5].UserId,
                                UpdatedDate = null},
                    new Warehouse { WarehouseName = "ĐCV Thanh Hóa", CreatedAt = DateTime.Now, CreatedDate = DateTime.Now,
                                IsDeleted = false, CreatorId = listUser[0].UserId, WarehouseKeeperid = listUser[6].UserId,
                                UpdatedDate = null},
                    new Warehouse { WarehouseName = "ĐCV Cầu Giát", CreatedAt = DateTime.Now, CreatedDate = DateTime.Now,
                                IsDeleted = false, CreatorId = listUser[0].UserId, WarehouseKeeperid = listUser[7].UserId,
                                UpdatedDate = null},
                    new Warehouse { WarehouseName = "ĐCV Vinh", CreatedAt = DateTime.Now, CreatedDate = DateTime.Now,
                                IsDeleted = false, CreatorId = listUser[0].UserId, WarehouseKeeperid = listUser[8].UserId,
                                UpdatedDate = null},
                    new Warehouse { WarehouseName = "ĐCV Hà Tĩnh", CreatedAt = DateTime.Now, CreatedDate = DateTime.Now,
                                IsDeleted = false, CreatorId = listUser[0].UserId, WarehouseKeeperid = listUser[9].UserId,
                                UpdatedDate = null},
                    new Warehouse { WarehouseName = "ĐCV Kỳ Anh", CreatedAt = DateTime.Now, CreatedDate = DateTime.Now,
                                IsDeleted = false, CreatorId = listUser[0].UserId, WarehouseKeeperid = listUser[10].UserId,
                                UpdatedDate = null},
                    new Warehouse { WarehouseName = "ĐCV Đồng Hới", CreatedAt = DateTime.Now, CreatedDate = DateTime.Now,
                                IsDeleted = false, CreatorId = listUser[0].UserId, WarehouseKeeperid = listUser[11].UserId,
                                UpdatedDate = null},
                    new Warehouse { WarehouseName = "ĐCV Huế", CreatedAt = DateTime.Now, CreatedDate = DateTime.Now,
                                IsDeleted = false, CreatorId = listUser[0].UserId, WarehouseKeeperid = listUser[12].UserId,
                                UpdatedDate = null},
                    new Warehouse { WarehouseName = "ĐCV Phú Lộc", CreatedAt = DateTime.Now, CreatedDate = DateTime.Now,
                                IsDeleted = false, CreatorId = listUser[0].UserId, WarehouseKeeperid = listUser[13].UserId,
                                UpdatedDate = null},
                    new Warehouse { WarehouseName = "FFC Uông Bí", CreatedAt = DateTime.Now, CreatedDate = DateTime.Now,
                                IsDeleted = false, CreatorId = listUser[0].UserId, WarehouseKeeperid = listUser[14].UserId,
                                UpdatedDate = null},
                };
                await context.AddRangeAsync(listWarehouse);
                await context.SaveChangesAsync();
            }
            if (!(await context.Suppliers.AnyAsync()))
            {
                var listSuppliers = new List<Supplier>
                {
                    new Supplier { SupplierName = "Nhà cung cấp A", Country = "Vietnam", CreatedAt=DateTime.Now, CreatorId = listUser[0].UserId,
                                    IsDeleted = false},
                    new Supplier { SupplierName = "Nhà cung cấp B", Country = "Trung Quoc", CreatedAt=DateTime.Now, CreatorId = listUser[0].UserId,
                                    IsDeleted = false},
                    new Supplier { SupplierName = "Nhà cung cấp C", Country = "Hoa Ky", CreatedAt=DateTime.Now, CreatorId = listUser[0].UserId,
                                    IsDeleted = false},
                };
                await context.AddRangeAsync(listSuppliers);
                await context.SaveChangesAsync();
            }

            if (!(await context.CableCategories.AnyAsync()))
            {
                var listCableCates = new List<CableCategory> {
                    new CableCategory {CableCategoryName = "Cáp quang ADSS 24FO KV100"},
                    new CableCategory {CableCategoryName = "Cáp quang ADSS 24FO KV300"},
                    new CableCategory {CableCategoryName = "Cáp quang ADSS 36FO KV100 (30FO G.652D + 6FO G.655)"},
                    new CableCategory {CableCategoryName = "Cáp quang ADSS 48FO KV400"},
                    new CableCategory {CableCategoryName = "Cáp quang ADSS 24FO KV400"},
                    new CableCategory {CableCategoryName = "Cáp quang ADSS 36FO KV500"},
                    new CableCategory {CableCategoryName = "Cáp quang ADSS 24FO G.652D KV100 (Chống sóc cắn) - LS"},
                    new CableCategory {CableCategoryName = "demo1"},
                    new CableCategory {CableCategoryName = "demo2"},
                };
                await context.AddRangeAsync(listCableCates);
                await context.SaveChangesAsync();
            }
            var listCables = new List<Cable>();
            var listCables2 = new List<Cable>();
            if (!(await context.Cables.AnyAsync()))
            {
                listCables = new List<Cable>
                {
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 1, WarehouseId = 1, SupplierId = 1, //0
                        CableParentId=null,  StartPoint = 0, EndPoint=1000,Length = 1000, Code = "20000202",
                        Status = "Cũ", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2019, CreatorId = listUser[0].UserId},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 2, WarehouseId = null, SupplierId = 1, //1
                        CableParentId=null,  StartPoint = 0, EndPoint=500,Length = 500, Code = "20000203",
                        Status = "Mới", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2022, CreatorId = listUser[0].UserId,
                        IsExportedToUse = true},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 3, //2
                        CableParentId=null,  StartPoint = 0, EndPoint=2000,Length = 2000, WarehouseId = 1, SupplierId = 1, Code = "20000204",
                        Status = "Mới", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2022, CreatorId = listUser[0].UserId},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 4, Code = "20000205", //3
                        CableParentId=null,  StartPoint = 0, EndPoint=3000,Length = 3000, WarehouseId = 1, SupplierId = 2,
                        Status = "Cũ", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2020, CreatorId = listUser[0].UserId},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 5, Code = "20000206", //4
                        CableParentId=null,  StartPoint = 0, EndPoint=2500,Length = 2500, WarehouseId = 1, SupplierId = 2,
                        Status = "Mới", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2022, CreatorId = listUser[0].UserId},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 6, CableParentId = null, StartPoint = 0, Code = "20000207", //5
                        EndPoint = 1000, Length = 1000, WarehouseId = 1, SupplierId = 1, Status = "mới", IsDeleted = false, CreatedAt = DateTime.Now, YearOfManufacture = 2022, CreatorId = listUser[0].UserId},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 7, //6
                        CableParentId=null,  StartPoint = 0, EndPoint=1000,Length = 1000, WarehouseId = 1, SupplierId = 2,Code = "20000208",
                        Status = "Mới", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2023, CreatorId = listUser[0].UserId},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 7, //7
                        CableParentId=null,  StartPoint = 0, EndPoint=1000,Length = 1000, WarehouseId = 2, SupplierId = 1,Code = "20000209",
                        Status = "Mới", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2023, CreatorId = listUser[0].UserId},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 8, //8
                        CableParentId=null,  StartPoint = 0, EndPoint=5000,Length = 5000, WarehouseId = 2, SupplierId = 1,Code = "20000210",
                        Status = "Mới", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2023, CreatorId = listUser[0].UserId},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 2, //9
                        CableParentId=null,  StartPoint = 0, EndPoint=4000,Length = 4000, WarehouseId = 3, SupplierId = 1,Code = "20000211",
                        Status = "Cũ", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2020, CreatorId = listUser[0].UserId},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 2, //10
                        CableParentId=null,  StartPoint = 0, EndPoint=3000,Length = 3000, WarehouseId = 3, SupplierId = 1,Code = "20000212",
                        Status = "Mới", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2021, CreatorId = listUser[0].UserId},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 9, //11
                        CableParentId=null,  StartPoint = 0, EndPoint=3000,Length = 3000, WarehouseId = 3, SupplierId = 1,Code = "20000213",
                        Status = "Mới", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2022, CreatorId = listUser[0].UserId},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 3, //12
                        CableParentId=null,  StartPoint = 0, EndPoint=2000,Length = 2000, WarehouseId = 2, SupplierId = 1,Code = "20000214",
                        Status = "Mới", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2022, CreatorId = listUser[0].UserId},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 4, //13
                        CableParentId=null,  StartPoint = 0, EndPoint=4000,Length = 4000, WarehouseId = 2, SupplierId = 1,Code = "20000215",
                        Status = "Mới", IsDeleted = !false, CreatedAt = DateTime.Now, YearOfManufacture = 2020, CreatorId = listUser[0].UserId},

                };
                await context.AddRangeAsync(listCables);
                await context.SaveChangesAsync();
                listCables2.AddRange(new List<Cable> {
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 4, //2.0
                        CableParentId=listCables[13].CableId,  StartPoint = 0, EndPoint=500,Length = 500, WarehouseId = 2, SupplierId = 1,Code = "20000215",
                        Status = "Mới", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2020, CreatorId = listUser[0].UserId},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 4, //2.1
                        CableParentId=listCables[13].CableId,  StartPoint = 500, EndPoint=2500,Length = 2000, WarehouseId = null, SupplierId = 1,Code = "20000215",
                        Status = "Mới", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2020, CreatorId = listUser[0].UserId
                        , IsExportedToUse = true},
                    new Cable {CableId = Guid.NewGuid(), CableCategoryId = 4, //2.2
                        CableParentId=listCables[13].CableId,  StartPoint = 2500, EndPoint=4000,Length = 1500, WarehouseId = 2, SupplierId = 1,Code = "20000215",
                        Status = "Mới", IsDeleted=false, CreatedAt = DateTime.Now, YearOfManufacture = 2020, CreatorId = listUser[0].UserId},
                });
                await context.AddRangeAsync(listCables2);
                await context.SaveChangesAsync();
            }
            if (!(await context.OtherMaterials.AnyAsync()))
            {
                var listMateCates = new List<OtherMaterialsCategory> {
                    new OtherMaterialsCategory { OtherMaterialsCategoryName = "Bộ treo cáp ADSS 24FO KV100"},
                    new OtherMaterialsCategory { OtherMaterialsCategoryName = "Bộ treo cáp ADSS 24FO KV200"},
                    new OtherMaterialsCategory { OtherMaterialsCategoryName = "Bộ treo cáp ADSS 24FO KV300"},
                    new OtherMaterialsCategory { OtherMaterialsCategoryName = "Bộ treo cáp ADSS 24FO KV400"},
                    new OtherMaterialsCategory { OtherMaterialsCategoryName = "Bộ treo cáp ADSS 24FO KV500"},
                    new OtherMaterialsCategory { OtherMaterialsCategoryName = "Bộ treo cáp ADSS 48FO KV100"},
                    new OtherMaterialsCategory { OtherMaterialsCategoryName = "Măng Xông 24FO NWC"},
                    new OtherMaterialsCategory { OtherMaterialsCategoryName = "Măng Xông 48FO NWC"},
                    new OtherMaterialsCategory { OtherMaterialsCategoryName = "Hộp ODF 24 Core SC/APC"},
                    new OtherMaterialsCategory { OtherMaterialsCategoryName = "Chống rung 1350 (L1 1300mm)"},

                };
                await context.AddRangeAsync(listMateCates);
                await context.SaveChangesAsync();
            }

            if (!(await context.OtherMaterials.AnyAsync()))
            {
                var listOtherMaterials = new List<OtherMaterial>
                {
                    new OtherMaterial { OtherMaterialsCategoryId = 1, IsDeleted=false, CreatedAt = DateTime.Now, //1
                       SupplierId = 1, Unit = "Bộ", Quantity = 100, WarehouseId = 1, Code = "20000202", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 2, IsDeleted=false, CreatedAt = DateTime.Now, //2
                         SupplierId = 1, Unit = "Bộ", Quantity = 150, WarehouseId = 1, Code = "20000203", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 3, IsDeleted=false, CreatedAt = DateTime.Now, //3
                       SupplierId = 1, Unit = "Bộ", Quantity = 600, WarehouseId = 1 , Code = "20000204", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 4, IsDeleted=false, CreatedAt = DateTime.Now, //4
                         SupplierId = 1, Unit = "Bộ", Quantity = 700, WarehouseId = 1, Code = "20000205", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 5, IsDeleted=false, CreatedAt = DateTime.Now, //5
                         SupplierId = 1, Unit = "Bộ", Quantity = 200, WarehouseId = 1, Code = "20000206", Status = "Mới" },
                    new OtherMaterial { OtherMaterialsCategoryId = 6, IsDeleted=false, CreatedAt = DateTime.Now, //6
                         SupplierId = 1, Unit = "Bộ", Quantity = 201, WarehouseId = 1, Code = "20000207", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 6, IsDeleted=false, CreatedAt = DateTime.Now, //7
                         SupplierId = 1, Unit = "Bộ", Quantity = 160, WarehouseId = 1, Code = "20000208", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 7, IsDeleted=false, CreatedAt = DateTime.Now, //8
                         SupplierId = 1, Unit = "Bộ", Quantity = 300, WarehouseId = 1 , Code = "20000209", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 8, IsDeleted=false, CreatedAt = DateTime.Now, //9
                         SupplierId = 1, Unit = "Bộ", Quantity = 345, WarehouseId = 1, Code = "20000210", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 9, IsDeleted=false, CreatedAt = DateTime.Now, //10
                         SupplierId = 1, Unit = "Bộ", Quantity = 790, WarehouseId = 1, Code = "20000211", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 10, IsDeleted=false, CreatedAt = DateTime.Now, //11
                         SupplierId = 2, Unit = "Bộ", Quantity = 979, WarehouseId = 1, Code = "20000212", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 1, IsDeleted=false, CreatedAt = DateTime.Now, //12
                       SupplierId = 1, Unit = "Bộ", Quantity = 100, WarehouseId = 2, Code = "20000213", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 2, IsDeleted=false, CreatedAt = DateTime.Now, //13
                         SupplierId = 1, Unit = "Bộ", Quantity = 150, WarehouseId = 2, Code = "20000214", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 3, IsDeleted=false, CreatedAt = DateTime.Now, //14
                       SupplierId = 1, Unit = "Bộ", Quantity = 600, WarehouseId = 2, Code = "200002015", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 4, IsDeleted=false, CreatedAt = DateTime.Now, //15
                         SupplierId = 1, Unit = "Bộ", Quantity = 700, WarehouseId = 2, Code = "20000216", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 10, IsDeleted=false, CreatedAt = DateTime.Now, //16
                         SupplierId = 2, Unit = "Bộ", Quantity = 979, WarehouseId = 2, Code = "20000217", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 1, IsDeleted=false, CreatedAt = DateTime.Now, //17
                       SupplierId = 1, Unit = "Bộ", Quantity = 100, WarehouseId = 3, Code = "20000218", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 1, IsDeleted=false, CreatedAt = DateTime.Now, //18
                       SupplierId = 1, Unit = "Bộ", Quantity = 100, WarehouseId = 2, Code = "20000202", Status = "Mới"},
                    new OtherMaterial { OtherMaterialsCategoryId = 1, IsDeleted=false, CreatedAt = DateTime.Now, //19
                       SupplierId = 1, Unit = "Bộ", Quantity = 20, WarehouseId = 2, Code = "20000202", Status = "Cũ"},
                };
                await context.AddRangeAsync(listOtherMaterials);
                await context.SaveChangesAsync();
            }
            var listIssue = new List<Issue>();
            if (!(await context.Issues.AnyAsync()))
            {
                listIssue = new List<Issue> {
                    new Issue {IssueId = Guid.NewGuid(), IssueName = "Sự cố 1", IssueCode = "SC20230505-01", CreatorId = listUser[3].UserId,
                                CreatedAt = new DateTime(2023,05,05), Description = "Mô tả ...", IsDeleted = false, Status = IssueStatusConstants.Doing,
                                CableRoutingName = "Tuyến cáp A", Group = "Nhóm A", CreatedDate = new DateTime(2023,01,01)},
                    new Issue {IssueId = Guid.NewGuid(), IssueName = "Sự cố 2", IssueCode = "SC20230506-02", CreatorId = listUser[3].UserId,
                                CreatedAt = new DateTime(2023,05,06), Description = "Mô tả ...", IsDeleted = false, Status = IssueStatusConstants.Doing,
                                CableRoutingName = "Tuyến cáp B", Group = "Nhóm B", CreatedDate = new DateTime(2023,02,01)},
                    new Issue {IssueId = Guid.NewGuid(), IssueName = "Sự cố 3", IssueCode = "SC20230605-03", CreatorId = listUser[3].UserId,
                                CreatedAt = new DateTime(2023,06,05), Description = "Mô tả ...", IsDeleted = false, Status = IssueStatusConstants.Doing,
                                CableRoutingName = "Tuyến cáp A", Group = "Nhóm A", CreatedDate = new DateTime(2023,03,01)},
                    new Issue {IssueId = Guid.NewGuid(), IssueName = "Sự cố 2", IssueCode = "SC20230506-04", CreatorId = listUser[3].UserId,
                                CreatedAt = new DateTime(2023,02,06), Description = "Mô tả ...", IsDeleted = false, Status = IssueStatusConstants.Doing,
                                CableRoutingName = "Tuyến cáp A", Group = "Nhóm A", CreatedDate = new DateTime(2023,03,06)},
                    new Issue {IssueId = Guid.NewGuid(), IssueName = "Sự cố 2", IssueCode = "SC20230506-05", CreatorId = listUser[3].UserId,
                                CreatedAt = new DateTime(2023,03,06), Description = "Mô tả ...", IsDeleted = false, Status = IssueStatusConstants.Done,
                                CableRoutingName = "Tuyến cáp A", Group = "Nhóm A", CreatedDate = new DateTime(2023,03,12)},
                    new Issue {IssueId = Guid.NewGuid(), IssueName = "Sự cố 2", IssueCode = "SC20230506-06", CreatorId = listUser[3].UserId, CreatedAt = new DateTime(2023, 03, 06), Description = "Mô tả ...", IsDeleted = false, Status = IssueStatusConstants.Done,
                        CableRoutingName = "Tuyến cáp A", Group = "Nhóm A", CreatedDate = new DateTime(2023, 04, 04)},
                    new Issue {IssueId = Guid.NewGuid(), IssueName = "Sự cố 2", IssueCode = "SC20230506-07", CreatorId = listUser[3].UserId, CreatedAt = new DateTime(2023, 04, 06), Description = "Mô tả ...", IsDeleted = false, Status = IssueStatusConstants.Done,
                        CableRoutingName = "Tuyến cáp A", Group = "Nhóm A" , CreatedDate = new DateTime(2023,05,01)  },
                    new Issue {IssueId = Guid.NewGuid(), IssueName = "Sự cố 2", IssueCode = "SC20230506-08", CreatorId = listUser[3].UserId,
                                CreatedAt = new DateTime(2023,01,06), Description = "Mô tả ...", IsDeleted = false, Status = IssueStatusConstants.Done,
                                CableRoutingName = "Tuyến cáp A", Group = "Nhóm A", CreatedDate = new DateTime(2023, 03, 01)},
                    new Issue {IssueId = Guid.NewGuid(), IssueName = "Sự cố 2", IssueCode = "SC20230506-09", CreatorId = listUser[3].UserId,
                                CreatedAt = new DateTime(2023,02,06), Description = "Mô tả ...", IsDeleted = false, Status = IssueStatusConstants.Done,
                                CableRoutingName = "Tuyến cáp A", Group = "Nhóm A"  , CreatedDate = new DateTime(2023, 02, 01)},
                    new Issue {IssueId = Guid.NewGuid(), IssueName = "Sự cố 2", IssueCode = "SC20230506-10", CreatorId = listUser[3].UserId,
                                CreatedAt = new DateTime(2023,03,06), Description = "Mô tả ...", IsDeleted = false, Status = IssueStatusConstants.Done,
                                CableRoutingName = "Tuyến cáp C", Group = "Nhóm C", CreatedDate = new DateTime(2022, 12, 01)},
                    new Issue {IssueId = Guid.NewGuid(), IssueName = "Sự cố 2", IssueCode = "SC20230506-11", CreatorId = listUser[3].UserId,
                                CreatedAt = new DateTime(2023,06,06), Description = "Mô tả ...", IsDeleted = false, Status = IssueStatusConstants.Done,
                                CableRoutingName = "Tuyến cáp C", Group = "Nhóm C", CreatedDate = new DateTime(2022, 11, 01)},
                };
                await context.AddRangeAsync(listIssue);
                await context.SaveChangesAsync();
            }
            if (!(await context.RequestCategories.AnyAsync()))
            {
                var listRqCates = new List<RequestCategory>
                {
                    new RequestCategory{ RequestCategoryName = "Xuất Kho"},
                    new RequestCategory{ RequestCategoryName = "Nhập Kho"},
                    new RequestCategory{ RequestCategoryName = "Thu Hồi"},
                    new RequestCategory{ RequestCategoryName = "Chuyển Kho"},
                    new RequestCategory{ RequestCategoryName = "Hủy"},
                    new RequestCategory{ RequestCategoryName = "Hủy Ngoài Kho"},
                    new RequestCategory{ RequestCategoryName = "Khác"},
                };
                await context.AddRangeAsync(listRqCates);
                await context.SaveChangesAsync();
            }
            var listRequests = new List<Request>();
            if (!(await context.Requests.AnyAsync()))
            {
                listRequests = new List<Request> {
                    new Request { RequestId = Guid.NewGuid(), ApproverId = listUser[1].UserId, CreatorId = listUser[3].UserId, //0
                        RequestName = "Yêu cầu 1 ", CreatedAt = new DateTime(2023,05,05), UpdateAt = new DateTime(2023,05,05), DeliverWarehouseId = null,
                        Status = RequestStatusConstant.Approved, RequestCategoryId = (int)RequestCategoryEnum.Export, IssueId = listIssue[0].IssueId,
                    },
                    new Request { RequestId = Guid.NewGuid(), ApproverId = listUser[1].UserId, CreatorId = listUser[3].UserId, //1
                        RequestName = "Yêu cầu 2 ", CreatedAt = new DateTime(2023,06,05), UpdateAt = new DateTime(2023, 06, 05), DeliverWarehouseId = null,
                        Status = RequestStatusConstant.Approved, RequestCategoryId = (int)RequestCategoryEnum.Export, IssueId = listIssue[0].IssueId,
                    },
                    new Request { RequestId = Guid.NewGuid(), ApproverId = null, CreatorId = listUser[3].UserId, //2
                        RequestName = "Yêu cầu 3 ", CreatedAt = new DateTime(2023,06,10), UpdateAt = new DateTime(2023, 06, 10), DeliverWarehouseId = null,
                        Status = RequestStatusConstant.Pending, RequestCategoryId = (int)RequestCategoryEnum.Export, IssueId = listIssue[0].IssueId,
                    },
                    new Request { RequestId = Guid.NewGuid(), ApproverId = listUser[1].UserId, CreatorId = listUser[3].UserId, //3
                        RequestName = "Yêu cầu 4 ", CreatedAt = new DateTime(2023,07,01), UpdateAt = new DateTime(2023, 07, 01), DeliverWarehouseId = 2,
                        Status = RequestStatusConstant.Approved, RequestCategoryId = (int)RequestCategoryEnum.Deliver, IssueId = null,
                    },
                    new Request { RequestId = Guid.NewGuid(), ApproverId = listUser[1].UserId, CreatorId = listUser[3].UserId, //4
                        RequestName = "Yêu cầu 5 ", CreatedAt = new DateTime(2023,07,01), UpdateAt = new DateTime(2023, 07, 01), DeliverWarehouseId = null,
                        Status = RequestStatusConstant.Approved, RequestCategoryId = (int)RequestCategoryEnum.Recovery, IssueId = null,
                    },
                };
                await context.AddRangeAsync(listRequests);
                await context.SaveChangesAsync();

                if (!(await context.RequestCables.AnyAsync()))
                {
                    var listReqestCabs = new List<RequestCable> {
                    new RequestCable { RequestId = listRequests[0].RequestId, CableId = listCables2[1].CableId, StartPoint = 500,
                    EndPoint = 2500, RecoveryDestWarehouseId = null,Status = null },
                    new RequestCable { RequestId = listRequests[1].RequestId, CableId = listCables[1].CableId, StartPoint = 0,
                    EndPoint = 500, RecoveryDestWarehouseId = null,Status = null },
                    new RequestCable { RequestId = listRequests[2].RequestId, CableId = listCables2[2].CableId, StartPoint = 2500,
                    EndPoint = 4000, RecoveryDestWarehouseId = null,Status = null },
                    new RequestCable { RequestId = listRequests[3].RequestId, CableId = listCables[13].CableId, StartPoint = 0,
                    EndPoint = 4000, RecoveryDestWarehouseId = null,Status = null },
                    new RequestCable { RequestId = listRequests[4].RequestId, CableId = listCables2[0].CableId, StartPoint = 0,
                    EndPoint = 500, RecoveryDestWarehouseId = 2,Status = CableStatusConstant.New },
                    };
                    await context.AddRangeAsync(listReqestCabs);
                    await context.SaveChangesAsync();
                }

                if (!(await context.RequestOtherMaterials.AnyAsync()))
                {
                    var listRequestMats = new List<RequestOtherMaterial>
                    {
                        new RequestOtherMaterial {RequestId = listRequests[0].RequestId, OtherMaterialsId = 1, Quantity = 10
                        ,RecoveryDestWarehouseId = null, Status = null},
                        new RequestOtherMaterial {RequestId = listRequests[0].RequestId, OtherMaterialsId = 2, Quantity = 15
                        ,RecoveryDestWarehouseId = null, Status = null},
                        new RequestOtherMaterial {RequestId = listRequests[1].RequestId, OtherMaterialsId = 4, Quantity = 20
                        ,RecoveryDestWarehouseId = null, Status = null},
                        new RequestOtherMaterial {RequestId = listRequests[2].RequestId, OtherMaterialsId = 4, Quantity = 10
                        ,RecoveryDestWarehouseId = null, Status = null},
                        new RequestOtherMaterial {RequestId = listRequests[3].RequestId, OtherMaterialsId = 1, Quantity = 10
                        ,RecoveryDestWarehouseId = null, Status = null},
                        new RequestOtherMaterial {RequestId = listRequests[4].RequestId, OtherMaterialsId = 1, Quantity = 20
                        ,RecoveryDestWarehouseId = 2, Status = OtherMaterialStatusConstant.Old},
                    };
                    await context.AddRangeAsync(listRequestMats);
                    await context.SaveChangesAsync();
                }

                if (!(await context.TransactionHistories.AnyAsync()))
                {
                    var listTransactions = new List<TransactionHistory> {
                        new TransactionHistory {TransactionId = Guid.NewGuid(), RequestId = listRequests[0].RequestId , //0
                            CreatedAt = listRequests[0].CreatedAt, TransactionCategoryName = "Xuất Kho", IssueId = listIssue[0].IssueId,
                        WarehouseId = 1, Description = "",FromWarehouseId = null, ToWarehouseId = null},
                        new TransactionHistory {TransactionId = Guid.NewGuid(), RequestId = listRequests[0].RequestId , //1
                            CreatedAt = listRequests[0].CreatedAt, TransactionCategoryName = "Xuất Kho", IssueId = listIssue[0].IssueId,
                        WarehouseId = 2, Description = ""},
                        new TransactionHistory {TransactionId = Guid.NewGuid(), RequestId = listRequests[1].RequestId , //2
                            CreatedAt = listRequests[1].CreatedAt, TransactionCategoryName = "Xuất Kho", IssueId = listIssue[0].IssueId,
                        WarehouseId = 1, Description = ""},
                        new TransactionHistory {TransactionId = Guid.NewGuid(), RequestId = listRequests[3].RequestId , //3
                            CreatedAt = listRequests[3].CreatedAt, TransactionCategoryName = "Xuất Kho", IssueId = null,
                        WarehouseId = 1, Description = "", ToWarehouseId = 2},
                        new TransactionHistory {TransactionId = Guid.NewGuid(), RequestId = listRequests[3].RequestId , //4
                            CreatedAt = listRequests[3].CreatedAt, TransactionCategoryName = "Nhập Kho", IssueId = null,
                        WarehouseId = 2, Description = "", FromWarehouseId = 1},
                        new TransactionHistory {TransactionId = Guid.NewGuid(), RequestId = listRequests[4].RequestId , //5
                            CreatedAt = listRequests[4].CreatedAt, TransactionCategoryName = "Nhập Kho", IssueId = listIssue[0].IssueId,
                        WarehouseId = 2, Description = ""},
                    };
                    await context.AddRangeAsync(listTransactions);
                    await context.SaveChangesAsync();
                    if (!(await context.TransactionCables.AnyAsync()))
                    {
                        var listTranCabs = new List<TransactionCable> {
                            new TransactionCable{TransactionId = listTransactions[1].TransactionId, CableId = listCables2[1].CableId,
                                StartPoint = 500, EndPoint = 2500, Length = 2000, CreatedAt = listTransactions[1].CreatedAt},
                            new TransactionCable{TransactionId = listTransactions[2].TransactionId, CableId = listCables[1].CableId,
                                StartPoint = 500, EndPoint = 2500, Length = 2000, CreatedAt = listTransactions[2].CreatedAt},
                            new TransactionCable{TransactionId = listTransactions[3].TransactionId, CableId = listCables[13].CableId,
                                StartPoint = 0, EndPoint = 4000, Length = 4000, CreatedAt = listTransactions[3].CreatedAt},
                            new TransactionCable{TransactionId = listTransactions[4].TransactionId, CableId = listCables[13].CableId,
                                StartPoint = 0, EndPoint = 4000, Length = 4000, CreatedAt = listTransactions[4].CreatedAt},
                            new TransactionCable{TransactionId = listTransactions[5].TransactionId, CableId = listCables2[0].CableId,
                                StartPoint = 0, EndPoint = 500, Length = 500, CreatedAt = listTransactions[5].CreatedAt},
                        };
                        await context.AddRangeAsync(listTranCabs);
                        await context.SaveChangesAsync();
                    }
                    if (!(await context.TransactionOtherMaterials.AnyAsync()))
                    {
                        var listTranMats = new List<TransactionOtherMaterial> {
                            new TransactionOtherMaterial {TransactionId = listTransactions[0].TransactionId, OtherMaterialsId = 1,
                            Quantity = 10, CreatedAt = listTransactions[0].CreatedAt},
                            new TransactionOtherMaterial {TransactionId = listTransactions[0].TransactionId, OtherMaterialsId = 2,
                            Quantity = 15, CreatedAt = listTransactions[0].CreatedAt},
                            new TransactionOtherMaterial {TransactionId = listTransactions[2].TransactionId, OtherMaterialsId = 4,
                            Quantity = 20, CreatedAt = listTransactions[2].CreatedAt},
                            new TransactionOtherMaterial {TransactionId = listTransactions[3].TransactionId, OtherMaterialsId = 1,
                            Quantity = 10, CreatedAt = listTransactions[3].CreatedAt},
                            new TransactionOtherMaterial {TransactionId = listTransactions[4].TransactionId, OtherMaterialsId = 18,
                            Quantity = 10, CreatedAt = listTransactions[4].CreatedAt},
                            new TransactionOtherMaterial {TransactionId = listTransactions[5].TransactionId, OtherMaterialsId = 19,
                            Quantity = 20, CreatedAt = listTransactions[5].CreatedAt},
                        };
                        await context.AddRangeAsync(listTranMats);
                        await context.SaveChangesAsync();
                    }
                }
            }
        }


    }
}

