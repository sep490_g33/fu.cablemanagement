﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class updatetransactionwarehousematerials : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "WarehouseId",
                table: "TransactionHistory",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MaxQuantity",
                table: "OtherMaterials",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MinQuantity",
                table: "OtherMaterials",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionHistory_WarehouseId",
                table: "TransactionHistory",
                column: "WarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionHistory_Warehouses_WarehouseId",
                table: "TransactionHistory",
                column: "WarehouseId",
                principalTable: "Warehouses",
                principalColumn: "WarehouseId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionHistory_Warehouses_WarehouseId",
                table: "TransactionHistory");

            migrationBuilder.DropIndex(
                name: "IX_TransactionHistory_WarehouseId",
                table: "TransactionHistory");

            migrationBuilder.DropColumn(
                name: "WarehouseId",
                table: "TransactionHistory");

            migrationBuilder.DropColumn(
                name: "MaxQuantity",
                table: "OtherMaterials");

            migrationBuilder.DropColumn(
                name: "MinQuantity",
                table: "OtherMaterials");
        }
    }
}
