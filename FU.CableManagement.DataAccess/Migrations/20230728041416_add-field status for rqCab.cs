﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class addfieldstatusforrqCab : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "TransactionOtherMaterials");

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "RequestCable",
                type: "nvarchar(15)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "RequestCable");

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "TransactionOtherMaterials",
                type: "nvarchar(15)",
                nullable: true);
        }
    }
}
