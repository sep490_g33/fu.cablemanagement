﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class changepropertyintabletransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionHistory_Warehouses_WarehouseId",
                table: "TransactionHistory");

            migrationBuilder.DropColumn(
                name: "FromWarehouseName",
                table: "TransactionHistory");

            migrationBuilder.DropColumn(
                name: "IssueCode",
                table: "TransactionHistory");

            migrationBuilder.DropColumn(
                name: "ToWarehouseName",
                table: "TransactionHistory");

            migrationBuilder.AlterColumn<int>(
                name: "WarehouseId",
                table: "TransactionHistory",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "FromWarehouseId",
                table: "TransactionHistory",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "IssueId",
                table: "TransactionHistory",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ToWarehouseId",
                table: "TransactionHistory",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OtherMaterialsCategoryName",
                table: "OtherMaterialsCategory",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CableCategoryName",
                table: "CableCategory",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionHistory_FromWarehouseId",
                table: "TransactionHistory",
                column: "FromWarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionHistory_IssueId",
                table: "TransactionHistory",
                column: "IssueId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionHistory_ToWarehouseId",
                table: "TransactionHistory",
                column: "ToWarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionHistory_Issue_IssueId",
                table: "TransactionHistory",
                column: "IssueId",
                principalTable: "Issue",
                principalColumn: "IssueId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionHistory_Warehouses_FromWarehouseId",
                table: "TransactionHistory",
                column: "FromWarehouseId",
                principalTable: "Warehouses",
                principalColumn: "WarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionHistory_Warehouses_ToWarehouseId",
                table: "TransactionHistory",
                column: "ToWarehouseId",
                principalTable: "Warehouses",
                principalColumn: "WarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionHistory_Warehouses_WarehouseId",
                table: "TransactionHistory",
                column: "WarehouseId",
                principalTable: "Warehouses",
                principalColumn: "WarehouseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionHistory_Issue_IssueId",
                table: "TransactionHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionHistory_Warehouses_FromWarehouseId",
                table: "TransactionHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionHistory_Warehouses_ToWarehouseId",
                table: "TransactionHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionHistory_Warehouses_WarehouseId",
                table: "TransactionHistory");

            migrationBuilder.DropIndex(
                name: "IX_TransactionHistory_FromWarehouseId",
                table: "TransactionHistory");

            migrationBuilder.DropIndex(
                name: "IX_TransactionHistory_IssueId",
                table: "TransactionHistory");

            migrationBuilder.DropIndex(
                name: "IX_TransactionHistory_ToWarehouseId",
                table: "TransactionHistory");

            migrationBuilder.DropColumn(
                name: "FromWarehouseId",
                table: "TransactionHistory");

            migrationBuilder.DropColumn(
                name: "IssueId",
                table: "TransactionHistory");

            migrationBuilder.DropColumn(
                name: "ToWarehouseId",
                table: "TransactionHistory");

            migrationBuilder.AlterColumn<int>(
                name: "WarehouseId",
                table: "TransactionHistory",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FromWarehouseName",
                table: "TransactionHistory",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IssueCode",
                table: "TransactionHistory",
                type: "varchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ToWarehouseName",
                table: "TransactionHistory",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OtherMaterialsCategoryName",
                table: "OtherMaterialsCategory",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CableCategoryName",
                table: "CableCategory",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionHistory_Warehouses_WarehouseId",
                table: "TransactionHistory",
                column: "WarehouseId",
                principalTable: "Warehouses",
                principalColumn: "WarehouseId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
