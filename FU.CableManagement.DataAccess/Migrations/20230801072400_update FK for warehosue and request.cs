﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class updateFKforwarehosueandrequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ImportedWarehouseId",
                table: "RequestOtherMaterials",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ImportedWarehouseId",
                table: "RequestCable",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RequestOtherMaterials_ImportedWarehouseId",
                table: "RequestOtherMaterials",
                column: "ImportedWarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestOtherMaterials_RecoveryDestWarehouseId",
                table: "RequestOtherMaterials",
                column: "RecoveryDestWarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestCable_ImportedWarehouseId",
                table: "RequestCable",
                column: "ImportedWarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestCable_RecoveryDestWarehouseId",
                table: "RequestCable",
                column: "RecoveryDestWarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_Request_DeliverWarehouseId",
                table: "Request",
                column: "DeliverWarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Request_Warehouses_DeliverWarehouseId",
                table: "Request",
                column: "DeliverWarehouseId",
                principalTable: "Warehouses",
                principalColumn: "WarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestCable_Warehouses_ImportedWarehouseId",
                table: "RequestCable",
                column: "ImportedWarehouseId",
                principalTable: "Warehouses",
                principalColumn: "WarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestCable_Warehouses_RecoveryDestWarehouseId",
                table: "RequestCable",
                column: "RecoveryDestWarehouseId",
                principalTable: "Warehouses",
                principalColumn: "WarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestOtherMaterials_Warehouses_ImportedWarehouseId",
                table: "RequestOtherMaterials",
                column: "ImportedWarehouseId",
                principalTable: "Warehouses",
                principalColumn: "WarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestOtherMaterials_Warehouses_RecoveryDestWarehouseId",
                table: "RequestOtherMaterials",
                column: "RecoveryDestWarehouseId",
                principalTable: "Warehouses",
                principalColumn: "WarehouseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Request_Warehouses_DeliverWarehouseId",
                table: "Request");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestCable_Warehouses_ImportedWarehouseId",
                table: "RequestCable");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestCable_Warehouses_RecoveryDestWarehouseId",
                table: "RequestCable");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestOtherMaterials_Warehouses_ImportedWarehouseId",
                table: "RequestOtherMaterials");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestOtherMaterials_Warehouses_RecoveryDestWarehouseId",
                table: "RequestOtherMaterials");

            migrationBuilder.DropIndex(
                name: "IX_RequestOtherMaterials_ImportedWarehouseId",
                table: "RequestOtherMaterials");

            migrationBuilder.DropIndex(
                name: "IX_RequestOtherMaterials_RecoveryDestWarehouseId",
                table: "RequestOtherMaterials");

            migrationBuilder.DropIndex(
                name: "IX_RequestCable_ImportedWarehouseId",
                table: "RequestCable");

            migrationBuilder.DropIndex(
                name: "IX_RequestCable_RecoveryDestWarehouseId",
                table: "RequestCable");

            migrationBuilder.DropIndex(
                name: "IX_Request_DeliverWarehouseId",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "ImportedWarehouseId",
                table: "RequestOtherMaterials");

            migrationBuilder.DropColumn(
                name: "ImportedWarehouseId",
                table: "RequestCable");
        }
    }
}
