﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class fixcables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Cable_Code",
                table: "Cable");

            migrationBuilder.DropColumn(
                name: "Utilities",
                table: "Cable");

            migrationBuilder.DropColumn(
                name: "Volume",
                table: "Cable");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Utilities",
                table: "Cable",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Volume",
                table: "Cable",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Cable_Code",
                table: "Cable",
                column: "Code",
                unique: true,
                filter: "[Code] IS NOT NULL");
        }
    }
}
