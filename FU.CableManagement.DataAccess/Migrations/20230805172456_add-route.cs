﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class addroute : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Node",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NodeCode",
                table: "Node",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NodeNumberSign",
                table: "Node",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "Node",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NumberOrder",
                table: "Node",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "RouteId",
                table: "Node",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "Node",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Route",
                columns: table => new
                {
                    RouteId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RouteName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Route", x => x.RouteId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Node_RouteId",
                table: "Node",
                column: "RouteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Node_Route_RouteId",
                table: "Node",
                column: "RouteId",
                principalTable: "Route",
                principalColumn: "RouteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Node_Route_RouteId",
                table: "Node");

            migrationBuilder.DropTable(
                name: "Route");

            migrationBuilder.DropIndex(
                name: "IX_Node_RouteId",
                table: "Node");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "Node");

            migrationBuilder.DropColumn(
                name: "NodeCode",
                table: "Node");

            migrationBuilder.DropColumn(
                name: "NodeNumberSign",
                table: "Node");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "Node");

            migrationBuilder.DropColumn(
                name: "NumberOrder",
                table: "Node");

            migrationBuilder.DropColumn(
                name: "RouteId",
                table: "Node");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Node");
        }
    }
}
