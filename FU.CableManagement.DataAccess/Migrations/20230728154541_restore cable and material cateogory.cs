﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class restorecableandmaterialcateogory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OtherMaterialsName",
                table: "OtherMaterials");

            migrationBuilder.DropColumn(
                name: "CableName",
                table: "Cable");

            migrationBuilder.AddColumn<int>(
                name: "OtherMaterialsCategoryId",
                table: "OtherMaterials",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CableCategoryId",
                table: "Cable",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CableCategory",
                columns: table => new
                {
                    CableCategoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CableCategoryName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CableCategory", x => x.CableCategoryId);
                });

            migrationBuilder.CreateTable(
                name: "OtherMaterialsCategory",
                columns: table => new
                {
                    OtherMaterialsCategoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OtherMaterialsCategoryName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OtherMaterialsCategory", x => x.OtherMaterialsCategoryId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OtherMaterials_OtherMaterialsCategoryId",
                table: "OtherMaterials",
                column: "OtherMaterialsCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Cable_CableCategoryId",
                table: "Cable",
                column: "CableCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cable_CableCategory_CableCategoryId",
                table: "Cable",
                column: "CableCategoryId",
                principalTable: "CableCategory",
                principalColumn: "CableCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_OtherMaterials_OtherMaterialsCategory_OtherMaterialsCategoryId",
                table: "OtherMaterials",
                column: "OtherMaterialsCategoryId",
                principalTable: "OtherMaterialsCategory",
                principalColumn: "OtherMaterialsCategoryId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cable_CableCategory_CableCategoryId",
                table: "Cable");

            migrationBuilder.DropForeignKey(
                name: "FK_OtherMaterials_OtherMaterialsCategory_OtherMaterialsCategoryId",
                table: "OtherMaterials");

            migrationBuilder.DropTable(
                name: "CableCategory");

            migrationBuilder.DropTable(
                name: "OtherMaterialsCategory");

            migrationBuilder.DropIndex(
                name: "IX_OtherMaterials_OtherMaterialsCategoryId",
                table: "OtherMaterials");

            migrationBuilder.DropIndex(
                name: "IX_Cable_CableCategoryId",
                table: "Cable");

            migrationBuilder.DropColumn(
                name: "OtherMaterialsCategoryId",
                table: "OtherMaterials");

            migrationBuilder.DropColumn(
                name: "CableCategoryId",
                table: "Cable");

            migrationBuilder.AddColumn<string>(
                name: "OtherMaterialsName",
                table: "OtherMaterials",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CableName",
                table: "Cable",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");
        }
    }
}
