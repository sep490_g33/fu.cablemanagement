﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class updatetransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionHistory_User_ApproverRequestId",
                table: "TransactionHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionHistory_User_CreatorRequestId",
                table: "TransactionHistory");

            migrationBuilder.DropIndex(
                name: "IX_TransactionHistory_ApproverRequestId",
                table: "TransactionHistory");

            migrationBuilder.DropColumn(
                name: "ApproverRequestId",
                table: "TransactionHistory");

            migrationBuilder.RenameColumn(
                name: "CreatorRequestId",
                table: "TransactionHistory",
                newName: "RequestId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionHistory_CreatorRequestId",
                table: "TransactionHistory",
                newName: "IX_TransactionHistory_RequestId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionHistory_Request_RequestId",
                table: "TransactionHistory",
                column: "RequestId",
                principalTable: "Request",
                principalColumn: "RequestId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionHistory_Request_RequestId",
                table: "TransactionHistory");

            migrationBuilder.RenameColumn(
                name: "RequestId",
                table: "TransactionHistory",
                newName: "CreatorRequestId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionHistory_RequestId",
                table: "TransactionHistory",
                newName: "IX_TransactionHistory_CreatorRequestId");

            migrationBuilder.AddColumn<Guid>(
                name: "ApproverRequestId",
                table: "TransactionHistory",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_TransactionHistory_ApproverRequestId",
                table: "TransactionHistory",
                column: "ApproverRequestId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionHistory_User_ApproverRequestId",
                table: "TransactionHistory",
                column: "ApproverRequestId",
                principalTable: "User",
                principalColumn: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionHistory_User_CreatorRequestId",
                table: "TransactionHistory",
                column: "CreatorRequestId",
                principalTable: "User",
                principalColumn: "UserId");
        }
    }
}
