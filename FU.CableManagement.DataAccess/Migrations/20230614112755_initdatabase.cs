﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class initdatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CableCategory",
                columns: table => new
                {
                    CableCategoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CableCategoryName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CableCategory", x => x.CableCategoryId);
                });

            migrationBuilder.CreateTable(
                name: "OtherMaterialsCategories",
                columns: table => new
                {
                    OtherMaterialsCategoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OtherMaterialsCategoryName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OtherMaterialsCategories", x => x.OtherMaterialsCategoryId);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    RoleId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Rolename = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "TransactionHistory",
                columns: table => new
                {
                    TransactionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    TransactionName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Transact__55433A6B23692FC5", x => x.TransactionId);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    Username = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    Firstname = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Lastname = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Password = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false),
                    Email = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    Phone = table.Column<string>(type: "varchar(25)", unicode: false, maxLength: 25, nullable: true),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.UserId);
                    table.ForeignKey(
                        name: "FK__User__RoleId__276EDEB3",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "RoleId");
                });

            migrationBuilder.CreateTable(
                name: "Issue",
                columns: table => new
                {
                    IssueId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    IssueName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IssueCode = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    CreatorId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Issue", x => x.IssueId);
                    table.ForeignKey(
                        name: "FK__Issue__CreatorId__4F7CD00D",
                        column: x => x.CreatorId,
                        principalTable: "User",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "Supplier",
                columns: table => new
                {
                    SupplierId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SupplierName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Country = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    CreatorId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    SupplierDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supplier", x => x.SupplierId);
                    table.ForeignKey(
                        name: "FK__Supplier__Creato__300424B4",
                        column: x => x.CreatorId,
                        principalTable: "User",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "Warehouses",
                columns: table => new
                {
                    WarehouseId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WarehouseName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    WarehouseKeeperid = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatorId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Warehouses", x => x.WarehouseId);
                    table.ForeignKey(
                        name: "FK_Warehouses_User_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Warehouses_User_WarehouseKeeperid",
                        column: x => x.WarehouseKeeperid,
                        principalTable: "User",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "Request",
                columns: table => new
                {
                    RequestId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    RequestName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Content = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    CreatorId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ApproverId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IssueId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Status = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Request", x => x.RequestId);
                    table.ForeignKey(
                        name: "FK__Request__Approve__5441852A",
                        column: x => x.ApproverId,
                        principalTable: "User",
                        principalColumn: "UserId");
                    table.ForeignKey(
                        name: "FK__Request__Creator__534D60F1",
                        column: x => x.CreatorId,
                        principalTable: "User",
                        principalColumn: "UserId");
                    table.ForeignKey(
                        name: "FK__Request__IssueId__5535A963",
                        column: x => x.IssueId,
                        principalTable: "Issue",
                        principalColumn: "IssueId");
                });

            migrationBuilder.CreateTable(
                name: "OtherMaterials",
                columns: table => new
                {
                    OtherMaterialsId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OtherMaterialsName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Unit = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: true),
                    OtherMaterialsCategoryId = table.Column<int>(type: "int", nullable: true),
                    SupplierId = table.Column<int>(type: "int", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__OtherMat__14E82BF4B0F17E3B", x => x.OtherMaterialsId);
                    table.ForeignKey(
                        name: "FK__OtherMate__Other__32E0915F",
                        column: x => x.OtherMaterialsCategoryId,
                        principalTable: "OtherMaterialsCategories",
                        principalColumn: "OtherMaterialsCategoryId");
                    table.ForeignKey(
                        name: "FK__OtherMate__Suppl__33D4B598",
                        column: x => x.SupplierId,
                        principalTable: "Supplier",
                        principalColumn: "SupplierId");
                });

            migrationBuilder.CreateTable(
                name: "Cable",
                columns: table => new
                {
                    CableId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    CableName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    WarehouseId = table.Column<int>(type: "int", nullable: true),
                    SupplierId = table.Column<int>(type: "int", nullable: true),
                    StartPoint = table.Column<int>(type: "int", nullable: false),
                    EndPoint = table.Column<int>(type: "int", nullable: false),
                    Length = table.Column<int>(type: "int", nullable: false),
                    Volume = table.Column<int>(type: "int", nullable: false),
                    YearOfManufacture = table.Column<int>(type: "int", nullable: true),
                    CableCategoryId = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Utilities = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CreatorId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CableParentId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cable", x => x.CableId);
                    table.ForeignKey(
                        name: "FK__Cable__CableCate__3F466844",
                        column: x => x.CableCategoryId,
                        principalTable: "CableCategory",
                        principalColumn: "CableCategoryId");
                    table.ForeignKey(
                        name: "FK__Cable__CreatorId__403A8C7D",
                        column: x => x.CreatorId,
                        principalTable: "User",
                        principalColumn: "UserId");
                    table.ForeignKey(
                        name: "FK__Cable__SupplierI__3E52440B",
                        column: x => x.SupplierId,
                        principalTable: "Supplier",
                        principalColumn: "SupplierId");
                    table.ForeignKey(
                        name: "FK__Cable__Warehouse__3D5E1FD2",
                        column: x => x.WarehouseId,
                        principalTable: "Warehouses",
                        principalColumn: "WarehouseId");
                });

            migrationBuilder.CreateTable(
                name: "OtherMaterialsWarehouse",
                columns: table => new
                {
                    WarehouseId = table.Column<int>(type: "int", nullable: false),
                    OtherMaterialsId = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__OtherMat__77462D46A9C24245", x => new { x.WarehouseId, x.OtherMaterialsId });
                    table.ForeignKey(
                        name: "FK__OtherMate__Other__37A5467C",
                        column: x => x.OtherMaterialsId,
                        principalTable: "OtherMaterials",
                        principalColumn: "OtherMaterialsId");
                    table.ForeignKey(
                        name: "FK__OtherMate__Wareh__36B12243",
                        column: x => x.WarehouseId,
                        principalTable: "Warehouses",
                        principalColumn: "WarehouseId");
                });

            migrationBuilder.CreateTable(
                name: "RequestOtherMaterials",
                columns: table => new
                {
                    RequestId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OtherMaterialsId = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__RequestO__62E6D3C5FBF475BA", x => new { x.RequestId, x.OtherMaterialsId });
                    table.ForeignKey(
                        name: "FK__RequestOt__Other__5CD6CB2B",
                        column: x => x.OtherMaterialsId,
                        principalTable: "OtherMaterials",
                        principalColumn: "OtherMaterialsId");
                    table.ForeignKey(
                        name: "FK__RequestOt__Reque__5BE2A6F2",
                        column: x => x.RequestId,
                        principalTable: "Request",
                        principalColumn: "RequestId");
                });

            migrationBuilder.CreateTable(
                name: "TransactionOtherMaterials",
                columns: table => new
                {
                    TransactionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OtherMaterialsId = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    Note = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Transact__040DB8D40D72C562", x => new { x.TransactionId, x.OtherMaterialsId });
                    table.ForeignKey(
                        name: "FK__Transacti__Other__4BAC3F29",
                        column: x => x.OtherMaterialsId,
                        principalTable: "OtherMaterials",
                        principalColumn: "OtherMaterialsId");
                    table.ForeignKey(
                        name: "FK__Transacti__Trans__4AB81AF0",
                        column: x => x.TransactionId,
                        principalTable: "TransactionHistory",
                        principalColumn: "TransactionId");
                });

            migrationBuilder.CreateTable(
                name: "RequestCable",
                columns: table => new
                {
                    RequestId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CableId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StartPoint = table.Column<int>(type: "int", nullable: false),
                    EndPoint = table.Column<int>(type: "int", nullable: false),
                    Length = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__RequestC__9AC47BE7FD0DAC77", x => new { x.RequestId, x.CableId });
                    table.ForeignKey(
                        name: "FK__RequestCa__Cable__59063A47",
                        column: x => x.CableId,
                        principalTable: "Cable",
                        principalColumn: "CableId");
                    table.ForeignKey(
                        name: "FK__RequestCa__Reque__5812160E",
                        column: x => x.RequestId,
                        principalTable: "Request",
                        principalColumn: "RequestId");
                });

            migrationBuilder.CreateTable(
                name: "TransactionCable",
                columns: table => new
                {
                    TransactionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CableId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StartPoint = table.Column<int>(type: "int", nullable: false),
                    EndPoint = table.Column<int>(type: "int", nullable: false),
                    Length = table.Column<int>(type: "int", nullable: false),
                    Note = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Transact__FC2F10F68675DA26", x => new { x.TransactionId, x.CableId });
                    table.ForeignKey(
                        name: "FK__Transacti__Cable__47DBAE45",
                        column: x => x.CableId,
                        principalTable: "Cable",
                        principalColumn: "CableId");
                    table.ForeignKey(
                        name: "FK__Transacti__Trans__46E78A0C",
                        column: x => x.TransactionId,
                        principalTable: "TransactionHistory",
                        principalColumn: "TransactionId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cable_CableCategoryId",
                table: "Cable",
                column: "CableCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Cable_CreatorId",
                table: "Cable",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Cable_SupplierId",
                table: "Cable",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_Cable_WarehouseId",
                table: "Cable",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_Issue_CreatorId",
                table: "Issue",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_OtherMaterials_OtherMaterialsCategoryId",
                table: "OtherMaterials",
                column: "OtherMaterialsCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_OtherMaterials_SupplierId",
                table: "OtherMaterials",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_OtherMaterialsWarehouse_OtherMaterialsId",
                table: "OtherMaterialsWarehouse",
                column: "OtherMaterialsId");

            migrationBuilder.CreateIndex(
                name: "IX_Request_ApproverId",
                table: "Request",
                column: "ApproverId");

            migrationBuilder.CreateIndex(
                name: "IX_Request_CreatorId",
                table: "Request",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Request_IssueId",
                table: "Request",
                column: "IssueId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestCable_CableId",
                table: "RequestCable",
                column: "CableId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestOtherMaterials_OtherMaterialsId",
                table: "RequestOtherMaterials",
                column: "OtherMaterialsId");

            migrationBuilder.CreateIndex(
                name: "IX_Supplier_CreatorId",
                table: "Supplier",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionCable_CableId",
                table: "TransactionCable",
                column: "CableId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionOtherMaterials_OtherMaterialsId",
                table: "TransactionOtherMaterials",
                column: "OtherMaterialsId");

            migrationBuilder.CreateIndex(
                name: "IX_User_RoleId",
                table: "User",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Warehouses_CreatorId",
                table: "Warehouses",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Warehouses_WarehouseKeeperid",
                table: "Warehouses",
                column: "WarehouseKeeperid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OtherMaterialsWarehouse");

            migrationBuilder.DropTable(
                name: "RequestCable");

            migrationBuilder.DropTable(
                name: "RequestOtherMaterials");

            migrationBuilder.DropTable(
                name: "TransactionCable");

            migrationBuilder.DropTable(
                name: "TransactionOtherMaterials");

            migrationBuilder.DropTable(
                name: "Request");

            migrationBuilder.DropTable(
                name: "Cable");

            migrationBuilder.DropTable(
                name: "OtherMaterials");

            migrationBuilder.DropTable(
                name: "TransactionHistory");

            migrationBuilder.DropTable(
                name: "Issue");

            migrationBuilder.DropTable(
                name: "CableCategory");

            migrationBuilder.DropTable(
                name: "Warehouses");

            migrationBuilder.DropTable(
                name: "OtherMaterialsCategories");

            migrationBuilder.DropTable(
                name: "Supplier");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Role");
        }
    }
}
