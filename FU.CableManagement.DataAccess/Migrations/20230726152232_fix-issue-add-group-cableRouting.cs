﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class fixissueaddgroupcableRouting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TransactionName",
                table: "TransactionHistory",
                newName: "TransactionCategoryName");

            migrationBuilder.AddColumn<string>(
                name: "CableRoutingName",
                table: "Issue",
                type: "nvarchar(100)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Group",
                table: "Issue",
                type: "nvarchar(50)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CableRoutingName",
                table: "Issue");

            migrationBuilder.DropColumn(
                name: "Group",
                table: "Issue");

            migrationBuilder.RenameColumn(
                name: "TransactionCategoryName",
                table: "TransactionHistory",
                newName: "TransactionName");
        }
    }
}
