﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class updatereqMat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK__RequestO__62E6D3C5FBF475BA",
                table: "RequestOtherMaterials");

            migrationBuilder.AddPrimaryKey(
                name: "PK__RequestO__62E6D3C5FBF475BA",
                table: "RequestOtherMaterials",
                columns: new[] { "RequestId", "OtherMaterialsId", "Quantity" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK__RequestO__62E6D3C5FBF475BA",
                table: "RequestOtherMaterials");

            migrationBuilder.AddPrimaryKey(
                name: "PK__RequestO__62E6D3C5FBF475BA",
                table: "RequestOtherMaterials",
                columns: new[] { "RequestId", "OtherMaterialsId" });
        }
    }
}
