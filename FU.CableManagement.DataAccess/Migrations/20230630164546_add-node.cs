﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class addnode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Node",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Longitude = table.Column<float>(type: "real", nullable: false),
                    Latitude = table.Column<float>(type: "real", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Node", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NodeCable",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CableId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NodeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NodeCable", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NodeCable_Cable_CableId",
                        column: x => x.CableId,
                        principalTable: "Cable",
                        principalColumn: "CableId");
                    table.ForeignKey(
                        name: "FK_NodeCable_Node_NodeId",
                        column: x => x.NodeId,
                        principalTable: "Node",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "NodeMaterial",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OtherMaterialsId = table.Column<int>(type: "int", nullable: false),
                    NodeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NodeMaterial", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NodeMaterial_Node_NodeId",
                        column: x => x.NodeId,
                        principalTable: "Node",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NodeMaterial_OtherMaterials_OtherMaterialsId",
                        column: x => x.OtherMaterialsId,
                        principalTable: "OtherMaterials",
                        principalColumn: "OtherMaterialsId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_NodeCable_CableId",
                table: "NodeCable",
                column: "CableId");

            migrationBuilder.CreateIndex(
                name: "IX_NodeCable_NodeId",
                table: "NodeCable",
                column: "NodeId");

            migrationBuilder.CreateIndex(
                name: "IX_NodeMaterial_NodeId",
                table: "NodeMaterial",
                column: "NodeId");

            migrationBuilder.CreateIndex(
                name: "IX_NodeMaterial_OtherMaterialsId",
                table: "NodeMaterial",
                column: "OtherMaterialsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NodeCable");

            migrationBuilder.DropTable(
                name: "NodeMaterial");

            migrationBuilder.DropTable(
                name: "Node");
        }
    }
}
