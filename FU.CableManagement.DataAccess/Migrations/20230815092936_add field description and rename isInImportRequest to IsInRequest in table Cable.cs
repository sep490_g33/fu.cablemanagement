﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class addfielddescriptionandrenameisInImportRequesttoIsInRequestintableCable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsInImportRequest",
                table: "Cable",
                newName: "IsInRequest");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Cable",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Cable");

            migrationBuilder.RenameColumn(
                name: "IsInRequest",
                table: "Cable",
                newName: "IsInImportRequest");
        }
    }
}
