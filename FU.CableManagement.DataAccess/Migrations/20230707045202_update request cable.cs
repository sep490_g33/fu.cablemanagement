﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class updaterequestcable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK__RequestC__9AC47BE7FD0DAC77",
                table: "RequestCable");

            migrationBuilder.AddPrimaryKey(
                name: "PK__RequestC__9AC47BE7FD0DAC77",
                table: "RequestCable",
                columns: new[] { "RequestId", "CableId", "StartPoint", "EndPoint" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK__RequestC__9AC47BE7FD0DAC77",
                table: "RequestCable");

            migrationBuilder.AddPrimaryKey(
                name: "PK__RequestC__9AC47BE7FD0DAC77",
                table: "RequestCable",
                columns: new[] { "RequestId", "CableId" });
        }
    }
}
