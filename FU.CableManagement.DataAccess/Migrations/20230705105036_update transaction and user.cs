﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class updatetransactionanduser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ApproverRequestId",
                table: "TransactionHistory",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "CreatorRequestId",
                table: "TransactionHistory",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_TransactionHistory_ApproverRequestId",
                table: "TransactionHistory",
                column: "ApproverRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionHistory_CreatorRequestId",
                table: "TransactionHistory",
                column: "CreatorRequestId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionHistory_User_ApproverRequestId",
                table: "TransactionHistory",
                column: "ApproverRequestId",
                principalTable: "User",
                principalColumn: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionHistory_User_CreatorRequestId",
                table: "TransactionHistory",
                column: "CreatorRequestId",
                principalTable: "User",
                principalColumn: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionHistory_User_ApproverRequestId",
                table: "TransactionHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionHistory_User_CreatorRequestId",
                table: "TransactionHistory");

            migrationBuilder.DropIndex(
                name: "IX_TransactionHistory_ApproverRequestId",
                table: "TransactionHistory");

            migrationBuilder.DropIndex(
                name: "IX_TransactionHistory_CreatorRequestId",
                table: "TransactionHistory");

            migrationBuilder.DropColumn(
                name: "ApproverRequestId",
                table: "TransactionHistory");

            migrationBuilder.DropColumn(
                name: "CreatorRequestId",
                table: "TransactionHistory");
        }
    }
}
