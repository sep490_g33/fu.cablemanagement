﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class fixdbmaterialfieldcode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_OtherMaterials_Code",
                table: "OtherMaterials");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_OtherMaterials_Code",
                table: "OtherMaterials",
                column: "Code",
                unique: true,
                filter: "[Code] IS NOT NULL");
        }
    }
}
