﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class addFKforrqrqCate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "RequestCategoryName",
                table: "RequestCategories",
                type: "nvarchar(50)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)");

            migrationBuilder.CreateIndex(
                name: "IX_Request_RequestCategoryId",
                table: "Request",
                column: "RequestCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Request_RequestCategories_RequestCategoryId",
                table: "Request",
                column: "RequestCategoryId",
                principalTable: "RequestCategories",
                principalColumn: "RequestCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Request_RequestCategories_RequestCategoryId",
                table: "Request");

            migrationBuilder.DropIndex(
                name: "IX_Request_RequestCategoryId",
                table: "Request");

            migrationBuilder.AlterColumn<string>(
                name: "RequestCategoryName",
                table: "RequestCategories",
                type: "nvarchar(50)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldNullable: true);
        }
    }
}
