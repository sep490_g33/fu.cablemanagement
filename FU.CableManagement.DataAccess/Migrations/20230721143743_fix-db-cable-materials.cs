﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class fixdbcablematerials : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__Cable__CableCate__3F466844",
                table: "Cable");

            migrationBuilder.DropForeignKey(
                name: "FK__OtherMate__Other__32E0915F",
                table: "OtherMaterials");

            migrationBuilder.DropTable(
                name: "CableCategory");

            migrationBuilder.DropTable(
                name: "OtherMaterialsCategories");

            migrationBuilder.DropIndex(
                name: "IX_OtherMaterials_OtherMaterialsCategoryId",
                table: "OtherMaterials");

            migrationBuilder.DropIndex(
                name: "IX_Cable_CableCategoryId",
                table: "Cable");

            migrationBuilder.RenameColumn(
                name: "OtherMaterialsCategoryId",
                table: "OtherMaterials",
                newName: "Code");

            migrationBuilder.RenameColumn(
                name: "CableCategoryId",
                table: "Cable",
                newName: "Code");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Code",
                table: "OtherMaterials",
                newName: "OtherMaterialsCategoryId");

            migrationBuilder.RenameColumn(
                name: "Code",
                table: "Cable",
                newName: "CableCategoryId");

            migrationBuilder.CreateTable(
                name: "CableCategory",
                columns: table => new
                {
                    CableCategoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CableCategoryName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CableCategory", x => x.CableCategoryId);
                });

            migrationBuilder.CreateTable(
                name: "OtherMaterialsCategories",
                columns: table => new
                {
                    OtherMaterialsCategoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    OtherMaterialsCategoryName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OtherMaterialsCategories", x => x.OtherMaterialsCategoryId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OtherMaterials_OtherMaterialsCategoryId",
                table: "OtherMaterials",
                column: "OtherMaterialsCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Cable_CableCategoryId",
                table: "Cable",
                column: "CableCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK__Cable__CableCate__3F466844",
                table: "Cable",
                column: "CableCategoryId",
                principalTable: "CableCategory",
                principalColumn: "CableCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK__OtherMate__Other__32E0915F",
                table: "OtherMaterials",
                column: "OtherMaterialsCategoryId",
                principalTable: "OtherMaterialsCategories",
                principalColumn: "OtherMaterialsCategoryId");
        }
    }
}
