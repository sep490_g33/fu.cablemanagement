﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class updaterqCabrqMat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RecoveryDestWarehouseId",
                table: "RequestOtherMaterials",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RecoveryDestWarehouseId",
                table: "RequestCable",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RecoveryDestWarehouseId",
                table: "RequestOtherMaterials");

            migrationBuilder.DropColumn(
                name: "RecoveryDestWarehouseId",
                table: "RequestCable");
        }
    }
}
