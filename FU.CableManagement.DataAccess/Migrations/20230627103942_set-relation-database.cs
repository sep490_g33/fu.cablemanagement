﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class setrelationdatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OtherMaterialsWarehouse");

            migrationBuilder.AddColumn<int>(
                name: "WarehouseId",
                table: "OtherMaterials",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OtherMaterials_WarehouseId",
                table: "OtherMaterials",
                column: "WarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK__OtherMate__Wareh__36B12233",
                table: "OtherMaterials",
                column: "WarehouseId",
                principalTable: "Warehouses",
                principalColumn: "WarehouseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__OtherMate__Wareh__36B12233",
                table: "OtherMaterials");

            migrationBuilder.DropIndex(
                name: "IX_OtherMaterials_WarehouseId",
                table: "OtherMaterials");

            migrationBuilder.DropColumn(
                name: "WarehouseId",
                table: "OtherMaterials");

            migrationBuilder.CreateTable(
                name: "OtherMaterialsWarehouse",
                columns: table => new
                {
                    WarehouseId = table.Column<int>(type: "int", nullable: false),
                    OtherMaterialsId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__OtherMat__77462D46A9C24245", x => new { x.WarehouseId, x.OtherMaterialsId });
                    table.ForeignKey(
                        name: "FK__OtherMate__Other__37A5467C",
                        column: x => x.OtherMaterialsId,
                        principalTable: "OtherMaterials",
                        principalColumn: "OtherMaterialsId");
                    table.ForeignKey(
                        name: "FK__OtherMate__Wareh__36B12243",
                        column: x => x.WarehouseId,
                        principalTable: "Warehouses",
                        principalColumn: "WarehouseId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_OtherMaterialsWarehouse_OtherMaterialsId",
                table: "OtherMaterialsWarehouse",
                column: "OtherMaterialsId");
        }
    }
}
