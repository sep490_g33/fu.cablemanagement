﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class deletefieldcodeinrqMatandtranMat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "TransactionOtherMaterials");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "RequestOtherMaterials");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "TransactionOtherMaterials",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "RequestOtherMaterials",
                type: "nvarchar(50)",
                nullable: true);
        }
    }
}
