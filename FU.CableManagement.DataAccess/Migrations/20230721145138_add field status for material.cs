﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class addfieldstatusformaterial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "OtherMaterials",
                type: "nvarchar(15)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OtherMaterials_Code",
                table: "OtherMaterials",
                column: "Code",
                unique: true,
                filter: "[Code] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Cable_Code",
                table: "Cable",
                column: "Code",
                unique: true,
                filter: "[Code] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_OtherMaterials_Code",
                table: "OtherMaterials");

            migrationBuilder.DropIndex(
                name: "IX_Cable_Code",
                table: "Cable");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "OtherMaterials");
        }
    }
}
