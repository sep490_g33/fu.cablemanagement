﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class nodeMaterialCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NodeMaterialCategory",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OtherMaterialCategoryId = table.Column<int>(type: "int", nullable: false),
                    NodeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NodeMaterialCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NodeMaterialCategory_Node_NodeId",
                        column: x => x.NodeId,
                        principalTable: "Node",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NodeMaterialCategory_OtherMaterialsCategory_OtherMaterialCategoryId",
                        column: x => x.OtherMaterialCategoryId,
                        principalTable: "OtherMaterialsCategory",
                        principalColumn: "OtherMaterialsCategoryId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_NodeMaterialCategory_NodeId",
                table: "NodeMaterialCategory",
                column: "NodeId");

            migrationBuilder.CreateIndex(
                name: "IX_NodeMaterialCategory_OtherMaterialCategoryId",
                table: "NodeMaterialCategory",
                column: "OtherMaterialCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NodeMaterialCategory");
        }
    }
}
