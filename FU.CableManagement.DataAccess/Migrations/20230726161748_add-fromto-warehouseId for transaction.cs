﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FU.CableManagement.DataAccess.Migrations
{
    public partial class addfromtowarehouseIdfortransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FromWarehouseName",
                table: "TransactionHistory",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ToWarehouseName",
                table: "TransactionHistory",
                type: "nvarchar(50)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FromWarehouseName",
                table: "TransactionHistory");

            migrationBuilder.DropColumn(
                name: "ToWarehouseName",
                table: "TransactionHistory");
        }
    }
}
