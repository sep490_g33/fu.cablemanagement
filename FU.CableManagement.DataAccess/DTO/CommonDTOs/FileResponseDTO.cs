﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FU.CableManagement.DataAccess.DTO.CommonDTOs
{
    public class FileResponseDTO
    {
        public string FileName { get; set; }
        public byte[] fileBytes { get; set; }
    }
}
