﻿using System.Net;

namespace FU.CableManagement.DataAccess.DTO.CommonDTOs
{
    public class MetaDataDTO
    {
        public int CurrentPage { get; set; }

        public int PageSize { get; set; }

        public int RowCount { get; set; }
    }
    public class BaseResponseDTO<T>
    {
        public bool Success { get; set; }
        public int Code { set; get; }
        public string Message { get; set; }
        public MetaDataDTO MetaData { set; get; }
        public T Data { get; set; }

        public BaseResponseDTO(T data, string message, int code)
        {
            Data = data;
            Message = message;
            Code = code;
            Success = true;
        }

        public BaseResponseDTO()
        {
            Success = true;
            Message = string.Empty;
            Code = 200;
        }

        public BaseResponseDTO(T data)
        {
            Success = true;
            Message = string.Empty;
            Data = data;
            Code = (int)HttpStatusCode.OK;
        }

        public BaseResponseDTO(string message, int code)
        {
            Success = false;
            Code = code;
            Message = message;
        }
    }
}
