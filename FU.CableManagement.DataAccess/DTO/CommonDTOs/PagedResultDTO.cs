﻿namespace FU.CableManagement.DataAccess.DTO.CommonDTOs
{
    public class PagedResultDTO<T> : PagedResultBaseDTO where T : class
    {
        public List<T> Results { get; set; }
        public int Sum { get; set; }
        public PagedResultDTO(int currentPage, int pageSize, int rowCount)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            RowCount = rowCount;
            PageCount = (int)Math.Ceiling((double)rowCount / pageSize);
        }

        public PagedResultDTO(int currentPage, int pageSize, int rowCount, List<T> results)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            RowCount = rowCount;
            PageCount = (int)Math.Ceiling((double)rowCount / pageSize);
            Results = results;
        }

        public PagedResultDTO(int currentPage, int pageSize, int rowCount, List<T> results, int sum)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            RowCount = rowCount;
            PageCount = (int)Math.Ceiling((double)rowCount / pageSize);
            Results = results;
            Sum = sum;
        }

        public PagedResultDTO()
        {
            Results = new List<T>();
        }
    }
}
