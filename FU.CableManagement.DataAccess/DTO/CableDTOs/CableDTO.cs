﻿
using FU.CableManagement.DataAccess.Data.Entities;

namespace FU.CableManagement.DataAccess.DTO.CableDTOs
{
    public class CableDTO
    {
        public Guid CableId { get; set; }
        public int CableCategoryId { get; set; }
        public string CableCategoryName { get; set; }
        public int? WarehouseId { get; set; }
        public string? Code { get; set; }
        public string? WarehouseName { get; set; }
        public int? SupplierId { get; set; }
        public string? SupplierName { get; set; }
        public int StartPoint { get; set; }
        public int EndPoint { get; set; }
        public int Length { get; set; }
        public int? YearOfManufacture { get; set; }
        public bool IsExportedToUse { get; set; }
        public string Description { get; set; }
        public bool IsInRequest { get; set; }
        public string Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdateAt { get; set; }
        public Guid? CableParentId { get; set; }
        public List<NodeCable> NodeCables { get; set; }
    }
}
