﻿namespace FU.CableManagement.DataAccess.DTO.CableDTOs
{
    public class ExportCableDTO
    {
        public Guid CableId { get; set; }
        public int? StartPoint { get; set; } = null;
        public int? EndPoint { get; set; } = null;
        public int? WarehouseId { get; set; } = null;
    }
}
