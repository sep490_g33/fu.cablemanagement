﻿
namespace FU.CableManagement.DataAccess.DTO.CableDTOs
{
    public class CutCableDTO
    {
        public Guid CableId { get; set; }
        public int StartPoint { get; set; } = 0;
        public int EndPoint { get; set; } = 0;
        public string? CableParentId { get; set; } = null;

    }
}
