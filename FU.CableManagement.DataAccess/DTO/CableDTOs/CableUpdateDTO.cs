﻿
namespace FU.CableManagement.DataAccess.DTO.CableDTOs
{
    public class CableUpdateDTO
    {
        public Guid CableId { get; set; }
        public int CableCategoryId { get; set; }
        public string? Code { get; set; }
        public int? WarehouseId { get; set; }
        public int? SupplierId { get; set; }
        public int StartPoint { get; set; }
        public int EndPoint { get; set; }
        public int? YearOfManufacture { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public Guid? CableParentId { get; set; }
    }
}
