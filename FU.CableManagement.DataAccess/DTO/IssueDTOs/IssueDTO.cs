﻿using FU.CableManagement.DataAccess.DTO.UserDTOs;

namespace FU.CableManagement.DataAccess.DTO.IssueDTOs
{
    public class IssueDTO
    {
        public Guid IssueId { get; set; }
        public string IssueName { get; set; }
        public string IssueCode { get; set; }
        public string Group { get; set; }
        public string CableRoutingName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdateAt { get; set; }
        public Guid CreatorId { get; set; }
        public UserDTO Creator { get; set; }
    }
}
