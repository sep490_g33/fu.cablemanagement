﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FU.CableManagement.DataAccess.DTO.RequestCategoryDTOs
{
    public class RequestCategoryDTO
    {
        public int RequestCategoryId { get; set; }
        public string RequestCategoryName { get; set; }

    }
}
