﻿namespace FU.CableManagement.DataAccess.DTO.RequestDTOs
{
    public class RequestMaterialDTO
    {
        public Guid RequestId { get; set; }
        public int OtherMaterialsId { get; set; }
        public string OtherMaterialsCategoryName { get; set; }
        public int Quantity { get; set; }
        public int? RecoveryDestWarehouseId { get; set; }
        public string? RecoveryDestWarehouseName { get; set; }
        public int? ImportedWarehouseId { get; set; }
        public string? ImportedWarehouseName { get; set; }
    }
}
