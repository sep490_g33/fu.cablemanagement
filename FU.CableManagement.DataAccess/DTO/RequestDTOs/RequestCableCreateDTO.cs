﻿
namespace FU.CableManagement.DataAccess.DTO.RequestDTOs
{
    public class RequestCableCreateDTO
    {
        public Guid CableId { get; set; }
        public int StartPoint { get; set; }
        public int EndPoint { get; set; }
        public string? Status { get; set; }
        public int? RecoveryDestWarehouseId { get; set; }
        //public int Length { get; set; }
    }
}
