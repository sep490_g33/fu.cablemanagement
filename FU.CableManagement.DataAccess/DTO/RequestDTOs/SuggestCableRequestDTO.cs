﻿namespace FU.CableManagement.DataAccess.DTO.RequestDTOs
{
    public class SuggestCableRequestDTO
    {
        public int Length { get; set; }
        public List<int> ListCableCategoryIds { get; set; } = new List<int>();
        public List<int> ListWarehouseIds { get; set; } = new List<int>();
    }
}
