﻿
namespace FU.CableManagement.DataAccess.DTO.RequestDTOs
{
    public class RequestUpdateDTO
    {
        public Guid RequestId { get; set; }
        public string RequestName { get; set; }
        public string Content { get; set; }
        public Guid? IssueId { get; set; }
        public int RequestCategoryId { get; set; }
        public int? DeliverWarehouseId { get; set; }
        public List<RequestCableCreateDTO> RequestCableDTOs { get; set; }
        public List<RequestMaterialCreateDTO> RequestOtherMaterialDTOs { get; set; }
    }
}
