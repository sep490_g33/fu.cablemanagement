﻿
namespace FU.CableManagement.DataAccess.DTO.RequestDTOs
{
    public class RequestDTO
    {
        public Guid RequestId { get; set; }
        public string RequestName { get; set; }
        public string Content { get; set; }
        public Guid CreatorId { get; set; }
        public string CreatorName { get; set; }
        public Guid? ApproverId { get; set; }
        public string? ApproverName { get; set; }
        public Guid? IssueId { get; set; }
        public string IssueName { get; set; }
        public string IssueCode { get; set; }
        public string Group { get; set; }
        public string CableRoutingName { get; set; }
        public string Status { get; set; }
        public int RequestCategoryId { get; set; }
        public string RequestCategoryName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdateAt { get; set; }
        public int? DeliverWarehouseId { get; set; }
        public string? DeliverWarehouseName { get; set; }
        public List<RequestCableDTO> RequestCableDTOs { get; set; }
        public List<RequestMaterialDTO> RequestMaterialDTOs { get; set; }
    }
}
