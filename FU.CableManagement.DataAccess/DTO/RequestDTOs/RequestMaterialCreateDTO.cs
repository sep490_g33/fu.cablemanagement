﻿namespace FU.CableManagement.DataAccess.DTO.RequestDTOs
{
    public class RequestMaterialCreateDTO
    {
        public int? OtherMaterialsId { get; set; }
        public string? Status { get; set; }
        public int Quantity { get; set; }
        public int? RecoveryDestWarehouseId { get; set; }
    }
}
