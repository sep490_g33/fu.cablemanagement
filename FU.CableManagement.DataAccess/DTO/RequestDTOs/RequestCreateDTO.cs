﻿
using FU.CableManagement.DataAccess.DTO.CableDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialsDTOs;

namespace FU.CableManagement.DataAccess.DTO.RequestDTOs
{
    public class RequestCreateDTO
    {
        public string RequestName { get; set; }
        public string Content { get; set; }
        public Guid? IssueId { get; set; }
        public int RequestCategoryId { get; set; }
        public int? DeliverWarehouseId { get; set; }
        public List<CableImportDTO> CableImportDTOs { get; set; }
        public List<OtherMaterialAddDTO> OtherMaterialAddDTOs { get; set; }
        public List<RequestCableCreateDTO> RequestCableDTOs { get; set; }
        public List<RequestMaterialCreateDTO> RequestOtherMaterialDTOs { get; set; }
    }
}
