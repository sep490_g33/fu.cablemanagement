﻿

namespace FU.CableManagement.DataAccess.DTO.RequestDTOs
{
    public class RequestCableDTO
    {
        public Guid RequestId { get; set; }
        public Guid CableId { get; set; }
        public string CableCategoryName { get; set; }
        public int StartPoint { get; set; }
        public int EndPoint { get; set; }
        public int Length { get; set; }
        public int? RecoveryDestWarehouseId { get; set; }
        public string? RecoveryDestWarehouseName { get; set; }
        public int? ImportedWarehouseId { get; set; }
        public string? ImportedWarehouseName { get; set; }
    }
}
