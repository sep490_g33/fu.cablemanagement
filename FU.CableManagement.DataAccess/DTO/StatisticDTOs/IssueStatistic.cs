﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FU.CableManagement.DataAccess.DTO.StatisticDTOs
{
    public class IssueStatistic
    {
        public int TotalIssue { get; set; }
        public int TotalIssueDone { get; set; }
        public int TotalIssueDoing { get; set; }
    }
}
