﻿
namespace FU.CableManagement.DataAccess.DTO.StatisticDTOs
{
    public class MaterialFluctuation
    {
        public string MaterialName { get; set; }
        public int Variation { get; set; }
        public string Unit { get; set; }
    }
}
