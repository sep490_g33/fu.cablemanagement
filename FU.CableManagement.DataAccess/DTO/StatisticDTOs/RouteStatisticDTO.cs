﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FU.CableManagement.DataAccess.DTO.StatisticDTOs
{
    public class RouteStatisticDTO
    {
        public int OtherMaterialCategoryId { get; set; }
        public string OtherMaterialCategoryName { get; set; }
        public int Quantity { get; set; }
    }
}
