﻿
namespace FU.CableManagement.DataAccess.DTO.StatisticDTOs
{
    public class CableCategoryStatistic
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int Length { get; set; }
    }
}
