﻿namespace FU.CableManagement.DataAccess.DTO.StatisticDTOs
{
    public class ExchangeMaterial
    {
        public int ImportCables { get; set; }
        public int ExportCables { get; set; }
        public int ImportMaterials { get; set; }
        public int ExportMaterials { get; set; }
    }
}
