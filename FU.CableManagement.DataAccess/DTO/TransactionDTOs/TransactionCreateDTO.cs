﻿using FU.CableManagement.DataAccess.DTO.WareHouseDTOs;

namespace FU.CableManagement.DataAccess.DTO.TransactionDTOs
{
    public class TransactionCreateDTO
    {
        public string TransactionCategoryName { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int WarehouseId { get; set; }
        public int? FromWarehouseId { get; set; }
        public int? ToWarehouseId { get; set; }
        public virtual WareHouseDTO Warehouse { get; set; }
        public List<TransactionCableDTO> CableTransactions { get; set; }
        public List<TransactionMaterialDTO> MaterialsTransaction { get; set; }
    }
}
