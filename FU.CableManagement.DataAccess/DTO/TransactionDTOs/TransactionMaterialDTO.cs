﻿
namespace FU.CableManagement.DataAccess.DTO.TransactionDTOs
{
    public class TransactionMaterialDTO
    {
        public Guid TransactionId { get; set; }
        public int OtherMaterialsId { get; set; }
        public string OtherMaterialsCategoryName { get; set; }
        public string? Code { get; set; }
        public string Status { get; set; }
        public int Quantity { get; set; }
        public string Note { get; set; }
    }
}
