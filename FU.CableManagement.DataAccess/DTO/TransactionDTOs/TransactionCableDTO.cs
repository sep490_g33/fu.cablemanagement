﻿namespace FU.CableManagement.DataAccess.DTO.TransactionDTOs
{
    public class TransactionCableDTO
    {
        public Guid TransactionId { get; set; }
        public Guid CableId { get; set; }
        public string CableCategoryName { get; set; }
        public int StartPoint { get; set; }
        public int EndPoint { get; set; }
        public int Length { get; set; }
        public string Note { get; set; }
    }
}
