﻿

namespace FU.CableManagement.DataAccess.DTO.TransactionDTOs
{
    public class TransactionDTO
    {
        public Guid TransactionId { get; set; }
        public string TransactionCategoryName { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int WarehouseId { get; set; }
        public string? IssueCode { get; set; }
        public string WarehouseName { get; set; }
        public string? FromWarehouseName { get; set; }
        public string? ToWarehouseName { get; set; }
        public string? WarehouseNote { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<TransactionCableDTO> CableTransactions { get; set; }
        public List<TransactionMaterialDTO> MaterialsTransaction { get; set; }
    }
}
