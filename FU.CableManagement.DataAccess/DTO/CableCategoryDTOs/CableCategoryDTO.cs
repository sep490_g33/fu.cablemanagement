﻿
namespace FU.CableManagement.DataAccess.DTO.CableCategoryDTOs
{
    public class CableCategoryDTO
    {
        public int CableCategoryId { get; set; }
        public string CableCategoryName { get; set; }
    }
}
