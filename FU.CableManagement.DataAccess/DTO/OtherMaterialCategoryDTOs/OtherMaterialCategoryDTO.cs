﻿
namespace FU.CableManagement.DataAccess.DTO.OtherMaterialCategoryDTOs
{
    public class OtherMaterialCategoryDTO
    {
        public int OtherMaterialsCategoryId { get; set; }
        public string OtherMaterialsCategoryName { get; set; }
    }
}
