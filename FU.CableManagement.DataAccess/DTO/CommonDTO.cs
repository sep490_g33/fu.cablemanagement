﻿namespace FU.CableManagement.DataAccess.DTO
{
    public class CommonDTO
    {
        public DateTime CreatedAt { get; set; }
        public DateTime UpdateAt { get; set; }
        public bool isDeleted { get; set; }
    }
}

