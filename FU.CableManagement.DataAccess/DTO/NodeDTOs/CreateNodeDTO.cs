﻿namespace FU.CableManagement.DataAccess.DTO.NodeDTOs
{
    public class CreateNodeDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public string NodeCode { get; set; }
        public string NodeNumberSign { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public string Note { get; set; }
        public Guid? RouteId { get; set; }
        public int NumberOrder { get; set; }
        public string MaterialCategory { get; set; }
    }
}
