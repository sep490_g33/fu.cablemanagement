﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FU.CableManagement.DataAccess.DTO.NodeDTOs
{
    public class NodeMaterialCategoryDTO
    {
        public Guid Id { get; set; }
        public int OtherMaterialCategoryId { get; set; }
        public string OtherMaterialCategoryName { get; set; }
        public Guid NodeId { get; set; }
        public int Quantity { get; set; }
    }
}
