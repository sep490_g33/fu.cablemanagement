﻿using FU.CableManagement.DataAccess.Data.Entities;

namespace FU.CableManagement.DataAccess.DTO.NodeDTOs
{
    public class NodeDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdateAt { get; set; }
        public virtual ICollection<NodeCable> NodeCables { get; set; }
        public virtual ICollection<NodeMaterial> NodeMaterials { get; set; }
        public virtual ICollection<NodeMaterialCategoryDTO> NodeMaterialCategories { get; set; }
        public string NodeCode { get; set; }
        public string NodeNumberSign { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public string Note { get; set; }
        public Guid? RouteId { get; set; }
        public int NumberOrder { get; set; }
    }
}
