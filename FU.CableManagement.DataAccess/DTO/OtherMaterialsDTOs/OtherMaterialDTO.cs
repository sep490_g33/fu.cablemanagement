﻿namespace FU.CableManagement.DataAccess.DTO.OtherMaterialsDTOs
{
    public class OtherMaterialDTO
    {
        public int OtherMaterialsId { get; set; }
        public int OtherMaterialsCategoryId { get; set; }
        public string OtherMaterialsCategoryName { get; set; }
        public string? Code { get; set; }
        public string Unit { get; set; }
        public int? Quantity { get; set; }
        public string Status { get; set; }

        public int? SupplierId { get; set; }
        public string SupplierName { get; set; }
        public int? WarehouseId { get; set; }
        public string? WarehouseName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdateAt { get; set; }

    }
}
