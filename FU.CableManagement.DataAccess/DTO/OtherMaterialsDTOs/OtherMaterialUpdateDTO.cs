﻿namespace FU.CableManagement.DataAccess.DTO.OtherMaterialsDTOs
{
    public class OtherMaterialUpdateDTO
    {
        public int OtherMaterialsId { get; set; }
        public int OtherMaterialsCategoryId { get; set; }
        public string? Code { get; set; }
        public string Unit { get; set; }
        public int? Quantity { get; set; }
        public int? SupplierId { get; set; }
        public int? WarehouseId { get; set; }
        public string Status { get; set; }


    }
}
