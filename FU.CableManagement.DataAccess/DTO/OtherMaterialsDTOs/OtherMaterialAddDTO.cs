﻿
namespace FU.CableManagement.DataAccess.DTO.OtherMaterialsDTOs
{
    public class OtherMaterialAddDTO
    {
        public int OtherMaterialsCategoryId { get; set; }
        public string Unit { get; set; }
        public string? Code { get; set; }
        public int? Quantity { get; set; }
        public int? WarehouseId { get; set; }
        public int? SupplierId { get; set; }
        public string Status { get; set; }

    }
}
