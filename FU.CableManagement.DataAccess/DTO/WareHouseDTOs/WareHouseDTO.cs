﻿using FU.CableManagement.DataAccess.DTO.UserDTOs;

namespace FU.CableManagement.DataAccess.DTO.WareHouseDTOs
{
    public class WareHouseDTO
    {
        public int WarehouseId { get; set; }
        public string WarehouseName { get; set; }
        public string WarehouseAddress { get; set; }
        //public int Capacity { get; set; }
        public Guid? WarehouseKeeperid { get; set; }
        public Guid CreatorId { get; set; }
        public UserDTO Creator { get; set; }
        public UserDTO WarehouseKeeper { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdateAt { get; set; }

    }
}
