﻿namespace FU.CableManagement.DataAccess.DTO.UserDTOs
{
    public class TokenResponse
    {
        public string Access_Token { get; set; }
    }
}
