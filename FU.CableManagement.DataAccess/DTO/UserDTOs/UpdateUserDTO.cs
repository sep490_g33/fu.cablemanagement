﻿using System;

namespace FU.CableManagement.API.Models.UserModels
{
    public class UpdateUserDTO
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int RoleId { get; set; }
    }
}
