﻿using FU.CableManagement.DataAccess.DTO.NodeDTOs;

namespace FU.CableManagement.DataAccess.DTO.RouteDTOs
{
    public class UpdateRouteDTO
    {
        public Guid RouteId { get; set; }
        public string RouteName { get; set; }
        public List<UpdateNodeDTO> ListNodeUpdates { get; set; }
    }
}
