﻿using FU.CableManagement.DataAccess.DTO.NodeDTOs;

namespace FU.CableManagement.DataAccess.DTO.RouteDTOs
{
    public class CreateRouteDTO
    {
        public string RouteName { get; set; }
        public List<CreateNodeDTO> ListNodes { get; set; } = new List<CreateNodeDTO>();
    }
}
