﻿using FU.CableManagement.DataAccess.Data.Entities;

namespace FU.CableManagement.DataAccess.DTO.RouteDTOs
{
    public class RouteDTO
    {
        public Guid RouteId { get; set; }
        public string RouteName { get; set; }
        public virtual ICollection<Node> ListNodes { get; set; } = new List<Node>();
    }
}
