﻿namespace FU.CableManagement.DataAccess.Common.Constants
{
    public class RequestStatusConstant
    {
        public const string Pending = "Pending";
        public const string Approved = "Approved";
        public const string Rejected = "Rejected";
    }
}
