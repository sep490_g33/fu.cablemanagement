﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FU.CableManagement.DataAccess.Common.Constants
{
    public class CableStatusConstant
    {
        public const string Old = "Cũ";
        public const string New = "Mới";
    }
}
