﻿
namespace FU.CableManagement.DataAccess.Common.Constants
{
    public class IssueStatusConstants
    {
        public const string Doing = "Đang xử lý";
        public const string Done = "Đã xử lý";

    }
}
