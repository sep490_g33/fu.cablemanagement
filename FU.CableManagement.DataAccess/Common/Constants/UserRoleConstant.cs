﻿namespace FU.CableManagement.DataAccess.Common.Constants
{
    public static class UserRoleConstant
    {
        public const string Admin = "admin";
        public const string Leader = "leader";
        public const string Staff = "staff";
        public const string WarehouseKeeper = "warehouse-keeper";
    }
}
