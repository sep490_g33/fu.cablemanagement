﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FU.CableManagement.DataAccess.Common.Constants
{
    public class WarehouseCapacityConstant
    {
        public const float MinRate = 1.1f; // rate is used to warn user that this material's quantity is low in warehouse
        public const float MaxRate = 0.9f; // rate is used to warn user that this material's quantity is nearly full in warehouse

    }
}
