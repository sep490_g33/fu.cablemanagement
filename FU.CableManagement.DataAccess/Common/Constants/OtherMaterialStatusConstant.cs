﻿
namespace FU.CableManagement.DataAccess.Common.Constants
{
    public class OtherMaterialStatusConstant
    {
        public const string Old = "Cũ";
        public const string New = "Mới";
    }
}
