﻿
namespace FU.CableManagement.DataAccess.Common.Constants
{
    public class TransactionCategoryContants
    {
        public const string Import = "Nhập Kho";
        public const string Export = "Xuất Kho";
        public const string Cancel = "Hủy";
    }
}
