﻿
namespace FU.CableManagement.DataAccess.Common.Enums
{
    public enum RequestCategoryEnum : short
    {
        Export = 1, 
        Import = 2, 
        Recovery = 3, //Thu hồi
        Deliver = 4, 
        Cancel = 5,
        CancelOutside = 6,
        Other = 7,
    }
}
