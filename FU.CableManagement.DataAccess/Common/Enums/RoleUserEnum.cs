﻿namespace FU.CableManagement.DataAccess.Common.Enums
{
    public enum RoleUserEnum : short
    {
        None = 0,
        Admin = 1,
        Leader = 2,
        Staff = 3,
        WarehouseKeeper = 4
    }
}
