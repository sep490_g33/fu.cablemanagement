﻿using AutoMapper;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CableCategoryDTOs;
using FU.CableManagement.DataAccess.DTO.CableDTOs;
using FU.CableManagement.DataAccess.DTO.IssueDTOs;
using FU.CableManagement.DataAccess.DTO.NodeDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialCategoryDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialsDTOs;
using FU.CableManagement.DataAccess.DTO.RequestCategoryDTOs;
using FU.CableManagement.DataAccess.DTO.RequestDTOs;
using FU.CableManagement.DataAccess.DTO.RouteDTOs;
using FU.CableManagement.DataAccess.DTO.SupplierDTOs;
using FU.CableManagement.DataAccess.DTO.TransactionDTOs;
using FU.CableManagement.DataAccess.DTO.UserDTOs;
using FU.CableManagement.DataAccess.DTO.WareHouseDTOs;


namespace FU.CableManagement.API
{
    public class DataMappingProfile : Profile
    {
        public DataMappingProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(dest => dest.Rolename, opt => opt.MapFrom(source => source.Role.Rolename));
            CreateMap<Supplier, SupplierDTO>().ReverseMap();
            CreateMap<Warehouse, WareHouseDTO>().ReverseMap();
            CreateMap<Issue, IssueDTO>().ReverseMap();
            CreateMap<Cable, CableDTO>()
                .ForMember(dest => dest.SupplierName, opt => opt.MapFrom(source => source.Supplier.SupplierName))
                .ForMember(dest => dest.WarehouseName, opt => opt.MapFrom(source => source.Warehouse.WarehouseName))
                .ForMember(dest => dest.CableCategoryName, opt => opt.MapFrom(source => source.CableCategory.CableCategoryName))
                .ReverseMap();
            CreateMap<CableCategory, CableCategoryDTO>().ReverseMap();
            CreateMap<OtherMaterialsCategory, OtherMaterialCategoryDTO>().ReverseMap();
            CreateMap<CableImportDTO, Cable>();
            CreateMap<CableUpdateDTO, Cable>();
            CreateMap<CutCableDTO, Cable>();
            CreateMap<OtherMaterial, OtherMaterialDTO>()
                .ForMember(dest => dest.SupplierName, opt => opt.MapFrom(source => source.Supplier.SupplierName))
                .ForMember(dest => dest.WarehouseName, opt => opt.MapFrom(source => source.WareHouse.WarehouseName))
                .ForMember(dest => dest.OtherMaterialsCategoryName, opt => opt.MapFrom(source => source.OtherMaterialsCategory.OtherMaterialsCategoryName))
                ;
            CreateMap<OtherMaterialAddDTO, OtherMaterial>();
            CreateMap<OtherMaterialUpdateDTO, OtherMaterial>();
            CreateMap<Request, RequestDTO>()
                .ForMember(dest => dest.RequestCategoryName, opt => opt.MapFrom(source => source.RequestCategory.RequestCategoryName))
                .ForMember(dest => dest.ApproverName, opt => opt.MapFrom(source => source.Approver.Lastname + " " + source.Approver.Firstname))
                .ForMember(dest => dest.CreatorName, opt => opt.MapFrom(source => source.Creator.Lastname + " " + source.Creator.Firstname))
                .ForMember(dest => dest.IssueName, opt => opt.MapFrom(source => source.Issue.IssueName))
                .ForMember(dest => dest.IssueCode, opt => opt.MapFrom(source => source.Issue.IssueCode))
                .ForMember(dest => dest.Group, opt => opt.MapFrom(source => source.Issue.Group))
                .ForMember(dest => dest.CableRoutingName, opt => opt.MapFrom(source => source.Issue.CableRoutingName))
                .ForMember(dest => dest.DeliverWarehouseName, opt => opt.MapFrom(source => source.DeliverWarehouse.WarehouseName))
                ;
            CreateMap<RequestCableCreateDTO, RequestCable>()
                .ForMember(dest => dest.Length, opt => opt.MapFrom(source => source.EndPoint - source.StartPoint));

            CreateMap<NodeMaterialCategory, NodeMaterialCategoryDTO>()
                .ForMember(dest => dest.OtherMaterialCategoryName, opt => opt.MapFrom(source => source.MaterialCategory.OtherMaterialsCategoryName));


            CreateMap<RequestCreateDTO, Request>();
            CreateMap<RequestUpdateDTO, Request>();
            CreateMap<RequestMaterialCreateDTO, RequestOtherMaterial>();
            CreateMap<RequestCable, RequestCableDTO>()
                .ForMember(dest => dest.CableCategoryName, opt => opt.MapFrom(source => source.Cable.CableCategory.CableCategoryName))
                .ForMember(dest => dest.RecoveryDestWarehouseName, opt => opt.MapFrom(source => source.RecoveryDestWarehouse.WarehouseName))
                .ForMember(dest => dest.ImportedWarehouseName, opt => opt.MapFrom(source => source.ImportedWarehouse.WarehouseName))
                ;
            CreateMap<RequestOtherMaterial, RequestMaterialDTO>()
                .ForMember(dest => dest.OtherMaterialsCategoryName, opt => opt.MapFrom(source => source.OtherMaterials.OtherMaterialsCategory.OtherMaterialsCategoryName))
                .ForMember(dest => dest.RecoveryDestWarehouseName, opt => opt.MapFrom(source => source.RecoveryDestWarehouse.WarehouseName))
                .ForMember(dest => dest.ImportedWarehouseName, opt => opt.MapFrom(source => source.ImportedWarehouse.WarehouseName))
                ;
            CreateMap<CreateNodeDTO, Node>();
            CreateMap<Node, NodeDTO>();
            CreateMap<UpdateNodeDTO, Node>();
            CreateMap<TransactionHistory, TransactionDTO>()
                .ForMember(dest => dest.WarehouseName, opt => opt.MapFrom(source => source.Warehouse.WarehouseName))
                .ForMember(dest => dest.IssueCode, opt => opt.MapFrom(source => source.Issue.IssueCode))
                .ForMember(dest => dest.FromWarehouseName, opt => opt.MapFrom(source => source.FromWarehouse.WarehouseName))
                .ForMember(dest => dest.ToWarehouseName, opt => opt.MapFrom(source => source.ToWarehouse.WarehouseName))
                .ForMember(dest => dest.WarehouseNote, opt => opt.MapFrom
                (source => (source.FromWarehouse != null ? "Nhập từ kho " + source.FromWarehouse.WarehouseName : 
                (source.ToWarehouse != null ? "Xuất đến kho " + source.ToWarehouse.WarehouseName : null))))
                ;
            CreateMap<TransactionCable, TransactionCableDTO>()
                .ForMember(dest => dest.CableCategoryName, opt => opt.MapFrom(source => source.Cable.CableCategory.CableCategoryName))
                ;
            CreateMap<TransactionOtherMaterial, TransactionMaterialDTO>()
                .ForMember(dest => dest.OtherMaterialsCategoryName, opt => opt.MapFrom(source => source.OtherMaterials.OtherMaterialsCategory.OtherMaterialsCategoryName))
                .ForMember(dest => dest.Code, opt => opt.MapFrom(source => source.OtherMaterials.Code))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(source => source.OtherMaterials.Status))
                ;
            CreateMap<TransactionCreateDTO, TransactionHistory>();
            CreateMap<RequestCategory, RequestCategoryDTO>().ReverseMap();


            CreateMap<CreateRouteDTO, Route>();
            CreateMap<UpdateRouteDTO, Route>();
            CreateMap<Route, RouteDTO>().ReverseMap();
        }
    }
}
