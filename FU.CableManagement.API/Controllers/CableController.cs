﻿using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.DTO.CableDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    /// <summary>
    /// Controller for Cable Controller
    /// </summary>
    public class CableController : BaseAPIController
    {
        private readonly ICableService cableService;
        public CableController(IServiceProvider serviceProvider, ICableService cableService) : base(serviceProvider)
        {
            this.cableService = cableService;
        }
        /// <summary>
        /// Get All Cable With filter
        /// </summary>
        /// <param name="filter">search string </param>
        /// <param name="pageSize">number of item per page</param>
        /// <param name="pageIndex">page number </param>
        /// <param name="warehouseId">with specific warehouseid</param>
        /// <returns>Return Paging List Cable sastify filter</returns>
        [HttpGet("cable/get-all-cables")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<CableDTO>>> GetAllCables([FromQuery] string filter, int pageSize = 12,
            int pageIndex = 1, int? warehouseId = null, bool? isExportedToUse = null)
        {
            return await cableService.GetAllCables(filter, pageSize, pageIndex, warehouseId, isExportedToUse);
        }

        /// <summary>
        /// Get Cable By Specific Id
        /// </summary>
        /// <param name="cableId">Calbe Id</param>
        /// <returns>Return Cable with Id equals param or throw exception else</returns>
        [HttpGet("cable/get-cable-by-id")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<CableDTO>> GetCableById([FromQuery] Guid cableId)
        {
            return await cableService.GetCableById(cableId);
        }

        /// <summary>
        /// Create Cable To Database
        /// </summary>
        /// <param name="cableDTO">Cable want to create</param>
        /// <returns>True if create successfull, else false</returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPost("cable/import-cable")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ImportCable([FromBody] CableImportDTO cableDTO)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            var creatorId = GetClaim("userid");
            return await cableService.AddCable(cableDTO, creatorId);
        }

        /// <summary>
        /// Update Cable To Database
        /// </summary>
        /// <param name="cableDTO">Cable want to create</param>
        /// <returns>True if create successfull, else false</returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("cable/update-cable")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> UpdateCable([FromBody] CableUpdateDTO cableDTO)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await cableService.UpdateCable(cableDTO);
        }

        /// <summary>
        /// Delete Cable from Database with specific id
        /// </summary>
        /// <param name="cableId">given cable id want delete</param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpDelete("cable/delete-cable")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> DeleteCable([FromQuery] Guid cableId)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await cableService.DeleteCable(cableId);
        }

        /// <summary>
        /// Get All Cable In a Warehouse
        /// </summary>
        /// <param name="warehouseId"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [HttpGet("cable/get-cables-in-a-warehouse")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<CableDTO>>> GetCablesInAWarehouse([FromQuery] int warehouseId, int pageSize = 12,
            int pageIndex = 1)
        {
            return await cableService.GetCablesInAWarehouse(warehouseId, pageSize, pageIndex);
        }

        /// <summary>
        /// Import Cable from Excels
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [DisableRequestSizeLimit]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        [HttpPost("cable/import-excel-cable")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ImportExcelCable(IFormFile file)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            var creatorId = GetClaim("userid");
            return await cableService.ImportExcelCables(file, creatorId);
        }
    }
}
