﻿using DocumentFormat.OpenXml.Drawing.Charts;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.Common.Enums;
using FU.CableManagement.DataAccess.DTO.CableDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.RequestDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic; 
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    //[AllowAnonymous]
    public class RequestController : BaseAPIController
    {
        private readonly IRequestService requestService;
        public RequestController(IServiceProvider serviceProvider, IRequestService requestService) : base(serviceProvider)
        {
            this.requestService = requestService;
        }

        /// <summary>
        /// Create request to export material from warehouses with role Staff
        /// </summary>
        /// <param name="requestCreateDTO"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        /// <exception cref="ApplicationException"></exception>
        [HttpPost("request/create-request-export")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> CreateRequestExport([FromBody] RequestCreateDTO requestCreateDTO)
        {
            //if (!IsStaff())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            if (requestCreateDTO == null)
            {
                throw new ApplicationException("Yêu cầu không hợp lẹ");
            }
            if (requestCreateDTO.RequestCategoryId != (int)RequestCategoryEnum.Export) throw new ApplicationException("Không phải yêu cầu xuất kho");
            var creatorId = GetClaim("userid");
            return await requestService.CreateRequest(requestCreateDTO, creatorId);
        }

        /// <summary>
        /// Get request with specific id
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpGet("request/get-request-by-id")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<RequestDTO>> GetRequestById(Guid requestId)
        {
            //if (!IsLeader() && !IsStaff() && !IsAdmin())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            return await requestService.GetRequestById(requestId);
        }

        /// <summary>
        /// Get all requests in database with role Leader
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="issueId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpGet("request/get-all-requests")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<RequestDTO>>> GetAllRequest(string filter, int pageSize = 12,
            int pageIndex = 1, Guid? issueId = null)
        {
            //if (!IsAdmin() && !IsLeader())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            return await requestService.GetAllRequest(filter, pageSize, pageIndex, issueId);
        }

        /// <summary>
        /// get all requests, which belongt to a specific staff
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="issueId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpGet("request/get-requests-by-staff")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<RequestDTO>>> GetRequestByStaff(string filter, int pageSize = 12,
            int pageIndex = 1, Guid? issueId = null)
        {
            //if (!IsLeader() && !IsStaff())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            var staffId = GetClaim("userid");
            return await requestService.GetRequestByStaff(filter, pageSize, pageIndex, staffId, issueId);
        }

        /// <summary>
        /// Approve request to export materials from warehouse with role Leader
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("request/approve-request-export")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ApproveRequestExport([FromQuery] Guid requestId)
        {
            //if (!IsAdmin() && !IsLeader())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            var approverId = GetClaim("userid");
            return await requestService.ApproveRequestExport(requestId, approverId);
        }

        /// <summary>
        /// Reject a specific request with role Leader
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("request/reject-request-by-id")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> RejectRequest(Guid requestId)
        {
            //if (!IsAdmin() && !IsLeader())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            var rejectorId = GetClaim("userid");
            return await requestService.RejectRequest(requestId, rejectorId);
        }
        /// <summary>
        /// Update a pending request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("request/update-request-by-id")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> UpdateRequest([FromBody] RequestUpdateDTO model)
        {
            //if (!IsStaff())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            return await requestService.UpdateRequest(model);
        }
        /// <summary>
        /// Delete a pending request
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpDelete("request/delete-request")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> DeleteRequest([FromQuery] Guid requestId)
        {
            //if (!IsAdmin() && !IsStaff())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            return await requestService.DeleteRequest(requestId);
        }
        /// <summary>
        /// Get all cables, which are used for a specific issue
        /// </summary>
        /// <param name="issueId"></param>
        /// <returns></returns>
        [HttpGet("request/get-all-cables-by-issue")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<List<RequestCableDTO>>> GetAllCablesByIssue([FromQuery] Guid issueId)
        {
            return await requestService.GetAllCablesByIssue(issueId);
        }
        /// <summary>
        /// Create a request which is used for deleivery of materials to a specific warehouse
        /// </summary>
        /// <param name="requestCreateDTO"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        /// <exception cref="ApplicationException"></exception>
        [HttpPost("request/create-request-for-delivery")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> CreateRequestForDeliver([FromBody] RequestCreateDTO requestCreateDTO)
        {
            //if (!IsStaff())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            if (requestCreateDTO.RequestCategoryId != (int)RequestCategoryEnum.Deliver) throw new ApplicationException("Không phải yêu cầu chuyển kho");
            var creatorId = GetClaim("userid");

            return await requestService.CreateRequest(requestCreateDTO, creatorId);
        }
        /// <summary>
        /// Approve a request for delivery
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("request/approve-request-for-deliver")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ApproveRequestForDeliver([FromQuery] Guid requestId)
        {
            //if (!IsAdmin() && !IsLeader())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            var approverId = GetClaim("userid");
            return await requestService.ApproveRequestForDeliver(requestId, approverId);
        }
        /// <summary>
        /// Create a request for recovery to take materials from area, in which issue is resolved, to warehouses
        /// </summary>
        /// <param name="requestCreateDTO"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        /// <exception cref="ApplicationException"></exception>
        //[HttpPost("request/create-request-for-recovery")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //public async Task<BaseResponseDTO<bool>> CreateRequestForRecovery([FromBody] RequestCreateDTO requestCreateDTO)
        //{
        //    //if (!IsStaff())
        //    //{
        //    //    throw new UnauthorizedAccessException();
        //    //}
        //    if (requestCreateDTO.RequestCategoryId != (int)RequestCategoryEnum.Recovery) throw new ApplicationException("It's not request for recovery");
        //    var creatorId = GetClaim("userid");
        //    return await requestService.CreateRequest(requestCreateDTO, creatorId);
        //}

        /// <summary>
        /// Approve request for recovery with role Leader
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        //[HttpPut("request/approve-request-for-recovery")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //public async Task<BaseResponseDTO<bool>> ApproveRequestForRecovery(Guid requestId)
        //{
        //    //if (!IsAdmin() && !IsLeader())
        //    //{
        //    //    throw new UnauthorizedAccessException();
        //    //}
        //    var approverId = GetClaim("userid");

        //    return await requestService.ApproveRequestForRecovery(requestId, approverId);
        //}
        /// <summary>
        /// Create request for cancel materials in warehouse
        /// </summary>
        /// <param name="requestCreateDTO"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        /// <exception cref="ApplicationException"></exception>
        [HttpPost("request/create-request-for-cancel-in-warehouse")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> CreateRequestForCancelInWarehouse([FromBody] RequestCreateDTO requestCreateDTO)
        {
            //if (!IsStaff())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            if (requestCreateDTO.RequestCategoryId != (int)RequestCategoryEnum.Cancel) throw new ApplicationException("Không phải yêu cầu hủy");
            var creatorId = GetClaim("userid");
            return await requestService.CreateRequest(requestCreateDTO, creatorId);
        }
        /// <summary>
        /// Approve request for cancel materials in warehouse with role Leader
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("request/approve-request-for-cancel-in-warehouse")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ApproveRequestForCancelInWarehouse(Guid requestId)
        {
            //if (!IsAdmin() && !IsLeader())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            var approverId = GetClaim("userid");

            return await requestService.ApproveRequestForCancelInWarehouse(requestId, approverId);
        }

        /// <summary>
        /// Suggest for users to help them to choose the most suitable cable
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPost("request/suggest-cable")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<List<CableDTO>>> SuggestCable([FromBody] SuggestCableRequestDTO request)
        {
            //if (!IsStaff())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            return await requestService.SuggestCable(request);
        }

        /// <summary>
        /// Create request to import materials
        /// </summary>
        /// <param name="requestCreateDTO"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        /// <exception cref="ApplicationException"></exception>
        [DisableRequestSizeLimit]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        [HttpPost("request/create-request-for-import")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> CreateRequestForImport([FromForm] RequestCreateDTO requestCreateDTO, IFormFile? cableFile = null,
            IFormFile? otherMaterialFile = null)
        {
            //if (!IsStaff())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            if (requestCreateDTO.RequestCategoryId != (int)RequestCategoryEnum.Import) throw new ApplicationException("Không phải yêu cầu nhập kho");
            var creatorId = GetClaim("userid");
            return await requestService.CreateRequestForImport(requestCreateDTO, creatorId, cableFile, otherMaterialFile);
        }
        /// <summary>
        /// Approve request to import materials
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("request/approve-request-for-import")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ApproveRequestForImport(Guid requestId)
        {
            //if (!IsAdmin() && !IsLeader())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            var approverId = GetClaim("userid");

            return await requestService.ApproveRequestForImport(requestId, approverId);
        }

        /// <summary>
        /// Create request for recovery with unavailable materials
        /// </summary>
        /// <param name="requestCreateDTO"></param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        [HttpPost("request/create-request-for-recovery")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> CreateRequestForRecovery([FromBody] RequestCreateDTO requestCreateDTO)
        {
            //if (!IsStaff())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            if (requestCreateDTO.RequestCategoryId != (int)RequestCategoryEnum.Recovery) throw new ApplicationException("Không phải yêu cầu thu hồi");
            var creatorId = GetClaim("userid");
            return await requestService.CreateRequestForRecovery(requestCreateDTO, creatorId);
        }
        /// <summary>
        /// Approve request for recovery with unavailable materials
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        [HttpPut("request/approve-request-for-recovery")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ApproveRequestForRecovery(Guid requestId)
        {
            //if (!IsAdmin() && !IsLeader())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            var approverId = GetClaim("userid");

            return await requestService.ApproveRequestForRecovery(requestId, approverId);
        }

        /// <summary>
        /// Create request for cancel material outside warehouse
        /// </summary>
        /// <param name="requestCreateDTO"></param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        [HttpPost("request/create-request-for-cancel-material-outside-warehouse")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> CreateRequestForCancelMaterialOutsideWarehouse([FromBody] RequestCreateDTO requestCreateDTO)
        {
            //if (!IsStaff())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            if (requestCreateDTO.RequestCategoryId != (int)RequestCategoryEnum.CancelOutside) throw new ApplicationException("Không phải yêu cầu hủy ngoài kho");
            var creatorId = GetClaim("userid");
            return await requestService.CreateRequestForCancelMaterialOutsideWarehouse(requestCreateDTO, creatorId);
        }
        /// <summary>
        ///  Approve request for cancel material outside warehouse
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        [HttpPut("request/approve-request-for-cancel-material-outside-warehouse")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ApproveRequestForCancelMaterialOutsideWarehouse(Guid requestId)
        {
            //if (!IsAdmin() && !IsLeader())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            var approverId = GetClaim("userid");

            return await requestService.ApproveRequestForCancelMaterialOutsideWarehouse(requestId, approverId);
        }
    }
}
