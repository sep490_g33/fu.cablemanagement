﻿using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.RouteDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    /// <summary>
    /// BinhHD - Route Controller
    /// </summary>
    public class RouteController : BaseAPIController
    {
        private readonly IRouteService _routeService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="routeService"></param>
        public RouteController(IServiceProvider serviceProvider,
            IRouteService routeService) : base(serviceProvider)
        {
            _routeService = routeService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("route/create-route")]
        public async Task<BaseResponseDTO<bool>> CreateRoute([FromBody] CreateRouteDTO model)
        {
            return await _routeService.CreateRoute(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("route/update-route")]
        public async Task<BaseResponseDTO<bool>> UpdateRoute([FromBody] UpdateRouteDTO model)
        {
            return await _routeService.UpdateRoute(model);
        }

        [HttpDelete("route/delete-route/{routeId}")]
        public async Task<BaseResponseDTO<bool>> DeleteRoute([FromRoute] Guid routeId)
        {
            return await _routeService.DeleteRoute(routeId);
        }
        [HttpGet("route/get-all-route")]
        public async Task<BaseResponseDTO<PagedResultDTO<RouteDTO>>> GetAllRoute(string filter, int pageIndex = 1, int pageSize = 12)
        {
            var result = await _routeService.GetAllRoute(filter, pageIndex, pageSize);
            return result;
        }


        /// <summary>
        /// Import From Excel
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [DisableRequestSizeLimit]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        [HttpPost("route/import-excel-route")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<List<string>>> ImportRouteFromExcel(IFormFile file)
        {
            //if (!IsAdmin() && !IsStaff())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            return await _routeService.ImportRouteExcel(file);
        }
    }
}
