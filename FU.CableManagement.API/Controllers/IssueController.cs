﻿using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.IssueDTOs;
using FU.CableManagement.DataAccess.DTO.RequestDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class IssueController : BaseAPIController
    {
        private readonly IIssueService issueService;
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="issueService"></param>
        public IssueController(IServiceProvider serviceProvider, IIssueService issueService) : base(serviceProvider)
        {
            this.issueService = issueService;
        }
        /// <summary>
        /// Get Issue By Id
        /// </summary>
        /// <param name="issueId"></param>
        /// <returns></returns>
        [HttpGet("issue/get-issue-by-id")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<IssueDTO>> GetIssueById([FromQuery] Guid issueId) => await issueService.GetIssueById(issueId);

        /// <summary>
        /// Get All Issue With Filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [HttpGet("issue/get-all-issue")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<IssueDTO>>> GetAllIssue([FromQuery] string filter, int pageSize = 12, int pageIndex = 1) => await issueService.GetAllIssue(filter, pageSize, pageIndex);


        

        [HttpGet("issue/get-all-issue-not-resolved")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<IssueDTO>>> GetAllIssueNotResolved([FromQuery] string filter, int pageSize = 12, int pageIndex = 1) => await issueService.GetAllIssueNotResolved(filter, pageSize, pageIndex);

        /// Create Issue
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPost("issue/create-issue")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> CreateIssue([FromBody] IssueDTO model)
        {
            if (!IsAdmin() && !IsStaff())
            {
                throw new UnauthorizedAccessException();
            }
            var creatorId = GetClaim("userid");
            return await issueService.CreateIssue(model, creatorId);
        }

        /// <summary>
        /// Update Issue
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("issue/update-issue")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> UpdateIssue([FromBody] IssueDTO model)
        {
            if (!IsAdmin() && !IsStaff())
            {
                throw new UnauthorizedAccessException();
            }
            return await issueService.UpdateIssue(model);
        }
        /// <summary>
        /// Delete with given Id
        /// </summary>
        /// <param name="issueId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpDelete("issue/delete-issue")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> DeleteIssue([FromQuery] Guid issueId)
        {
            if (!IsAdmin() && !IsStaff())
            {
                throw new UnauthorizedAccessException();
            }
            return await issueService.DeleteIssue(issueId);
        }

        /// <summary>
        /// Import Issue from Excel
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [DisableRequestSizeLimit]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        [HttpPost("issue/import-excel-issue")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ImportExcelIssue(IFormFile file)
        {
            if (!IsAdmin() && !IsStaff())
            {
                throw new UnauthorizedAccessException();
            }
            var creatorId = GetClaim("userid");
            return await issueService.ImportExcelIssue(file, creatorId);
        }

        /// <summary>
        /// Get All Material In 1 Issue
        /// </summary>
        /// <param name="issueId"></param>
        /// <returns></returns>
        [HttpGet("issue/get-all-materials-by-issue-id")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<List<RequestDTO>>> GetAllMaterialByIssue([FromQuery] Guid issueId)
        {
            return await issueService.GetAllMaterialByIssue(issueId);
        }
    }
}
