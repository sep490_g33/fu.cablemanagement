﻿using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.Common.Enums;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace FU.CableManagement.API.Controllers
{

    public class BaseAPIController : ControllerBase
    {
        private readonly IServiceProvider _serviceProvider;

        public BaseAPIController(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public Guid UserId
        {
            get { return Guid.Parse(User.Claims.First(c => c.Type.ToLower() == "userid").Value); }
        }

        public string UserName
        {
            get { return User.Claims.First(c => c.Type.ToLower() == "username").Value; }
        }

        public string Email
        {
            get { return User.Claims.FirstOrDefault(c => c.Type.ToLower() == ClaimTypes.Email)?.Value; }
        }

        public List<string> GetListRole()
        {
            if (User == null)
            {
                return new List<string>();
            }
            var listRole = User.Claims.Where(c => c.Type.ToLower() == ClaimTypes.Role).Select(c => c.Value.ToLower()).ToList();
            return listRole;
        }

        public bool IsAdmin()
        {
            var listRole = GetListRole();
            return listRole != null && listRole.Contains(UserRoleConstant.Admin);
        }
        public bool IsStaff()
        {
            var listRole = GetListRole();
            return listRole != null && listRole.Contains(UserRoleConstant.Staff);
        }

        public bool IsLeader()
        {
            var listRole = GetListRole();
            return listRole != null && listRole.Contains(UserRoleConstant.Leader);
        }

        public bool IsLeaderOrStaff()
        {
            var listRole = GetListRole();
            return listRole != null && (listRole.Contains(UserRoleConstant.Leader) || listRole.Contains(UserRoleConstant.Staff));
        }

        public bool IsWarehouseKeeper()
        {
            var listRole = GetListRole();
            return listRole != null && (listRole.Contains(UserRoleConstant.WarehouseKeeper));
        }

        public string GetClaim(string type)
        {
            return User.Claims.Where(c => c.Type.ToLower() == type.ToLower()).Select(c => c.Value).FirstOrDefault();
        }

    }
}
