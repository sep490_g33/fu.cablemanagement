﻿using FU.CableManagement.API.Models.UserModels;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.API.Utils;
using FU.CableManagement.DataAccess.Common.Enums;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.EmailDTOs;
using FU.CableManagement.DataAccess.DTO.UserDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    public class UserController : BaseAPIController
    {
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;
        public UserController(IServiceProvider serviceProvider,
            IUserService userService,
            IEmailService emailService) : base(serviceProvider)
        {
            _userService = userService;
            _emailService = emailService;
        }
        /// <summary>
        /// Get Access Token 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("user/get-accesstoken")]
        public async Task<BaseResponseDTO<TokenResponse>> GetAccessToken([FromBody][Required] UserLoginModel model)
        {
            return await _userService.Get_AccessToken(model);
        }
        /// <summary>
        ///  Get List User With Filter 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpGet]
        [Route("user/get-listaccount")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<UserDTO>>> GetListUser([FromQuery] string filter, int pageSize = 12, int pageIndex = 1)
        {
            if (!IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            return await _userService.GetUsers(filter, pageSize, pageIndex);
        }
        /// <summary>
        /// Update Profile User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut]
        [Route("user/update-account")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> UpdateUser([FromBody] UpdateUserDTO model)
        {
            if (!IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            return await _userService.UpdateUser(model);
        }
        /// <summary>
        /// Delete User
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpDelete]
        [Route("user/delete-account/{userId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> DeleteUser([FromRoute] Guid userId)
        {
            if (!IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            return await _userService.DeleteUser(userId);
        }
        /// <summary>
        /// Register Account
        /// </summary>
        /// <param name="newUser"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        [HttpPost]
        [Route("user/register-account")]
        public async Task<BaseResponseDTO<bool>> RegisterUser([FromBody] RegisterUserDTO newUser)
        {
            try
            {
                string tmpPw = UserUtils.RandomPasword(8);
                string newPw = UserUtils.HashPassword(tmpPw);
                EmailDTO emailDTO = new EmailDTO();
                emailDTO.Subject = "Welcome to Cable Management System";
                emailDTO.Body = UserUtils.BodyEmailForNewUser(tmpPw);
                emailDTO.To = newUser.Email;
                await _emailService.sendEmail(emailDTO);
                User account = new User()
                {
                    UserId = Guid.NewGuid(),
                    Username = newUser.UserName,
                    Lastname = newUser.Lastname,
                    Firstname = newUser.Firstname,
                    Email = newUser.Email,
                    Password = newPw,
                    Phone = newUser.Phone,
                    RoleId = (int)RoleUserEnum.Staff,
                    CreatedDate = DateTime.Now,
                    IsDeleted = false
                };
                return await _userService.RegisterUser(account);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="userLogin"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("user/login")]
        public async Task<BaseResponseDTO<TokenResponse>> Login([FromBody] UserLoginModel userLogin)
        {
            var hashPw = UserUtils.HashPassword(userLogin.Password);
            userLogin.Password = hashPw;
            var user = await _userService.GetUser(userLogin.Username, hashPw);
            if (user != null)
            {
                return await _userService.Get_AccessToken(userLogin);
            }
            return new BaseResponseDTO<TokenResponse>("Username or password wrong", StatusCodes.Status401Unauthorized);
        }

        /// <summary>
        /// Forget Password Send Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        //[HttpGet]
        //[Route("account/logout")]
        //public async Task<BaseResponseDTO<bool>> Logout()
        //{
        //    var authorizationHeader = Request.Headers["Authorization"];
        //}
        [HttpPost("user/forget-password")]
        public async Task<BaseResponseDTO<bool>> ForgetPassword([FromQuery] string email)
        {
            try
            {
                string tmpPw = UserUtils.RandomPasword(8);
                string newPw = UserUtils.HashPassword(tmpPw);
                EmailDTO emailDTO = new EmailDTO();
                emailDTO.Subject = "Welcome to Cable Management System";
                emailDTO.Body = UserUtils.BodyEmailForForgetPassword(tmpPw);
                emailDTO.To = email;
                await _emailService.sendEmail(emailDTO);
                await _userService.ChangePassword(email, newPw);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true, "Email sent successfully", StatusCodes.Status200OK);
        }

        /// <summary>
        /// Import User From Excel
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [DisableRequestSizeLimit]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        [HttpPost("user/import-excel-user")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ImportExcelUser(IFormFile file)
        {
            if (!IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            return await _userService.ImportExcelUser(file);
        }

        /// <summary>
        /// change password for user
        /// </summary>
        /// <param name="newPw"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("user/change-password")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ChangePassword([FromBody] string newPw)
        {
            var email = GetClaim(ClaimTypes.Email);
            string tmpPw = UserUtils.HashPassword(newPw);
            return await _userService.ChangePassword(email,tmpPw);
        }
    }
}
