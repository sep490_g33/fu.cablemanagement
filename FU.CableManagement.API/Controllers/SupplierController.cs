﻿using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.SupplierDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    //[Route("[controller]")]
    public class SupplierController : BaseAPIController
    {
        private readonly ISupplierService supplierService;
        public SupplierController(IServiceProvider serviceProvider, ISupplierService supplierService) : base(serviceProvider)
        {
            this.supplierService = supplierService;
        }

        /// <summary>
        /// Get Supplier with id
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        [HttpGet("supplier/get-supplier-by-id")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<SupplierDTO>> GetSupplierById([FromQuery] int supplierId) => await supplierService.GetSupplierById(supplierId);

        /// <summary>
        /// Get All Supplier with filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [HttpGet("supplier/get-all-supplier")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<SupplierDTO>>> GetAllSupplier([FromQuery] string filter, int pageSize = 12, int pageIndex = 1) => await supplierService.GetAllSupplier(filter, pageSize, pageIndex);

        /// <summary>
        /// Create Supplier
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPost("supplier/create-supplier")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> CreateSupplier([FromBody] SupplierDTO model)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            var creatorId = GetClaim("userid");
            return await supplierService.CreateSupplier(model, creatorId);
        }

        /// <summary>
        /// Update Supplier
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("supplier/update-supplier")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> UpdateSupplier([FromBody] SupplierDTO model)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await supplierService.UpdateSupplier(model);
        }
        /// <summary>
        /// Delete Supplier
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpDelete("supplier/delete-supplier")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> DeleteSupplier([FromQuery] int supplierId)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await supplierService.DeleteSupplier(supplierId);
        }

        /// <summary>
        /// Import Supplier From Excel
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [DisableRequestSizeLimit]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        [HttpPost("supplier/import-supplier")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ImportSuppliers(IFormFile file)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            var creatorId = GetClaim("userid");
            return await supplierService.ImportSupplier(file, creatorId);
        }

        /// <summary>
        /// Export Supplier Data
        /// </summary>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpGet("supplier/export-supplier")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<FileResponseDTO>> ExportSuppliers()
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await supplierService.ExportSupplier();
        }
    }
}
