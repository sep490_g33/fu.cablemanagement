﻿using FU.CableManagement.API.Models.NodeModel;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.NodeDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class NodeController : BaseAPIController
    {
        private readonly INodeService nodeService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="nodeService"></param>
        public NodeController(IServiceProvider serviceProvider, INodeService nodeService) : base(serviceProvider)
        {
            this.nodeService = nodeService;
        }
        /// <summary>
        /// Get node By Id
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        [HttpGet("node/get-node-by-id")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<NodeDTO>> GetNodeById([FromQuery] Guid nodeId) => await nodeService.GetNodeById(nodeId);
        /// <summary>
        ///  Get All Nodes With Filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [HttpGet("node/get-all-node")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<NodeDTO>>> GetAllNodes([FromQuery] string filter, Guid? RouteId, int pageSize = 12, int pageIndex = 1) => await nodeService.GetAllNodes(filter, RouteId, pageSize, pageIndex);

        /// <summary>
        /// Create Node
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPost("node/create-node")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> CreateNode([FromBody] CreateNodeDTO model)
        {
            if (!IsStaff() && !IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            return await nodeService.CreateNode(model);
        }

        /// <summary>
        /// Update Node
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("node/update-node")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> UpdateNode([FromBody] UpdateNodeDTO model)
        {
            if (!IsStaff() && !IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            return await nodeService.UpdateNode(model);
        }

        /// <summary>
        /// Delete Node
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpDelete("node/delete-node")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> DeleteNode([FromQuery] Guid nodeId)
        {
            if (!IsStaff() && !IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            return await nodeService.DeleteNode(nodeId);
        }

        /// <summary>
        /// Import Node from Excel
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [DisableRequestSizeLimit]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        [HttpPost("node/import-node")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ImportNodes(IFormFile file)
        {
            if (!IsStaff() && !IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            return await nodeService.ImportNodes(file);
        }

        /// <summary>
        /// Add Cable Route to Node
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPost("node/add-update-node-cable")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> AddOrUpdateNodeCable([FromBody] AddOrUpdateNodeCable model)
        {
            if (!IsStaff() && !IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            return await nodeService.AddOrUpdateNodeCable(model);
        }

        /// <summary>
        /// Add Material to Node
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPost("node/add-update-node-material")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> AddOrUpdateNodeMaterial([FromBody] AddOrUpdateNodeMaterial model)
        {
            if (!IsStaff() && !IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            return await nodeService.AddOrUpdateNodeMaterial(model);
        }

        [HttpPost("node/add-update-node-materialcategory")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> AddOrUpdateNodeMaterialCategory([FromBody] AddOrUpdateNodeMaterialCategory model)
        {
            //if (!IsStaff() && !IsAdmin())
            //{
            //    throw new UnauthorizedAccessException();
            //}
            return await nodeService.AddOrUpdateNodeMaterialCategory(model);
        }


        //[HttpGet("node/export-node")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //public async Task<BaseResponseDTO<FileResponseDTO>> Exportnodes()
        //{
        //    if (!IsAdmin())
        //    {
        //        throw new UnauthorizedAccessException();
        //    }
        //    return await nodeService.Exportnode();
        //}

    }
}
