﻿using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.TransactionDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    public class TransactionController : BaseAPIController
    {
        private readonly ITransactionService _transactionService;
        public TransactionController(IServiceProvider serviceProvider, ITransactionService transactionService) : base(serviceProvider)
        {
            _transactionService = transactionService;
        }
        /// <summary>
        /// Get all Transactions with filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="warehouseId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpGet("transaction/get-all-transactions")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<TransactionDTO>>> GetAllTransactions([FromQuery] string filter, int pageSize = 12, int pageIndex = 1,
            int? warehouseId = null)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await _transactionService.GetAllTransactions(filter, pageSize, pageIndex, warehouseId);
        }

        /// <summary>
        /// Get Transaction with id
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpGet("transaction/get-transaction-by-id")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<TransactionDTO>> GetTransactionById(Guid transactionId)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await _transactionService.GetTransactionById(transactionId);
        }

    }
}
