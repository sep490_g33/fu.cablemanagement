﻿using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.API.Services.Services;
using FU.CableManagement.DataAccess.DTO.CableDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialsDTOs;
using FU.CableManagement.DataAccess.DTO.SupplierDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class OtherMaterialsController : BaseAPIController
    {
        private readonly IOtherMaterialsService _otherMaterialsService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="otherMaterialsService"></param>
        public OtherMaterialsController(IServiceProvider serviceProvider, IOtherMaterialsService otherMaterialsService) : base(serviceProvider)
        {
            _otherMaterialsService = otherMaterialsService;
        }

        /// <summary>
        /// Get Material With Id
        /// </summary>
        /// <param name="otherMaterialId"></param>
        /// <returns></returns>
        [HttpGet("other-materials/get-other-material-by-id")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<OtherMaterialDTO>> GetMaterialsById([FromQuery] int otherMaterialId)
        {
            return await _otherMaterialsService.GetOtherMaterialById(otherMaterialId);
        }

        /// <summary>
        /// Get All Material With filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="warehouseId"></param>
        /// <returns></returns>
        [HttpGet("other-materials/get-all-other-materials")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<OtherMaterialDTO>>> GetAllMaterials([FromQuery] string filter, int pageSize = 12
            , int pageIndex = 1, int? warehouseId = null)
        {
            return await _otherMaterialsService.GetAllOtherMaterials(filter, pageSize, pageIndex, warehouseId);
        }

        /// <summary>
        ///  Delete Material
        /// </summary>
        /// <param name="otherMaterialId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpDelete("other-materials/delete-other-material")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> DeleteMaterialsById([FromQuery] int otherMaterialId)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await _otherMaterialsService.DeleteOtherMaterial(otherMaterialId);
        }

        /// <summary>
        /// Update Material
        /// </summary>
        /// <param name="otherMaterialDTO"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("other-materials/update-other-material")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> UpdateMaterialsById([FromBody] OtherMaterialUpdateDTO otherMaterialDTO)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await _otherMaterialsService.UpdateOtherMaterial(otherMaterialDTO);
        }

        /// <summary>
        /// Add Material
        /// </summary>
        /// <param name="otherMaterialDTO"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPost("other-materials/add-other-material")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> AddMaterials([FromBody] OtherMaterialAddDTO otherMaterialDTO)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await _otherMaterialsService.AddOtherMaterial(otherMaterialDTO);
        }

        /// <summary>
        /// Import Material From Excel
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [DisableRequestSizeLimit]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        [HttpPost("other-materials/import-excel-material")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ImportExcelMaterials(IFormFile file)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await _otherMaterialsService.ImportExcelMaterials(file);
        }

        /// <summary>
        /// Warning For import More Material
        /// </summary>
        /// <param name="materialId"></param>
        /// <returns></returns>
        [HttpGet("other-materials/warning-for-import-more-material")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<string>> WarningForImportMoreMaterial([FromQuery] int materialId)
        {
            return await _otherMaterialsService.WarningForImportMoreMaterial(materialId);
        }
    }
}
