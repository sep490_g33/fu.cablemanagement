﻿using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.RequestCategoryDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    public class RequestCategoryController : BaseAPIController
    {
        private readonly IRequestCategoryService _requestCategoryService;
        public RequestCategoryController(IServiceProvider serviceProvider, IRequestCategoryService requestCategoryService) : base(serviceProvider)
        {
            _requestCategoryService = requestCategoryService;
        }
        /// <summary>
        /// Get All Request Category With Filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [HttpGet("request-category/get-all-request-category")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<RequestCategoryDTO>>> GetAllRequestCategory([FromQuery] string filter, int pageSize = 12,
            int pageIndex = 1)
        {
            return await _requestCategoryService.GetAllRequestCategory(filter, pageSize, pageIndex);
        }

        /// <summary>
        /// Get Request Category With Id
        /// </summary>
        /// <param name="requestCategoryId"></param>
        /// <returns></returns>
        [HttpGet("request-category/get-request-category-by-id")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<RequestCategoryDTO>> GetRequestCategoryById([FromQuery] int requestCategoryId)
        {
            return await _requestCategoryService.GetRequestCategoryById(requestCategoryId);
        }

        //[HttpGet("request-category/delete-request-category")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //public async Task<BaseResponseDTO<bool>> DeleteRequestCategory([FromQuery] int requestCategoryId)
        //{
        //    return await _requestCategoryService.DeleteRequestCategory(requestCategoryId);
        //}

        //[HttpPost("request-category/add-request-category")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //public async Task<BaseResponseDTO<bool>> AddRequestCategory([FromBody] RequestCategoryDTO requestCategoryDTO)
        //{
        //    if (!IsAdmin())
        //    {
        //        throw new UnauthorizedAccessException();
        //    }
        //    return await _requestCategoryService.AddRequestCategory(requestCategoryDTO);
        //}

        //[HttpPut("request-category/update-request-category")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //public async Task<BaseResponseDTO<bool>> UpdateRequestCategory([FromBody] RequestCategoryDTO requestCategoryDTO)
        //{
        //    if (!IsAdmin())
        //    {
        //        throw new UnauthorizedAccessException();
        //    }
        //    return await _requestCategoryService.UpdateRequestCategory(requestCategoryDTO);
        //}
    }
}
