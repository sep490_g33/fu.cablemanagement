﻿using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.API.Services.Services;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialCategoryDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialsDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class OtherMaterialCategoryController : BaseAPIController
    {
        private readonly IOtherMaterialCateogryService otherMaterialCateogryService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="otherMaterialCateogryService"></param>
        public OtherMaterialCategoryController(IServiceProvider serviceProvider, IOtherMaterialCateogryService otherMaterialCateogryService) : base(serviceProvider)
        {
            this.otherMaterialCateogryService = otherMaterialCateogryService;
        }

        /// <summary>
        /// Get Material Category With Id
        /// </summary>
        /// <param name="otherMaterialCategoryId"></param>
        /// <returns></returns>
        [HttpGet("other-material-categories/get-other-material-category-by-id")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<OtherMaterialCategoryDTO>> GetMaterialCategoryById([FromQuery] int otherMaterialCategoryId)
        {
            return await otherMaterialCateogryService.GetOtherMaterialCategoryById(otherMaterialCategoryId);
        }

        /// <summary>
        /// Get all Material Category With Filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [HttpGet("other-material-categories/get-all-other-material-categories")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<OtherMaterialCategoryDTO>>> GetAllMaterialCategories([FromQuery] string filter, int pageSize = 12, int pageIndex = 1)
        {
            return await otherMaterialCateogryService.GetAllOtherMaterialCategories(filter, pageSize, pageIndex);
        }

        /// <summary>
        /// Add Material Category
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPost("other-material-categories/add-other-material-category")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> AddMaterialCategory([FromBody] OtherMaterialCategoryDTO model)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await otherMaterialCateogryService.AddOtherMaterialCategory(model);
        }

        /// <summary>
        /// Update Material Category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("other-material-categories/update-other-material-category")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> UpdateMaterialCategory([FromBody] OtherMaterialCategoryDTO model)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await otherMaterialCateogryService.UpdateOtherMaterialCategory(model);
        }

        /// <summary>
        /// Delete Material Category
        /// </summary>
        /// <param name="otherMaterialCategoryId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpDelete("other-material-categories/delete-other-material-category")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> DeleteMaterialCategory([FromQuery] int otherMaterialCategoryId)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await otherMaterialCateogryService.DeleteOtherMaterialCategory(otherMaterialCategoryId);
        }

        //[DisableRequestSizeLimit]
        //[RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        //[HttpPost("other-material-categories/import-excel-other-material-categories")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //public async Task<BaseResponseDTO<bool>> ImportExcelMaterialCategories(IFormFile file)
        //{
        //    if (!IsAdmin() && !IsWarehouseKeeper())
        //    {
        //        throw new UnauthorizedAccessException();
        //    }
        //    return await otherMaterialCateogryService.ImportExcelOtherMaterialCategories(file);
        //}
    }
}
