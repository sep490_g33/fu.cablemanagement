﻿using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.WareHouseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    public class WareHouseController : BaseAPIController
    {
        private readonly IWareHouseService wareHouseService;
        public WareHouseController(IServiceProvider serviceProvider, IWareHouseService wareHouseService) : base(serviceProvider)
        {
            this.wareHouseService = wareHouseService;
        }
        /// <summary>
        /// Get warehouse by id
        /// </summary>
        /// <param name="wareHouseId"></param>
        /// <returns></returns>
        [HttpGet("warehouse/get-warehouse-by-id")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<WareHouseDTO>> GetWareHouseById([FromQuery] int wareHouseId) => await wareHouseService.GetWareHouseById(wareHouseId);


        /// <summary>
        /// Get all warehouse with filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [HttpGet("warehouse/get-all-warehouse")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<WareHouseDTO>>> GetAllWareHouse([FromQuery] string filter, int pageSize = 12, int pageIndex = 1) => await wareHouseService.GetAllWareHouse(filter, pageSize, pageIndex);

        /// <summary>
        /// create warehouse
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPost("warehouse/create-warehouse")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> CreateWareHouse([FromBody] WareHouseDTO model)
        {
            if (!IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            var creatorId = GetClaim("userid");
            return await wareHouseService.CreateWareHouse(model, creatorId);
        }

        /// <summary>
        /// update warehouse
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("warehouse/update-warehouse")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> UpdateWareHouse([FromBody] WareHouseDTO model)
        {
            if (!IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            return await wareHouseService.UpdateWareHouse(model);
        }
        /// <summary>
        /// delete warehouse
        /// </summary>
        /// <param name="wareHouseId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpDelete("warehouse/delete-warehouse")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> DeleteWareHouse([FromQuery] int wareHouseId)
        {
            if (!IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            return await wareHouseService.DeleteWareHouse(wareHouseId);
        }
        /// <summary>
        /// import warehouse from excel
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [DisableRequestSizeLimit]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        [HttpPost("warehouse/import-excel-warehouse")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> ImportExcelWarehouse(IFormFile file)
        {
            if (!IsAdmin())
            {
                throw new UnauthorizedAccessException();
            }
            var creatorId = GetClaim("userid");
            return await wareHouseService.ImportExcelWarehouse(file, creatorId);
        }
    }
}
