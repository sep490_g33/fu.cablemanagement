﻿using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.DTO.CableCategoryDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    public class CableCategoryController : BaseAPIController
    {
        private readonly ICableCategoryService cableCategoryService;
        public CableCategoryController(IServiceProvider serviceProvider, ICableCategoryService cableCategoryService) : base(serviceProvider)
        {
            this.cableCategoryService = cableCategoryService;
        }
        /// <summary>
        /// get cable category with specific cable category id
        /// </summary>
        /// <param name="cableCategoryId">cable category</param>
        /// <returns>cablecategory with id with param or exception</returns>
        [HttpGet("cable-categories/get-cable-category-by-id")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<CableCategoryDTO>> GetCableCategoryById([FromQuery] int cableCategoryId)
        {
            return await cableCategoryService.GetCableCategoryById(cableCategoryId);
        }

        /// <summary>
        /// Get All Cable Category with filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [HttpGet("cable-categories/get-all-cable-categories")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<PagedResultDTO<CableCategoryDTO>>> GetAllCableCategories([FromQuery] string filter, int pageSize = 12, int pageIndex = 1)
        {
            return await cableCategoryService.GetCableCategories(filter, pageSize, pageIndex);
        }

        /// <summary>
        /// Add Cable Category
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPost("cable-categories/add-cable-category")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> AddCableCategory([FromBody] CableCategoryDTO model)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await cableCategoryService.AddCableCategory(model);
        }

        /// <summary>
        /// Update Cable Category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpPut("cable-categories/update-cable-category")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> UpdateCableCategory([FromBody] CableCategoryDTO model)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await cableCategoryService.UpdateCableCategory(model);
        }

        /// <summary>
        /// Delete Cable Category With Id
        /// </summary>
        /// <param name="cableCategoryId"></param>
        /// <returns></returns>
        /// <exception cref="UnauthorizedAccessException"></exception>
        [HttpDelete("cable-categories/delete-cable-category")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<BaseResponseDTO<bool>> DeleteCableCategory([FromQuery] int cableCategoryId)
        {
            if (!IsAdmin() && !IsWarehouseKeeper())
            {
                throw new UnauthorizedAccessException();
            }
            return await cableCategoryService.DeleteCableCategory(cableCategoryId);
        }

        //[DisableRequestSizeLimit]
        //[RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        //[HttpPost("cable-categories/import-excel-cable-categories")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //public async Task<BaseResponseDTO<bool>> ImportExcelCableCateogories(IFormFile file)
        //{
        //    if (!IsAdmin() && !IsWarehouseKeeper())
        //    {
        //        throw new UnauthorizedAccessException();
        //    }
        //    return await cableCategoryService.ImportExcelCableCategories(file);
        //}
    }
}
