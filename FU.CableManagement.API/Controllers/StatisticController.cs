﻿using FU.CableManagement.API.Services.IServices;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Controllers
{
    public class StatisticController : BaseAPIController
    {
        private readonly IStatisticService _statisticService;
        public StatisticController(IServiceProvider serviceProvider, IStatisticService statisticService) : base(serviceProvider)
        {
            _statisticService = statisticService;
        }
        //[HttpGet("statistic/exchange-material")]
        //public async Task<IActionResult> GetExchangeMaterial(int? warehouseId, DateTime? startDate, DateTime? endDate)
        //{
        //    return Ok(await _statisticService.GetExchangeMaterial(warehouseId, startDate, endDate));
        //}

        //[HttpGet("statistic/material-fluctuation")]
        //public async Task<IActionResult> GetMaterialFluctuation(string materialOrCableName, DateTime? startDate, DateTime? endDate)
        //{
        //    return Ok(await _statisticService.GetMaterialFluctuation(materialOrCableName, startDate, endDate));
        //}
        /// <summary>
        /// Statistic Report Issue In Time
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet("statistic/issue-statistic")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetIssueStatistic(DateTime? startDate, DateTime? endDate)
        {
            return Ok(await _statisticService.GetStatisticOfIssues(startDate, endDate));
        }

        /// <summary>
        /// Statistic For Material Fluctuation Per Year
        /// </summary>
        /// <param name="materialCategoryId"></param>
        /// <param name="warehouseId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("statistic/material-fluctuation-per-year")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetMaterialFluctuationPerYear(int? materialCategoryId, int? warehouseId, int? year)
        {
            return Ok(await _statisticService.GetMaterialFluctuationPerYear(materialCategoryId, warehouseId, year));
        }

        /// <summary>
        /// Statistic for cable Fluctuation Per Year
        /// </summary>
        /// <param name="cableCategoryId"></param>
        /// <param name="warehouseId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("statistic/cable-fluctuation-per-year")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetCableFluctuationPerYear(int? cableCategoryId, int? warehouseId, int? year)
        {
            return Ok(await _statisticService.GetCableFluctuationPerYear(cableCategoryId, warehouseId, year));
        }

        [HttpGet("statistic/cable-category-statistic-in-warehouse")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetCableCategoryStatisticInWarehouse(int? warehouseId)
        {
            return Ok(await _statisticService.GetCableCategoryStatistic(warehouseId));
        }
        [HttpGet("statistic/other-material-category-statistic-in-warehouse")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetMaterialCategoryStatisticInWarehouse(int? warehouseId)
        {
            return Ok(await _statisticService.GetOtherMaterialCategoryStatistic(warehouseId));
        }

        [HttpGet("statistic/route-statistic")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetMaterialInRoute(Guid RouteId)

        {
            return Ok(await _statisticService.GetMaterialInRoute(RouteId));
        }
    }
}
