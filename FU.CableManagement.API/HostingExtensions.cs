﻿using FU.CableManagement.API.Infra.ErrorHandling;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.API.Services.Services;
using FU.CableManagement.DataAccess.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;

namespace FU.CableManagement.API
{
    public static class HostingExtensions
    {
        private const string MyAllowSpecificOrigins = "AllowOrigin";
        public static void ConfigureServices(this WebApplicationBuilder builder)
        {
            builder.Host.UseSerilog((hostContext, services, configuration) => { configuration.ReadFrom.Configuration(hostContext.Configuration); });
            string connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
            var migrationsAssembly = typeof(Program).Assembly.GetName().Name;

            // config services
            builder.Services.AddScoped<IUserService, UserService>();
            builder.Services.AddScoped<ISupplierService, SupplierService>();
            builder.Services.AddScoped<IWareHouseService, WareHouseService>();
            builder.Services.AddScoped<IEmailService, EmailService>();
            builder.Services.AddScoped<ICableService, CableService>();
            builder.Services.AddScoped<IIssueService, IssueService>();
            builder.Services.AddScoped<IOtherMaterialsService, OtherMaterialsService>();
            builder.Services.AddScoped<IRequestService, RequestService>();
            builder.Services.AddScoped<INodeService, NodeService>();
            builder.Services.AddScoped<IStatisticService, StatisticService>();
            builder.Services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
            builder.Services.AddScoped<ITransactionService, TransactionService>();
            builder.Services.AddScoped<IRequestCategoryService, RequestCategoryService>();
            builder.Services.AddScoped<IOtherMaterialCateogryService, OtherMaterialCateogryService>();
            builder.Services.AddScoped<ICableCategoryService, CableCategoryService>();
            builder.Services.AddScoped<IRouteService, RouteService>();
            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            // config database

            builder.Services.AddDbContext<CableManagementContext>(options =>
            {
                options.UseSqlServer(connectionString);
            });


            builder.Services.AddCors(o => o.AddPolicy("AllowOrigin", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = builder.Configuration["Jwt:Issuer"],
                    ValidAudience = builder.Configuration["Jwt:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"]))
                });
            builder.Services.AddSwaggerGen(c =>
            {
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            //    .AddCookie(options =>
            //    {
            //        options.LoginPath = "/Account/Unauthorized/";
            //        options.AccessDeniedPath = "/Account/Forbidden/";
            //    })
            //    .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
            //    {
            //        options.Audience = "http://localhost:5001/";
            //        options.Authority = "http://localhost:5000/";
            //    });
        }


        public static WebApplication ConfigurePipeline(this WebApplication app)
        {
            app.UseSerilogRequestLogging();

            InitializeDatabase(app);

            if (app.Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //InitializeDatabase(app);
            app.UseSwagger();
            app.UseSwaggerUI();

            app.UseStaticFiles();
            app.UseRouting();
            app.UseCors(MyAllowSpecificOrigins);
            app.ConfigureExceptionHandler();

            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


            app.MapControllers();
            return app;
        }

        private async static void InitializeDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var utcNow = DateTime.UtcNow;
                var context = serviceScope.ServiceProvider.GetService<CableManagementContext>();
                await context.Database.MigrateAsync();
                await SeedData.Seed(context);
            }

        }
    }
}
