﻿using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialsDTOs;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    public interface IOtherMaterialsService
    {
        Task<BaseResponseDTO<PagedResultDTO<OtherMaterialDTO>>> GetAllOtherMaterials(string filter, int pageSize, int pageIndex, int? warehouseId);

        Task<BaseResponseDTO<OtherMaterialDTO>> GetOtherMaterialById(int otherMaterialId);
        Task<BaseResponseDTO<bool>> UpdateOtherMaterial(OtherMaterialUpdateDTO model);
        Task<BaseResponseDTO<bool>> DeleteOtherMaterial(int otherMaterialId);
        Task<BaseResponseDTO<bool>> AddOtherMaterial(OtherMaterialAddDTO model);
        Task<BaseResponseDTO<bool>> ImportExcelMaterials(IFormFile file);
        Task<BaseResponseDTO<string>> WarningForImportMoreMaterial(int materialId);
        Task<List<OtherMaterial>> ImportExcelMaterialsForImportRequest(IFormFile file);
    }
}
