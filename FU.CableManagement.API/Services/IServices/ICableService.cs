﻿using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CableDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    public interface ICableService
    {
        Task<BaseResponseDTO<PagedResultDTO<CableDTO>>> GetAllCables(string filter, int pageSize, int pageIndex, int? warehouseId,
            bool? isExportedToUse);

        Task<BaseResponseDTO<bool>> AddCable(CableImportDTO model, string creatorId);
        Task<BaseResponseDTO<bool>> UpdateCable(CableUpdateDTO model);
        Task<BaseResponseDTO<CableDTO>> GetCableById(Guid cableId);
        Task<BaseResponseDTO<bool>> DeleteCable(Guid cableId);
        Task<List<Cable>> CutCable(CutCableDTO cutCableDTO);
        Task<BaseResponseDTO<PagedResultDTO<CableDTO>>> GetCablesInAWarehouse(int warehouseId, int pageSize, int pageIndex);
        Task<BaseResponseDTO<bool>> ImportExcelCables(IFormFile file, string creatorId);
        Task<List<Cable>> ImportExcelCablesInImportRequest(IFormFile file, string creatorId);
    }
}
