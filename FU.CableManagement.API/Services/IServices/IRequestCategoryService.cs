﻿using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.RequestCategoryDTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    public interface IRequestCategoryService
    {
        Task<BaseResponseDTO<PagedResultDTO<RequestCategoryDTO>>> GetAllRequestCategory(string filter, int pageSize,
            int pageIndex);
        Task<BaseResponseDTO<bool>> AddRequestCategory(RequestCategoryDTO requestCategoryDTO);
        Task<BaseResponseDTO<bool>> UpdateRequestCategory(RequestCategoryDTO requestCategoryDTO);
        Task<BaseResponseDTO<bool>> DeleteRequestCategory(int requestCategoryid);
        Task<BaseResponseDTO<RequestCategoryDTO>> GetRequestCategoryById(int requestCategoryId);
    }
}
