﻿using FU.CableManagement.DataAccess.DTO.CableDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.RequestDTOs;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    public interface IRequestService
    {
        Task<BaseResponseDTO<PagedResultDTO<RequestDTO>>> GetAllRequest(string filter, int pageSize, int pageIndex, Guid? issueId);
        Task<BaseResponseDTO<PagedResultDTO<RequestDTO>>> GetRequestByStaff(string filter, int pageSize, int pageIndex, string staffId, Guid? issueId);
        Task<BaseResponseDTO<RequestDTO>> GetRequestById(Guid requestId);
        Task<BaseResponseDTO<bool>> CreateRequest(RequestCreateDTO requestCreateDTO, string creatorId);
        Task<BaseResponseDTO<bool>> UpdateRequest(RequestUpdateDTO model);
        Task<BaseResponseDTO<bool>> DeleteRequest(Guid requestId);
        Task<BaseResponseDTO<bool>> ApproveRequestExport(Guid requestId, string approverId);
        Task<BaseResponseDTO<bool>> RejectRequest(Guid requestId, string rejectorId);
        Task<BaseResponseDTO<List<RequestCableDTO>>> GetAllCablesByIssue(Guid issuedId);
        Task<BaseResponseDTO<bool>> ApproveRequestForDeliver(Guid requestId, string approverId);
        //Task<BaseResponseDTO<bool>> ApproveRequestForRecovery(Guid requestId, string approverId);

        Task<BaseResponseDTO<List<CableDTO>>> SuggestCable(SuggestCableRequestDTO request);

        Task<BaseResponseDTO<bool>> ApproveRequestForCancelInWarehouse(Guid requestId, string approverId);
        Task<BaseResponseDTO<bool>> ApproveRequestForImport(Guid requestId, string approverId);
        Task<BaseResponseDTO<bool>> CreateRequestForImport(RequestCreateDTO requestCreateDTO, string creatorId, IFormFile? cableFile,
            IFormFile? otherMaterialFile);
        Task<BaseResponseDTO<bool>> CreateRequestForRecovery(RequestCreateDTO requestCreateDTO, string creatorId);
        Task<BaseResponseDTO<bool>> ApproveRequestForRecovery(Guid requestId, string approverId);
        Task<BaseResponseDTO<bool>> CreateRequestForCancelMaterialOutsideWarehouse(RequestCreateDTO requestCreateDTO, string creatorId);
        Task<BaseResponseDTO<bool>> ApproveRequestForCancelMaterialOutsideWarehouse(Guid requestId, string approverId);

    }
}
