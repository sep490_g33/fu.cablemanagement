﻿using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.IssueDTOs;
using FU.CableManagement.DataAccess.DTO.RequestDTOs;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    public interface IIssueService
    {
        Task<BaseResponseDTO<bool>> CreateIssue(IssueDTO model, string creatorId);
        Task<BaseResponseDTO<bool>> UpdateIssue(IssueDTO model);
        Task<BaseResponseDTO<IssueDTO>> GetIssueById(Guid issueId);
        Task<BaseResponseDTO<bool>> DeleteIssue(Guid issueId);
        Task<BaseResponseDTO<PagedResultDTO<IssueDTO>>> GetAllIssue(string filter, int pageSize, int pageIndex);
        Task<BaseResponseDTO<PagedResultDTO<IssueDTO>>> GetAllIssueNotResolved(string filter, int pageSize, int pageIndex);
        Task<BaseResponseDTO<bool>> ImportExcelIssue(IFormFile file, string creatorId);
        Task<BaseResponseDTO<List<RequestDTO>>> GetAllMaterialByIssue(Guid issueId);
    }
}
