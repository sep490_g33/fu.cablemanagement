﻿using FU.CableManagement.API.Models.NodeModel;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.NodeDTOs;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface INodeService
    {
        Task<BaseResponseDTO<PagedResultDTO<NodeDTO>>> GetAllNodes(string filter, Guid? RouteId, int pageSize, int pageIndex);
        Task<BaseResponseDTO<bool>> CreateNode(CreateNodeDTO model);
        Task<BaseResponseDTO<bool>> UpdateNode(UpdateNodeDTO model);
        Task<BaseResponseDTO<NodeDTO>> GetNodeById(Guid NodeId);
        Task<BaseResponseDTO<bool>> DeleteNode(Guid NodeId);
        Task<BaseResponseDTO<bool>> ImportNodes(IFormFile file);
        Task<BaseResponseDTO<bool>> AddOrUpdateNodeCable(AddOrUpdateNodeCable model);
        Task<BaseResponseDTO<bool>> AddOrUpdateNodeMaterial(AddOrUpdateNodeMaterial model);
        Task<BaseResponseDTO<bool>> AddOrUpdateNodeMaterialCategory(AddOrUpdateNodeMaterialCategory model);
        Task<BaseResponseDTO<bool>> AddOrUpdateNodeMaterialCategoryMany(List<AddOrUpdateNodeMaterialCategory> models);
    }
}
