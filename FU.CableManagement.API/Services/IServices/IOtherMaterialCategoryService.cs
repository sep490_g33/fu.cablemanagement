﻿using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialCategoryDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialsDTOs;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    public interface IOtherMaterialCateogryService
    {
        Task<BaseResponseDTO<PagedResultDTO<OtherMaterialCategoryDTO>>> GetAllOtherMaterialCategories(string filter, int pageSize, int pageIndex);
        Task<BaseResponseDTO<OtherMaterialCategoryDTO>> GetOtherMaterialCategoryById(int otherMaterialCateogryId);
        Task<BaseResponseDTO<bool>> DeleteOtherMaterialCategory(int otherMaterialCateogryId);
        Task<BaseResponseDTO<bool>> AddOtherMaterialCategory(OtherMaterialCategoryDTO model);
        Task<BaseResponseDTO<bool>> UpdateOtherMaterialCategory(OtherMaterialCategoryDTO name);
        //Task<BaseResponseDTO<bool>> ImportExcelOtherMaterialCategories(IFormFile file);

    }
}
