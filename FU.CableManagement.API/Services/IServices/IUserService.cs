﻿using FU.CableManagement.API.Models.UserModels;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.UserDTOs;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    public interface IUserService
    {
        Task<BaseResponseDTO<TokenResponse>> Get_AccessToken(UserLoginModel model);
        Task<BaseResponseDTO<PagedResultDTO<UserDTO>>> GetUsers(string filter, int pageSize , int pageIndex);
        Task<User> GetUser(string username, string password);
        Task<BaseResponseDTO<bool>> UpdateUser(UpdateUserDTO model);
        Task<BaseResponseDTO<bool>> ChangePassword(string email, string password);
        Task<BaseResponseDTO<bool>> DeleteUser(Guid userId);
        Task<BaseResponseDTO<bool>> RegisterUser(User newUser);
        Task<BaseResponseDTO<bool>> ImportExcelUser (IFormFile file);

    }
}
