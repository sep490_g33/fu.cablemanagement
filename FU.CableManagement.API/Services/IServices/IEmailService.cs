﻿using FU.CableManagement.DataAccess.DTO.EmailDTOs;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    public interface IEmailService
    {
        Task sendEmail(EmailDTO request);
    }
}
