﻿using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.RouteDTOs;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    /// <summary>
    /// BinhHD - Route Service Interface
    /// </summary>
    public interface IRouteService
    {
        Task<BaseResponseDTO<bool>> CreateRoute(CreateRouteDTO model);
        Task<BaseResponseDTO<bool>> UpdateRoute(UpdateRouteDTO model);
        Task<BaseResponseDTO<bool>> DeleteRoute(Guid RouteId);
        Task<BaseResponseDTO<List<string>>> ImportRouteExcel(IFormFile file);
        Task<BaseResponseDTO<PagedResultDTO<RouteDTO>>> GetAllRoute(string filter, int pageIndex, int pageSize);
    }
}
