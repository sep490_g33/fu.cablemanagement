﻿using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.TransactionDTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    public interface ITransactionService
    {
        Task<BaseResponseDTO<PagedResultDTO<TransactionDTO>>> GetAllTransactions(string filter, int pageSize, int pageIndex,
            int? warehouseId );
        Task<BaseResponseDTO<TransactionDTO>> GetTransactionById(Guid transactionId);
        Task<Task> AddTransaction(Request request,bool? action);
    }
}
