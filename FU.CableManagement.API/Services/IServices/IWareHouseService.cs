﻿using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.WareHouseDTOs;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    public interface IWareHouseService
    {
        Task<BaseResponseDTO<bool>> CreateWareHouse(WareHouseDTO model, string creatorId);
        Task<BaseResponseDTO<bool>> UpdateWareHouse(WareHouseDTO model);
        Task<BaseResponseDTO<WareHouseDTO>> GetWareHouseById(int wareHouseId);
        Task<BaseResponseDTO<bool>> DeleteWareHouse(int wareHouseId);
        Task<BaseResponseDTO<PagedResultDTO<WareHouseDTO>>> GetAllWareHouse(string filter, int pageSize, int pageIndex);
        Task<BaseResponseDTO<bool>> ImportExcelWarehouse(IFormFile file, string creatorId);

    }
}
