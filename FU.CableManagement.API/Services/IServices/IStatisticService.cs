﻿using DocumentFormat.OpenXml.Office2010.ExcelAc;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.StatisticDTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    public interface IStatisticService
    {
        //Task<BaseResponseDTO<ExchangeMaterial>> GetExchangeMaterial(int? warehouseId, DateTime? startDate, DateTime? endDate);
        //Task<BaseResponseDTO<MaterialFluctuation>> GetMaterialFluctuation(string materialOrCableName, DateTime? startDate, DateTime? endDate);
        Task<BaseResponseDTO<IssueStatistic>> GetStatisticOfIssues(DateTime? startDate, DateTime? endDate);
        Task<BaseResponseDTO<MaterialFluctuationPerYear>> GetMaterialFluctuationPerYear(int? materialId, int? warehouseId
            , int? year);
        Task<BaseResponseDTO<CableFluctuationPerYear>> GetCableFluctuationPerYear(int? cableCategoryId, int? warehouseId
            , int? year);
        Task<BaseResponseDTO<List<CableCategoryStatistic>>> GetCableCategoryStatistic(int? warehouseId);
        Task<BaseResponseDTO<List<OtherMaterialCateogoryStatistic>>> GetOtherMaterialCategoryStatistic(int? warehouseId);
        Task<BaseResponseDTO<List<RouteStatisticDTO>>> GetMaterialInRoute(Guid RouteId);
    }
}
