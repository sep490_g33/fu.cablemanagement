﻿using FU.CableManagement.DataAccess.DTO.CableCategoryDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    public interface ICableCategoryService
    {
        Task<BaseResponseDTO<PagedResultDTO<CableCategoryDTO>>> GetCableCategories(string filter, int pageSize, int pageIndex);
        Task<BaseResponseDTO<CableCategoryDTO>> GetCableCategoryById(int cableCategoryId);
        Task<BaseResponseDTO<bool>> DeleteCableCategory(int cableCategoryId);
        Task<BaseResponseDTO<bool>> AddCableCategory(CableCategoryDTO model);
        Task<BaseResponseDTO<bool>> UpdateCableCategory(CableCategoryDTO model);
        //Task<BaseResponseDTO<bool>> ImportExcelCableCategories(IFormFile file);

    }
}
