﻿using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.SupplierDTOs;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.IServices
{
    public interface ISupplierService
    {
        Task<BaseResponseDTO<bool>> CreateSupplier(SupplierDTO model, string creatorId);
        Task<BaseResponseDTO<bool>> UpdateSupplier(SupplierDTO model);
        Task<BaseResponseDTO<SupplierDTO>> GetSupplierById(int supplierId);
        Task<BaseResponseDTO<bool>> DeleteSupplier(int supplierId);
        Task<BaseResponseDTO<PagedResultDTO<SupplierDTO>>> GetAllSupplier(string filter, int pageSize, int pageIndex);
        Task<BaseResponseDTO<bool>> ImportSupplier(IFormFile file, string creatorId);
        Task<BaseResponseDTO<FileResponseDTO>> ExportSupplier();
    }
}
