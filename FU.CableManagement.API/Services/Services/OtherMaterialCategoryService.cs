﻿using AutoMapper;
using FU.CableManagement.API.Infra.Constants;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialCategoryDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialsDTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    public class OtherMaterialCateogryService : IOtherMaterialCateogryService
    {
        private readonly ILogger<OtherMaterialCateogryService> _logger;
        private readonly IConfiguration _configuration;
        private readonly CableManagementContext _context;
        private readonly IMapper _mapper;

        //private const string a1_value = "Tên Loại Vật Liệu";

        public OtherMaterialCateogryService(ILogger<OtherMaterialCateogryService> logger, IConfiguration configuration, CableManagementContext context, IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
            _mapper = mapper;
        }

        public async Task<BaseResponseDTO<bool>> AddOtherMaterialCategory(OtherMaterialCategoryDTO model)
        {
            try
            {
                var tmp = await _context.OtherMaterialsCategories.FirstOrDefaultAsync(x => x.OtherMaterialsCategoryName.ToLower() == model.OtherMaterialsCategoryName.ToLower());
                if(tmp != null)
                {
                    throw new ApplicationException("Chủng loại vật liệu đã tồn tại");
                }
                OtherMaterialsCategory otherMaterialsCategory = new OtherMaterialsCategory();
                otherMaterialsCategory.OtherMaterialsCategoryName = model.OtherMaterialsCategoryName;
                otherMaterialsCategory.IsDeleted = false;
                otherMaterialsCategory.CreatedAt = DateTime.Now;
                await _context.AddAsync(otherMaterialsCategory);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new BaseResponseDTO<bool>(true, "Thêm thành công", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<bool>> DeleteOtherMaterialCategory(int otherMaterialCateogryId)
        {
            var tmp = await _context.OtherMaterialsCategories.FirstOrDefaultAsync(x => x.OtherMaterialsCategoryId == otherMaterialCateogryId && x.IsDeleted == false);
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.OtherMaterialCategory.Other_Material_Category_Not_Found);
            }
            tmp.IsDeleted = true;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<PagedResultDTO<OtherMaterialCategoryDTO>>> GetAllOtherMaterialCategories(string filter, int pageSize, int pageIndex)
        {
            if (filter != null) filter = filter.Trim();
            var query = _context.OtherMaterialsCategories
                .Where(x => x.IsDeleted == false && (string.IsNullOrEmpty(filter) || x.OtherMaterialsCategoryName.ToLower().Contains(filter.ToLower()) 
                || EF.Functions.Collate(x.OtherMaterialsCategoryName.ToLower(), "Latin1_General_CI_AI").Contains(filter.ToLower())))
                ;
            var data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .ToListAsync();
            var result = _mapper.Map<List<OtherMaterialCategoryDTO>>(data);
            var pageResult = new PagedResultDTO<OtherMaterialCategoryDTO>(pageIndex, pageSize, await query.CountAsync(), result);
            return new BaseResponseDTO<PagedResultDTO<OtherMaterialCategoryDTO>>(pageResult);
        }

        public async Task<BaseResponseDTO<OtherMaterialCategoryDTO>> GetOtherMaterialCategoryById(int otherMaterialCateogryId)
        {
            var data = await _context.OtherMaterialsCategories.FirstOrDefaultAsync(x => x.OtherMaterialsCategoryId == otherMaterialCateogryId && x.IsDeleted == false);
            if (data == null)
            {
                throw new ApplicationException(MessageConst.OtherMaterialCategory.Other_Material_Category_Not_Found);
            }
            var result = _mapper.Map<OtherMaterialCategoryDTO>(data);
            return new BaseResponseDTO<OtherMaterialCategoryDTO>(result);
        }

        //public async Task<BaseResponseDTO<bool>> ImportExcelOtherMaterialCategories(IFormFile file)
        //{
        //    if (file == null)
        //    {
        //        throw new ApplicationException("File not null");
        //    }
        //    var extension = Path.GetExtension(file.FileName);
        //    if (extension != ".xlsx")
        //    {
        //        throw new ApplicationException("File extension must be xlsx");
        //    }
        //    var baseDirectory = Directory.GetCurrentDirectory();

        //    string filePath = Path.Combine(baseDirectory, file.FileName);
        //    if (File.Exists(filePath))
        //        File.Delete(filePath);
        //    ExcelPackage.LicenseContext = LicenseContext.Commercial;
        //    using (var stream = new FileStream(filePath, FileMode.Create))
        //    {
        //        await file.CopyToAsync(stream);
        //        using (var package = new ExcelPackage(stream))
        //        {

        //            // Get the first worksheet from the package
        //            var worksheet = package.Workbook.Worksheets[0];
        //            var validate = ValidateExcel(worksheet);
        //            if (!validate)
        //            {
        //                stream.Close();
        //                File.Delete(filePath);
        //                throw new ApplicationException("File Import Wrong Format");
        //            }
        //            // Read the data from the worksheet
        //            var listMaterialCates = new List<OtherMaterialsCategory>();
        //            var rowCount = worksheet.Dimension.Rows;
        //            //var columnCount = worksheet.Dimension.Columns;
        //            for (int i = 2; i <= rowCount; ++i)
        //            {
        //                var name = worksheet.Cells[i, 1].Value?.ToString().Trim();
        //                if (string.IsNullOrEmpty(name))
        //                    break;
        //                listMaterialCates.Add(new OtherMaterialsCategory
        //                {
        //                    OtherMaterialsCategoryName = name,
        //                    CreatedAt = DateTime.Now,
        //                    IsDeleted = false
        //                });
        //            }
        //            await _context.AddRangeAsync(listMaterialCates);
        //            await _context.SaveChangesAsync();
        //        }
        //        stream.Close();
        //        File.Delete(filePath);
        //    }

        //    return new BaseResponseDTO<bool>(true);
        //}

        //private bool ValidateExcel(ExcelWorksheet worksheet)
        //{
        //    var rowCount = worksheet.Dimension.Rows;
        //    var columnCount = worksheet.Dimension.Columns;
        //    if (rowCount < 1)
        //    {
        //        return false;
        //    }
        //    string A1 = worksheet.Cells[1, 1].Value?.ToString().Trim();
        //    if (A1 != a1_value) return false;
        //    return true;
        //}

        public async Task<BaseResponseDTO<bool>> UpdateOtherMaterialCategory(OtherMaterialCategoryDTO model)
        {
            try
            {
                var tmp = await _context.OtherMaterialsCategories.FirstOrDefaultAsync(x => x.OtherMaterialsCategoryId == model.OtherMaterialsCategoryId
                && x.IsDeleted == false);
                if (tmp == null)
                {
                    throw new ApplicationException(MessageConst.OtherMaterialCategory.Other_Material_Category_Not_Found);
                }
                var tmp2 = await _context.OtherMaterialsCategories.FirstOrDefaultAsync(x => x.OtherMaterialsCategoryName.ToLower() == model.OtherMaterialsCategoryName.ToLower()
                && x.IsDeleted == false && x.OtherMaterialsCategoryId != tmp.OtherMaterialsCategoryId);
                if (tmp2 != null)
                {
                    throw new ApplicationException("Chủng loại vật liệu đã tồn tại");
                }
                tmp = _mapper.Map(model, tmp);
                _context.Update(tmp);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true, "Cập nhật thành công", StatusCodes.Status200OK);
        }
    }
}
