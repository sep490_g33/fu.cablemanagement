﻿using AutoMapper;
using FU.CableManagement.API.Infra.Constants;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.WareHouseDTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    public class WareHouseService : IWareHouseService
    {
        private readonly ILogger<WareHouseService> _logger;
        private readonly IConfiguration _configuration;
        private readonly CableManagementContext _context;
        private readonly IMapper _mapper;

        private const string a1_value = "Tên Kho";
        private const string b1_value = "Địa Chỉ";
        private const string c1_value = "Email Thủ Kho";

        public WareHouseService(ILogger<WareHouseService> logger,
            IConfiguration configuration,
            CableManagementContext context,
            IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
            _mapper = mapper;
        }

        public async Task<BaseResponseDTO<bool>> CreateWareHouse(WareHouseDTO model, string creatorId)
        {
            try
            {
                var tmp = _mapper.Map<Warehouse>(model);
                tmp.CreatorId = Guid.Parse(creatorId);
                await _context.AddAsync(tmp);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<bool>> DeleteWareHouse(int wareHouseId)
        {
            var tmp = await _context.Warehouses.FirstOrDefaultAsync(x => x.WarehouseId == wareHouseId && x.IsDeleted == false);
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.WareHouseMessage.WareHouse_Not_Found);
            }
            tmp.IsDeleted = true;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<PagedResultDTO<WareHouseDTO>>> GetAllWareHouse(string filter, int pageSize, int pageIndex)
        {
            if (filter != null) filter = filter.Trim();
            var query = _context.Warehouses.Include(x => x.Creator)
                .Include(x => x.WarehouseKeeper)
                .Where(x => x.IsDeleted == false && (string.IsNullOrEmpty(filter) 
                || x.WarehouseName.ToLower().Contains(filter.ToLower())
                || EF.Functions.Collate(x.WarehouseName.ToLower(), "Latin1_General_CI_AI").Contains(filter.ToLower())
                ));
            var data = await query.OrderByDescending(x => x.UpdateAt).Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .ToListAsync();
            var result = _mapper.Map<List<WareHouseDTO>>(data);
            var pageResult = new PagedResultDTO<WareHouseDTO>(pageIndex, pageSize, await query.CountAsync(), result);
            return new BaseResponseDTO<PagedResultDTO<WareHouseDTO>>(pageResult);
        }

        public async Task<BaseResponseDTO<WareHouseDTO>> GetWareHouseById(int wareHouseId)
        {
            var data = await _context.Warehouses.Include(x => x.Creator).FirstOrDefaultAsync(x => x.IsDeleted == false && x.WarehouseId == wareHouseId);
            if (data == null)
            {
                throw new ApplicationException(MessageConst.WareHouseMessage.WareHouse_Not_Found);
            }
            var result = _mapper.Map<WareHouseDTO>(data);
            return new BaseResponseDTO<WareHouseDTO>(result);
        }

        public async Task<BaseResponseDTO<bool>> ImportExcelWarehouse(IFormFile file, string creatorId)
        {
            if (file == null)
            {
                throw new ApplicationException("File không được để trống");
            }
            var extension = Path.GetExtension(file.FileName);
            if (extension != ".xlsx")
            {
                throw new ApplicationException("Định dạng file phải là xlsx");
            }
            var baseDirectory = Directory.GetCurrentDirectory();

            string filePath = Path.Combine(baseDirectory, file.FileName);
            if (File.Exists(filePath))
                File.Delete(filePath);
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {

                    // Get the first worksheet from the package
                    var worksheet = package.Workbook.Worksheets[0];
                    var validate = ValidateExcel(worksheet);
                    if (!validate)
                    {
                        stream.Close();
                        File.Delete(filePath);
                        throw new ApplicationException("Sai định dạng trường dữ liệu");
                    }
                    // Read the data from the worksheet
                    var listWarehouses = new List<Warehouse>();
                    var rowCount = worksheet.Dimension.Rows;
                    //var columnCount = worksheet.Dimension.Columns;
                    for (int i = 2; i <= rowCount; ++i)
                    {
                        var name = worksheet.Cells[i, 1].Value?.ToString().Trim();
                        var address = worksheet.Cells[i, 2].Value?.ToString().Trim();
                        var warehouseKeeper = worksheet.Cells[i, 3].Value?.ToString().Trim();
                        if (string.IsNullOrEmpty(name))
                            break;
                        if (!string.IsNullOrEmpty(warehouseKeeper))
                        {
                            var warehouseKeeperId = await _context.Users.Where(x => x.Email.ToLower() == warehouseKeeper.ToLower()).Select(x => x.UserId).SingleOrDefaultAsync();
                            var tmp = await _context.Warehouses.FirstOrDefaultAsync(x => x.WarehouseKeeperid == warehouseKeeperId);
                            if(tmp != null)
                            {
                                throw new ApplicationException("Chủ kho email "+ warehouseKeeper + " đã quản lý 1 kho hàng");
                            }
                            listWarehouses.Add(new Warehouse
                            {
                                WarehouseName = name,
                                WarehouseAddress = address,
                                WarehouseKeeperid = warehouseKeeperId,
                                CreatorId = Guid.Parse(creatorId)
                            });
                        }
                        
                    }
                    await _context.AddRangeAsync(listWarehouses);
                    await _context.SaveChangesAsync();
                }
                stream.Close();
                File.Delete(filePath);
            }

            return new BaseResponseDTO<bool>(true);
        }

        private bool ValidateExcel(ExcelWorksheet worksheet)
        {
            var rowCount = worksheet.Dimension.Rows;
            var columnCount = worksheet.Dimension.Columns;
            if (rowCount < 1)
            {
                return false;
            }
            string A1 = worksheet.Cells[1, 1].Value?.ToString().Trim();
            string B1 = worksheet.Cells[1, 2].Value?.ToString().Trim();
            string C1 = worksheet.Cells[1, 3].Value?.ToString().Trim();
            if (A1 != a1_value || B1 != b1_value || C1 != c1_value ) return false;
            return true;
        }
        public async Task<BaseResponseDTO<bool>> UpdateWareHouse(WareHouseDTO model)
        {
            var tmp = await _context.Warehouses.FirstOrDefaultAsync(x => x.WarehouseId == model.WarehouseId && x.IsDeleted == false);
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.WareHouseMessage.WareHouse_Not_Found);
            }
            if (!string.IsNullOrEmpty(model.WarehouseName))
            {
                tmp.WarehouseName = model.WarehouseName;
            }
            if (!string.IsNullOrEmpty(model.WarehouseAddress))
            {
                tmp.WarehouseAddress = model.WarehouseAddress;
            }
            if (model.WarehouseKeeperid != null)
            {
                tmp.WarehouseKeeperid = model.WarehouseKeeperid;
            }
            tmp.UpdateAt = DateTime.UtcNow;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }
    }
}
