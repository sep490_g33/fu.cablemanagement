﻿using AutoMapper;
using FU.CableManagement.API.Infra.Constants;
using FU.CableManagement.API.Models.NodeModel;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.NodeDTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class NodeService : INodeService
    {
        private readonly ILogger<NodeService> _logger;
        private readonly IConfiguration _configuration;
        private readonly CableManagementContext _context;
        private readonly IMapper _mapper;

        private const string a1_value = "Name";
        private const string b1_value = "Description";
        private const string c1_value = "Longitude";
        private const string d1_value = "Latitude";
        public NodeService(ILogger<NodeService> logger,
            IConfiguration configuration,
            CableManagementContext context,
            IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
            _mapper = mapper;
        }
        public async Task<BaseResponseDTO<bool>> CreateNode(CreateNodeDTO model)
        {
            var node = _mapper.Map<Node>(model);
            await _context.AddAsync(node);
            int cnt = await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(cnt > 0);
        }

        public async Task<BaseResponseDTO<bool>> DeleteNode(Guid NodeId)
        {
            var entry = await _context.Nodes.FirstOrDefaultAsync(x => x.Id == NodeId && !x.IsDeleted);
            if (entry == null)
            {
                throw new ApplicationException(MessageConst.NodeMessage.Node_Not_Found);
            }
            entry.IsDeleted = true;
            _context.Update(entry);
            int cnt = await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(cnt > 0);
        }

        public async Task<BaseResponseDTO<PagedResultDTO<NodeDTO>>> GetAllNodes(string filter, Guid? RouteId, int pageSize, int pageIndex)
        {
            var query = _context.Nodes.Include(x => x.NodeCables).ThenInclude(x => x.Cable).ThenInclude(x => x.CableCategory)
                .Include(x => x.NodeMaterials).ThenInclude(x => x.OtherMaterial).ThenInclude(x => x.OtherMaterialsCategory)
                .Include(x => x.NodeMaterialCategories.Where(x => x.IsDeleted == false)).ThenInclude(x => x.MaterialCategory)
                .Where(x => x.IsDeleted == false
                        && (RouteId == null || x.RouteId == RouteId.Value)
                        && (string.IsNullOrEmpty(filter) || x.Name.ToLower().Contains(filter.ToLower())));
            var data = await query.OrderByDescending(x => x.UpdateAt).Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .ToListAsync();
            var result = _mapper.Map<List<NodeDTO>>(data);
            var pageResult = new PagedResultDTO<NodeDTO>(pageIndex, pageSize, await query.CountAsync(), result);
            return new BaseResponseDTO<PagedResultDTO<NodeDTO>>(pageResult);
        }

        public async Task<BaseResponseDTO<NodeDTO>> GetNodeById(Guid NodeId)
        {
            var entry = await _context.Nodes
                .Include(x => x.NodeCables.Where(x => x.IsDeleted == false)).ThenInclude(x => x.Cable).ThenInclude(x => x.CableCategory)
                .Include(x => x.NodeMaterials.Where(x => x.IsDeleted == false)).ThenInclude(x => x.OtherMaterial).ThenInclude(x => x.OtherMaterialsCategory)
                .Include(x => x.NodeMaterialCategories.Where(x => x.IsDeleted == false)).ThenInclude(x => x.MaterialCategory)
                .FirstOrDefaultAsync(x => x.Id == NodeId && !x.IsDeleted);
            if (entry == null)
            {
                throw new ApplicationException(MessageConst.NodeMessage.Node_Not_Found);
            }
            return new BaseResponseDTO<NodeDTO>(_mapper.Map<NodeDTO>(entry));

        }

        public async Task<BaseResponseDTO<bool>> UpdateNode(UpdateNodeDTO model)
        {
            var entry = await _context.Nodes.FirstOrDefaultAsync(x => x.Id == model.Id && !x.IsDeleted);
            if (entry == null)
            {
                throw new ApplicationException(MessageConst.NodeMessage.Node_Not_Found);
            }
            entry.Name = model.Name;
            entry.Description = model.Description;
            entry.Longitude = model.Longitude;
            entry.Latitude = model.Latitude;
            entry.UpdateAt = DateTime.UtcNow;
            entry.NodeCode = model.NodeCode;
            entry.NodeNumberSign = model.NodeNumberSign;
            entry.Address = model.Address;
            entry.Status = model.Status;
            entry.Note = model.Note;
            _context.Update(entry);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<bool>> ImportNodes(IFormFile file)
        {
            if (file == null)
            {
                throw new ApplicationException("File không được để trống");
            }
            var extension = Path.GetExtension(file.FileName);
            if (extension != ".xlsx")
            {
                throw new ApplicationException("Định dạng file phài là xlsx");
            }
            var baseDirectory = Directory.GetCurrentDirectory();

            string filePath = Path.Combine(baseDirectory, file.FileName);
            if (File.Exists(filePath))
                File.Delete(filePath);
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {

                    // Get the first worksheet from the package
                    var worksheet = package.Workbook.Worksheets[0];
                    var validate = ValidateExcel(worksheet);
                    if (!validate)
                    {
                        stream.Close();
                        File.Delete(filePath);
                        throw new ApplicationException("Sai định dạng trường dữ liệu");
                    }
                    // Read the data from the worksheet
                    var listNodes = new List<Node>();
                    var rowCount = worksheet.Dimension.Rows;
                    //var columnCount = worksheet.Dimension.Columns;
                    for (int i = 2; i <= rowCount; ++i)
                    {
                        var name = worksheet.Cells[i, 1].Value?.ToString().Trim();
                        var description = worksheet.Cells[i, 2].Value?.ToString().Trim();
                        var longitude = worksheet.Cells[i, 3].Value?.ToString().Trim();
                        var latitude = worksheet.Cells[i, 4].Value?.ToString().Trim();
                        if (string.IsNullOrEmpty(name))
                            break;
                        if (!float.TryParse(longitude, out var _))
                            throw new ApplicationException(MessageConst.NodeMessage.Cannot_Parse_Double);
                        if (!float.TryParse(latitude, out var _))
                            throw new ApplicationException(MessageConst.NodeMessage.Cannot_Parse_Double);
                        listNodes.Add(new Node
                        {
                            Name = name,
                            Description = description,
                            Longitude = float.Parse(longitude),
                            Latitude = float.Parse(latitude)
                        });
                    }
                    await _context.AddRangeAsync(listNodes);
                    await _context.SaveChangesAsync();
                }
                stream.Close();
                File.Delete(filePath);
            }

            return new BaseResponseDTO<bool>(true);
        }

        private bool ValidateExcel(ExcelWorksheet worksheet)
        {
            try
            {
                var rowCount = worksheet.Dimension.Rows;
                var columnCount = worksheet.Dimension.Columns;
                if (rowCount < 1)
                {
                    return false;
                }
                string A1 = worksheet.Cells[1, 1].Value?.ToString().Trim();
                string B1 = worksheet.Cells[1, 2].Value?.ToString().Trim();
                string C1 = worksheet.Cells[1, 3].Value?.ToString().Trim();
                string D1 = worksheet.Cells[1, 4].Value?.ToString().Trim();
                if (A1 != a1_value || B1 != b1_value || C1 != c1_value || D1 != d1_value) return false;
            }
            catch
            {
                return false;
            }
            return true;
        }

        public async Task<BaseResponseDTO<bool>> AddOrUpdateNodeCable(AddOrUpdateNodeCable model)
        {
            var listNodeCables = await _context.NodeCables.Where(x => x.IsDeleted == false && model.CableId == x.CableId).ToListAsync();
            var listUpdateCalbe = listNodeCables.Where(x => model.ListNodeIds.Contains(x.NodeId)).ToList();
            var listDeleteCable = listNodeCables.Except(listUpdateCalbe).ToList();
            var listAddCable = new List<NodeCable>();
            listDeleteCable.ForEach(x => x.IsDeleted = true);
            int id = 0;
            foreach (var nodeId in model.ListNodeIds)
            {
                id++;
                var tmp = listUpdateCalbe.FirstOrDefault(x => x.NodeId == nodeId);
                if (tmp != null)
                {
                    tmp.OrderIndex = id;
                }
                else listAddCable.Add(new NodeCable
                {
                    CableId = model.CableId,
                    NodeId = nodeId,
                    OrderIndex = id
                });
            }
            await _context.AddRangeAsync(listAddCable);
            _context.UpdateRange(listDeleteCable);
            _context.UpdateRange(listUpdateCalbe);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<bool>> AddOrUpdateNodeMaterial(AddOrUpdateNodeMaterial model)
        {
            var listMatsInNode = await _context.NodeMaterials.Where(x => x.IsDeleted == false && x.NodeId == model.NodeId).ToListAsync();

            var listUpdateMats = listMatsInNode.Where(x => model.ListAddOrUpdateNodeMaterials.Select(x => x.MaterialId).Contains(x.OtherMaterialsId)).ToList();
            var listDeleteMats = listMatsInNode.Except(listUpdateMats).ToList();
            var listAddMats = new List<NodeMaterial>();
            listDeleteMats.ForEach(x => x.IsDeleted = true);
            foreach (var item in model.ListAddOrUpdateNodeMaterials)
            {
                var tmp = listUpdateMats.FirstOrDefault(x => x.OtherMaterialsId == item.MaterialId);
                if (tmp != null)
                {
                    tmp.Quantity = item.Quantity;
                }
                else
                {
                    listAddMats.Add(new NodeMaterial
                    {
                        Quantity = item.Quantity,
                        NodeId = model.NodeId,
                        OtherMaterialsId = item.MaterialId
                    });
                }
            }
            await _context.AddRangeAsync(listAddMats);
            _context.UpdateRange(listDeleteMats);
            _context.UpdateRange(listUpdateMats);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<bool>> AddOrUpdateNodeMaterialCategory(AddOrUpdateNodeMaterialCategory model)
        {
            var listMatCatesInNode = await _context.NodeMaterialCategories.Where(x => x.IsDeleted == false && x.NodeId == model.NodeId).ToListAsync();

            var listUpdateMatCates = listMatCatesInNode.Where(x => model.ListAddOrUpdateNodeMaterialCategories
                                            .Select(x => x.MaterialCategoryId).Contains(x.OtherMaterialCategoryId)).ToList();
            var listDeleteMatCats = listMatCatesInNode.Except(listUpdateMatCates).ToList();
            var listAddMats = new List<NodeMaterialCategory>();
            listDeleteMatCats.ForEach(x => x.IsDeleted = true);
            foreach (var item in model.ListAddOrUpdateNodeMaterialCategories)
            {
                var tmp = listUpdateMatCates.FirstOrDefault(x => x.OtherMaterialCategoryId == item.MaterialCategoryId);
                if (tmp != null)
                {
                    tmp.Quantity = item.Quantity;
                }
                else
                {
                    listAddMats.Add(new NodeMaterialCategory
                    {
                        Quantity = item.Quantity,
                        NodeId = model.NodeId,
                        OtherMaterialCategoryId = item.MaterialCategoryId
                    });
                }
            }
            await _context.AddRangeAsync(listAddMats);
            _context.UpdateRange(listDeleteMatCats);
            _context.UpdateRange(listUpdateMatCates);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public Task<BaseResponseDTO<bool>> AddOrUpdateNodeMaterialCategoryMany(List<AddOrUpdateNodeMaterialCategory> models)
        {
            throw new NotImplementedException();
        }
    }
}
