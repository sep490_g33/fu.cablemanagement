﻿using AutoMapper;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using FU.CableManagement.API.Infra.Constants;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.IssueDTOs;
using FU.CableManagement.DataAccess.DTO.RequestDTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    public class IssueService : IIssueService
    {
        private readonly ILogger<IssueService> _logger;
        private readonly IConfiguration _configuration;
        private readonly CableManagementContext _context;
        private readonly IMapper _mapper;

        private const string a1_value = "Tên Sự Cố";
        private const string b1_value = "Mã Sự Cố";
        private const string c1_value = "Mô Tả";
        private const string d1_value = "Trạng thái";

        public IssueService(ILogger<IssueService> logger,
            IConfiguration configuration,
            CableManagementContext context,
            IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
            _mapper = mapper;
        }
        public async Task<BaseResponseDTO<bool>> CreateIssue(IssueDTO model, string creatorId)
        {
            try
            {
                var tmp = _mapper.Map<Issue>(model);
                tmp.CreatorId = Guid.Parse(creatorId);
                tmp.Status = IssueStatusConstants.Doing;
                if (string.IsNullOrEmpty(model.IssueName))
                {
                    throw new ApplicationException("Tên sự cố không được để trống");
                }
                if (string.IsNullOrEmpty(model.IssueCode))
                {
                    throw new ApplicationException("Mã sự cố không được để trống");
                }
                var checkExistIssue = await _context.Issues.FirstOrDefaultAsync(x => x.IssueCode == model.IssueCode);
                if(checkExistIssue != null) { throw new ApplicationException("Mã sự cố đã tồn tại"); }
                await _context.AddAsync(tmp);
                await _context.SaveChangesAsync();
                return new BaseResponseDTO<bool>(true);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<BaseResponseDTO<bool>> DeleteIssue(Guid issueId)
        {
            var tmp = await _context.Issues.FirstOrDefaultAsync(x => x.IssueId == issueId && x.IsDeleted == false);
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.IssueMessage.Issue_Not_Found);
            }
            tmp.IsDeleted = true;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<PagedResultDTO<IssueDTO>>> GetAllIssue(string filter, int pageSize, int pageIndex)
        {
            if (filter != null) filter = filter.Trim();
            var query = _context.Issues.Include(x => x.Creator)
                .Where(x => x.IsDeleted == false && (string.IsNullOrEmpty(filter) || x.IssueName.ToLower().Contains(filter.ToLower()) 
                || EF.Functions.Collate(x.IssueName.ToLower(), "Latin1_General_CI_AI").Contains(filter.ToLower())
                    || x.IssueCode.ToLower().Contains(filter.ToLower())
                ));
            var data = await query.OrderByDescending(x => x.Status).ThenByDescending(x => x.UpdateAt)
                .Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
            var result = _mapper.Map<List<IssueDTO>>(data);
            var pageResult = new PagedResultDTO<IssueDTO>(pageIndex, pageSize, await query.CountAsync(), result);
            return new BaseResponseDTO<PagedResultDTO<IssueDTO>>(pageResult);
        }

        public async Task<BaseResponseDTO<IssueDTO>> GetIssueById(Guid issueId)
        {
            var data = await _context.Issues.Include(x => x.Creator).FirstOrDefaultAsync(x => x.IsDeleted == false && x.IssueId == issueId);
            if (data == null)
            {
                throw new ApplicationException(MessageConst.IssueMessage.Issue_Not_Found);
            }
            var result = _mapper.Map<IssueDTO>(data);
            return new BaseResponseDTO<IssueDTO>(result);
        }

        public async Task<BaseResponseDTO<bool>> ImportExcelIssue(IFormFile file, string creatorId)
        {
            if (file == null)
            {
                throw new ApplicationException("File không được để trống");
            }
            var extension = Path.GetExtension(file.FileName);
            if (extension != ".xlsx")
            {
                throw new ApplicationException("Định dạng file phài là xlsx");
            }
            var baseDirectory = Directory.GetCurrentDirectory();

            string filePath = Path.Combine(baseDirectory, file.FileName);
            if (File.Exists(filePath))
                File.Delete(filePath);
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {

                    // Get the first worksheet from the package
                    var worksheet = package.Workbook.Worksheets[0];
                    var validate = ValidateExcel(worksheet);
                    if (!validate)
                    {
                        stream.Close();
                        File.Delete(filePath);
                        throw new ApplicationException("Sai định dạng trường dữ liệu");
                    }
                    // Read the data from the worksheet
                    var listIssue = new List<Issue>();
                    var rowCount = worksheet.Dimension.Rows;
                    //var columnCount = worksheet.Dimension.Columns;
                    for (int i = 2; i <= rowCount; ++i)
                    {
                        var name = worksheet.Cells[i, 1].Value?.ToString().Trim();
                        var issueCode = worksheet.Cells[i, 2].Value?.ToString().Trim();
                        var description = worksheet.Cells[i, 3].Value?.ToString().Trim();
                        var status = worksheet.Cells[i, 4].Value?.ToString().Trim();
                        if (string.IsNullOrEmpty(name))
                            break;
                        listIssue.Add(new Issue
                        {
                            IssueName = name,
                            IssueCode = issueCode,
                            Description = description,
                            CreatorId = Guid.Parse(creatorId),
                            Status = status,
                            IsDeleted = false,
                            CreatedAt = DateTime.Now,
                        });
                    }
                    await _context.AddRangeAsync(listIssue);
                    await _context.SaveChangesAsync();
                }
                stream.Close();
                File.Delete(filePath);
            }

            return new BaseResponseDTO<bool>(true);
        }
        private bool ValidateExcel(ExcelWorksheet worksheet)
        {
            var rowCount = worksheet.Dimension.Rows;
            var columnCount = worksheet.Dimension.Columns;
            if (rowCount < 1)
            {
                return false;
            }
            string A1 = worksheet.Cells[1, 1].Value?.ToString().Trim();
            string B1 = worksheet.Cells[1, 2].Value?.ToString().Trim();
            string C1 = worksheet.Cells[1, 3].Value?.ToString().Trim();
            string D1 = worksheet.Cells[1, 4].Value?.ToString().Trim();
            if (A1 != a1_value || B1 != b1_value || C1 != c1_value || D1 != d1_value) return false;
            return true;
        }

        public async Task<BaseResponseDTO<bool>> UpdateIssue(IssueDTO model)
        {
            var tmp = await _context.Issues.FirstOrDefaultAsync(x => x.IssueId == model.IssueId && x.IsDeleted == false);
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.IssueMessage.Issue_Not_Found);
            }
            if (!string.IsNullOrEmpty(model.IssueName))
            {
                tmp.IssueName = model.IssueName;
            }
            if (!string.IsNullOrEmpty(model.IssueCode))
            {
                tmp.IssueCode = model.IssueCode;
            }
            if (!string.IsNullOrEmpty(model.Description))
            {
                tmp.Description = model.Description;
            }
            if (!string.IsNullOrEmpty(model.CableRoutingName))
            {
                tmp.CableRoutingName = model.CableRoutingName;
            }
            if (!string.IsNullOrEmpty(model.Group))
            {
                tmp.Group = model.Group;
            }
            if (!string.IsNullOrEmpty(model.Status))
            {
                tmp.Status = IssueStatusConstants.Doing;
                if (model.Status == IssueStatusConstants.Done) 
                    tmp.Status = IssueStatusConstants.Done;
            }
            var checkExistIssue = await _context.Issues.FirstOrDefaultAsync(x => x.IssueCode == model.IssueCode && x.IssueId != model.IssueId);
            if (checkExistIssue != null) { throw new ApplicationException("Mã sự cố đã được sử dụng"); }
            tmp.UpdateAt = DateTime.UtcNow;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<List<RequestDTO>>> GetAllMaterialByIssue(Guid issueId)
        {
            var query = _context.Requests.Include(x => x.Approver).Include(x => x.RequestCategory)
                .Include(x => x.Issue).Include(x => x.Creator)
                .Where(x => x.IsDeleted == false && x.Status.ToLower().Equals(RequestStatusConstant.Approved)
                && x.IssueId == issueId).OrderBy(x=>x.RequestCategoryId);
            var data = await query.ToListAsync();
            var result = _mapper.Map<List<RequestDTO>>(data);
            foreach (var item in result)
            {
                var reCab = _mapper.Map<List<RequestCableDTO>>(await _context.RequestCables.Include(x => x.Cable).ThenInclude(x => x.CableCategory)
                .Where(x => x.RequestId == item.RequestId).ToListAsync());
                var reMat = _mapper.Map<List<RequestMaterialDTO>>(await _context.RequestOtherMaterials.Include(x => x.OtherMaterials).ThenInclude(x => x.OtherMaterialsCategory)
                .Where(x => x.RequestId == item.RequestId).ToListAsync());
                item.RequestCableDTOs = reCab;
                item.RequestMaterialDTOs = reMat;
            }
            return new BaseResponseDTO<List<RequestDTO>>(result);
        }

        public async Task<BaseResponseDTO<PagedResultDTO<IssueDTO>>> GetAllIssueNotResolved(string filter, int pageSize, int pageIndex)
        {
            var query = _context.Issues.Include(x => x.Creator)
                .Where(x => x.IsDeleted == false && x.Status.ToLower() == IssueStatusConstants.Doing
                && (string.IsNullOrEmpty(filter) || x.IssueName.ToLower().Contains(filter.ToLower())
                    || x.IssueCode.ToLower().Equals(filter.ToLower())
                ));
            var data = await query.OrderByDescending(x => x.UpdateAt)
                .Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
            var result = _mapper.Map<List<IssueDTO>>(data);
            var pageResult = new PagedResultDTO<IssueDTO>(pageIndex, pageSize, await query.CountAsync(), result);
            return new BaseResponseDTO<PagedResultDTO<IssueDTO>>(pageResult);
        }
    }
}
