﻿using AutoMapper;
using FU.CableManagement.API.Infra.Constants;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CableCategoryDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialCategoryDTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    public class CableCategoryService : ICableCategoryService
    {
        private ILogger<CableCategoryService> _logger;
        private readonly IConfiguration _configuration;
        private readonly CableManagementContext _context;
        private readonly IMapper _mapper;

        //private const string a1_value = "Tên Loại Cáp";

        public CableCategoryService(ILogger<CableCategoryService> logger, IConfiguration configuration, CableManagementContext context, IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
            _mapper = mapper;
        }

        public async Task<BaseResponseDTO<bool>> AddCableCategory(CableCategoryDTO model)
        {
            try
            {
                var tmp = await _context.CableCategories.FirstOrDefaultAsync(x => x.CableCategoryName.ToLower() == model.CableCategoryName.ToLower());
                if (tmp != null)
                {
                    throw new ApplicationException("Chủng loại cáp đã tồn tại");
                }
                CableCategory cableCategory = new CableCategory();
                cableCategory.CableCategoryName = model.CableCategoryName;
                cableCategory.IsDeleted = false;
                cableCategory.CreatedAt = DateTime.Now;
                await _context.AddAsync(cableCategory);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new BaseResponseDTO<bool>(true, "Thêm thành công", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<bool>> DeleteCableCategory(int cableCategoryId)
        {
            var tmp = await _context.CableCategories.FirstOrDefaultAsync(x => x.CableCategoryId == cableCategoryId && x.IsDeleted == false);
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.CableCategoryMessage.Cable_Category_Not_Found);
            }
            tmp.IsDeleted = true;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<PagedResultDTO<CableCategoryDTO>>> GetCableCategories(string filter, int pageSize, int pageIndex)
        {
            if (filter != null) filter = filter.Trim();
            var query = _context.CableCategories
                .Where(x => x.IsDeleted == false && (string.IsNullOrEmpty(filter)
            || x.CableCategoryName.ToLower().Contains(filter.ToLower())
            || EF.Functions.Collate(x.CableCategoryName.ToLower(), "Latin1_General_CI_AI").Contains(filter.ToLower())
            ))
                ;
            var data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .ToListAsync();
            var result = _mapper.Map<List<CableCategoryDTO>>(data);
            var pageResult = new PagedResultDTO<CableCategoryDTO>(pageIndex, pageSize, await query.CountAsync(), result);
            return new BaseResponseDTO<PagedResultDTO<CableCategoryDTO>>(pageResult);
        }

        public async Task<BaseResponseDTO<CableCategoryDTO>> GetCableCategoryById(int cableCategoryId)
        {
            var data = await _context.CableCategories.FirstOrDefaultAsync(x => x.CableCategoryId == cableCategoryId && x.IsDeleted == false);
            if (data == null)
            {
                throw new ApplicationException(MessageConst.CableCategoryMessage.Cable_Category_Not_Found);
            }
            var result = _mapper.Map<CableCategoryDTO>(data);
            return new BaseResponseDTO<CableCategoryDTO>(result);
        }

        public async Task<BaseResponseDTO<bool>> UpdateCableCategory(CableCategoryDTO model)
        {
            try
            {
                var tmp = await _context.CableCategories.FirstOrDefaultAsync(x => x.CableCategoryId == model.CableCategoryId
                && x.IsDeleted == false);
                if (tmp == null)
                {
                    throw new ApplicationException(MessageConst.CableCategoryMessage.Cable_Category_Not_Found);
                }
                var tmp2 = await _context.CableCategories.FirstOrDefaultAsync(x => x.CableCategoryName.ToLower() == model.CableCategoryName.ToLower()
                && x.IsDeleted == false && x.CableCategoryId != tmp.CableCategoryId);
                if (tmp2 != null)
                {
                    throw new ApplicationException("Chủng loại cáp đã tồn tại");
                }
                tmp = _mapper.Map(model, tmp);
                _context.Update(tmp);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true, "Cập nhật thành công", StatusCodes.Status200OK);
        }

        //public async Task<BaseResponseDTO<bool>> ImportExcelCableCategories(IFormFile file)
        //{
        //    if (file == null)
        //    {
        //        throw new ApplicationException("File not null");
        //    }
        //    var extension = Path.GetExtension(file.FileName);
        //    if (extension != ".xlsx")
        //    {
        //        throw new ApplicationException("File extension must be xlsx");
        //    }
        //    var baseDirectory = Directory.GetCurrentDirectory();

        //    string filePath = Path.Combine(baseDirectory, file.FileName);
        //    if (File.Exists(filePath))
        //        File.Delete(filePath);
        //    ExcelPackage.LicenseContext = LicenseContext.Commercial;
        //    using (var stream = new FileStream(filePath, FileMode.Create))
        //    {
        //        await file.CopyToAsync(stream);
        //        using (var package = new ExcelPackage(stream))
        //        {

        //            // Get the first worksheet from the package
        //            var worksheet = package.Workbook.Worksheets[0];
        //            var validate = ValidateExcel(worksheet);
        //            if (!validate)
        //            {
        //                stream.Close();
        //                File.Delete(filePath);
        //                throw new ApplicationException("File Import Wrong Format");
        //            }
        //            // Read the data from the worksheet
        //            var listCableCates = new List<CableCategory>();
        //            var rowCount = worksheet.Dimension.Rows;
        //            //var columnCount = worksheet.Dimension.Columns;
        //            for (int i = 2; i <= rowCount; ++i)
        //            {
        //                var name = worksheet.Cells[i, 1].Value?.ToString().Trim();
        //                if (string.IsNullOrEmpty(name))
        //                    break;
        //                listCableCates.Add(new CableCategory
        //                {
        //                    CableCategoryName = name,
        //                    CreatedAt = DateTime.Now,
        //                    IsDeleted = false
        //                });
        //            }
        //            await _context.AddRangeAsync(listCableCates);
        //            await _context.SaveChangesAsync();
        //        }
        //        stream.Close();
        //        File.Delete(filePath);
        //    }

        //    return new BaseResponseDTO<bool>(true);
        //}

        //private bool ValidateExcel(ExcelWorksheet worksheet)
        //{
        //    var rowCount = worksheet.Dimension.Rows;
        //    var columnCount = worksheet.Dimension.Columns;
        //    if (rowCount < 1)
        //    {
        //        return false;
        //    }
        //    string A1 = worksheet.Cells[1, 1].Value?.ToString().Trim();
        //    if (A1 != a1_value) return false;
        //    return true;
        //}
    }
}
