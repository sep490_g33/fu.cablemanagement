﻿using AutoMapper;
using FU.CableManagement.API.Models.UserModels;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.API.Utils;
using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.Common.Enums;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.UserDTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    public class UserService : IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly IConfiguration _configuration;
        private readonly CableManagementContext _context;
        private readonly IMapper _mapper;

        private const string a1_value = "Tên Tài Khoản - Username";
        private const string b1_value = "Tên";
        private const string c1_value = "Họ";
        private const string d1_value = "Mật Khẩu";
        private const string e1_value = "Email";
        private const string f1_value = "Số Điện Thoại";
        private const string g1_value = "ID Role - vị trí nhân viên";
        private const string h1_value = "Ngày Tạo";

        public UserService(ILogger<UserService> logger,
            IConfiguration configuration,
            CableManagementContext context,
            IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
            _mapper = mapper;
        }
        public async Task<BaseResponseDTO<TokenResponse>> Get_AccessToken(UserLoginModel model)
        {
            var user = await Authenticate(model);

            if (user != null)
            {
                var token = Generate(user);
                return new BaseResponseDTO<TokenResponse>(new TokenResponse { Access_Token = token });
            }

            throw new Exception("UserName hoặc mật khẩu sai");
        }

        private string Generate(User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim("username", user.Username),
                new Claim("userid", user.UserId.ToString()),
                new Claim(ClaimTypes.Email, user.Email??""),
                new Claim("phone",user.Phone??""),
                new Claim("firstname",user.Firstname??""),
                new Claim("Lastname",user.Lastname??""),
                new Claim(ClaimTypes.Role, convertRole((RoleUserEnum)user.RoleId))
            };

            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
              _configuration["Jwt:Audience"],
              claims,
              expires: DateTime.Now.AddDays(14),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private async Task<User> Authenticate(UserLoginModel userLogin)
        {
            var account = await _context.Users.FirstOrDefaultAsync(x => x.Username == userLogin.Username && x.Password == userLogin.Password);
            return account;
        }

        private string convertRole(RoleUserEnum role)
        {
            switch (role)
            {
                case RoleUserEnum.Admin:
                    return UserRoleConstant.Admin;
                case RoleUserEnum.Leader:
                    return UserRoleConstant.Leader;
                case RoleUserEnum.Staff:
                    return UserRoleConstant.Staff;
                case RoleUserEnum.WarehouseKeeper:
                    return UserRoleConstant.WarehouseKeeper;
            }
            return UserRoleConstant.Staff;
        }

        public async Task<BaseResponseDTO<PagedResultDTO<UserDTO>>> GetUsers(string filter, int pageSize = 12, int pageIndex = 1)
        {
            try
            {
                if (filter != null) filter = filter.Trim();
                var query = _context.Users.Include(x => x.Role).Where(x => x.IsDeleted == false
                    && (string.IsNullOrEmpty(filter) || x.Username.ToLower().Contains(filter.ToLower()) || x.Email.ToLower().Contains(filter.ToLower())
                    || x.Firstname.ToLower().Contains(filter.ToLower())
                    || EF.Functions.Collate(x.Firstname.ToLower(), "Latin1_General_CI_AI").Contains(filter.ToLower())
                    || x.Lastname.ToLower().Contains(filter.ToLower())
                    || EF.Functions.Collate(x.Lastname.ToLower(), "Latin1_General_CI_AI").Contains(filter.ToLower())
                    || x.Role.Rolename.ToLower().Contains(filter.ToLower())));
                var listAccount = await query.OrderByDescending(x => x.UpdateAt).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
                var result = new List<UserDTO>();

                foreach (var account in listAccount)
                {
                    var tmp = _mapper.Map<UserDTO>(account);
                    result.Add(tmp);
                }
                var pageResult = new PagedResultDTO<UserDTO>(pageIndex, pageSize, await query.CountAsync(), result);
                return new BaseResponseDTO<PagedResultDTO<UserDTO>>(pageResult);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<BaseResponseDTO<bool>> UpdateUser(UpdateUserDTO model)
        {
            var tmp = await _context.Users.FirstOrDefaultAsync(x => x.UserId == model.UserId && x.IsDeleted == false);
            if (tmp == null)
            {
                throw new KeyNotFoundException();
            }
            var tmp2 = await _context.Users.FirstOrDefaultAsync(x => x.UserId != model.UserId && x.IsDeleted == false &&
            (x.Email.ToLower() == model.Email.ToLower() || x.Username.ToLower() == model.UserName.ToLower()
            ));
            if(tmp2 != null)
            {
                throw new ApplicationException("Email hoặc username đã được sử dụng.");
            }
            tmp.Username = model.UserName;
            tmp.Firstname = model.Firstname;
            tmp.Lastname = model.Lastname;
            tmp.Email = model.Email;
            tmp.Phone = model.Phone;
            tmp.RoleId = model.RoleId;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<bool>> DeleteUser(Guid UserId)
        {
            var tmp = await _context.Users.FirstOrDefaultAsync(x => x.UserId == UserId && x.IsDeleted == false);
            if (tmp == null)
            {
                throw new KeyNotFoundException();
            }
            tmp.IsDeleted = true;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<bool>> RegisterUser(User newUser)
        {
            try
            {
                var tmp = await _context.Users.FirstOrDefaultAsync(x => (x.Email.Equals(newUser.Email) || x.Username.Equals(newUser.Username)) 
                && x.IsDeleted == false);
                if (tmp != null)
                {
                    return new BaseResponseDTO<bool>("Email hoặc username đã được sử dụng", StatusCodes.Status409Conflict);

                }


                await _context.Users.AddAsync(newUser);
                await _context.SaveChangesAsync();

                return new BaseResponseDTO<bool>(true);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<User> GetUser(string username, string password)
        {
            var account = await _context.Users.FirstOrDefaultAsync(x => x.Username == username && x.Password == password);

            return account;
        }

        public async Task<BaseResponseDTO<bool>> ImportExcelUser(IFormFile file)
        {
            try
            {
                if (file == null)
                {
                    throw new ApplicationException("File không được để trống");
                }
                var extension = Path.GetExtension(file.FileName);
                if (extension != ".xlsx")
                {
                    throw new ApplicationException("Định dạng file phải là xlsx");
                }
                var baseDirectory = Directory.GetCurrentDirectory();

                string filePath = Path.Combine(baseDirectory, file.FileName);
                if (File.Exists(filePath))
                    File.Delete(filePath);
                ExcelPackage.LicenseContext = LicenseContext.Commercial;
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                    using (var package = new ExcelPackage(stream))
                    {

                        // Get the first worksheet from the package
                        var worksheet = package.Workbook.Worksheets[0];
                        var validate = ValidateExcel(worksheet);
                        if (!validate)
                        {
                            stream.Close();
                            File.Delete(filePath);
                            throw new ApplicationException("Sai định dạng trường dữ liệu");
                        }
                        // Read the data from the worksheet
                        var listUsers = new List<User>();
                        var rowCount = worksheet.Dimension.Rows;
                        //var columnCount = worksheet.Dimension.Columns;
                        for (int i = 2; i <= rowCount; ++i)
                        {

                            var username = worksheet.Cells[i, 1].Value?.ToString().Trim();
                            var firstname = worksheet.Cells[i, 2].Value?.ToString().Trim();
                            var lastname = worksheet.Cells[i, 3].Value?.ToString().Trim();
                            var password = worksheet.Cells[i, 4].Value?.ToString().Trim();
                            var email = worksheet.Cells[i, 5].Value?.ToString().Trim();
                            var phone = worksheet.Cells[i, 6].Value?.ToString().Trim();
                            var roleId = worksheet.Cells[i, 7].Value?.ToString().Trim();
                            var createdDate = worksheet.Cells[i, 8].Value?.ToString().Trim();
                            if (string.IsNullOrEmpty(username))
                                break;
                            var tmp = await _context.Users.FirstOrDefaultAsync(x => x.Email.Equals(email)
                            || x.Username.Equals(username));
                            if (tmp != null)
                            {
                                return new BaseResponseDTO<bool>(false, "Email hoặc username đã được sử dụng", StatusCodes.Status409Conflict);

                            }
                            listUsers.Add(new User
                            {
                                UserId = Guid.NewGuid(),
                                Username = username,
                                Firstname = firstname,
                                Lastname = lastname,
                                Password = password,
                                Email = email,
                                Phone = phone,
                                RoleId = int.Parse(roleId),
                                IsDeleted = false,
                                CreatedDate = string.IsNullOrEmpty(createdDate) ? DateTime.Now : DateTime.Parse(createdDate)
                            });
                        }
                        await _context.AddRangeAsync(listUsers);
                        await _context.SaveChangesAsync();
                    }
                    stream.Close();
                    File.Delete(filePath);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new BaseResponseDTO<bool>(true);
        }

        private bool ValidateExcel(ExcelWorksheet worksheet)
        {
            var rowCount = worksheet.Dimension.Rows;
            var columnCount = worksheet.Dimension.Columns;
            if (rowCount < 1)
            {
                return false;
            }
            string A1 = worksheet.Cells[1, 1].Value?.ToString().Trim();
            string B1 = worksheet.Cells[1, 2].Value?.ToString().Trim();
            string C1 = worksheet.Cells[1, 3].Value?.ToString().Trim();
            string D1 = worksheet.Cells[1, 4].Value?.ToString().Trim();
            string E1 = worksheet.Cells[1, 5].Value?.ToString().Trim();
            string F1 = worksheet.Cells[1, 6].Value?.ToString().Trim();
            string G1 = worksheet.Cells[1, 7].Value?.ToString().Trim();
            string H1 = worksheet.Cells[1, 8].Value?.ToString().Trim();
            if (A1 != a1_value || B1 != b1_value || C1 != c1_value || D1 != d1_value || E1 != e1_value || F1 != f1_value
                || G1 != g1_value || H1 != h1_value) return false;
            return true;
        }

        public async Task<BaseResponseDTO<bool>> ChangePassword(string email, string password)
        {
            var tmp = await _context.Users.FirstOrDefaultAsync(x => x.Email == email);
            if (tmp != null)
            {
                tmp.Password = password;
                _context.Update(tmp);
                await _context.SaveChangesAsync();
            }
            return new BaseResponseDTO<bool>(true);
        }

        
    }
}
