﻿using AutoMapper;
using FU.CableManagement.API.Infra.Constants;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CableDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    public class CableService : ICableService
    {
        private readonly ILogger<CableService> _logger;
        private readonly IConfiguration _configuration;
        private readonly CableManagementContext _context;
        private readonly IMapper _mapper;

        private const string a1_value = "Tên Loại Cáp";
        private const string b1_value = "Tên Kho Hàng";
        private const string c1_value = "Tên Nhà Cung Cấp";
        private const string d1_value = "Số Đầu";
        private const string e1_value = "Số Cuối";
        private const string f1_value = "Năm Sản Xuất";
        private const string g1_value = "Mã Hàng";
        private const string h1_value = "Trạng Thái";
        public CableService(ILogger<CableService> logger, IConfiguration configuration, CableManagementContext context, IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
            _mapper = mapper;
        }

        public async Task<BaseResponseDTO<bool>> DeleteCable(Guid cableId)
        {
            var tmp = await _context.Cables.FirstOrDefaultAsync(x => x.CableId == cableId && x.IsDeleted == false);
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.CableMessage.Cable_Not_Found);
            }
            tmp.IsDeleted = true;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        
        public async Task<BaseResponseDTO<PagedResultDTO<CableDTO>>> GetAllCables(string filter, int pageSize, int pageIndex, int? warehouseId, bool? isExportedToUse)
        {
            if (filter != null) filter = filter.Trim();
            var query = _context.Cables.Include(x => x.Warehouse).Include(x => x.CableCategory)
                .Include(x => x.Supplier)
                .Where(x => x.IsDeleted == false && x.IsInRequest == false && (string.IsNullOrEmpty(filter)
            || x.CableCategory.CableCategoryName.ToLower().Contains(filter.ToLower())
            || EF.Functions.Collate(x.CableCategory.CableCategoryName.ToLower(), "Latin1_General_CI_AI").Contains(filter.ToLower())
            || x.Code.ToLower().Contains(filter.ToLower())
            || x.Supplier.SupplierName.ToLower().Contains(filter.ToLower())
                ));
            if (isExportedToUse == true) query = query.Where(x => x.IsExportedToUse == true);
            else query = query.Where(x => x.IsExportedToUse == false);
            if (isExportedToUse == false)
            {
                if (warehouseId != null) query = query.Where(x => x.WarehouseId == warehouseId);
            }
            
            var sum = await query.SumAsync(x => x.Length);
            var data = await query.OrderByDescending(x => x.UpdateAt).Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .ToListAsync();
            var result = _mapper.Map<List<CableDTO>>(data);
            var pageResult = new PagedResultDTO<CableDTO>(pageIndex, pageSize, await query.CountAsync(), result, sum);
            return new BaseResponseDTO<PagedResultDTO<CableDTO>>(pageResult);
        }

        public async Task<BaseResponseDTO<CableDTO>> GetCableById(Guid cableId)
        {
            var data = await _context.Cables
                .Include(x => x.NodeCables.Where(x => x.IsDeleted == false))
                .ThenInclude(x => x.Node)
                .FirstOrDefaultAsync(x => x.CableId == cableId && x.IsDeleted == false);
            if (data == null)
            {
                throw new ApplicationException(MessageConst.CableMessage.Cable_Not_Found);
            }
            var result = _mapper.Map<CableDTO>(data);
            return new BaseResponseDTO<CableDTO>(result);
        }

        public async Task<BaseResponseDTO<bool>> AddCable(CableImportDTO model, string creatorId)
        {
            try
            {
                if (model.SupplierId == null || model.SupplierId == 0) throw new ApplicationException("Nhà cung cấp không được để trống");
                if (model.WarehouseId == null || model.WarehouseId == 0) throw new ApplicationException("Kho hàng không được để trống");
                var checkTmp = await _context.Cables.FirstOrDefaultAsync(x => x.Code.Equals(model.Code) && x.CableParentId == model.CableParentId
                && x.IsDeleted == false && x.Status.ToLower().Equals(model.Status.ToLower()) && x.StartPoint == model.StartPoint
                && x.EndPoint == model.EndPoint && x.SupplierId == model.SupplierId && x.YearOfManufacture == model.YearOfManufacture);
                if (checkTmp != null) throw new ApplicationException("Cáp đã tồn tại trong hệ thống");
                var tmp = _mapper.Map<Cable>(model);
                tmp.CableId = Guid.NewGuid();
                tmp.IsDeleted = false;
                tmp.Length = tmp.EndPoint - tmp.StartPoint;
                tmp.CreatorId = Guid.Parse(creatorId);
                await _context.AddAsync(tmp);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new BaseResponseDTO<bool>(true, "Thêm thành công", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<bool>> UpdateCable(CableUpdateDTO model)
        {
            try
            {
                if (model.SupplierId == null || model.SupplierId == 0) throw new ApplicationException("Nhà cung cấp không được để trống");
                if (model.WarehouseId == null || model.WarehouseId == 0) throw new ApplicationException("Kho hành không được để trống");
                var tmp = await _context.Cables.FirstOrDefaultAsync(x => x.CableId == model.CableId && x.IsDeleted == false);
                if (tmp == null)
                {
                    throw new ApplicationException(MessageConst.CableMessage.Cable_Not_Found);
                }
                tmp = _mapper.Map(model, tmp);
                tmp.Length = tmp.EndPoint - tmp.StartPoint;
                _context.Update(tmp);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true, "Cập nhật thành công", StatusCodes.Status200OK);
        }

        public async Task<List<Cable>> CutCable(CutCableDTO cutCableDTO)
        {
            try
            {
                var tmp = await _context.Cables.Include(x => x.Warehouse)
                    .Include(x => x.Supplier)
                    .FirstOrDefaultAsync(x => x.CableId == cutCableDTO.CableId);

                var listCables = new List<Cable>();
                var listCutPoint = new List<int>();
                if (cutCableDTO.StartPoint == tmp.StartPoint && cutCableDTO.EndPoint == tmp.EndPoint)
                {
                    listCables.Add(tmp);
                    return listCables;
                }
                if (cutCableDTO.StartPoint == tmp.StartPoint && cutCableDTO.EndPoint != tmp.EndPoint) listCutPoint.Add(cutCableDTO.EndPoint);
                if (cutCableDTO.EndPoint == tmp.EndPoint && cutCableDTO.StartPoint != tmp.StartPoint) listCutPoint.Add(cutCableDTO.StartPoint);
                if (cutCableDTO.StartPoint != tmp.StartPoint && cutCableDTO.EndPoint != tmp.EndPoint)
                {
                    listCutPoint.Add(cutCableDTO.StartPoint);
                    listCutPoint.Add(cutCableDTO.EndPoint);
                }
                for (int i = 0; i < listCutPoint.Count(); i++)
                {
                    Cable tmp1 = new Cable();
                    if (listCables.Count > i && listCables[i] != null)
                    {
                        tmp1 = listCables[i];
                        listCables.RemoveAt(i);
                    }
                    else
                    {
                        tmp1 = tmp;
                    }
                    listCables.AddRange(CutCable(tmp1, listCutPoint[i], Guid.Parse(cutCableDTO.CableParentId)));
                }
                tmp.IsDeleted = true;
                _context.Update(tmp);
                await _context.AddRangeAsync(listCables);
                await _context.SaveChangesAsync();
                return listCables;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private List<Cable> CutCable(Cable cable, int cutPoint, Guid? cableParentId)
        {
            var listCables = new List<Cable>();
            Cable newCable1 = new Cable
            {
                CableId = Guid.NewGuid(),
                CableCategoryId = cable.CableCategoryId,
                Code= cable.Code,
                WarehouseId = cable.WarehouseId,
                SupplierId = cable.SupplierId,
                CableParentId = cableParentId,
                StartPoint = cable.StartPoint,
                EndPoint = cutPoint,
                Length = cutPoint - cable.StartPoint,
                Status = cable.Status,
                IsDeleted = false,
                CreatedAt = DateTime.Now,
                YearOfManufacture = cable.YearOfManufacture,
                CreatorId = cable.CreatorId,
            };
            Cable newCable2 = new Cable
            {
                CableId = Guid.NewGuid(),
                CableCategoryId = cable.CableCategoryId,
                Code= cable.Code,
                WarehouseId = cable.WarehouseId,
                SupplierId = cable.SupplierId,
                CableParentId = cableParentId,
                StartPoint = cutPoint,
                EndPoint = cable.EndPoint,
                Length = cable.EndPoint - cutPoint,
                Status = cable.Status,
                IsDeleted = false,
                CreatedAt = DateTime.Now,
                YearOfManufacture = cable.YearOfManufacture,
                CreatorId = cable.CreatorId,
            };
            listCables.Add(newCable1);
            listCables.Add(newCable2);
            return listCables;
        }

        //public async Task<BaseResponseDTO<List<CableDTO>>> CutCables(List<CutCableDTO> cutCableDTOs)
        //{
        //    List<CableDTO> listCables = new List<CableDTO>();
        //    using (var transaction = await _context.Database.BeginTransactionAsync())
        //    {
        //        foreach (var cable in cutCableDTOs)
        //        {
        //            listCables.AddRange(CutCable(cable).Result.Data.ToList());
        //        }
        //        await transaction.CommitAsync();
        //    }
        //        return new BaseResponseDTO<List<CableDTO>>(listCables);
        //}

        public async Task<BaseResponseDTO<PagedResultDTO<CableDTO>>> GetCablesInAWarehouse(int warehouseId, int pageSize, int pageIndex)
        {
            var query = _context.Cables.Include(x => x.Warehouse).Include(x => x.CableCategory)
                .Include(x => x.Supplier)
                .Where(x => x.IsDeleted == false && (x.WarehouseId == warehouseId)
                );
            var data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .ToListAsync();
            var result = _mapper.Map<List<CableDTO>>(data);
            var pageResult = new PagedResultDTO<CableDTO>(pageIndex, pageSize, await query.CountAsync(), result);
            return new BaseResponseDTO<PagedResultDTO<CableDTO>>(pageResult);
        }

        public async Task<BaseResponseDTO<bool>> ImportExcelCables(IFormFile file, string creatorId)
        {
            
                if (file == null)
                {
                    throw new ApplicationException("File không được để trống");
                }
                var extension = Path.GetExtension(file.FileName);
                if (extension != ".xlsx")
                {
                    throw new ApplicationException("Định dạng file phài là xlsx");
                }
                var baseDirectory = Directory.GetCurrentDirectory();

                string filePath = Path.Combine(baseDirectory, file.FileName);
            try
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);
                ExcelPackage.LicenseContext = LicenseContext.Commercial;
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                    using (var package = new ExcelPackage(stream))
                    {

                        // Get the first worksheet from the package
                        var worksheet = package.Workbook.Worksheets[0];
                        var validate = ValidateExcel(worksheet);
                        if (!validate)
                        {
                            stream.Close();
                            File.Delete(filePath);
                            throw new ApplicationException("Sai định dạng trường dữ liệu");
                        }
                        // Read the data from the worksheet
                        var listCable = new List<Cable>();
                        var rowCount = worksheet.Dimension.Rows;
                        //var columnCount = worksheet.Dimension.Columns;
                        using (var transaction = await _context.Database.BeginTransactionAsync())
                        {
                            for (int i = 2; i <= rowCount; ++i)
                            {
                                var cableName = worksheet.Cells[i, 1].Value?.ToString().Trim();
                                var warehouse = worksheet.Cells[i, 2].Value?.ToString().Trim();
                                var supplier = worksheet.Cells[i, 3].Value?.ToString().Trim();
                                var startPoint = worksheet.Cells[i, 4].Value?.ToString().Trim();
                                var endPoint = worksheet.Cells[i, 5].Value?.ToString().Trim();
                                var yearOfManufacture = worksheet.Cells[i, 6].Value?.ToString().Trim();
                                var code = worksheet.Cells[i, 7].Value?.ToString().Trim();
                                var status = worksheet.Cells[i, 8].Value?.ToString().Trim();
                                if (string.IsNullOrEmpty(cableName) || string.IsNullOrEmpty(warehouse) || string.IsNullOrEmpty(code)
                                    || string.IsNullOrEmpty(startPoint) || string.IsNullOrEmpty(endPoint))
                                    throw new ApplicationException("Các ô dữ liệu bắt buộc không được để trống");
                                //break;
                                var warehouseId = await _context.Warehouses.Where(x => x.WarehouseName.ToLower().Equals(warehouse.ToLower()))
                                    .Select(x => x.WarehouseId).SingleOrDefaultAsync();
                                if (warehouseId == 0) throw new ApplicationException("Kho hàng \'" + warehouse + "\' không tồn tại trong hệ thống");
                                int? supplierId = null;
                                if (supplier != null) supplierId = await _context.Suppliers.Where(x => x.SupplierName.ToLower().Equals(supplier.ToLower()))
                                    .Select(x => x.SupplierId).SingleOrDefaultAsync();
                                var cableCategoryId = await _context.CableCategories.Where(x => x.CableCategoryName.ToLower().Equals(cableName.ToLower()))
                                    .Select(x => x.CableCategoryId).SingleOrDefaultAsync();
                                if (cableCategoryId == 0)
                                {
                                    //var newCableCategory = new CableCategory { CableCategoryName = cableName };
                                    //await _context.AddAsync(newCableCategory);
                                    //await _context.SaveChangesAsync();
                                    //cableCategoryId = newCableCategory.CableCategoryId;
                                    throw new ApplicationException("Hệ thống không có chủng loại cáp: " + cableName);
                                }
                                listCable.Add(new Cable
                                {
                                    CableId = Guid.NewGuid(),
                                    CableCategoryId = cableCategoryId,
                                    Code = code,
                                    WarehouseId = warehouseId,
                                    SupplierId = supplierId,
                                    //CableParentId = string.IsNullOrEmpty(cableParentId) ? null : Guid.Parse(cableParentId),
                                    StartPoint = int.Parse(startPoint),
                                    EndPoint = int.Parse(endPoint),
                                    Length = int.Parse(endPoint) - int.Parse(startPoint),
                                    Status = status,
                                    IsDeleted = false,
                                    CreatedAt = DateTime.Now,
                                    YearOfManufacture = int.Parse(yearOfManufacture),
                                    CreatorId = Guid.Parse(creatorId),
                                });
                            }
                            await _context.AddRangeAsync(listCable);
                            await _context.SaveChangesAsync();
                            await transaction.CommitAsync();
                        }
                    }
                    stream.Close();
                    File.Delete(filePath);
                }

                return new BaseResponseDTO<bool>(true);
            }
            catch(Exception ex) {
                if (File.Exists(filePath))
                    File.Delete(filePath);
                throw new Exception(ex.Message);
            }
        }

        private bool ValidateExcel(ExcelWorksheet worksheet)
        {
            var rowCount = worksheet.Dimension.Rows;
            var columnCount = worksheet.Dimension.Columns;
            if (rowCount < 1)
            {
                return false;
            }
            string A1 = worksheet.Cells[1, 1].Value?.ToString().Trim();
            string B1 = worksheet.Cells[1, 2].Value?.ToString().Trim();
            string C1 = worksheet.Cells[1, 3].Value?.ToString().Trim();
            string D1 = worksheet.Cells[1, 4].Value?.ToString().Trim();
            string E1 = worksheet.Cells[1, 5].Value?.ToString().Trim();
            string F1 = worksheet.Cells[1, 6].Value?.ToString().Trim();
            string G1 = worksheet.Cells[1, 7].Value?.ToString().Trim();
            string H1 = worksheet.Cells[1, 8].Value?.ToString().Trim();
            if (A1 != a1_value || B1 != b1_value || C1 != c1_value || D1 != d1_value || E1 != e1_value || F1 != f1_value
                || G1 != g1_value || H1 != h1_value ) return false;
            return true;
        }

        public async Task<List<Cable>> ImportExcelCablesInImportRequest(IFormFile file, string creatorId)
        {
            if (file == null)
            {
                throw new ApplicationException("File không được để trống");
            }
            var extension = Path.GetExtension(file.FileName);
            if (extension != ".xlsx")
            {
                throw new ApplicationException("Định dạng file phài là xlsx");
            }
            var baseDirectory = Directory.GetCurrentDirectory();

            string filePath = Path.Combine(baseDirectory, file.FileName);
            if (File.Exists(filePath))
                File.Delete(filePath);
                var listCable = new List<Cable>();
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {

                    // Get the first worksheet from the package
                    var worksheet = package.Workbook.Worksheets[0];
                    var validate = ValidateExcel(worksheet);
                    if (!validate)
                    {
                        stream.Close();
                        File.Delete(filePath);
                        throw new ApplicationException("Sai định dạng trường dữ liệu");
                    }
                    // Read the data from the worksheet
                    
                    var rowCount = worksheet.Dimension.Rows;
                    //var columnCount = worksheet.Dimension.Columns;
                    
                        for (int i = 2; i <= rowCount; ++i)
                        {
                            var cableName = worksheet.Cells[i, 1].Value?.ToString().Trim();
                            var warehouse = worksheet.Cells[i, 2].Value?.ToString().Trim();
                            var supplier = worksheet.Cells[i, 3].Value?.ToString().Trim();
                            var startPoint = worksheet.Cells[i, 4].Value?.ToString().Trim();
                            var endPoint = worksheet.Cells[i, 5].Value?.ToString().Trim();
                            var yearOfManufacture = worksheet.Cells[i, 6].Value?.ToString().Trim();
                            var code = worksheet.Cells[i, 7].Value?.ToString().Trim();
                            var status = worksheet.Cells[i, 8].Value?.ToString().Trim();
                            if (string.IsNullOrEmpty(cableName) || string.IsNullOrEmpty(warehouse) || string.IsNullOrEmpty(code)
                            || string.IsNullOrEmpty(startPoint) || string.IsNullOrEmpty(endPoint))
                                throw new ApplicationException("Các ô dữ liệu bắt buộc không được để trống");
                            //break;
                            var warehouseId = await _context.Warehouses.Where(x => x.WarehouseName.ToLower().Equals(warehouse.ToLower()))
                                .Select(x => x.WarehouseId).SingleOrDefaultAsync();
                            if (warehouseId == 0) throw new ApplicationException("Kho hàng \'" + warehouse + "\' không tồn tại trong hệ thống");
                            
                            var cableCategoryId = await _context.CableCategories.Where(x => x.CableCategoryName.ToLower().Equals(cableName.ToLower()))
                                .Select(x => x.CableCategoryId).SingleOrDefaultAsync();

                            int? supplierId = null;
                            if (supplier != null) supplierId = await _context.Suppliers.Where(x => x.SupplierName.ToLower().Equals(supplier.ToLower()))
                            .Select(x => x.SupplierId).SingleOrDefaultAsync();

                        if (cableCategoryId == 0)
                            {
                                //var newCableCategory = new CableCategory { CableCategoryName = cableName };
                                //await _context.AddAsync(newCableCategory);
                                //await _context.SaveChangesAsync();
                                //cableCategoryId = newCableCategory.CableCategoryId;
                                throw new ApplicationException("Hệ thống không có chủng loại cáp: " + cableName);
                            }
                            listCable.Add(new Cable
                            {
                                CableId = Guid.NewGuid(),
                                CableCategoryId = cableCategoryId,
                                Code = code,
                                WarehouseId = warehouseId,
                                SupplierId = supplierId,
                                //CableParentId = string.IsNullOrEmpty(cableParentId) ? null : Guid.Parse(cableParentId),
                                StartPoint = int.Parse(startPoint),
                                EndPoint = int.Parse(endPoint),
                                Length = int.Parse(endPoint) - int.Parse(startPoint),
                                Status = status,
                                IsDeleted = false,
                                CreatedAt = DateTime.Now,
                                YearOfManufacture = int.Parse(yearOfManufacture),
                                CreatorId = Guid.Parse(creatorId),
                                IsInRequest = true,
                            });
                        }
                        await _context.AddRangeAsync(listCable);
                        await _context.SaveChangesAsync();
                        
                }
                stream.Close();
                File.Delete(filePath);
            }

            return listCable;
        }

       
        
    }
}
