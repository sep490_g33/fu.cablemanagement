﻿using AutoMapper;
using FU.CableManagement.API.Infra.Constants;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.NodeDTOs;
using FU.CableManagement.DataAccess.DTO.RouteDTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    /// <summary>
    /// Route
    /// </summary>
    public class RouteService : IRouteService
    {
        private readonly CableManagementContext _context;
        private readonly IMapper _mapper;

        private const string a1_value = "STT";
        private const string b1_value = "Mã up";
        private const string c1_value = "Số hiệu trụ";
        private const string d1_value = "Địa chỉ";
        private const string e1_value = "Latitude";
        private const string f1_value = "Longitude";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="mapper"></param>
        public RouteService(CableManagementContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Create Route
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        public async Task<BaseResponseDTO<bool>> CreateRoute(CreateRouteDTO model)
        {
            var existedRoute = await _context.Routes.FirstOrDefaultAsync(x => x.RouteName.ToLower() == model.RouteName.ToLower() && x.IsDeleted == false);
            var listMatCate = await _context.OtherMaterialsCategories.Where(x => x.IsDeleted == false).ToListAsync();
            if (existedRoute != null)
                throw new ApplicationException(MessageConst.RouteMessage.Route_Name_Existed);
            int id = 0;
            foreach (var item in model.ListNodes)
            {
                id++;
                item.NumberOrder = id;
            }
            var route = _mapper.Map<Route>(model);
            await _context.AddAsync(route);

            var listNodeMaterial = new List<NodeMaterialCategory>();
            foreach (var node in route.ListNodes)
            {
                if (!string.IsNullOrEmpty(node.MaterialCategory))
                {
                    var listMat = node.MaterialCategory.Split(';').ToList();
                    listMat.ForEach(str =>
                    {
                        var nameMat = ConvertName(str);
                        var tmp = listMatCate.FirstOrDefault(x => x.OtherMaterialsCategoryName.Trim().ToLower() == nameMat.Item1.Trim().ToLower());
                        if (tmp != null)
                        {
                            listNodeMaterial.Add(new NodeMaterialCategory
                            {
                                NodeId = node.Id,
                                OtherMaterialCategoryId = tmp.OtherMaterialsCategoryId,
                                Quantity = nameMat.Item2
                            });
                        }
                    });
                }
            }
            await _context.AddRangeAsync(listNodeMaterial);

            try
            {

                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {

            }
            return new BaseResponseDTO<bool>(true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RouteId"></param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        public async Task<BaseResponseDTO<bool>> DeleteRoute(Guid RouteId)
        {
            var existedRoute = await _context.Routes.Include(x => x.ListNodes).FirstOrDefaultAsync(x => x.RouteId == RouteId);
            if (existedRoute == null)
                throw new ApplicationException(MessageConst.RouteMessage.Route_Not_Found);
            existedRoute.IsDeleted = true;
            foreach (var node in existedRoute.ListNodes)
            {
                node.IsDeleted = true;
            }
            _context.Update(existedRoute);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        public async Task<BaseResponseDTO<bool>> UpdateRoute(UpdateRouteDTO model)
        {
            var existedRoute = await _context.Routes
                .Include(x => x.ListNodes)
                .ThenInclude(x => x.NodeMaterialCategories).FirstOrDefaultAsync(x => x.RouteId == model.RouteId);
            if (existedRoute == null)
                throw new ApplicationException(MessageConst.RouteMessage.Route_Not_Found);
            var listMatCate = await _context.OtherMaterialsCategories.Where(x => x.IsDeleted == false).ToListAsync();
            existedRoute.RouteName = model.RouteName;
            foreach (var item in existedRoute.ListNodes)
            {
                var tmp = model.ListNodeUpdates.FirstOrDefault(x => x.Id == item.Id);
                if (tmp == null)
                {
                    item.Route = null;
                    item.RouteId = null;
                    item.IsDeleted = true;
                }
            }

            var listAddNode = new List<Node>();
            var listNodeMaterial = new List<NodeMaterialCategory>();
            int id = 0;

            foreach (var item in model.ListNodeUpdates)
            {
                var tmp = existedRoute.ListNodes.FirstOrDefault(x => x.IsDeleted == false && x.Id == item.Id);
                var node = _mapper.Map<Node>(item);
                if (tmp == null)
                {
                    node.Route = null;
                    node.RouteId = existedRoute.RouteId;
                    node.NumberOrder = ++id;
                    listAddNode.Add(node);
                }
                else
                {
                    tmp.NumberOrder = ++id;
                    tmp.NodeMaterialCategories.ToList().ForEach(x =>
                    {
                        x.IsDeleted = true;
                    });
                }
                if (!string.IsNullOrEmpty(item.MaterialCategory))
                {
                    var listMat = item.MaterialCategory.Split(';').ToList();
                    listMat.ForEach(str =>
                    {
                        var nameMat = ConvertName(str);
                        var tmp_2 = listMatCate.FirstOrDefault(x => x.OtherMaterialsCategoryName.Trim().ToLower() == nameMat.Item1.Trim().ToLower());
                        if (tmp_2 != null)
                        {
                            var addNodeMat = new NodeMaterialCategory
                            {
                                NodeId = node.Id,
                                OtherMaterialCategoryId = tmp_2.OtherMaterialsCategoryId,
                                Quantity = nameMat.Item2
                            };
                            if (tmp == null)
                            {
                                addNodeMat.Node = node;
                            }
                            else
                            {
                                addNodeMat.NodeId = tmp.Id;
                            }
                            listNodeMaterial.Add(addNodeMat);
                        }
                    });
                }
            }
            await _context.AddRangeAsync(listNodeMaterial);
            await _context.AddRangeAsync(listAddNode);
            _context.Update(existedRoute);
            await _context.SaveChangesAsync();

            return new BaseResponseDTO<bool>(true);
        }

        /// <summary>
        /// Get All Route with Filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<BaseResponseDTO<PagedResultDTO<RouteDTO>>> GetAllRoute(string filter, int pageIndex, int pageSize)
        {
            var query = _context.Routes
                .Include(x => x.ListNodes.Where(x => x.IsDeleted == false))
               .Where(x => x.IsDeleted == false && (string.IsNullOrEmpty(filter) || x.RouteName.ToLower().Contains(filter.ToLower())));
            var data = await query.OrderByDescending(x => x.UpdateAt).Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .ToListAsync();
            var result = _mapper.Map<List<RouteDTO>>(data);
            foreach (var item in result)
            {
                item.ListNodes = item.ListNodes.OrderBy(x => x.NumberOrder).ToList();
                item.ListNodes.ToList().ForEach(
                    x => x.Route = null);

            }
            var pageResult = new PagedResultDTO<RouteDTO>(pageIndex, pageSize, await query.CountAsync(), result);
            return new BaseResponseDTO<PagedResultDTO<RouteDTO>>(pageResult);
        }

        public async Task<BaseResponseDTO<List<string>>> ImportRouteExcel(IFormFile file)
        {
            var errors = new List<string>();
            try
            {
                if (file == null)
                {
                    throw new ApplicationException("File không được để trống");
                }
                var extension = Path.GetExtension(file.FileName);
                if (extension != ".xlsx")
                {
                    throw new ApplicationException("Định dạng file phải là xlsx");
                }
                var baseDirectory = Directory.GetCurrentDirectory();

                string filePath = Path.Combine(baseDirectory, file.FileName);
                if (File.Exists(filePath))
                    File.Delete(filePath);
                ExcelPackage.LicenseContext = LicenseContext.Commercial;
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                    using (var package = new ExcelPackage(stream))
                    {

                        // Get the first worksheet from the package
                        var numberOfSheet = package.Workbook.Worksheets.Count();
                        foreach (var worksheet in package.Workbook.Worksheets)
                        {
                            var sheetName = worksheet.Name;
                            if (!ValidateExcel(worksheet))
                            {
                                errors.Add($"Tran tính {sheetName} sai định tiêu đề/trường dữ liệu!");
                                continue;
                            }
                            var existedRoute = await _context.Routes.FirstOrDefaultAsync(x => x.RouteName.ToLower() == sheetName.Trim().ToLower() && x.IsDeleted == false);
                            if (existedRoute != null)
                            {
                                var listNodes = GetUpdateNodesFromExcel(worksheet);
                                var updateRoute = new UpdateRouteDTO
                                {
                                    ListNodeUpdates = listNodes,
                                    RouteId = existedRoute.RouteId,
                                    RouteName = sheetName
                                };
                                await UpdateRoute(updateRoute);
                            }
                            else
                            {
                                var listNodes = GetCreateNodesFromExcel(worksheet);
                                var createRoute = new CreateRouteDTO
                                {
                                    RouteName = sheetName,
                                    ListNodes = listNodes
                                };
                                await CreateRoute(createRoute);
                            }
                        }
                    }
                    stream.Close();
                    File.Delete(filePath);
                }
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message);
            }
            return new BaseResponseDTO<List<string>>(errors);
        }

        private bool ValidateExcel(ExcelWorksheet worksheet)
        {
            if (worksheet.Dimension == null) return false;
            var rowCount = worksheet.Dimension.Rows;
            var columnCount = worksheet.Dimension.Columns;
            if (rowCount < 1)
            {
                return false;
            }
            string A1 = worksheet.Cells[2, 1].Value?.ToString().Trim();
            string B1 = worksheet.Cells[2, 2].Value?.ToString().Trim();
            string C1 = worksheet.Cells[2, 3].Value?.ToString().Trim();
            string D1 = worksheet.Cells[2, 4].Value?.ToString().Trim();
            string E1 = worksheet.Cells[2, 5].Value?.ToString().Trim();
            string F1 = worksheet.Cells[2, 6].Value?.ToString().Trim();
            if (A1 != a1_value || B1 != b1_value || C1 != c1_value || D1 != d1_value || E1 != e1_value || F1 != f1_value) return false;
            return true;
        }

        private List<CreateNodeDTO> GetCreateNodesFromExcel(ExcelWorksheet worksheet)
        {
            var result = new List<CreateNodeDTO>();
            var rowCount = worksheet.Dimension.Rows;
            //var columnCount = worksheet.Dimension.Columns;
            for (int i = 3; i <= rowCount; ++i)
            {

                var nodeCode = worksheet.Cells[i, 2].Value?.ToString().Trim();
                var nodeNumberSign = worksheet.Cells[i, 3].Value?.ToString().Trim();
                var adddress = worksheet.Cells[i, 4].Value?.ToString().Trim();
                var longitude = worksheet.Cells[i, 6].Value?.ToString().Trim();
                var latitude = worksheet.Cells[i, 5].Value?.ToString().Trim();
                var materialCategory = worksheet.Cells[i, 9].Value?.ToString().Trim();
                var name = string.Empty;
                var status = worksheet.Cells[i, 13].Value?.ToString().Trim();
                var note = worksheet.Cells[i, 14].Value?.ToString().Trim();
                if (string.IsNullOrEmpty(nodeCode))
                    break;
                if (!float.TryParse(longitude, out var _))
                    throw new ApplicationException(MessageConst.NodeMessage.Cannot_Parse_Double);
                if (!float.TryParse(latitude, out var _))
                    throw new ApplicationException(MessageConst.NodeMessage.Cannot_Parse_Double);
                result.Add(new CreateNodeDTO
                {
                    Name = name,
                    NodeCode = nodeCode,
                    NodeNumberSign = nodeNumberSign,
                    Address = adddress,
                    Longitude = float.Parse(longitude),
                    Latitude = float.Parse(latitude),
                    Status = status,
                    Note = note,
                    MaterialCategory = materialCategory,
                });
            }
            return result;
        }

        private List<UpdateNodeDTO> GetUpdateNodesFromExcel(ExcelWorksheet worksheet)
        {
            var result = new List<UpdateNodeDTO>();
            var rowCount = worksheet.Dimension.Rows;
            //var columnCount = worksheet.Dimension.Columns;
            for (int i = 3; i <= rowCount; ++i)
            {
                var nodeCode = worksheet.Cells[i, 2].Value?.ToString().Trim();
                var nodeNumberSign = worksheet.Cells[i, 3].Value?.ToString().Trim();
                var adddress = worksheet.Cells[i, 4].Value?.ToString().Trim();
                var longitude = worksheet.Cells[i, 6].Value?.ToString().Trim();
                var latitude = worksheet.Cells[i, 5].Value?.ToString().Trim();
                var materialCategory = worksheet.Cells[i, 9].Value?.ToString().Trim();
                var name = string.Empty;
                var status = worksheet.Cells[i, 13].Value?.ToString().Trim();

                var note = worksheet.Cells[i, 14].Value?.ToString().Trim();

                if (string.IsNullOrEmpty(nodeCode))
                    break;
                if (!float.TryParse(longitude, out var _))
                    throw new ApplicationException(MessageConst.NodeMessage.Cannot_Parse_Double);
                if (!float.TryParse(latitude, out var _))
                    throw new ApplicationException(MessageConst.NodeMessage.Cannot_Parse_Double);
                result.Add(new UpdateNodeDTO
                {
                    Name = name,
                    NodeCode = nodeCode,
                    NodeNumberSign = nodeNumberSign,
                    Address = adddress,
                    Longitude = float.Parse(longitude),
                    Latitude = float.Parse(latitude),
                    Status = status,
                    Note = note,
                    MaterialCategory = materialCategory,
                });
            }
            return result;
        }

        private Tuple<string, int> ConvertName(string name)
        {
            var index = name.IndexOf('(');
            if (index == -1) return new Tuple<string, int>(name, 1);
            var indexEnd = name.IndexOf(')');
            if (indexEnd <= index) return new Tuple<string, int>(name, 1);
            var ftName = name.Substring(0, index);
            var number = name.Substring(index + 1, indexEnd - index - 1);
            ftName = ftName.Trim();
            number = number.Trim();
            if (!int.TryParse(number, out var _))
            {
                return new Tuple<string, int>(ftName, 1);
            }
            return new Tuple<string, int>(ftName, int.Parse(number));
        }
    }
}

