﻿using FU.CableManagement.API.Infra.Constants;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.StatisticDTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    public class StatisticService : IStatisticService
    {
        private readonly CableManagementContext _context;
        public StatisticService(CableManagementContext context)
        {
            _context = context;
        }

        public async Task<BaseResponseDTO<ExchangeMaterial>> GetExchangeMaterial(int? warehouseId, DateTime? startDate, DateTime? endDate)
        {
            var data = new ExchangeMaterial();
            if (startDate == null)
            {
                startDate = DateTime.MinValue;
            }
            if (endDate == null)
            {
                endDate = DateTime.MaxValue;
            }
            var listRequestCable = await _context.RequestCables.Include(x => x.Request).ThenInclude(x => x.RequestCategory).Where(x => x.CreatedAt >= startDate && x.CreatedAt <= endDate).ToListAsync();
            var listRequestMaterial = await _context.RequestOtherMaterials.Include(x => x.Request).ThenInclude(x => x.RequestCategory).Where(x => x.CreatedAt >= startDate && x.CreatedAt <= endDate).ToListAsync();

            data.ExportMaterials = listRequestMaterial.Where(x => x.Request.RequestCategory.RequestCategoryName == "Xuất Kho"
                        && x.Request.Status == RequestStatusConstant.Approved)
                .Sum(x => x.Quantity);

            data.ExportCables = listRequestCable.Where(x => x.Request.RequestCategory.RequestCategoryName == "Xuất Kho"
                && x.Request.Status == RequestStatusConstant.Approved)
                .Sum(x => x.Length);

            data.ImportMaterials = listRequestMaterial.Where(x => x.Request.RequestCategory.RequestCategoryName == "Nhập Kho"
                && x.Request.Status == RequestStatusConstant.Approved)
                .Sum(x => x.Quantity);

            data.ImportCables = listRequestCable.Where(x => x.Request.RequestCategory.RequestCategoryName == "Nhập Kho"
                && x.Request.Status == RequestStatusConstant.Approved)
                .Sum(x => x.Length);
            return new BaseResponseDTO<ExchangeMaterial>(data);
        }

        public async Task<BaseResponseDTO<MaterialFluctuation>> GetMaterialFluctuation(string materialOrCableName, DateTime? startDate, DateTime? endDate)
        {
            materialOrCableName = materialOrCableName.Trim();
            var data = new MaterialFluctuation();
            if (startDate == null)
            {
                startDate = DateTime.MinValue;
            }
            if (endDate == null)
            {
                endDate = DateTime.MaxValue;
            }
            var check = IsCableOrOtherMaterial(materialOrCableName).Result;

            if (check == true)
            {
                var material = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory.OtherMaterialsCategoryName).FirstOrDefaultAsync();
                var listMaterials = await _context.RequestOtherMaterials.Include(x => x.Request).ThenInclude(x => x.RequestCategory)
                    .Include(x => x.OtherMaterials).ThenInclude(x => x.OtherMaterialsCategory)
                    .Where(x => x.CreatedAt <= endDate && x.OtherMaterials.OtherMaterialsCategory.OtherMaterialsCategoryName == materialOrCableName
                        && x.Request.Status == RequestStatusConstant.Approved)
                    .ToListAsync();
                var listMaterialsBefore = await _context.RequestOtherMaterials.Include(x => x.Request).ThenInclude(x => x.RequestCategory)
                    .Include(x => x.OtherMaterials).ThenInclude(x => x.OtherMaterialsCategory)
                    .Where(x => x.CreatedAt <= startDate && x.OtherMaterials.OtherMaterialsCategory.OtherMaterialsCategoryName == materialOrCableName
                        && x.Request.Status == RequestStatusConstant.Approved)
                    .ToListAsync();
                var sumAfter = listMaterials.Sum(x => x.Quantity);
                var sumBefore = listMaterialsBefore.Sum(x => x.Quantity);

                data.MaterialName = materialOrCableName;
                data.Unit = material.Unit;
                data.Variation = sumAfter - sumBefore;
            }
            if (check == false)
            {
                var cable = await _context.Cables.Include(x => x.CableCategory.CableCategoryName).FirstOrDefaultAsync();
                var listMaterials = await _context.RequestCables.Include(x => x.Request).ThenInclude(x => x.RequestCategory)
                    .Include(x => x.Cable).ThenInclude(x => x.CableCategory)
                    .Where(x => x.CreatedAt <= endDate && x.Cable.CableCategory.CableCategoryName == materialOrCableName
                        && x.Request.Status == RequestStatusConstant.Approved)
                    .ToListAsync();
                var listMaterialsBefore = await _context.RequestCables.Include(x => x.Request).ThenInclude(x => x.RequestCategory)
                    .Include(x => x.Cable).ThenInclude(x => x.CableCategory)
                    .Where(x => x.CreatedAt <= startDate && x.Cable.CableCategory.CableCategoryName == materialOrCableName
                        && x.Request.Status == RequestStatusConstant.Approved)
                    .ToListAsync();
                var sumAfter = listMaterials.Sum(x => x.Length);
                var sumBefore = listMaterialsBefore.Sum(x => x.Length);

                data.MaterialName = materialOrCableName;
                data.Unit = "M";
                data.Variation = sumAfter - sumBefore;
            }
            return new BaseResponseDTO<MaterialFluctuation>(data);
        }

        private async Task<bool?> IsCableOrOtherMaterial(string materialName) //true: Other Material, false: Cable
        {
            bool? result = null;
            var tmpMat = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory)
                .FirstOrDefaultAsync(x => x.OtherMaterialsCategory.OtherMaterialsCategoryName.ToLower() == materialName.ToLower());
            var tmpCab = await _context.Cables.Include(x => x.CableCategory)
                .FirstOrDefaultAsync(x => x.CableCategory.CableCategoryName.ToLower() == materialName.ToLower());
            if (tmpMat != null && tmpCab == null) { result = true; }
            if (tmpCab != null && tmpMat == null) { result = false; }
            return result;
        }
        public async Task<BaseResponseDTO<IssueStatistic>> GetStatisticOfIssues(DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null)
            {
                startDate = DateTime.MinValue;
            }
            if (endDate == null)
            {
                endDate = DateTime.MaxValue;
            }
            var data = new IssueStatistic();
            var doneIssue = await _context.Issues.Where(x => x.Status == IssueStatusConstants.Done).ToListAsync();
            var totalIssue = await _context.Issues.ToListAsync();
            var doingIssue = totalIssue.Count - doneIssue.Count;

            data.TotalIssue = totalIssue.Count;
            data.TotalIssueDone = doneIssue.Count;
            data.TotalIssueDoing = doingIssue;

            return new BaseResponseDTO<IssueStatistic>(data);
        }

        public async Task<BaseResponseDTO<MaterialFluctuationPerYear>> GetMaterialFluctuationPerYear(int? materialCategoryId, int? warehouseId, int? year)
        {
            var data = new MaterialFluctuationPerYear();
            if (year == null) year = DateTime.Now.Year;
            var query = _context.TransactionOtherMaterials.Include(x => x.Transaction)
                    .Include(x => x.OtherMaterials).ThenInclude(x => x.OtherMaterialsCategory)
                    .Where(x => x.Transaction.CreatedAt.Year <= year);
            if (materialCategoryId != null)
            {
                var material = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory)
                .FirstOrDefaultAsync(x => x.OtherMaterialsCategoryId == materialCategoryId);
                if (material == null)
                {
                    throw new ApplicationException("Vật liệu không tồn tại");
                }
                data.MaterialName = material.OtherMaterialsCategory.OtherMaterialsCategoryName;
                query = query.Where(x => x.OtherMaterials.OtherMaterialsCategory.OtherMaterialsCategoryId == materialCategoryId
                            );
            }
            if (warehouseId != null)
            {
                data.WarehouseId = warehouseId;
                query = query.Where(x => x.Transaction.WarehouseId == warehouseId);
            }
            var increaseInJan = await query.Where(x => x.Transaction.CreatedAt.Month == 1 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Quantity);
            var decreaseInJan = await query.Where(x => x.Transaction.CreatedAt.Month == 1 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
                || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
                .SumAsync(x => x.Quantity);
            data.QuantityInJanuary = increaseInJan - decreaseInJan;

            var increaseInFeb = await query.Where(x => x.Transaction.CreatedAt.Month == 2 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Quantity);
            var decreaseInFeb = await query.Where(x => x.Transaction.CreatedAt.Month == 2 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Quantity);
            data.QuantityInFebruary = increaseInFeb - decreaseInFeb;
            var increaseInMar = await query.Where(x => x.Transaction.CreatedAt.Month == 3 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Quantity);
            var decreaseInMar = await query.Where(x => x.Transaction.CreatedAt.Month == 3 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Quantity);
            data.QuantityInMarch = increaseInMar - decreaseInMar;
            var increaseInApr = await query.Where(x => x.Transaction.CreatedAt.Month == 4 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Quantity);
            var decreaseInApr = await query.Where(x => x.Transaction.CreatedAt.Month == 4 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Quantity);
            data.QuantityInApril = increaseInApr - decreaseInApr;
            var increaseInMay = await query.Where(x => x.Transaction.CreatedAt.Month == 5 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Quantity);
            var decreaseInMay = await query.Where(x => x.Transaction.CreatedAt.Month == 5 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Quantity);
            data.QuantityInMay = increaseInMay - decreaseInMay;
            var increaseInJun = await query.Where(x => x.Transaction.CreatedAt.Month == 6 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Quantity);
            var decreaseInJun = await query.Where(x => x.Transaction.CreatedAt.Month == 6 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Quantity);
            data.QuantityInJune = increaseInJun - decreaseInJun;
            var increaseInJul = await query.Where(x => x.Transaction.CreatedAt.Month == 7 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Quantity);
            var decreaseInJul = await query.Where(x => x.Transaction.CreatedAt.Month == 7 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Quantity);
            data.QuantityInJuly = increaseInJul - decreaseInJul;
            var increaseInAug = await query.Where(x => x.Transaction.CreatedAt.Month == 8 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Quantity);
            var decreaseInAug = await query.Where(x => x.Transaction.CreatedAt.Month == 8 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Quantity);
            data.QuantityInAugust = increaseInAug - decreaseInAug;
            var increaseInSep = await query.Where(x => x.Transaction.CreatedAt.Month == 9 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Quantity);
            var decreaseInSep = await query.Where(x => x.Transaction.CreatedAt.Month == 9 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Quantity);
            data.QuantityInSeptember = increaseInSep - decreaseInSep;
            var increaseInOct = await query.Where(x => x.Transaction.CreatedAt.Month == 10 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Quantity);
            var decreaseInOct = await query.Where(x => x.Transaction.CreatedAt.Month == 10 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Quantity);
            data.QuantityInOctober = increaseInOct - decreaseInOct;
            var increaseInNov = await query.Where(x => x.Transaction.CreatedAt.Month == 11 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
               .SumAsync(x => x.Quantity);
            var decreaseInNov = await query.Where(x => x.Transaction.CreatedAt.Month == 11 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Quantity);
            data.QuantityInNovember = increaseInNov - decreaseInNov;
            var increaseInDec = await query.Where(x => x.Transaction.CreatedAt.Month == 12 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
               .SumAsync(x => x.Quantity);
            var decreaseInDec = await query.Where(x => x.Transaction.CreatedAt.Month == 12 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Quantity);
            data.QuantityInDecember = increaseInDec - decreaseInDec;
            return new BaseResponseDTO<MaterialFluctuationPerYear>(data);
        }

        public async Task<BaseResponseDTO<CableFluctuationPerYear>> GetCableFluctuationPerYear(int? cableCategoryId, int? warehouseId, int? year)
        {
            var data = new CableFluctuationPerYear();
            if (year == null) year = DateTime.Now.Year;
            var query = _context.TransactionCables.Include(x => x.Transaction)
                    .Include(x => x.Cable).ThenInclude(x => x.CableCategory)
                    .Where(x => x.Transaction.CreatedAt.Year == year);
            if (cableCategoryId != null)
            {
                var cable = await _context.Cables.Include(x => x.CableCategory)
                .FirstOrDefaultAsync(x => x.CableCategoryId == cableCategoryId);
                if (cable == null)
                {
                    throw new ApplicationException("Cáp không tồn tại");
                }
                data.CableName = cable.CableCategory.CableCategoryName;
                query = query.Where(x => x.Cable.CableCategoryId == cableCategoryId);
            }
            if (warehouseId != null)
            {
                data.WarehouseId = warehouseId;
                query = query.Where(x => x.Transaction.WarehouseId == warehouseId);
            }
            var increaseInJan = await query.Where(x => x.Transaction.CreatedAt.Month == 1 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Length);
            var decreaseInJan = await query.Where(x => x.Transaction.CreatedAt.Month == 1 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
                || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
                .SumAsync(x => x.Length);
            data.LengthInJanuary = increaseInJan - decreaseInJan;

            var increaseInFeb = await query.Where(x => x.Transaction.CreatedAt.Month == 2 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Length);
            var decreaseInFeb = await query.Where(x => x.Transaction.CreatedAt.Month == 2 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Length);
            data.LengthInFebruary = increaseInFeb - decreaseInFeb;
            var increaseInMar = await query.Where(x => x.Transaction.CreatedAt.Month == 3 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Length);
            var decreaseInMar = await query.Where(x => x.Transaction.CreatedAt.Month == 3 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Length);
            data.LengthInMarch = increaseInMar - decreaseInMar;
            var increaseInApr = await query.Where(x => x.Transaction.CreatedAt.Month == 4 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Length);
            var decreaseInApr = await query.Where(x => x.Transaction.CreatedAt.Month == 4 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Length);
            data.LengthInApril = increaseInApr - decreaseInApr;
            var increaseInMay = await query.Where(x => x.Transaction.CreatedAt.Month == 5 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Length);
            var decreaseInMay = await query.Where(x => x.Transaction.CreatedAt.Month == 5 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Length);
            data.LengthInMay = increaseInMay - decreaseInMay;
            var increaseInJun = await query.Where(x => x.Transaction.CreatedAt.Month == 6 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Length);
            var decreaseInJun = await query.Where(x => x.Transaction.CreatedAt.Month == 6 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Length);
            data.LengthInJune = increaseInJun - decreaseInJun;
            var increaseInJul = await query.Where(x => x.Transaction.CreatedAt.Month == 7 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Length);
            var decreaseInJul = await query.Where(x => x.Transaction.CreatedAt.Month == 7 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Length);
            data.LengthInJuly = increaseInJul - decreaseInJul;
            var increaseInAug = await query.Where(x => x.Transaction.CreatedAt.Month == 8 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Length);
            var decreaseInAug = await query.Where(x => x.Transaction.CreatedAt.Month == 8 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Length);
            data.LengthInAugust = increaseInAug - decreaseInAug;
            var increaseInSep = await query.Where(x => x.Transaction.CreatedAt.Month == 9 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Length);
            var decreaseInSep = await query.Where(x => x.Transaction.CreatedAt.Month == 9 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Length);
            data.LengthInSeptember = increaseInSep - decreaseInSep;
            var increaseInOct = await query.Where(x => x.Transaction.CreatedAt.Month == 10 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
                .SumAsync(x => x.Length);
            var decreaseInOct = await query.Where(x => x.Transaction.CreatedAt.Month == 10 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Length);
            data.LengthInOctober = increaseInOct - decreaseInOct;
            var increaseInNov = await query.Where(x => x.Transaction.CreatedAt.Month == 11 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
               .SumAsync(x => x.Length);
            var decreaseInNov = await query.Where(x => x.Transaction.CreatedAt.Month == 11 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Length);
            data.LengthInNovember = increaseInNov - decreaseInNov;
            var increaseInDec = await query.Where(x => x.Transaction.CreatedAt.Month == 12 && x.Transaction.TransactionCategoryName == TransactionCategoryContants.Import)
               .SumAsync(x => x.Length);
            var decreaseInDec = await query.Where(x => x.Transaction.CreatedAt.Month == 12 && (x.Transaction.TransactionCategoryName == TransactionCategoryContants.Export
               || x.Transaction.TransactionCategoryName == TransactionCategoryContants.Cancel))
               .SumAsync(x => x.Length);
            data.LengthInDecember = increaseInDec - decreaseInDec;
            return new BaseResponseDTO<CableFluctuationPerYear>(data);
        }

        public async Task<BaseResponseDTO<List<CableCategoryStatistic>>> GetCableCategoryStatistic(int? warehouseId)
        {
            var data = new List<CableCategoryStatistic>();
            IQueryable<Cable> query = _context.Cables.Include(x => x.CableCategory);
            if (warehouseId != null)
            {
                query = query.Where(x => x.WarehouseId == (int)warehouseId);
            }
            var listCate = await query.Where(x => x.IsExportedToUse == false && x.IsInRequest == false && x.IsDeleted == false)
                .GroupBy(x => x.CableCategoryId).Select(g => new
                {
                    CableCategoryId = g.Key,
                    CableCategory = g.Where(c => c.CableCategoryId == g.Key).Select(c => c.CableCategory.CableCategoryName).FirstOrDefault(),
                    SumOfLength = g.Select(l => l.Length).Sum(),
                }).ToListAsync();
            foreach (var item in listCate)
            {
                data.Add(new CableCategoryStatistic { CategoryId = item.CableCategoryId, CategoryName = item.CableCategory, Length = item.SumOfLength });
            }
            return new BaseResponseDTO<List<CableCategoryStatistic>>(data);
        }

        public async Task<BaseResponseDTO<List<OtherMaterialCateogoryStatistic>>> GetOtherMaterialCategoryStatistic(int? warehouseId)
        {
            var data = new List<OtherMaterialCateogoryStatistic>();
            IQueryable<OtherMaterial> query = _context.OtherMaterials.Include(x => x.OtherMaterialsCategory);
            if (warehouseId != null)
            {
                query = query.Where(x => x.WarehouseId == (int)warehouseId);
            }
            var listCate = await query.Where(x => x.IsDeleted == false)
                .GroupBy(x => x.OtherMaterialsCategoryId).Select(g => new
                {
                    OtherMaterialCategoryId = g.Key,
                    OtherMaterialCategory = g.Where(c => c.OtherMaterialsCategoryId == g.Key).Select(c => c.OtherMaterialsCategory.OtherMaterialsCategoryName).FirstOrDefault(),
                    SumOfQuantity = (int)g.Select(q => q.Quantity).Sum(),
                }).ToListAsync();
            foreach (var item in listCate)
            {
                data.Add(new OtherMaterialCateogoryStatistic { CategoryId = item.OtherMaterialCategoryId, CategoryName = item.OtherMaterialCategory, Quantity = item.SumOfQuantity });
            }
            return new BaseResponseDTO<List<OtherMaterialCateogoryStatistic>>(data);
        }

        public async Task<BaseResponseDTO<List<RouteStatisticDTO>>> GetMaterialInRoute(Guid RouteId)
        {
            var route = await _context.Routes.Include(x => x.ListNodes)
                .ThenInclude(x => x.NodeMaterialCategories).FirstOrDefaultAsync(x => x.RouteId == RouteId);
            if (route == null)
            {
                throw new ApplicationException(MessageConst.RouteMessage.Route_Not_Found);
            }
            var listNodeMaterialCategory = route.ListNodes.SelectMany(x => x.NodeMaterialCategories).ToList();

            var listMaterialCategory = await _context.OtherMaterialsCategories
                .Where(mat => listNodeMaterialCategory.Select(x => x.OtherMaterialCategoryId).Contains(mat.OtherMaterialsCategoryId)).ToListAsync();
            var result = new List<RouteStatisticDTO>();
            foreach (var mat in listMaterialCategory)
            {
                result.Add(new RouteStatisticDTO
                {
                    OtherMaterialCategoryId = mat.OtherMaterialsCategoryId,
                    OtherMaterialCategoryName = mat.OtherMaterialsCategoryName,
                    Quantity = listNodeMaterialCategory.Where(x => x.OtherMaterialCategoryId == mat.OtherMaterialsCategoryId && x.IsDeleted == false).Sum(x => x.Quantity)
                });
            }
            return new BaseResponseDTO<List<RouteStatisticDTO>>(result);
        }
    }
}
