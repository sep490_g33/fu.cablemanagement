﻿using AutoMapper;
using DocumentFormat.OpenXml.Bibliography;
using FU.CableManagement.API.Infra.Constants;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.IssueDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialsDTOs;
using FU.CableManagement.DataAccess.DTO.RequestCategoryDTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    public class RequestCategoryService : IRequestCategoryService
    {
        private readonly ILogger<RequestCategoryService> _logger;
        private readonly IConfiguration _configuration;
        private readonly CableManagementContext _context;
        private readonly IMapper _mapper;

        public RequestCategoryService(ILogger<RequestCategoryService> logger, IConfiguration configuration, CableManagementContext context, IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
            _mapper = mapper;
        }

        public async Task<BaseResponseDTO<bool>> AddRequestCategory(RequestCategoryDTO requestCategoryDTO)
        {
            var tmp = _mapper.Map<RequestCategory>(requestCategoryDTO);
            await _context.AddAsync(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<bool>> DeleteRequestCategory(int requestCategoryid)
        {
            var tmp = await _context.RequestCategories.FirstOrDefaultAsync(x => x.RequestCategoryId == requestCategoryid && x.IsDeleted == false);
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.RequestCategoryMessage.Request_Category_Not_Found);
            }
            tmp.IsDeleted = true;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<PagedResultDTO<RequestCategoryDTO>>> GetAllRequestCategory(string filter, int pageSize, int pageIndex)
        {
            var query = _context.RequestCategories
                .Where(x => x.IsDeleted == false && (string.IsNullOrEmpty(filter)
                ));
            var data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .ToListAsync();
            var result = _mapper.Map<List<RequestCategoryDTO>>(data);
            var pageResult = new PagedResultDTO<RequestCategoryDTO>(pageIndex, pageSize, await query.CountAsync(), result);
            return new BaseResponseDTO<PagedResultDTO<RequestCategoryDTO>>(pageResult);
        }

        public async Task<BaseResponseDTO<RequestCategoryDTO>> GetRequestCategoryById(int requestCategoryId)
        {
            var data = await _context.RequestCategories.FirstOrDefaultAsync(x => x.IsDeleted == false && x.RequestCategoryId == requestCategoryId);
            if (data == null)
            {
                throw new ApplicationException(MessageConst.RequestCategoryMessage.Request_Category_Not_Found);
            }
            var result = _mapper.Map<RequestCategoryDTO>(data);
            return new BaseResponseDTO<RequestCategoryDTO>(result);
            throw new System.NotImplementedException();
        }

        public async Task<BaseResponseDTO<bool>> UpdateRequestCategory(RequestCategoryDTO requestCategoryDTO)
        {
            var tmp = await _context.RequestCategories.FirstOrDefaultAsync(x => x.RequestCategoryId == requestCategoryDTO.RequestCategoryId && x.IsDeleted == false);
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.RequestCategoryMessage.Request_Category_Not_Found);
            }
            if (!string.IsNullOrEmpty(requestCategoryDTO.RequestCategoryName))
            {
                requestCategoryDTO.RequestCategoryName = requestCategoryDTO.RequestCategoryName;
            }
            tmp.UpdateAt = DateTime.UtcNow;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }
    }
}
