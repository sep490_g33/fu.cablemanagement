﻿using AutoMapper;
using DocumentFormat.OpenXml.Drawing.Charts;
using FU.CableManagement.API.Infra.Constants;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.Common.Enums;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.RequestDTOs;
using FU.CableManagement.DataAccess.DTO.TransactionDTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ILogger<TransactionService> _logger;
        private readonly IConfiguration _configuration;
        private readonly CableManagementContext _context;
        private readonly IMapper _mapper;

        public TransactionService(ILogger<TransactionService> logger, IConfiguration configuration, CableManagementContext context, IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
            _mapper = mapper;
        }

        public async Task<Task> AddTransaction(Request request, bool? action) //action: true => Nhap kho, false => Xuat kho
        {
            try
            {
                int? fromWarehouse = null;
                int? toWarehouse = null;
                
                if (action == false && request.RequestCategoryId == (int)RequestCategoryEnum.Deliver)
                {
                    toWarehouse = await _context.Warehouses.Where(x => x.WarehouseId == request.DeliverWarehouseId).Select(x => x.WarehouseId)
                        .FirstOrDefaultAsync();
                }
                var listWarehouseCab = await _context.RequestCables.Include(x => x.Cable).Include(x => x.Cable.Warehouse)
                    .Where(x => x.RequestId == request.RequestId)
                .GroupBy(x => x.Cable.WarehouseId).Select(x => x.Key).Cast<int>()
                .ToListAsync();
                var listWarehouseMat = await _context.RequestOtherMaterials.Include(x => x.OtherMaterials).Include(x => x.OtherMaterials.WareHouse)
                    .Where(x => x.RequestId == request.RequestId)
                    .GroupBy(x => x.OtherMaterials.WarehouseId).Select(x => x.Key).Cast<int>()
                    .ToListAsync();
                listWarehouseCab.AddRange(listWarehouseMat);
                List<int> warehouseIds = listWarehouseCab.GroupBy(x => x).Select(x => x.Key).ToList();
                List<TransactionHistory> transactions = new List<TransactionHistory>();
                var tmpIssue = await _context.Issues.Where(x => x.IssueId == request.IssueId).FirstOrDefaultAsync();
                Guid? issueId = null;
                if (tmpIssue != null) issueId = await _context.Issues.Where(x => x.IssueId == request.IssueId).Select(x => x.IssueId).FirstOrDefaultAsync();
                string name = string.Empty;
                if (action == null) { name = "Hủy"; }
                else { name = action == true ? "Nhập kho" : "Xuất kho"; } 
                foreach (var item in warehouseIds)
                {
                    if (action == true && request.RequestCategoryId == (int)RequestCategoryEnum.Deliver)
                    {
                        fromWarehouse = await _context.Warehouses.Where(x => x.WarehouseId == item).Select(x => x.WarehouseId)
                            .FirstOrDefaultAsync();
                    }
                    transactions.Add(new TransactionHistory
                    {
                        TransactionCategoryName = name,
                        CreatedAt = DateTime.Now,
                        IsDeleted = false,
                        WarehouseId = item,
                        CreatedDate = DateTime.Now,
                        RequestId = request.RequestId,
                        FromWarehouseId = fromWarehouse,
                        ToWarehouseId = toWarehouse,
                        IssueId = issueId,
                    });
                }
                foreach (var item in request.RequestCables)
                {
                    var tmp = await _context.Cables.FirstOrDefaultAsync(x => x.CableId == item.CableId);
                    foreach (var transaction in transactions.Where(x => x.WarehouseId == tmp.WarehouseId))
                    {
                        transaction.TransactionCables.Add(new TransactionCable
                        {
                            TransactionId = transaction.TransactionId,
                            CableId = item.CableId,
                            StartPoint = item.StartPoint,
                            EndPoint = item.EndPoint,
                            Length = item.Length,
                            CreatedAt = DateTime.Now,
                        });
                    }
                }
                foreach (var item in request.RequestOtherMaterials)
                {
                    var tmp = await _context.OtherMaterials.FirstOrDefaultAsync(x => x.OtherMaterialsId == item.OtherMaterialsId);
                    foreach (var transaction in transactions.Where(x => x.WarehouseId == tmp.WarehouseId))
                    {
                        transaction.TransactionOtherMaterials.Add(new TransactionOtherMaterial
                        {
                            TransactionId = transaction.TransactionId,
                            Quantity = item.Quantity,
                            OtherMaterialsId = item.OtherMaterialsId,
                            CreatedAt = DateTime.Now,
                        });
                    }
                }
                await _context.AddRangeAsync(transactions);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            return Task.CompletedTask;
        }

        public async Task<BaseResponseDTO<PagedResultDTO<TransactionDTO>>> GetAllTransactions(string filter, int pageSize, int pageIndex, int? warehouseId)
        {
            if (filter != null) filter = filter.Trim();
            var query = _context.TransactionHistories.Include(x => x.Warehouse).Include(x => x.TransactionCables)
                .Include(x => x.TransactionOtherMaterials).Include(x => x.Issue)
                .Where(x => x.IsDeleted == false && (string.IsNullOrEmpty(filter)
                || x.Warehouse.WarehouseName.ToLower().Contains(filter.ToLower())
                || EF.Functions.Collate(x.Warehouse.WarehouseName.ToLower(), "Latin1_General_CI_AI").Contains(filter.ToLower())
            || x.TransactionCategoryName.ToLower().Contains(filter.ToLower())
            ));
            if (warehouseId != null) query = query.Where(x => x.WarehouseId == warehouseId);
            var data = await query.OrderByDescending(x => x.CreatedAt).Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .ToListAsync();
            var result = _mapper.Map<List<TransactionDTO>>(data);
            var pageResult = new PagedResultDTO<TransactionDTO>(pageIndex, pageSize, await query.CountAsync(), result);
            return new BaseResponseDTO<PagedResultDTO<TransactionDTO>>(pageResult);
        }

        public async Task<BaseResponseDTO<TransactionDTO>> GetTransactionById(Guid transactionId)
        {
            var data = await _context.TransactionHistories
                .Include(x => x.TransactionCables).ThenInclude(x => x.Cable).ThenInclude(x => x.CableCategory)
                .Include(x => x.TransactionOtherMaterials).ThenInclude(x => x.OtherMaterials).ThenInclude(x => x.OtherMaterialsCategory)
                .Include(x => x.Warehouse).Include(x => x.Issue).Include(x => x.FromWarehouse).Include(x => x.ToWarehouse)
                .FirstOrDefaultAsync(x => x.TransactionId == transactionId && x.IsDeleted == false);
            if (data == null)
            {
                throw new ApplicationException(MessageConst.TransactionHistory.Transaction_History_Not_Found);
            }
            var result = _mapper.Map<TransactionDTO>(data);
            var tranCab = _mapper.Map<List<TransactionCableDTO>>(await _context.TransactionCables.Include(x => x.Cable)
                .Where(x => x.TransactionId == transactionId).ToListAsync());
            var tranMat = _mapper.Map<List<TransactionMaterialDTO>>(await _context.TransactionOtherMaterials.Include(x => x.OtherMaterials)
                .Where(x => x.TransactionId == transactionId).ToListAsync());
            result.CableTransactions = tranCab;
            result.MaterialsTransaction = tranMat;
            return new BaseResponseDTO<TransactionDTO>(result);
        }
    }
}
