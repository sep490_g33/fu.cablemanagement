﻿using AutoMapper;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Spreadsheet;
using FU.CableManagement.API.Infra.Constants;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.API.Utils;
using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.Common.Enums;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CableDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.EmailDTOs;
using FU.CableManagement.DataAccess.DTO.RequestDTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    public class RequestService : IRequestService
    {
        private readonly ILogger<RequestService> _logger;
        private readonly IConfiguration _configuration;
        private readonly CableManagementContext _context;
        private readonly IMapper _mapper;
        private readonly ICableService _cableService;
        private readonly ITransactionService _transactionService;
        private readonly IOtherMaterialsService _otherMaterialsService;
        private readonly IEmailService _emailService;

        public RequestService(ILogger<RequestService> logger, IConfiguration configuration, CableManagementContext context, IMapper mapper,
            ICableService cableService, ITransactionService transactionService, IOtherMaterialsService otherMaterialsService, IEmailService emailService)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
            _mapper = mapper;
            _cableService = cableService;
            _transactionService = transactionService;
            _otherMaterialsService = otherMaterialsService;
            _emailService = emailService;
        }
        /// <summary>
        ///    Approve a pending request for export:
        ///    - If cable is valid, change field 'IsExportedToUse' into 'true'. In case the needed startPoint and endPoint stand between
        ///     these 2 points of available cable in warehouse, the fucntion CutCable is used to cut this cable into multiple parts and 
        ///     system will choose one of them to change field 'IsExportedToUse' into 'true'.
        ///    - About OtherMaterial, 
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="approverId"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task<BaseResponseDTO<bool>> ApproveRequestExport(Guid requestId, string approverId)
        {
            try
            {
                var tmp = await _context.Requests.Include(x => x.RequestCables).ThenInclude(x => x.Cable).ThenInclude(x => x.CableCategory)
                    .Include(x => x.RequestCables).ThenInclude(x => x.Cable).ThenInclude(x => x.Warehouse)
                    .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.OtherMaterials).ThenInclude(x => x.OtherMaterialsCategory)
                    .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.OtherMaterials).ThenInclude(x => x.WareHouse)
                    .Include(x => x.RequestCategory)
                .FirstOrDefaultAsync(x => x.RequestId == requestId && x.IsDeleted == false
                && x.Status.ToLower().Equals(RequestStatusConstant.Pending.ToLower())
                );
                if (tmp == null)
                {
                    throw new ApplicationException(MessageConst.Request.Request_Not_Found);
                }
                if (tmp.RequestCategoryId != (int)RequestCategoryEnum.Export) throw new ApplicationException("Không phải loại yêu cầu xuất kho");


                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    List<RequestCable> listRqCabs = tmp.RequestCables.ToList();
                    if (listRqCabs.Count > 0)
                    {
                        for (int i = 0; i < listRqCabs.Count; i++)
                        {
                            var tmp1 = await _context.Cables.Include(x => x.CableCategory).FirstOrDefaultAsync(x => x.CableId == listRqCabs[i].CableId);
                            if (tmp1.IsDeleted == true)
                                tmp1 = await _context.Cables.Include(x => x.CableCategory).FirstOrDefaultAsync(x => x.CableParentId == listRqCabs[i].CableId
                                    && listRqCabs[i].StartPoint >= x.StartPoint && listRqCabs[i].EndPoint <= x.EndPoint);

                            if (tmp1 == null || listRqCabs[i].StartPoint < tmp1.StartPoint || listRqCabs[i].EndPoint > tmp1.EndPoint)
                                return new BaseResponseDTO<bool>(false, "Không thể xuất kho " + tmp1.CableCategory.CableCategoryName + " với ID: " + listRqCabs[i].CableId
                                    + " (chỉ số đầu: " + listRqCabs[i].StartPoint + ", chỉ số cuối: " + listRqCabs[i].EndPoint + ")", StatusCodes.Status409Conflict);

                            if (tmp1.IsExportedToUse == true)
                                return new BaseResponseDTO<bool>(false, tmp1.CableCategory.CableCategoryName + " với ID: " + listRqCabs[i].CableId
                                    + " (chỉ số đầu: " + listRqCabs[i].StartPoint + ", chỉ số cuối: " + listRqCabs[i].EndPoint + ") đã được sử dụng!", StatusCodes.Status409Conflict);

                            if (listRqCabs[i].StartPoint >= tmp1.StartPoint && listRqCabs[i].EndPoint <= tmp1.EndPoint)
                            {
                                if (listRqCabs[i].StartPoint == tmp1.StartPoint && listRqCabs[i].EndPoint == tmp1.EndPoint)
                                {
                                    tmp1.IsExportedToUse = true; tmp1.UpdateAt = DateTime.Now;
                                    _context.Update(tmp1);
                                    continue;
                                }
                                CutCableDTO cutCableDTO = new CutCableDTO
                                {
                                    CableId = tmp1.CableId,
                                    StartPoint = listRqCabs[i].StartPoint,
                                    EndPoint = listRqCabs[i].EndPoint,
                                    CableParentId = listRqCabs[i].CableId.ToString()
                                };
                                List<Cable> cables = await _cableService.CutCable(cutCableDTO);
                                var newCab = cables.Where(x => x.StartPoint == listRqCabs[i].StartPoint
                                        && x.EndPoint == listRqCabs[i].EndPoint).FirstOrDefault();
                                tmp.RequestCables.Add(
                                    new RequestCable
                                    {
                                        CableId = newCab.CableId,
                                        RequestId = listRqCabs[i].RequestId,
                                        StartPoint = listRqCabs[i].StartPoint,
                                        EndPoint = listRqCabs[i].EndPoint,
                                        CreatedAt = DateTime.Now,
                                        IsDeleted = false,
                                        Length = listRqCabs[i].EndPoint - listRqCabs[i].StartPoint
                                    });
                                var tmpRqCab = tmp.RequestCables.FirstOrDefault(x => x.CableId == listRqCabs[i].CableId);
                                var usedCable = await _context.Cables.FirstOrDefaultAsync(x => x.CableId == newCab.CableId);
                                usedCable.IsExportedToUse = true;
                                usedCable.UpdateAt = DateTime.Now;
                                tmp.RequestCables.Remove(tmpRqCab);
                                _context.Update(usedCable);
                            }
                        }
                    }
                    if (tmp.RequestOtherMaterials.Count > 0)
                    {
                        foreach (var tmpRqMat in tmp.RequestOtherMaterials)
                        {
                            var tmp1 = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory).FirstOrDefaultAsync(x => x.OtherMaterialsId == tmpRqMat.OtherMaterialsId);
                            if (tmp1.Quantity < tmpRqMat.Quantity)
                            {
                                return new BaseResponseDTO<bool>(false, "Không đủ số lượng " + tmp1.OtherMaterialsCategory.OtherMaterialsCategoryName, StatusCodes.Status409Conflict);
                            }
                            tmp1.Quantity -= tmpRqMat.Quantity; tmp1.UpdateAt = DateTime.Now;
                            _context.Update(tmp1);
                        }
                    }
                    tmp.Status = RequestStatusConstant.Approved;
                    tmp.ApproverId = Guid.Parse(approverId);
                    tmp.UpdateAt = DateTime.Now;
                    _context.Update(tmp);
                    await _context.SaveChangesAsync();
                    await _transactionService.AddTransaction(tmp, false); //true: Nhap kho, false: Xuat kho
                    string approver = await _context.Users.Where(x => x.UserId == tmp.ApproverId).Select(x => x.Lastname + " " + x.Firstname).SingleAsync();
                    string body = UserUtils.BodyEmailForApproveRequestExport(tmp, approver);
                    foreach (var item in tmp.RequestCables)
                    {
                        var tmp1 = await _context.Cables.FirstOrDefaultAsync(x => x.CableId == item.CableId);
                        tmp1.WarehouseId = null; tmp1.UpdateAt = DateTime.Now;
                        _context.Update(tmp1);
                    }
                    await _context.SaveChangesAsync();
                    //List<RequestCable> requestCables = await _context.
                    string email = await _context.Users.Where(x => x.UserId == tmp.CreatorId).Select(x => x.Email).SingleAsync();
                    EmailDTO emailDTO = new EmailDTO
                    {
                        Body = body,
                        Subject = "Thông báo yêu cầu được phê duyệt",
                        To = email
                    };
                    await transaction.CommitAsync();
                    await _emailService.sendEmail(emailDTO);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true, "Yêu cầu được phê duyệt", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<bool>> CreateRequest(RequestCreateDTO model, string creatorId)
        {
            try
            {

                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    var tmp = _mapper.Map<Request>(model);
                    if (tmp.RequestCategoryId != (int)RequestCategoryEnum.Deliver)
                    {
                        if (tmp.IssueId == null)
                        {
                            throw new ApplicationException("Sự vụ không được để trống");
                        }
                        var issue = await _context.Issues.FirstOrDefaultAsync(x => x.IssueId == tmp.IssueId);
                        if (issue == null)
                            throw new ApplicationException("Không tìm thấy sự vụ");
                        if (issue != null)
                        {
                            if (issue.Status.Equals(IssueStatusConstants.Done))
                                throw new ApplicationException("Sự vụ với mã " + issue.IssueCode + " đã được xử lý.");

                        }
                    }

                    tmp.RequestId = Guid.NewGuid();
                    tmp.IsDeleted = false;
                    tmp.CreatorId = Guid.Parse(creatorId);
                    tmp.CreatedAt = DateTime.Now;
                    tmp.ApproverId = null;
                    tmp.Status = RequestStatusConstant.Pending;
                    tmp.RequestCables = _mapper.Map<List<RequestCable>>(model.RequestCableDTOs);
                    tmp.RequestOtherMaterials = _mapper.Map<List<RequestOtherMaterial>>(model.RequestOtherMaterialDTOs);
                    if (tmp.RequestCables.Count > 0)
                    {
                        foreach (var tmpRqCab in tmp.RequestCables)
                        {
                            var tmp1 = await _context.Cables.FirstOrDefaultAsync(x => x.CableId == tmpRqCab.CableId);
                            if (tmp1 == null)
                            {
                                throw new ApplicationException("Không tìm thấy cáp với id " + tmpRqCab.CableId);
                            }
                            if (model.RequestCategoryId == (int)RequestCategoryEnum.Recovery || model.RequestCategoryId == (int)RequestCategoryEnum.Cancel)
                            {
                                tmpRqCab.StartPoint = tmp1.StartPoint;
                                tmpRqCab.EndPoint = tmp1.EndPoint;
                            }
                            if (tmpRqCab.StartPoint < tmp1.StartPoint || tmpRqCab.EndPoint > tmp1.EndPoint || tmpRqCab.StartPoint > tmpRqCab.EndPoint
                                || tmp1.IsDeleted == true)
                            {
                                throw new ApplicationException("Cáp với ID: " + tmp1.CableId + " có chỉ số đầu hoặc chỉ số cuối không hợp lệ hoặc đã bị hủy");
                            }
                            if (tmp1.IsExportedToUse == true && model.RequestCategoryId != (int)RequestCategoryEnum.Recovery)
                            {
                                throw new ApplicationException("Cáp với ID " + tmp1.CableId + " đã được sử dụng");
                            }
                        }
                    }
                    if (tmp.RequestOtherMaterials.Count > 0)
                    {
                        foreach (var tmpRqMat in tmp.RequestOtherMaterials)
                        {
                            var tmp1 = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory).FirstOrDefaultAsync(x => x.OtherMaterialsId == tmpRqMat.OtherMaterialsId);
                            if (tmp1 == null)
                            {
                                throw new ApplicationException("Không tìm thấy vật liệu " + tmp1.OtherMaterialsCategory.OtherMaterialsCategoryName);
                            }
                            if (tmp1.Quantity < tmpRqMat.Quantity)
                            {
                                throw new ApplicationException("Không có đủ số lượng " + tmp1.OtherMaterialsCategory.OtherMaterialsCategoryName);
                            }
                        }
                    }
                    await _context.AddAsync(tmp);
                    await _context.SaveChangesAsync();
                    await _context.RequestCategories.FirstOrDefaultAsync(x => x.RequestCategoryId == tmp.RequestCategoryId);
                    var userAdmins = await _context.Users.Where(x => x.RoleId == (int)RoleUserEnum.Admin).Select(x => x.Email).ToListAsync();
                    var body = UserUtils.BodyEmailForAdminReceiveRequest(tmp);
                    await transaction.CommitAsync();
                    foreach(var userAdmin in userAdmins)
                    {
                        EmailDTO emailDTO = new EmailDTO
                        {
                            Body = body,
                            Subject = "[FPT TELECOM CABLE MANAGEMENT] Thông báo có yêu cầu mới",
                            To = userAdmin
                        };
                        await _emailService.sendEmail(emailDTO);
                    } 
                        
                   
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new BaseResponseDTO<bool>(true, "Tạo yêu cầu thành công!", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<bool>> DeleteRequest(Guid requestId)
        {
            var tmp = await _context.Requests.FirstOrDefaultAsync(x => x.RequestId == requestId
                        && x.IsDeleted == false && x.Status.ToLower().Equals(RequestStatusConstant.Pending.ToLower()));
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.Request.Request_Not_Found);
            }
            tmp.IsDeleted = true;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true, "Xóa thành công", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<PagedResultDTO<RequestDTO>>> GetAllRequest(string filter, int pageSize, int pageIndex, Guid? issueId)
        {
            try
            {
                if (filter != null) filter = filter.Trim();
                var query = _context.Requests.Include(x => x.Approver).Include(x => x.RequestCategory)
                .Include(x => x.Issue).Include(x => x.Creator)
                .Where(x => x.IsDeleted == false && (string.IsNullOrEmpty(filter) || x.RequestName.ToLower().Contains(filter.ToLower()) 
                || EF.Functions.Collate(x.RequestName.ToLower(), "Latin1_General_CI_AI").Contains(filter.ToLower())
            || x.Status.ToLower().Contains(filter.ToLower())
            ));
                if (issueId != null) query = query.Where(x => x.IssueId == issueId);
                var data = await query.OrderBy(x => (x.Status.ToLower() == RequestStatusConstant.Pending) ? 0 : 1)
                    .ThenByDescending(x => x.UpdateAt).Skip((pageIndex - 1) * pageSize).Take(pageSize)
                    .ToListAsync();
                var result = _mapper.Map<List<RequestDTO>>(data);
                var pageResult = new PagedResultDTO<RequestDTO>(pageIndex, pageSize, await query.CountAsync(), result);
                return new BaseResponseDTO<PagedResultDTO<RequestDTO>>(pageResult);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        public async Task<BaseResponseDTO<PagedResultDTO<RequestDTO>>> GetRequestByStaff(string filter, int pageSize, int pageIndex, string staffId, Guid? issueId)
        {
            var query = _context.Requests.Include(x => x.RequestCables).Include(x => x.RequestOtherMaterials).Include(x => x.Approver)
                .Include(x => x.Issue).Include(x => x.Creator).Include(x => x.RequestCategory)
                .Where(x => x.IsDeleted == false && (string.IsNullOrEmpty(filter)
            || x.RequestName.ToLower().Contains(filter.ToLower())
            || x.Status.ToLower().Contains(filter.ToLower())) && x.CreatorId == Guid.Parse(staffId)
            );
            if (issueId != null) query = query.Where(x => x.IssueId == issueId);
            //var data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize)
            //    .ToListAsync();
            var data = await query.OrderBy(x => (x.Status.ToLower() == RequestStatusConstant.Pending) ? 0 : 1)
                    .ThenByDescending(x => x.UpdateAt).Skip((pageIndex - 1) * pageSize).Take(pageSize)
                    .ToListAsync();
            var result = _mapper.Map<List<RequestDTO>>(data);
            var pageResult = new PagedResultDTO<RequestDTO>(pageIndex, pageSize, await query.CountAsync(), result);
            return new BaseResponseDTO<PagedResultDTO<RequestDTO>>(pageResult);
        }
        public async Task<BaseResponseDTO<RequestDTO>> GetRequestById(Guid requestId)
        {
            var data = await _context.Requests.Include(x => x.RequestCategory)
                .Include(x => x.DeliverWarehouse)
                .Include(x => x.RequestCables).ThenInclude(x => x.ImportedWarehouse)
                .Include(x => x.RequestCables).ThenInclude(x => x.RecoveryDestWarehouse)
                .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.ImportedWarehouse)
                .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.RecoveryDestWarehouse)
                .Include(x => x.Approver)
                .Include(x => x.Issue).Include(x => x.Creator)
                .FirstOrDefaultAsync(x => x.RequestId == requestId && x.IsDeleted == false);
            if (data == null)
            {
                throw new ApplicationException(MessageConst.Request.Request_Not_Found);
            }
            var result = _mapper.Map<RequestDTO>(data);
            var reCab = _mapper.Map<List<RequestCableDTO>>(await _context.RequestCables.Include(x => x.Cable).ThenInclude(x => x.CableCategory)
                .Include(x => x.ImportedWarehouse).Include(x => x.RecoveryDestWarehouse)
                .Where(x => x.RequestId == requestId).ToListAsync());
            var reMat = _mapper.Map<List<RequestMaterialDTO>>(await _context.RequestOtherMaterials.Include(x => x.OtherMaterials).ThenInclude(x => x.OtherMaterialsCategory)
                .Include(x => x.ImportedWarehouse).Include(x => x.RecoveryDestWarehouse)
                .Where(x => x.RequestId == requestId).ToListAsync());
            result.RequestCableDTOs = reCab;
            result.RequestMaterialDTOs = reMat;
            return new BaseResponseDTO<RequestDTO>(result);
        }

        public async Task<BaseResponseDTO<bool>> RejectRequest(Guid requestId, string rejectorId)
        {
            var tmp = await _context.Requests.Include(x => x.RequestCables)
                .FirstOrDefaultAsync(x => x.RequestId == requestId && x.IsDeleted == false);
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.Request.Request_Not_Found);
            }
            //using(var transaction = await _context.Database.BeginTransactionAsync())
            //{
            //if (tmp.RequestCategoryId == (int)RequestCategoryEnum.Import)
            //{
            //    await RejectImportRequest(tmp.RequestCables.ToList(), tmp.RequestOtherMaterials.ToList());
            //}
            tmp.Status = RequestStatusConstant.Rejected;
            tmp.ApproverId = Guid.Parse(rejectorId);
            tmp.UpdateAt = DateTime.Now;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            //await transaction.CommitAsync();
            //}
            return new BaseResponseDTO<bool>(true);
        }
        //private async Task<Task> RejectImportRequest(List<RequestCable> requestCables, List<RequestOtherMaterial> requestOtherMaterials)
        //{
        //    foreach(var item in requestCables)
        //    {
        //        var tmp = await _context.Cables.FirstOrDefaultAsync(x => x.CableId == item.CableId && x.IsInImportRequest == true);
        //        _context.Remove(tmp);
        //    }
        //    foreach(var item in requestOtherMaterials)
        //    {
        //        var tmp = await _context.OtherMaterials.FirstOrDefaultAsync(x => x.OtherMaterialsId == item.OtherMaterialsId && x.Quantity == 0);
        //        _context.Remove(tmp);
        //    }
        //    await _context.SaveChangesAsync();
        //    return Task.CompletedTask;
        //}

        public async Task<BaseResponseDTO<bool>> UpdateRequest(RequestUpdateDTO model)
        {
            try
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    var tmp = await _context.Requests.FirstOrDefaultAsync(x => x.RequestId == model.RequestId
                        && x.IsDeleted == false && x.Status.ToLower().Equals(RequestStatusConstant.Pending.ToLower()));
                    if (tmp == null)
                    {
                        throw new ApplicationException(MessageConst.Request.Request_Not_Found);
                    }
                    tmp = _mapper.Map(model, tmp);
                    tmp.UpdateAt = DateTime.Now;
                    var check1 = await _context.RequestCables.Where(x => x.RequestId == tmp.RequestId).ToListAsync();

                    if (tmp.RequestCables.Count > 0)
                    {
                        var rqCabTmp = await _context.RequestCables.Where(x => x.RequestId == tmp.RequestId).ToListAsync();
                        _context.RemoveRange(rqCabTmp);
                    }
                    if (tmp.RequestOtherMaterials.Count > 0)
                    {
                        var rqCabMat = await _context.RequestOtherMaterials.Where(x => x.RequestId == tmp.RequestId).ToListAsync();
                        _context.RemoveRange(rqCabMat);
                    }
                    await _context.SaveChangesAsync();
                    tmp.RequestCables = _mapper.Map<List<RequestCable>>(model.RequestCableDTOs);
                    tmp.RequestOtherMaterials = _mapper.Map<List<RequestOtherMaterial>>(model.RequestOtherMaterialDTOs);
                    if (tmp.RequestCables.Count > 0)
                    {
                        foreach (var tmpRqCab in tmp.RequestCables)
                        {
                            var tmp1 = await _context.Cables.FirstOrDefaultAsync(x => x.CableId == tmpRqCab.CableId);
                            if (tmp1 == null)
                            {
                                throw new ApplicationException("Không tìm thấy cáp với ID " + tmp1.CableId );
                            }
                            if (tmpRqCab.StartPoint < tmp1.StartPoint || tmpRqCab.EndPoint > tmp1.EndPoint || tmpRqCab.StartPoint > tmpRqCab.EndPoint
                                || tmp1.IsDeleted == true)
                            {
                                throw new ApplicationException("Cáp với ID: " + tmp1.CableId + " có chỉ số đầu hoặc chỉ số cuối không hợp lệ hoặc đã bị hủy");
                            }
                            if (tmp1.IsExportedToUse == true && model.RequestCategoryId != (int)RequestCategoryEnum.Recovery)
                            {
                                throw new ApplicationException("Cáp với ID " + tmp1.CableId + " đã được sử dụng");
                            }

                        }
                    }
                    if (tmp.RequestOtherMaterials.Count > 0)
                    {
                        foreach (var tmpRqMat in tmp.RequestOtherMaterials)
                        {
                            var tmp1 = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory).FirstOrDefaultAsync(x => x.OtherMaterialsId == tmpRqMat.OtherMaterialsId);
                            if (tmp1 == null)
                            {
                                throw new ApplicationException("Không tìm thấy vật liệu " + tmp1.OtherMaterialsCategory.OtherMaterialsCategoryName);
                            }
                            if (tmp1.Quantity < tmpRqMat.Quantity)
                            {
                                throw new ApplicationException("Không có đủ số lượng " + tmp1.OtherMaterialsCategory.OtherMaterialsCategoryName);
                            }
                        }
                    }
                    _context.Update(tmp);
                    var check = await _context.RequestCables.Where(x => x.RequestId == tmp.RequestId).ToListAsync();
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true, "Cập nhật thành công", StatusCodes.Status200OK);

        }

        public async Task<BaseResponseDTO<List<RequestCableDTO>>> GetAllCablesByIssue(Guid issuedId)
        {
            var issueTmp = await _context.Issues.FirstOrDefaultAsync(x => x.IssueId == issuedId
                && x.Status.ToLower().Equals(IssueStatusConstants.Doing));
            if (issueTmp == null) throw new ApplicationException("Không tìm thấy sự vụ");
            var reqCables = await _context.RequestCables.Include(x => x.Request).Include(x => x.Cable).ThenInclude(x => x.CableCategory)
                .Where(x => x.Request.IssueId == issuedId && x.Request.Status.Equals(RequestStatusConstant.Approved)).ToListAsync();
            if (reqCables == null || reqCables.Count == 0)
            {
                throw new ApplicationException("Không có cáp trong sự vụ " + issueTmp.IssueCode);
            }
            var reqCabDtos = _mapper.Map<List<RequestCableDTO>>(reqCables);
            return new BaseResponseDTO<List<RequestCableDTO>>(reqCabDtos);
        }

        public async Task<BaseResponseDTO<bool>> ApproveRequestForDeliver(Guid requestId, string approverId)
        {
            try
            {
                var tmp = await _context.Requests.Include(x => x.RequestCables).ThenInclude(x => x.Cable).ThenInclude(x => x.CableCategory)
                    .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.OtherMaterials).ThenInclude(x => x.OtherMaterialsCategory)
                    .Include(x => x.DeliverWarehouse).Include(x => x.RequestCategory)
                    .Include(x => x.RequestCables).ThenInclude(x => x.Cable).ThenInclude(x => x.Warehouse)
                    .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.OtherMaterials).ThenInclude(x => x.WareHouse)
                .FirstOrDefaultAsync(x => x.RequestId == requestId && x.IsDeleted == false
                && x.Status.ToLower().Equals(RequestStatusConstant.Pending.ToLower())
                );
                if (tmp == null)
                {
                    throw new ApplicationException(MessageConst.Request.Request_Not_Found);
                }
                if (tmp.RequestCategoryId != (int)RequestCategoryEnum.Deliver) throw new ApplicationException("Không phải là yêu cầu chuyển kho");

                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    await _transactionService.AddTransaction(tmp, false);
                    string approver = await _context.Users.Where(x => x.UserId == Guid.Parse(approverId)).Select(x => x.Lastname + " " + x.Firstname).SingleAsync();
                    string body = UserUtils.BodyEmailForApproveRequestDeliever(tmp, approver);
                    List<RequestCable> listRqCabs = tmp.RequestCables.ToList();
                    if (listRqCabs.Count > 0)
                    {
                        for (int i = 0; i < listRqCabs.Count; i++)
                        {
                            var tmp1 = await _context.Cables.Include(x => x.CableCategory).FirstOrDefaultAsync(x => x.CableId == listRqCabs[i].CableId);
                            if (tmp1.IsDeleted == true)
                                tmp1 = await _context.Cables.Include(x => x.CableCategory).FirstOrDefaultAsync(x => x.CableParentId == listRqCabs[i].CableId
                                    && listRqCabs[i].StartPoint >= x.StartPoint && listRqCabs[i].EndPoint <= x.EndPoint);

                            if (tmp1 == null || listRqCabs[i].StartPoint < tmp1.StartPoint || listRqCabs[i].EndPoint > tmp1.EndPoint)
                                return new BaseResponseDTO<bool>(false, "Không thể xuất kho " + tmp1.CableCategory.CableCategoryName + " với ID: " + listRqCabs[i].CableId
                                    + " (chỉ số đầu: " + listRqCabs[i].StartPoint + ", chỉ số cuối: " + listRqCabs[i].EndPoint + ")", StatusCodes.Status409Conflict);

                            if (tmp1.IsExportedToUse == true)
                                return new BaseResponseDTO<bool>(false, tmp1.CableCategory.CableCategoryName + " với ID: " + listRqCabs[i].CableId
                                    + " (chỉ số đầu: " + listRqCabs[i].StartPoint + ", chỉ số cuối: " + listRqCabs[i].EndPoint + ") đã được dùng", StatusCodes.Status409Conflict);

                            if (listRqCabs[i].StartPoint >= tmp1.StartPoint && listRqCabs[i].EndPoint <= tmp1.EndPoint)
                            {
                                if (listRqCabs[i].StartPoint == tmp1.StartPoint && listRqCabs[i].EndPoint == tmp1.EndPoint)
                                {
                                    tmp1.WarehouseId = tmp.DeliverWarehouseId; tmp1.UpdateAt = DateTime.Now;
                                    _context.Update(tmp1);
                                    continue;
                                }
                                CutCableDTO cutCableDTO = new CutCableDTO
                                {
                                    CableId = tmp1.CableId,
                                    StartPoint = listRqCabs[i].StartPoint,
                                    EndPoint = listRqCabs[i].EndPoint,
                                    CableParentId = listRqCabs[i].CableId.ToString()
                                };
                                List<Cable> cables = await _cableService.CutCable(cutCableDTO);
                                var newCab = cables.Where(x => x.StartPoint == listRqCabs[i].StartPoint
                                        && x.EndPoint == listRqCabs[i].EndPoint).FirstOrDefault();
                                tmp.RequestCables.Add(
                                    new RequestCable
                                    {
                                        CableId = newCab.CableId,
                                        RequestId = listRqCabs[i].RequestId,
                                        StartPoint = listRqCabs[i].StartPoint,
                                        EndPoint = listRqCabs[i].EndPoint,
                                        CreatedAt = DateTime.Now,
                                        IsDeleted = false,
                                        Length = listRqCabs[i].EndPoint - listRqCabs[i].StartPoint
                                    });
                                var tmpRqCab = tmp.RequestCables.FirstOrDefault(x => x.CableId == listRqCabs[i].CableId);
                                var usedCable = await _context.Cables.FirstOrDefaultAsync(x => x.CableId == newCab.CableId);
                                usedCable.WarehouseId = tmp.DeliverWarehouseId;
                                tmp.RequestCables.Remove(tmpRqCab);
                                usedCable.UpdateAt = DateTime.Now;
                                _context.Update(usedCable);
                            }
                        }
                        _context.Update(tmp);

                        await _context.SaveChangesAsync();
                    }
                    if (tmp.RequestOtherMaterials.Count > 0)
                    {
                        List<RequestOtherMaterial> listRqMat = tmp.RequestOtherMaterials.ToList();
                        for (int i = 0; i < listRqMat.Count; i++)
                        {
                            var tmp1 = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory).FirstOrDefaultAsync(x => x.OtherMaterialsId == listRqMat[i].OtherMaterialsId);
                            if (tmp1.Quantity < listRqMat[i].Quantity)
                            {
                                throw new ApplicationException("Không có đủ số lượng " + tmp1.OtherMaterialsCategory.OtherMaterialsCategoryName);
                            }
                            var tmp2 = await _context.OtherMaterials.FirstOrDefaultAsync(x => x.Code == tmp1.Code && x.WarehouseId == tmp.DeliverWarehouseId
                                && x.Status.Equals(tmp1.Status) && x.Unit.ToLower() == tmp1.Unit.ToLower()
                            );
                            tmp1.Quantity -= listRqMat[i].Quantity;
                            if (tmp2 == null)
                            {
                                tmp2 = new OtherMaterial
                                {
                                    OtherMaterialsCategoryId = tmp1.OtherMaterialsCategoryId,
                                    Code = tmp1.Code,
                                    Unit = tmp1.Unit,
                                    Quantity = listRqMat[i].Quantity,
                                    WarehouseId = tmp.DeliverWarehouseId,
                                    Status = tmp1.Status,
                                    SupplierId = tmp1.SupplierId
                                };
                                await _context.AddAsync(tmp2);
                                await _context.SaveChangesAsync();
                                tmp.RequestOtherMaterials.Add(
                                    new RequestOtherMaterial
                                    {
                                        RequestId = tmp.RequestId,
                                        OtherMaterialsId = tmp2.OtherMaterialsId,
                                        Quantity = listRqMat[i].Quantity,
                                    });
                                var tmpRqMat = tmp.RequestOtherMaterials.FirstOrDefault(x => x.OtherMaterialsId == listRqMat[i].OtherMaterialsId);
                                tmp.RequestOtherMaterials.Remove(tmpRqMat);
                                _context.Update(tmp);
                            }
                            else { tmp2.Quantity += listRqMat[i].Quantity; tmp2.UpdateAt = DateTime.Now; _context.Update(tmp2); }
                            tmp1.UpdateAt = DateTime.Now;
                            _context.Update(tmp1);
                            _context.Update(tmp);
                        }
                    }

                    tmp.Status = RequestStatusConstant.Approved;
                    tmp.ApproverId = Guid.Parse(approverId);
                    tmp.UpdateAt = DateTime.Now;
                    _context.Update(tmp);
                    await _context.SaveChangesAsync();
                    await _transactionService.AddTransaction(tmp, true); //true: Nhap kho, false: Xuat kho
                    string email = await _context.Users.Where(x => x.UserId == tmp.CreatorId).Select(x => x.Email).SingleAsync();
                    EmailDTO emailDTO = new EmailDTO
                    {
                        Body = body,
                        Subject = "Thông báo yêu cầu được phê duyệt",
                        To = email
                    };
                    await transaction.CommitAsync();
                    await _emailService.sendEmail(emailDTO);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true, "Yêu cầu được phê duyệt", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<bool>> ApproveRequestForCancelInWarehouse(Guid requestId, string approverId)
        {
            try
            {
                var tmp = await _context.Requests.Include(x => x.RequestCables).ThenInclude(x => x.Cable).ThenInclude(x => x.CableCategory)
                    .Include(x => x.RequestCables).ThenInclude(x => x.Cable).ThenInclude(x => x.Warehouse)
                    .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.OtherMaterials).ThenInclude(x => x.OtherMaterialsCategory)
                    .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.OtherMaterials).ThenInclude(x => x.WareHouse)
                    .Include(x => x.RequestCategory)
                .FirstOrDefaultAsync(x => x.RequestId == requestId && x.IsDeleted == false
                && x.Status.ToLower().Equals(RequestStatusConstant.Pending.ToLower())
                );
                if (tmp == null)
                {
                    throw new ApplicationException(MessageConst.Request.Request_Not_Found);
                }
                if (tmp.RequestCategoryId != (int)RequestCategoryEnum.Cancel) throw new ApplicationException("Không phải yêu cầu hủy");


                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    List<RequestCable> listRqCabs = tmp.RequestCables.ToList();
                    if (tmp.RequestCables.Count > 0)
                    {
                        for (int i = 0; i < listRqCabs.Count; i++)
                        {
                            var tmp1 = await _context.Cables.Include(x => x.CableCategory).FirstOrDefaultAsync(x => x.CableId == listRqCabs[i].CableId);
                            if (tmp1.IsDeleted == true)
                                tmp1 = await _context.Cables.Include(x => x.CableCategory).FirstOrDefaultAsync(x => x.CableParentId == listRqCabs[i].CableId
                                    && listRqCabs[i].StartPoint >= x.StartPoint && listRqCabs[i].EndPoint <= x.EndPoint);

                            if (tmp1.IsExportedToUse == true)
                                return new BaseResponseDTO<bool>(false, tmp1.CableCategory.CableCategoryName + " vối ID: " + listRqCabs[i].CableId
                                    + " (chỉ số đầu: " + listRqCabs[i].StartPoint + ", chỉ số cuối: " + listRqCabs[i].EndPoint + ") đã được dùng", StatusCodes.Status409Conflict);

                            if (tmp1 == null || listRqCabs[i].StartPoint < tmp1.StartPoint || listRqCabs[i].EndPoint > tmp1.EndPoint)
                                return new BaseResponseDTO<bool>(false, "Không thể hủy " + tmp1.CableCategory.CableCategoryName + " với ID: " + listRqCabs[i].CableId
                                    + " (chỉ số đầu: " + listRqCabs[i].StartPoint + ", chỉ số cuối: " + listRqCabs[i].EndPoint +")", StatusCodes.Status409Conflict);

                            if (listRqCabs[i].StartPoint >= tmp1.StartPoint && listRqCabs[i].EndPoint <= tmp1.EndPoint)
                            {
                                if (listRqCabs[i].StartPoint == tmp1.StartPoint && listRqCabs[i].EndPoint == tmp1.EndPoint)
                                {
                                    //tmp1.WarehouseId = null; 
                                    tmp1.IsDeleted = true; tmp1.UpdateAt = DateTime.Now;
                                    _context.Update(tmp1);
                                    continue;
                                }
                                CutCableDTO cutCableDTO = new CutCableDTO
                                {
                                    CableId = tmp1.CableId,
                                    StartPoint = listRqCabs[i].StartPoint,
                                    EndPoint = listRqCabs[i].EndPoint,
                                    CableParentId = listRqCabs[i].CableId.ToString()
                                };
                                List<Cable> cables = await _cableService.CutCable(cutCableDTO);
                                var newCab = cables.Where(x => x.StartPoint == listRqCabs[i].StartPoint
                                        && x.EndPoint == listRqCabs[i].EndPoint).FirstOrDefault();
                                tmp.RequestCables.Add(
                                    new RequestCable
                                    {
                                        CableId = newCab.CableId,
                                        RequestId = listRqCabs[i].RequestId,
                                        StartPoint = listRqCabs[i].StartPoint,
                                        EndPoint = listRqCabs[i].EndPoint,
                                        CreatedAt = DateTime.Now,
                                        IsDeleted = false,
                                        Length = listRqCabs[i].EndPoint - listRqCabs[i].StartPoint
                                    });
                                var tmpRqCab = tmp.RequestCables.FirstOrDefault(x => x.CableId == listRqCabs[i].CableId);
                                var usedCable = await _context.Cables.FirstOrDefaultAsync(x => x.CableId == newCab.CableId);
                                //usedCable.WarehouseId = null; 
                                usedCable.IsDeleted = true;
                                usedCable.UpdateAt = DateTime.Now;
                                tmp.RequestCables.Remove(tmpRqCab);
                                _context.Update(usedCable);
                            }
                        }
                    }
                    if (tmp.RequestOtherMaterials.Count > 0)
                    {
                        foreach (var tmpRqMat in tmp.RequestOtherMaterials)
                        {
                            var tmp1 = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory).FirstOrDefaultAsync(x => x.OtherMaterialsId == tmpRqMat.OtherMaterialsId);
                            if (tmp1.Quantity < tmpRqMat.Quantity)
                            {
                                throw new ApplicationException("Không đủ số lượng " + tmp1.OtherMaterialsCategory.OtherMaterialsCategoryName);
                            }
                            tmp1.Quantity -= tmpRqMat.Quantity; tmp1.UpdateAt = DateTime.Now;
                            _context.Update(tmp1);
                        }
                    }
                    tmp.Status = RequestStatusConstant.Approved;
                    tmp.ApproverId = Guid.Parse(approverId);
                    tmp.UpdateAt = DateTime.Now;
                    _context.Update(tmp);
                    await _context.SaveChangesAsync();
                    await _transactionService.AddTransaction(tmp, null); //true: Nhap kho, false: Xuat kho, null: Huy
                    string email = await _context.Users.Where(x => x.UserId == tmp.CreatorId).Select(x => x.Email).SingleAsync();
                    string approver = await _context.Users.Where(x => x.UserId == tmp.ApproverId).Select(x => x.Lastname + " " + x.Firstname).SingleAsync();
                    string body = UserUtils.BodyEmailForApproveRequestCancelInWarehouse(tmp, approver);
                    EmailDTO emailDTO = new EmailDTO
                    {
                        Body = body,
                        Subject = "Thông báo yêu cầu được phê duyệt",
                        To = email
                    };
                    await transaction.CommitAsync();
                    await _emailService.sendEmail(emailDTO);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true, "Yêu cầu được phê duyệt", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<List<CableDTO>>> SuggestCable(SuggestCableRequestDTO request)
        {
            var listAvaiableCable = await _context.Cables.Include(x => x.CableCategory).Include(x => x.Warehouse).Include(x => x.Supplier)
                .Where(x => x.WarehouseId.HasValue && x.IsExportedToUse == false).ToListAsync();
            if (request.ListWarehouseIds.Any())
            {
                listAvaiableCable = listAvaiableCable.Where(x => request.ListWarehouseIds.Contains(x.WarehouseId.Value)).ToList();
            }
            if (request.ListCableCategoryIds.Any())
            {
                listAvaiableCable = listAvaiableCable.Where(x => request.ListCableCategoryIds.Contains(x.CableCategoryId)).ToList();
            }
            listAvaiableCable = listAvaiableCable.OrderBy(x => x.Length).ToList();
            var result = new List<Cable>();
            var totalLength = 0;
            foreach (var cable in listAvaiableCable)
            {
                if (totalLength >= request.Length) break;
                result.Add(cable);
                totalLength += cable.Length;
            }
            result.Reverse();
            for (int i = 0; i < result.Count(); ++i)
            {
                var cable = result[i];
                if (totalLength - cable.Length >= request.Length)
                {
                    totalLength -= cable.Length;
                    result.Remove(cable);
                }
            }
            return new BaseResponseDTO<List<CableDTO>>(_mapper.Map<List<CableDTO>>(result));
        }

        public async Task<BaseResponseDTO<bool>> ApproveRequestForImport(Guid requestId, string approverId)
        {
            try
            {
                var tmp = await _context.Requests.Include(x => x.RequestCategory)
                    .Include(x => x.RequestCables).ThenInclude(x => x.Cable).ThenInclude(x => x.CableCategory)
                    .Include(x => x.RequestCables).ThenInclude(x => x.Cable).ThenInclude(x => x.Warehouse)
                    .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.OtherMaterials).ThenInclude(x => x.OtherMaterialsCategory)
                    .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.OtherMaterials).ThenInclude(x => x.WareHouse)
                    .Include(x => x.Issue)
                .FirstOrDefaultAsync(x => x.RequestId == requestId && x.IsDeleted == false
                && x.Status.ToLower().Equals(RequestStatusConstant.Pending.ToLower())
                );
                if (tmp == null)
                {
                    throw new ApplicationException(MessageConst.Request.Request_Not_Found);
                }
                if (tmp.RequestCategoryId != (int)RequestCategoryEnum.Import) throw new ApplicationException("Không phải yêu cầu nhập kho");

                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    List<RequestCable> listRqCabs = tmp.RequestCables.ToList();
                    for (int i = 0; i < listRqCabs.Count; i++)
                    {
                        var tmp1 = await _context.Cables.Include(x => x.CableCategory).FirstOrDefaultAsync(x => x.CableId == listRqCabs[i].CableId
                        && x.IsInRequest == true);
                        if (tmp1 == null || tmp1.IsDeleted == true)
                            throw new ApplicationException(tmp1.CableCategory.CableCategoryName + " với ID: " + listRqCabs[i].CableId
                                + " không tồn tại ");

                        tmp1.IsInRequest = false;
                        tmp1.UpdateAt = DateTime.Now;
                        _context.Update(tmp1);

                    }
                    foreach (var tmpRqMat in tmp.RequestOtherMaterials)
                    {
                        var tmp1 = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory).FirstOrDefaultAsync(x => x.OtherMaterialsId == tmpRqMat.OtherMaterialsId);
                        //if (tmpRqMat.Quantity + tmp1.Quantity > tmp1.MaxQuantity) throw new ApplicationException("Warehouse cannot contain these " + tmp1.OtherMaterialsName);
                        tmp1.Quantity += tmpRqMat.Quantity;
                        tmp1.UpdateAt = DateTime.Now;
                        _context.Update(tmp1);
                    }
                    tmp.Status = RequestStatusConstant.Approved;
                    tmp.ApproverId = Guid.Parse(approverId);
                    tmp.UpdateAt = DateTime.Now;
                    _context.Update(tmp);
                    await _context.SaveChangesAsync();
                    await _transactionService.AddTransaction(tmp, true); //true: Nhap kho, false: Xuat kho
                    string email = await _context.Users.Where(x => x.UserId == tmp.CreatorId).Select(x => x.Email).SingleAsync();
                    string approver = await _context.Users.Where(x => x.UserId == tmp.ApproverId).Select(x => x.Lastname + " " + x.Firstname).SingleAsync();
                    string body = UserUtils.BodyEmailForApproveRequestImport(tmp, approver);
                    EmailDTO emailDTO = new EmailDTO
                    {
                        Body = body,
                        Subject = "Thông báo yêu cầu được phê duyệt",
                        To = email
                    };
                    await transaction.CommitAsync();
                    await _emailService.sendEmail(emailDTO);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true, "Yêu cầu được phê duyệt", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<bool>> CreateRequestForImport(RequestCreateDTO model, string creatorId,
            IFormFile? cableFile, IFormFile? otherMaterialFile)
        {
            try
            {

                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    var tmp = _mapper.Map<Request>(model);
                    if (tmp.RequestCategoryId != (int)RequestCategoryEnum.Import) throw new ApplicationException("Không phải yêu cầu nhập kho");

                    tmp.RequestId = Guid.NewGuid();
                    tmp.IsDeleted = false;
                    tmp.CreatorId = Guid.Parse(creatorId);
                    tmp.CreatedAt = DateTime.Now;
                    tmp.ApproverId = null;
                    tmp.Status = RequestStatusConstant.Pending;
                    List<Cable> listCables = new List<Cable>();
                    List<OtherMaterial> listMat = new List<OtherMaterial>();
                    if (cableFile != null) listCables = await _cableService.ImportExcelCablesInImportRequest(cableFile, creatorId);
                    if (otherMaterialFile != null) listMat = await _otherMaterialsService.ImportExcelMaterialsForImportRequest(otherMaterialFile);
                    if (listCables.Count > 0)
                    {
                        foreach (var item in listCables)
                        {
                            tmp.RequestCables.Add(
                                new RequestCable
                                {
                                    CableId = item.CableId,
                                    StartPoint = item.StartPoint,
                                    EndPoint = item.EndPoint,
                                    Length = item.EndPoint - item.StartPoint,
                                    ImportedWarehouseId = item.WarehouseId
                                }
                                );
                        }
                    }
                    if (listMat.Count > 0)
                    {
                        foreach (var item in listMat)
                        {
                            var tmp1 = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory).FirstOrDefaultAsync(x => x.OtherMaterialsCategoryId == item.OtherMaterialsCategoryId
                                && x.WarehouseId == item.WarehouseId && x.Status == item.Status && x.SupplierId == item.SupplierId && x.Unit == item.Unit
                                && x.Code == item.Code);
                            if (tmp1 == null)
                            {
                                OtherMaterial otherMaterial = new OtherMaterial
                                {
                                    OtherMaterialsCategoryId = item.OtherMaterialsCategoryId,
                                    Quantity = 0,
                                    Code = item.Code,
                                    Status = item.Status,
                                    SupplierId = item.SupplierId,
                                    Unit = item.Unit,
                                    WarehouseId = item.WarehouseId
                                };
                                await _context.AddAsync(otherMaterial);
                                await _context.SaveChangesAsync();
                                item.OtherMaterialsId = otherMaterial.OtherMaterialsId;
                            }
                            else
                            {
                                item.OtherMaterialsId = tmp1.OtherMaterialsId;
                            }
                            tmp.RequestOtherMaterials.Add(
                                new RequestOtherMaterial { OtherMaterialsId = item.OtherMaterialsId, Quantity = (int)item.Quantity,
                                ImportedWarehouseId = item.WarehouseId}
                                );
                        }
                    }
                    await _context.AddAsync(tmp);
                    await _context.SaveChangesAsync();
                    var userAdmins = await _context.Users.Where(x => x.RoleId == (int)RoleUserEnum.Admin).Select(x => x.Email).ToListAsync();
                    await _context.RequestCategories.FirstOrDefaultAsync(x => x.RequestCategoryId == tmp.RequestCategoryId);
                    var body = UserUtils.BodyEmailForAdminReceiveRequest(tmp);
                    await transaction.CommitAsync();
                    foreach (var userAdmin in userAdmins)
                    {
                        EmailDTO emailDTO = new EmailDTO
                        {
                            Body = body,
                            Subject = "[FPT TELECOM CABLE MANAGEMENT] Thông báo có yêu cầu mới",
                            To = userAdmin
                        };
                        await _emailService.sendEmail(emailDTO);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new BaseResponseDTO<bool>(true, "Tạo yêu cầu thành công", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<bool>> CreateRequestForRecovery(RequestCreateDTO model, string creatorId)
        {
            try
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    var tmp = _mapper.Map<Request>(model);
                    if (tmp.RequestCategoryId != (int)RequestCategoryEnum.Recovery) throw new ApplicationException("Không phải yêu cầu thu hồi");

                    tmp.RequestId = Guid.NewGuid();
                    tmp.IsDeleted = false;
                    tmp.CreatorId = Guid.Parse(creatorId);
                    tmp.CreatedAt = DateTime.Now;
                    tmp.ApproverId = null;
                    tmp.Status = RequestStatusConstant.Pending;
                    var listCables = _mapper.Map<List<Cable>>(model.CableImportDTOs);
                    var listMat = _mapper.Map<List<OtherMaterial>>(model.OtherMaterialAddDTOs);


                    if (listCables.Count > 0)
                    {
                        foreach (var item in listCables)
                        {
                            item.CableId = Guid.NewGuid();
                            item.Length = item.EndPoint - item.StartPoint;
                            item.CreatorId = Guid.Parse(creatorId);
                            item.IsInRequest = true;
                        }
                        await _context.AddRangeAsync(listCables);
                        await _context.SaveChangesAsync();
                        foreach (var item in listCables)
                        {
                            tmp.RequestCables.Add(
                                new RequestCable
                                {
                                    CableId = item.CableId,
                                    StartPoint = item.StartPoint,
                                    EndPoint = item.EndPoint,
                                    Length = item.EndPoint - item.StartPoint,
                                    RecoveryDestWarehouseId = item.WarehouseId
                                }
                                );
                        }
                    }
                    if (model.RequestCableDTOs != null && model.RequestCableDTOs.Count > 0)
                    {
                        var tmpReCabs = _mapper.Map<List<RequestCable>>(model.RequestCableDTOs);
                        foreach (var item in tmpReCabs)
                        {
                            tmp.RequestCables.Add(item);
                        }
                    }
                    if (listMat.Count > 0)
                    {
                        foreach (var item in listMat)
                        {
                            var tmp1 = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory).FirstOrDefaultAsync(x => x.OtherMaterialsCategoryId == item.OtherMaterialsCategoryId
                                && x.WarehouseId == item.WarehouseId && x.Status == item.Status && x.SupplierId == item.SupplierId && x.Unit == item.Unit
                                && x.Code == item.Code);
                            if (tmp1 == null)
                            {
                                OtherMaterial otherMaterial = new OtherMaterial
                                {
                                    OtherMaterialsCategoryId = item.OtherMaterialsCategoryId,
                                    Quantity = 0,
                                    Code = item.Code,
                                    Status = item.Status,
                                    SupplierId = item.SupplierId,
                                    Unit = item.Unit,
                                    WarehouseId = item.WarehouseId
                                };
                                await _context.AddAsync(otherMaterial);
                                await _context.SaveChangesAsync();
                                item.OtherMaterialsId = otherMaterial.OtherMaterialsId;
                            }
                            else
                            {
                                item.OtherMaterialsId = tmp1.OtherMaterialsId;
                            }
                            tmp.RequestOtherMaterials.Add(
                                new RequestOtherMaterial { OtherMaterialsId = item.OtherMaterialsId, Quantity = (int)item.Quantity,
                                RecoveryDestWarehouseId = item.WarehouseId}
                                );
                        }
                    }
                    await _context.AddAsync(tmp);
                    await _context.SaveChangesAsync();
                    var userAdmins = await _context.Users.Where(x => x.RoleId == (int)RoleUserEnum.Admin).Select(x => x.Email).ToListAsync();
                    await _context.RequestCategories.FirstOrDefaultAsync(x => x.RequestCategoryId == tmp.RequestCategoryId);
                    var body = UserUtils.BodyEmailForAdminReceiveRequest(tmp);
                    await transaction.CommitAsync();
                    foreach (var userAdmin in userAdmins)
                    {
                        EmailDTO emailDTO = new EmailDTO
                        {
                            Body = body,
                            Subject = "[FPT TELECOM CABLE MANAGEMENT] Thông báo có yêu cầu mới",
                            To = userAdmin
                        };
                        await _emailService.sendEmail(emailDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new BaseResponseDTO<bool>(true, "Tạo yêu cầu thành công", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<bool>> ApproveRequestForRecovery(Guid requestId, string approverId)
        {
            try
            {
                var tmp = await _context.Requests
                    .Include(x => x.RequestCables).ThenInclude(x => x.Cable).ThenInclude(x => x.CableCategory)
                    .Include(x => x.RequestCables).ThenInclude(x => x.Cable).ThenInclude(x => x.Warehouse)
                    .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.OtherMaterials).ThenInclude(x => x.OtherMaterialsCategory)
                    .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.OtherMaterials).ThenInclude(x => x.WareHouse)
                    .Include(x => x.RequestCables).ThenInclude(x => x.RecoveryDestWarehouse)
                    .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.RecoveryDestWarehouse)
                    .Include(x => x.RequestCategory)
                .FirstOrDefaultAsync(x => x.RequestId == requestId && x.IsDeleted == false
                && x.Status.ToLower().Equals(RequestStatusConstant.Pending.ToLower())
                );
                if (tmp == null)
                {
                    throw new ApplicationException(MessageConst.Request.Request_Not_Found);
                }
                if (tmp.RequestCategoryId != (int)RequestCategoryEnum.Recovery) throw new ApplicationException("Không phải yêu cầu thu hồi");

                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    List<RequestCable> listRqCabs = tmp.RequestCables.ToList();
                    for (int i = 0; i < listRqCabs.Count; i++)
                    {
                        var tmp1 = await _context.Cables.Include(x => x.CableCategory).FirstOrDefaultAsync(x => x.CableId == listRqCabs[i].CableId
                        //&& x.IsInRequest == true
                        );
                        if (tmp1 == null || tmp1.IsDeleted == true)
                            throw new ApplicationException(tmp1.CableCategory.CableCategoryName + " với ID: " + listRqCabs[i].CableId
                                + " không tồn tại ");
                        if (listRqCabs[i].RecoveryDestWarehouseId != null) tmp1.WarehouseId = listRqCabs[i].RecoveryDestWarehouseId;
                        tmp1.IsExportedToUse = false;
                        tmp1.IsInRequest = false;
                        tmp1.UpdateAt = DateTime.Now;
                        _context.Update(tmp1);

                    }
                    foreach (var tmpRqMat in tmp.RequestOtherMaterials)
                    {
                        var tmp1 = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory).FirstOrDefaultAsync(x => x.OtherMaterialsId == tmpRqMat.OtherMaterialsId);
                        //if (tmpRqMat.Quantity + tmp1.Quantity > tmp1.MaxQuantity) throw new ApplicationException("Warehouse cannot contain these " + tmp1.OtherMaterialsName);
                        tmp1.Quantity += tmpRqMat.Quantity;
                        tmp1.UpdateAt = DateTime.Now;
                        _context.Update(tmp1);
                    }
                    tmp.Status = RequestStatusConstant.Approved;
                    tmp.ApproverId = Guid.Parse(approverId);
                    tmp.UpdateAt = DateTime.Now;
                    _context.Update(tmp);
                    await _context.SaveChangesAsync();
                    await _transactionService.AddTransaction(tmp, true); //true: Nhap kho, false: Xuat kho
                    string email = await _context.Users.Where(x => x.UserId == tmp.CreatorId).Select(x => x.Email).SingleAsync();
                    string approver = await _context.Users.Where(x => x.UserId == tmp.ApproverId).Select(x => x.Lastname + " " + x.Firstname).SingleAsync();
                    string body = UserUtils.BodyEmailForApproveRequestRecovery(tmp, approver);
                    EmailDTO emailDTO = new EmailDTO
                    {
                        Body = body,
                        Subject = "Thông báo yêu cầu được phê duyệt",
                        To = email
                    };
                    await transaction.CommitAsync();
                    await _emailService.sendEmail(emailDTO);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true, "Yêu cầu được phê duyệt", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<bool>> CreateRequestForCancelMaterialOutsideWarehouse(RequestCreateDTO model, string creatorId)
        {
            try
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    var tmp = _mapper.Map<Request>(model);

                    tmp.RequestId = Guid.NewGuid();
                    tmp.IsDeleted = false;
                    tmp.CreatorId = Guid.Parse(creatorId);
                    tmp.CreatedAt = DateTime.Now;
                    tmp.ApproverId = null;
                    tmp.Status = RequestStatusConstant.Pending;
                    //tmp.RequestCables = _mapper.Map<List<RequestCable>>(model.RequestCableDTOs);
                    //tmp.RequestOtherMaterials = _mapper.Map<List<RequestOtherMaterial>>(model.RequestOtherMaterialDTOs);
                    var listCables = _mapper.Map<List<Cable>>(model.CableImportDTOs);
                    var listMat = _mapper.Map<List<OtherMaterial>>(model.OtherMaterialAddDTOs);


                    if (listCables.Count > 0)
                    {
                        foreach (var item in listCables)
                        {
                            item.CableId = Guid.NewGuid();
                            item.Length = item.EndPoint - item.StartPoint;
                            item.CreatorId = Guid.Parse(creatorId);
                            item.IsInRequest = true;
                        }
                        await _context.AddRangeAsync(listCables);
                        await _context.SaveChangesAsync();
                        foreach (var item in listCables)
                        {
                            tmp.RequestCables.Add(
                                new RequestCable
                                {
                                    CableId = item.CableId,
                                    StartPoint = item.StartPoint,
                                    EndPoint = item.EndPoint,
                                    Length = item.EndPoint - item.StartPoint
                                }
                                );
                        }
                    }
                    if (listMat.Count > 0)
                    {
                        foreach (var item in listMat)
                        {
                            var tmp1 = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory).FirstOrDefaultAsync(x => x.OtherMaterialsCategoryId == item.OtherMaterialsCategoryId
                                && x.Status == item.Status && x.SupplierId == item.SupplierId && x.Unit == item.Unit
                                && x.Code == item.Code && x.IsDeleted == true);
                            if (tmp1 == null)
                            {
                                OtherMaterial otherMaterial = new OtherMaterial
                                {
                                    OtherMaterialsCategoryId = item.OtherMaterialsCategoryId,
                                    Quantity = 0,
                                    Code = item.Code,
                                    Status = item.Status,
                                    SupplierId = item.SupplierId,
                                    Unit = item.Unit,
                                    IsDeleted = true,
                                };
                                await _context.AddAsync(otherMaterial);
                                await _context.SaveChangesAsync();
                                item.OtherMaterialsId = otherMaterial.OtherMaterialsId;
                            }
                            else
                            {
                                item.OtherMaterialsId = tmp1.OtherMaterialsId;
                            }
                            tmp.RequestOtherMaterials.Add(
                                new RequestOtherMaterial { OtherMaterialsId = item.OtherMaterialsId, Quantity = (int)item.Quantity }
                                );
                        }
                    }
                    await _context.AddAsync(tmp);
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true, "Tạo yêu cầu thành công!", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<bool>> ApproveRequestForCancelMaterialOutsideWarehouse(Guid requestId, string approverId)
        {
            try
            {
                var tmp = await _context.Requests.Include(x => x.RequestCables).ThenInclude(x => x.Cable).ThenInclude(x => x.CableCategory)
                    .Include(x => x.RequestOtherMaterials).ThenInclude(x => x.OtherMaterials).ThenInclude(x => x.OtherMaterialsCategory)
                    .Include(x => x.RequestCategory)
                    .Include(x => x.Issue)
                .FirstOrDefaultAsync(x => x.RequestId == requestId && x.IsDeleted == false
                && x.Status.ToLower().Equals(RequestStatusConstant.Pending.ToLower())
                );
                if (tmp == null)
                {
                    throw new ApplicationException(MessageConst.Request.Request_Not_Found);
                }
                if (tmp.RequestCategoryId != (int)RequestCategoryEnum.CancelOutside) throw new ApplicationException("Không phải yêu cầu hủy ngoài kho");

                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    List<RequestCable> listRqCabs = tmp.RequestCables.ToList();
                    for (int i = 0; i < listRqCabs.Count; i++)
                    {
                        var tmp1 = await _context.Cables.Include(x => x.CableCategory).FirstOrDefaultAsync(x => x.CableId == listRqCabs[i].CableId
                        && x.IsInRequest == true);
                        if (tmp1 == null || tmp1.IsDeleted == true)
                            throw new ApplicationException(tmp1.CableCategory.CableCategoryName + " với ID: " + listRqCabs[i].CableId
                                + " không tồn tại ");

                        tmp1.IsInRequest = false; tmp1.IsDeleted = true;
                        tmp1.UpdateAt = DateTime.Now;
                        _context.Update(tmp1);

                    }

                    tmp.Status = RequestStatusConstant.Approved;
                    tmp.ApproverId = Guid.Parse(approverId);
                    tmp.UpdateAt = DateTime.Now;
                    _context.Update(tmp);
                    await _context.SaveChangesAsync();
                    string email = await _context.Users.Where(x => x.UserId == tmp.CreatorId).Select(x => x.Email).SingleAsync();
                    string approver = await _context.Users.Where(x => x.UserId == tmp.ApproverId).Select(x => x.Lastname + " " + x.Firstname).SingleAsync();
                    string body = UserUtils.BodyEmailForApproveRequestCancelOutsideWarehouse(tmp, approver);
                    EmailDTO emailDTO = new EmailDTO
                    {
                        Body = body,
                        Subject = "Thông báo yêu cầu được phê duyệt",
                        To = email
                    };
                    await transaction.CommitAsync();
                    await _emailService.sendEmail(emailDTO);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true, "Yêu cầu được phê duyệt", StatusCodes.Status200OK);
        }
    }
}
