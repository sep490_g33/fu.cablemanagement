﻿using AutoMapper;
using DocumentFormat.OpenXml.Drawing.Charts;
using FU.CableManagement.API.Infra.Constants;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialsDTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    public class OtherMaterialsService : IOtherMaterialsService
    {
        private readonly ILogger<OtherMaterialsService> _logger;
        private readonly IConfiguration _configuration;
        private readonly CableManagementContext _context;
        private readonly IMapper _mapper;

        private const string a1_value = "Tên Loại Vật Liệu";
        private const string b1_value = "Đơn Vị";
        private const string c1_value = "Số Lượng";
        private const string d1_value = "Tên Kho";
        private const string e1_value = "Mã Hàng";
        private const string f1_value = "Tên Nhà Cung Cấp";
        private const string g1_value = "Trạng Thái";
        //private const string g1_value = "Số lượng tối thiểu";
        //private const string h1_value = "Số lượng tối đa";

        public OtherMaterialsService(ILogger<OtherMaterialsService> logger, IConfiguration configuration, CableManagementContext context, IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
            _mapper = mapper;
        }

        public async Task<BaseResponseDTO<bool>> AddOtherMaterial(OtherMaterialAddDTO model)
        {
            try
            {
                if (model.SupplierId == null || model.SupplierId == 0) throw new ApplicationException("Nhà cung cấp không được để trống");
                if (model.WarehouseId == null || model.WarehouseId == 0) throw new ApplicationException("Kho hàng không được để trống");
                var checkTmp = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory)
                    .Where(x => x.OtherMaterialsCategoryId == model.OtherMaterialsCategoryId
                                && x.Code == model.Code && x.WarehouseId == model.WarehouseId && x.Status.ToLower().Equals(model.Status.ToLower())
                                && x.SupplierId == model.SupplierId && x.Unit.ToLower().Equals(model.Unit.ToLower())).FirstOrDefaultAsync();
                if (checkTmp != null)
                {
                    checkTmp.Quantity += model.Quantity; checkTmp.UpdateAt = DateTime.Now;
                    _context.Update(checkTmp);
                    await _context.SaveChangesAsync();
                }
                else
                {
                    var tmp = _mapper.Map<OtherMaterial>(model);
                    tmp.IsDeleted = false;
                    tmp.CreatedAt = DateTime.Now;
                    await _context.AddAsync(tmp);
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new BaseResponseDTO<bool>(true, "Thêm thành công", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<bool>> DeleteOtherMaterial(int otherMaterialId)
        {
            var tmp = await _context.OtherMaterials.FirstOrDefaultAsync(x => x.OtherMaterialsId == otherMaterialId && x.IsDeleted == false);
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.OtherMaterial.Other_Material_Not_Found);
            }
            tmp.IsDeleted = true;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<PagedResultDTO<OtherMaterialDTO>>> GetAllOtherMaterials(string filter, int pageSize, int pageIndex,
            int? warehouseId)
        {
            if (filter != null) filter = filter.Trim();
            var query = _context.OtherMaterials.Include(x => x.Supplier)
                .Include(x => x.WareHouse).Include(x => x.OtherMaterialsCategory)
                .Where(x => x.IsDeleted == false && (string.IsNullOrEmpty(filter) || x.OtherMaterialsCategory.OtherMaterialsCategoryName.ToLower().Contains(filter.ToLower()) 
                || EF.Functions.Collate(x.OtherMaterialsCategory.OtherMaterialsCategoryName.ToLower(), "Latin1_General_CI_AI").Contains(filter.ToLower())
            || x.Code.ToLower().Contains(filter.ToLower())
            || x.Supplier.SupplierName.ToLower().Contains(filter.ToLower()))
                );
            if (warehouseId != null) query = query.Where(x => x.WarehouseId == warehouseId);
            var sum = await query.SumAsync(x => x.Quantity);
            var data = await query.OrderByDescending(x => x.UpdateAt).Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .ToListAsync();
            var result = _mapper.Map<List<OtherMaterialDTO>>(data);
            var pageResult = new PagedResultDTO<OtherMaterialDTO>(pageIndex, pageSize, await query.CountAsync(), result, (int)sum);
            return new BaseResponseDTO<PagedResultDTO<OtherMaterialDTO>>(pageResult);
        }

        public async Task<BaseResponseDTO<OtherMaterialDTO>> GetOtherMaterialById(int otherMaterialId)
        {
            var data = await _context.OtherMaterials.Include(x => x.Supplier).Include(x => x.WareHouse)
                .Include(x => x.NodeMaterials.Where(x => x.IsDeleted == false))
                .ThenInclude(x => x.Node).FirstOrDefaultAsync(x => x.OtherMaterialsId == otherMaterialId && x.IsDeleted == false);
            if (data == null)
            {
                throw new ApplicationException(MessageConst.OtherMaterial.Other_Material_Not_Found);
            }
            var result = _mapper.Map<OtherMaterialDTO>(data);
            return new BaseResponseDTO<OtherMaterialDTO>(result);
        }

        public async Task<BaseResponseDTO<bool>> ImportExcelMaterials(IFormFile file)
        {
            if (file == null)
            {
                throw new ApplicationException("File không được để trống");
            }
            var extension = Path.GetExtension(file.FileName);
            if (extension != ".xlsx")
            {
                throw new ApplicationException("Định dạng file phài là xlsx");
            }
            var baseDirectory = Directory.GetCurrentDirectory();

            string filePath = Path.Combine(baseDirectory, file.FileName);
            if (File.Exists(filePath))
                File.Delete(filePath);
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {

                    // Get the first worksheet from the package
                    var worksheet = package.Workbook.Worksheets[0];
                    var validate = ValidateExcel(worksheet);
                    if (!validate)
                    {
                        stream.Close();
                        File.Delete(filePath);
                        throw new ApplicationException("Sai định dạng trường dữ liệu");
                    }
                    // Read the data from the worksheet
                    var listMaterials = new List<OtherMaterial>();
                    var rowCount = worksheet.Dimension.Rows;
                    //var columnCount = worksheet.Dimension.Columns;
                    for (int i = 2; i <= rowCount; ++i)
                    {
                        var otherMaterialsName = worksheet.Cells[i, 1].Value?.ToString().Trim();
                        var unit = worksheet.Cells[i, 2].Value?.ToString().Trim();
                        var quantity = worksheet.Cells[i, 3].Value?.ToString().Trim();
                        var warehouse = worksheet.Cells[i, 4].Value?.ToString().Trim();
                        var code = worksheet.Cells[i, 5].Value?.ToString().Trim();
                        var supplier = worksheet.Cells[i, 6].Value?.ToString().Trim();
                        var status = worksheet.Cells[i, 7].Value?.ToString().Trim();
                        //var minQuantityRaw = worksheet.Cells[i, 7].Value?.ToString().Trim();
                        //var maxQuantityRaw = worksheet.Cells[i, 8].Value?.ToString().Trim();
                        if (string.IsNullOrEmpty(otherMaterialsName) || string.IsNullOrEmpty(unit) || string.IsNullOrEmpty(quantity)
                            || string.IsNullOrEmpty(code) || string.IsNullOrEmpty(warehouse) || string.IsNullOrEmpty(supplier)
                            || string.IsNullOrEmpty(status))
                            throw new ApplicationException("Các ô dữ liệu bắt buộc không được để trống");
                        var warehouseId = await _context.Warehouses.Where(x => x.WarehouseName.ToLower().Equals(warehouse.ToLower()))
                            .Select(x => x.WarehouseId).SingleOrDefaultAsync();
                        if (warehouseId == 0) throw new ApplicationException("Kho hàng \'" + warehouse + "\' không tồn tại trong hệ thống");

                        var supplierId = await _context.Suppliers.Where(x => x.SupplierName.ToLower().Equals(supplier.ToLower()))
                            .Select(x => x.SupplierId).SingleOrDefaultAsync();
                        if (supplierId == 0) throw new ApplicationException("Nhà cung cấp \'" + supplier + "\' không tồn tại trong hệ thống");
                        var otherCategoryId = await _context.OtherMaterialsCategories.Where(x => x.OtherMaterialsCategoryName.ToLower().Equals(otherMaterialsName.ToLower()))
                                    .Select(x => x.OtherMaterialsCategoryId).SingleOrDefaultAsync();

                        if(otherCategoryId == 0)
                        {
                            //var newMaterialCategory = new OtherMaterialsCategory { OtherMaterialsCategoryName = otherMaterialsName };
                            //await _context.AddAsync(newMaterialCategory);
                            //otherCategoryId = newMaterialCategory.OtherMaterialsCategoryId;
                            throw new ApplicationException("Hệ thống không có chủng loại vật liệu: " + otherMaterialsName);
                        }

                        var checkTmp = await _context.OtherMaterials.Where(x => x.OtherMaterialsCategoryId == otherCategoryId
                                && x.Code.Equals(code) && x.WarehouseId == warehouseId && x.Status.ToLower() == status.ToLower()
                                && x.SupplierId == supplierId && x.Unit.ToLower().Equals(unit.ToLower())).FirstOrDefaultAsync();
                        if (checkTmp != null)
                        {
                            checkTmp.Quantity += int.Parse(quantity); checkTmp.UpdateAt = DateTime.Now;
                            _context.Update(checkTmp);
                            continue;
                        }
                        listMaterials.Add(new OtherMaterial
                        {
                            OtherMaterialsCategoryId = otherCategoryId,
                            Unit = unit,
                            Quantity = int.Parse(quantity),
                            WarehouseId = warehouseId,
                            Code = code,
                            SupplierId = supplierId,
                            Status = status,
                            //MinQuantity = int.Parse(minQuantityRaw),
                            //MaxQuantity = int.Parse(maxQuantityRaw),
                            CreatedAt = DateTime.Now,
                            IsDeleted = false
                        });
                    }
                    await _context.AddRangeAsync(listMaterials);
                    await _context.SaveChangesAsync();
                }
                stream.Close();
                File.Delete(filePath);
            }
            return new BaseResponseDTO<bool>(true, "Import file thành công", StatusCodes.Status200OK);
        }
        private bool ValidateExcel(ExcelWorksheet worksheet)
        {
            var rowCount = worksheet.Dimension.Rows;
            var columnCount = worksheet.Dimension.Columns;
            if (rowCount < 1)
            {
                return false;
            }
            string A1 = worksheet.Cells[1, 1].Value?.ToString().Trim();
            string B1 = worksheet.Cells[1, 2].Value?.ToString().Trim();
            string C1 = worksheet.Cells[1, 3].Value?.ToString().Trim();
            string D1 = worksheet.Cells[1, 4].Value?.ToString().Trim();
            string E1 = worksheet.Cells[1, 5].Value?.ToString().Trim();
            string F1 = worksheet.Cells[1, 6].Value?.ToString().Trim();
            string G1 = worksheet.Cells[1, 7].Value?.ToString().Trim();
            if (A1 != a1_value || B1 != b1_value || C1 != c1_value || D1 != d1_value || E1 != e1_value || F1 != f1_value
                || G1 != g1_value
                //|| H1 != h1_value
                ) return false;
            return true;
        }
        public async Task<BaseResponseDTO<bool>> UpdateOtherMaterial(OtherMaterialUpdateDTO model)
        {
            try
            {
                if (model.SupplierId == null || model.SupplierId == 0) throw new ApplicationException("Nhà cung cấp không được để trống");
                if (model.WarehouseId == null || model.WarehouseId == 0) throw new ApplicationException("Kho hàng không được để trống");
                var tmp = await _context.OtherMaterials.FirstOrDefaultAsync(x => x.OtherMaterialsId == model.OtherMaterialsId
                && x.IsDeleted == false);
                if (tmp == null)
                {
                    throw new ApplicationException(MessageConst.OtherMaterial.Other_Material_Not_Found);
                }
                var checkTmp = await _context.OtherMaterials.Include(x => x.OtherMaterialsCategory)
                    .Where(x => x.OtherMaterialsCategoryId == model.OtherMaterialsCategoryId && x.OtherMaterialsId != tmp.OtherMaterialsId
                                && x.Code == model.Code && x.WarehouseId == model.WarehouseId && x.Status.ToLower().Equals(model.Status.ToLower())
                                && x.SupplierId == model.SupplierId && x.Unit.ToLower().Equals(model.Unit.ToLower())).FirstOrDefaultAsync();
                if (checkTmp != null)
                {
                    throw new ApplicationException("Vật liệu đã tồn tại");
                }
                tmp = _mapper.Map(model, tmp);
                tmp.UpdateAt = DateTime.Now;
                _context.Update(tmp);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true, "Cập nhật thành công", StatusCodes.Status200OK);
        }

        public async Task<BaseResponseDTO<string>> WarningForImportMoreMaterial(int materialId)
        {
            var tmp = await _context.OtherMaterials.Include(x => x.WareHouse).Include(x => x.OtherMaterialsCategory.OtherMaterialsCategoryName)
                .FirstOrDefaultAsync(x => x.Quantity <= x.MinQuantity * WarehouseCapacityConstant.MinRate);
            if(tmp == null)
            {
                return new BaseResponseDTO<string>(null);
            }
            return new BaseResponseDTO<string>(tmp.OtherMaterialsCategory.OtherMaterialsCategoryName + " in " + tmp.WareHouse.WarehouseName + " need to be imported more."
                , "Warning sent successfully!", StatusCodes.Status200OK);
        }

        public async Task<List<OtherMaterial>> ImportExcelMaterialsForImportRequest(IFormFile file)
        {
            if (file == null)
            {
                throw new ApplicationException("File không được để trống");
            }
            var extension = Path.GetExtension(file.FileName);
            if (extension != ".xlsx")
            {
                throw new ApplicationException("Định dạng file phải là xlsx");
            }
            var baseDirectory = Directory.GetCurrentDirectory();

            string filePath = Path.Combine(baseDirectory, file.FileName);
            try
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);
                ExcelPackage.LicenseContext = LicenseContext.Commercial;
                var listMaterials = new List<OtherMaterial>();
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                    using (var package = new ExcelPackage(stream))
                    {

                        // Get the first worksheet from the package
                        var worksheet = package.Workbook.Worksheets[0];
                        var validate = ValidateExcel(worksheet);
                        if (!validate)
                        {
                            stream.Close();
                            File.Delete(filePath);
                            throw new ApplicationException("Sai định dạng trường dữ liệu");
                        }
                        // Read the data from the worksheet
                        var rowCount = worksheet.Dimension.Rows;
                        //var columnCount = worksheet.Dimension.Columns;
                        for (int i = 2; i <= rowCount; ++i)
                        {
                            var otherMaterialsName = worksheet.Cells[i, 1].Value?.ToString().Trim();
                            var unit = worksheet.Cells[i, 2].Value?.ToString().Trim();
                            var quantity = worksheet.Cells[i, 3].Value?.ToString().Trim();
                            var warehouse = worksheet.Cells[i, 4].Value?.ToString().Trim();
                            var code = worksheet.Cells[i, 5].Value?.ToString().Trim();
                            var supplier = worksheet.Cells[i, 6].Value?.ToString().Trim();
                            var status = worksheet.Cells[i, 7].Value?.ToString().Trim();
                            //var minQuantityRaw = worksheet.Cells[i, 7].Value?.ToString().Trim();
                            //var maxQuantityRaw = worksheet.Cells[i, 8].Value?.ToString().Trim();
                            if (string.IsNullOrEmpty(otherMaterialsName) || string.IsNullOrEmpty(unit) || string.IsNullOrEmpty(quantity)
                                || string.IsNullOrEmpty(code) || string.IsNullOrEmpty(warehouse) || string.IsNullOrEmpty(supplier)
                                || string.IsNullOrEmpty(status))
                                throw new ApplicationException("Các ô dữ liệu bắt buộc không được để trống");
                            var warehouseId = await _context.Warehouses.Where(x => x.WarehouseName.ToLower().Equals(warehouse.ToLower()))
                                .Select(x => x.WarehouseId).SingleOrDefaultAsync();
                            if (warehouseId == 0) throw new ApplicationException("Kho hàng \'" + warehouse + "\' không tồn tại trong hệ thống");

                            var supplierId = await _context.Suppliers.Where(x => x.SupplierName.ToLower().Equals(supplier.ToLower()))
                                .Select(x => x.SupplierId).SingleOrDefaultAsync();
                            if (supplierId == 0) throw new ApplicationException("Nhà cung cấp \'" + supplier + "\' không tồn tại trong hệ thống");
                            var otherCategoryId = await _context.OtherMaterialsCategories.Where(x => x.OtherMaterialsCategoryName.ToLower().Equals(otherMaterialsName.ToLower()))
                                        .Select(x => x.OtherMaterialsCategoryId).SingleOrDefaultAsync();

                            if (otherCategoryId == 0)
                            {
                                //var newMaterialCategory = new OtherMaterialsCategory { OtherMaterialsCategoryName = otherMaterialsName };
                                //await _context.AddAsync(newMaterialCategory);
                                //await _context.SaveChangesAsync();
                                //otherCategoryId = newMaterialCategory.OtherMaterialsCategoryId;
                                throw new ApplicationException("Hệ thống không có chủng loại vật liệu: " + otherMaterialsName);
                            }
                            
                            listMaterials.Add(new OtherMaterial
                            {
                                OtherMaterialsCategoryId = otherCategoryId,
                                Unit = unit,
                                Quantity = int.Parse(quantity),
                                WarehouseId = warehouseId,
                                Code = code,
                                SupplierId = supplierId,
                                //MinQuantity = int.Parse(minQuantityRaw),
                                //MaxQuantity = int.Parse(maxQuantityRaw),
                                CreatedAt = DateTime.Now,
                                IsDeleted = false
                            });
                        }

                    }
                    stream.Close();
                    File.Delete(filePath);
                }
                return listMaterials;
            }
            catch (Exception ex)
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);
                throw new Exception(ex.Message);
            }
        }
    }
}
