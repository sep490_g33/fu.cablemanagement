﻿using AutoMapper;
using ClosedXML.Excel;
using FU.CableManagement.API.Infra.Constants;
using FU.CableManagement.API.Services.IServices;
using FU.CableManagement.DataAccess.Data;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.SupplierDTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FU.CableManagement.API.Services.Services
{
    public class SupplierService : ISupplierService
    {
        private readonly ILogger<SupplierService> _logger;
        private readonly IConfiguration _configuration;
        private readonly CableManagementContext _context;
        private readonly IMapper _mapper;

        private const string a1_value = "Tên nhà cung cấp";
        private const string b1_value = "Quốc gia";
        private const string c1_value = "Mô tả";


        public SupplierService(ILogger<SupplierService> logger,
            IConfiguration configuration,
            CableManagementContext context,
            IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
            _mapper = mapper;
        }

        public async Task<BaseResponseDTO<bool>> CreateSupplier(SupplierDTO model, string creatorId)
        {
            try
            {
                var checkTmp = await _context.Suppliers.FirstOrDefaultAsync(x => x.SupplierName.ToLower() == model.SupplierName.ToLower()
                && x.IsDeleted == false);
                if(checkTmp != null)
                {
                    throw new ApplicationException("Nhà cung cấp này đã tồn tại");
                }
                var tmp = _mapper.Map<Supplier>(model);
                tmp.CreatorId = Guid.Parse(creatorId);
                await _context.AddAsync(tmp);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<bool>> DeleteSupplier(int supplierId)
        {
            var tmp = await _context.Suppliers.FirstOrDefaultAsync(x => x.SupplierId == supplierId);
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.SupplierMessage.Supplier_Not_Found);
            }
            tmp.IsDeleted = true;
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<PagedResultDTO<SupplierDTO>>> GetAllSupplier(string filter, int pageSize, int pageIndex)
        {
            if (filter != null) filter = filter.Trim();
            var query = _context.Suppliers
                .Where(x => x.IsDeleted == false && (string.IsNullOrEmpty(filter) || x.SupplierName.ToLower().Contains(filter.ToLower()) 
                || EF.Functions.Collate(x.SupplierName.ToLower(), "Latin1_General_CI_AI").Contains(filter.ToLower())));
            var data = await query.OrderByDescending(x => x.UpdateAt).Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .ToListAsync();
            var result = _mapper.Map<List<SupplierDTO>>(data);
            var pageResult = new PagedResultDTO<SupplierDTO>(pageIndex, pageSize, await query.CountAsync(), result);
            return new BaseResponseDTO<PagedResultDTO<SupplierDTO>>(pageResult);
        }

        public async Task<BaseResponseDTO<SupplierDTO>> GetSupplierById(int supplierId)
        {
            var data = await _context.Suppliers.FirstOrDefaultAsync(x => x.IsDeleted == false && x.SupplierId == supplierId);
            if (data == null)
            {
                throw new ApplicationException(MessageConst.SupplierMessage.Supplier_Not_Found);
            }
            var result = _mapper.Map<SupplierDTO>(data);
            return new BaseResponseDTO<SupplierDTO>(result);
        }

        public async Task<BaseResponseDTO<bool>> ImportSupplier(IFormFile file, string creatorId)
        {
            if (file == null)
            {
                throw new ApplicationException("File không được để trống");
            }
            var extension = Path.GetExtension(file.FileName);
            if (extension != ".xlsx")
            {
                throw new ApplicationException("Định dạng file phải là xlsx");
            }
            var baseDirectory = Directory.GetCurrentDirectory();

            string filePath = Path.Combine(baseDirectory, file.FileName);
            if (File.Exists(filePath))
                File.Delete(filePath);
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {

                    // Get the first worksheet from the package
                    var worksheet = package.Workbook.Worksheets[0];
                    var validate = ValidateExcel(worksheet);
                    if (!validate)
                    {
                        stream.Close();
                        File.Delete(filePath);
                        throw new ApplicationException("Sai định dạng trường dữ liệu");
                    }
                    // Read the data from the worksheet
                    var listSupplier = new List<Supplier>();
                    var rowCount = worksheet.Dimension.Rows;
                    //var columnCount = worksheet.Dimension.Columns;
                    for (int i = 2; i <= rowCount; ++i)
                    {
                        var name = worksheet.Cells[i, 1].Value?.ToString().Trim();
                        var country = worksheet.Cells[i, 2].Value?.ToString().Trim();
                        var description = worksheet.Cells[i, 3].Value?.ToString().Trim();
                        if (string.IsNullOrEmpty(name))
                            break;
                        listSupplier.Add(new Supplier
                        {
                            SupplierName = name,
                            Country = country,
                            SupplierDescription = description,
                            CreatorId = Guid.Parse(creatorId)
                        });
                    }
                    await _context.AddRangeAsync(listSupplier);
                    await _context.SaveChangesAsync();
                }
                stream.Close();
                File.Delete(filePath);
            }

            return new BaseResponseDTO<bool>(true);
        }

        private bool ValidateExcel(ExcelWorksheet worksheet)
        {
            var rowCount = worksheet.Dimension.Rows;
            var columnCount = worksheet.Dimension.Columns;
            if (rowCount < 1)
            {
                return false;
            }
            string A1 = worksheet.Cells[1, 1].Value?.ToString().Trim();
            string B1 = worksheet.Cells[1, 2].Value?.ToString().Trim();
            string C1 = worksheet.Cells[1, 3].Value?.ToString().Trim();
            if (A1 != a1_value || B1 != b1_value || C1 != c1_value) return false;
            return true;
        }

        public async Task<BaseResponseDTO<bool>> UpdateSupplier(SupplierDTO model)
        {
            var tmp = await _context.Suppliers.FirstOrDefaultAsync(x => x.SupplierId == model.SupplierId);
            if (tmp == null)
            {
                throw new ApplicationException(MessageConst.SupplierMessage.Supplier_Not_Found);
            }
            var tmp2 = await _context.Suppliers.FirstOrDefaultAsync(x => x.SupplierName.ToLower() == model.SupplierName.ToLower()
                && x.IsDeleted == false && x.SupplierId != tmp.SupplierId);
            if (tmp2 != null)
            {
                throw new ApplicationException("Nhà cung cấp đã tồn tại");
            }
            _mapper.Map(model, tmp);
            _context.Update(tmp);
            await _context.SaveChangesAsync();
            return new BaseResponseDTO<bool>(true);
        }

        public async Task<BaseResponseDTO<FileResponseDTO>> ExportSupplier()
        {
            var data = await GetAllSupplier("", 1, int.MaxValue);
            var fileName = "export_suppliers.xlsx";
            var baseDirectory = Directory.GetCurrentDirectory();
            var folder = baseDirectory + "\\DataExport";
            string filePath = Path.Combine(folder, fileName);
            if (File.Exists(filePath))
                File.Delete(filePath);


            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            var workbook = new XLWorkbook();
            IXLWorksheet worksheet = workbook.Worksheets.Add("Report Supplier");
            worksheet.Cell(1, 1).Value = "binhdzai";

            //worksheet.Range($"A1:M{listUserEnrolledItem.Count + 1}").Style.Border.TopBorder =
            //        XLBorderStyleValues.Thin;
            //worksheet.Range($"A1:M{listUserEnrolledItem.Count + 1}").Style.Border.LeftBorder =
            //    XLBorderStyleValues.Thin;
            //worksheet.Range($"A1:M{listUserEnrolledItem.Count + 1}").Style.Border.BottomBorder =
            //    XLBorderStyleValues.Thin;
            //worksheet.Range($"A1:M{listUserEnrolledItem.Count + 1}").Style.Border.RightBorder =
            //    XLBorderStyleValues.Thin;

            worksheet.Column(2).Width = 20;
            worksheet.Column(3).Width = 50;
            worksheet.Column(4).Width = 30;

            var stream = new MemoryStream();
            workbook.SaveAs(stream);

            //if (!Directory.Exists(folder))
            //{
            //    Directory.CreateDirectory(folder);
            //}


            //File.WriteAllBytes(filePath, stream.ToArray());
            var result = new FileResponseDTO
            {
                fileBytes = stream.ToArray(),
                FileName = fileName
            };
            //if (File.Exists(filePath))
            //    File.Delete(filePath);
            return new BaseResponseDTO<FileResponseDTO>(result);


        }
    }
}
