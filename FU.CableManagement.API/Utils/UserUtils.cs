﻿using DocumentFormat.OpenXml.Office2010.ExcelAc;
using FU.CableManagement.DataAccess.Data.Entities;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace FU.CableManagement.API.Utils
{
    public static class UserUtils
    {
        public static string HashPassword(string password)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
        public static string RandomPasword(int size)
        {
            Random res = new Random();

            // String that contain both alphabets and numbers
            String str = "abcdefghijklmnopqrstuvwxyz0123456789QWERTYUIOPASDFGHJKLZXCVBNM";

            // Initializing the empty string
            String randomstring = "";

            for (int i = 0; i < size; i++)
            {

                // Selecting a index randomly
                int x = res.Next(str.Length);

                // Appending the character at the 
                // index to the random alphanumeric string.
                randomstring = randomstring + str[x];
            }
            return randomstring;
        }
        public static string BodyEmailForNewUser(string password)
        {
            string body = "<h1>Mật khẩu cho tài khoản mới</h1>\n" +
                            "<p>Mật khẩu của bạn là: " + password + "</p>\n";
            return body;
        }

        public static string BodyEmailForForgetPassword(string password)
        {
            string body = "<h1>Mật khẩu mới</h1>\n" +
                            "<p>Mật khẩu mới là: " + password + "</p>\n" +
                            "<p>Không nên chia sẻ mật khẩu của bạn với người khác.</p>";
            return body;
        }

        public static string BodyEmailForApproveRequestExport(Request request, string approver)
        {
            StringBuilder body = new StringBuilder("<h1>Yêu cầu với tên \"" + request.RequestName + "\" đã được duyệt</h1>\n" +
                            "<p>Loại yêu cầu: " + request.RequestCategory.RequestCategoryName + "</p>\n" +
                            "<p>Mã sụ vụ: " + request.Issue.IssueCode + "</p>\n" +
                            "<p>Thông tin chi tiết của yêu cầu:</p>\n");
            if(request.RequestCables.Count > 0)
            {
                foreach (var item in request.RequestCables)
                {
                    body.AppendLine("<p> - " + item.Cable.CableCategory.CableCategoryName + " xuất từ kho " + item.Cable.Warehouse.WarehouseName
                        + " (chỉ số đầu: " + item.StartPoint + ", chỉ số cuối: " + item.EndPoint + ", độ dài: " + item.Length + ")</p>");
                }
            }
            if(request.RequestOtherMaterials.Count > 0)
            {
                foreach (var item in request.RequestOtherMaterials)
                {
                    body.AppendLine("<p> - " + item.OtherMaterials.OtherMaterialsCategory.OtherMaterialsCategoryName + " xuất từ trong kho " +
                        item.OtherMaterials.WareHouse.WarehouseName + ", số lượng: " + item.Quantity.ToString() + "</p>");
                }
            }
            body.AppendLine("<p>Người duyệt</p>");
            body.AppendLine("<p>" + approver + "</p>");
            return body.ToString();
        }

        public static string BodyEmailForApproveRequestDeliever(Request request, string approver)
        {
            StringBuilder body = new StringBuilder("<h1>Yêu cầu với tên \"" + request.RequestName + "\" đã được duyệt</h1>\n" +
                            "<p>Loại yêu cầu: " + request.RequestCategory.RequestCategoryName + "</p>\n" 
                            + "<p> Vật liệu được chuyển đến kho " + request.DeliverWarehouse.WarehouseName + "</p>"
                            + "<p>Thông tin chi tiết của yêu cầu:</p>\n");
            if(request.RequestCables.Count > 0)
            {
                foreach (var item in request.RequestCables)
                {
                    body.AppendLine("<p> - " + item.Cable.CableCategory.CableCategoryName + " xuất từ kho " + item.Cable.Warehouse.WarehouseName

                        + " (chỉ số đầu: " + item.StartPoint + ", chỉ số cuối: " + item.EndPoint + ", độ dài: " + item.Length + ")</p>");
                }
            }
            
            if(request.RequestOtherMaterials.Count > 0)
            {
                foreach (var item in request.RequestOtherMaterials)
                {
                    body.AppendLine("<p> - " + item.OtherMaterials.OtherMaterialsCategory.OtherMaterialsCategoryName + " xuất từ kho " +
                        item.OtherMaterials.WareHouse.WarehouseName + ", số lượng: " + item.Quantity.ToString() + "</p>");
                }
            }
            body.AppendLine("<p>Người duyệt</p>");
            body.AppendLine("<p>" + approver + "</p>");
            return body.ToString();
        }

        public static string BodyEmailForApproveRequestRecovery(Request request, string approver)
        {
            StringBuilder body = new StringBuilder("<h1>Yêu cầu với tên \"" + request.RequestName + "\" đã được duyệt</h1>\n" +
                            "<p>Loại yêu cầu: " + request.RequestCategory.RequestCategoryName + "</p>\n" +
                            "<p>Mã sụ vụ: " + request.Issue.IssueCode + "</p>\n" +
                            "<p>Thông tin chi tiết của yêu cầu:</p>\n");
            if(request.RequestCables.Count > 0)
            {
                foreach (var item in request.RequestCables)
                {
                    body.AppendLine("<p> - " + item.Cable.CableCategory.CableCategoryName + " được thu hồi về kho " + item.Cable.Warehouse.WarehouseName
                        + " (chỉ số đầu: " + item.StartPoint + ", chỉ số cuối: " + item.EndPoint + ", độ dài: " + item.Length + ")</p>");
                }
            }
            if(request.RequestOtherMaterials.Count > 0)
            {
                foreach (var item in request.RequestOtherMaterials)
                {
                    body.AppendLine("<p> - " + item.OtherMaterials.OtherMaterialsCategory.OtherMaterialsCategoryName + " được thu hồi về kho " +
                        item.OtherMaterials.WareHouse.WarehouseName + ", số lượng: " + item.Quantity.ToString() + "</p>");
                }
            }
            body.AppendLine("<p>Người duyệt</p>");
            body.AppendLine("<p>" + approver + "</p>");
            return body.ToString();
        }

        public static string BodyEmailForApproveRequestCancelInWarehouse(Request request, string approver)
        {
            StringBuilder body = new StringBuilder("<h1>Yêu cầu với tên \"" + request.RequestName + "\" đã được duyệt</h1>\n" +
                            "<p>Loại yêu cầu: " + request.RequestCategory.RequestCategoryName + "</p>\n" +
                            "<p>Mã sụ vụ: " + request.Issue.IssueCode + "</p>\n" +
                            "<p>Thông tin chi tiết của yêu cầu:</p>\n");
            if(request.RequestCables.Count > 0)
            {
                foreach (var item in request.RequestCables)
                {
                    body.AppendLine("<p> - " + item.Cable.CableCategory.CableCategoryName + " được hủy trong kho " + item.Cable.Warehouse.WarehouseName
                        + " (chỉ số đầu: " + item.StartPoint + ", chỉ số cuối: " + item.EndPoint + ", độ dài: " + item.Length + ")</p>");
                }
            }
            if(request.RequestOtherMaterials.Count > 0)
            {
                foreach (var item in request.RequestOtherMaterials)
                {
                    body.AppendLine("<p> - " + item.OtherMaterials.OtherMaterialsCategory.OtherMaterialsCategoryName + " được hủy trong kho " +
                        item.OtherMaterials.WareHouse.WarehouseName + ", số lượng: " + item.Quantity.ToString() + "</p>");
                }
            }
            body.AppendLine("<p>Người duyệt</p>");
            body.AppendLine("<p>" + approver + "</p>");
            return body.ToString();
        }

        public static string BodyEmailForApproveRequestCancelOutsideWarehouse(Request request, string approver)
        {
            StringBuilder body = new StringBuilder("<h1>Yêu cầu với tên \"" + request.RequestName + "\" đã được duyệt</h1>\n" +
                            "<p>Loại yêu cầu: " + request.RequestCategory.RequestCategoryName + " (ngoài tuyến)</p>\n" +
                            "<p>Mã sụ vụ: " + request.Issue.IssueCode + "</p>\n" +
                            "<p>Thông tin chi tiết của yêu cầu:</p>\n");
            if(request.RequestCables.Count > 0)
            {
                foreach (var item in request.RequestCables)
                {
                    body.AppendLine("<p> - " + item.Cable.CableCategory.CableCategoryName
                        + " (chỉ số đầu: " + item.StartPoint + ", chỉ số cuối: " + item.EndPoint + ", độ dài: " + item.Length + ")</p>");
                }
            }
            if(request.RequestOtherMaterials.Count > 0)
            {
                foreach (var item in request.RequestOtherMaterials)
                {
                    body.AppendLine("<p> - " + item.OtherMaterials.OtherMaterialsCategory.OtherMaterialsCategoryName + ", số lượng: " + item.Quantity.ToString() + "</p>");
                }
            }
            body.AppendLine("<p>Người duyệt</p>");
            body.AppendLine("<p>" + approver + "</p>");
            return body.ToString();
        }

        public static string BodyEmailForApproveRequestImport(Request request, string approver)
        {
            StringBuilder body = new StringBuilder("<h1>Yêu cầu với tên \"" + request.RequestName + "\" đã được duyệt</h1>\n" +
                            "<p>Loại yêu cầu: " + request.RequestCategory.RequestCategoryName + "</p>\n" +
                            "<p>Mã sụ vụ: " + request.Issue.IssueCode + "</p>\n" +
                            "<p>Thông tin chi tiết của yêu cầu:</p>\n");
            if(request.RequestCables.Count > 0)
            {
                foreach (var item in request.RequestCables)
                {
                    body.AppendLine("<p> - " + item.Cable.CableCategory.CableCategoryName + " được nhập về kho " + item.Cable.Warehouse.WarehouseName
                        + " (chỉ số đầu: " + item.StartPoint + ", chỉ số cuối: " + item.EndPoint + ", độ dài: " + item.Length + ")</p>");
                }
            }
            if(request.RequestOtherMaterials.Count > 0)
            {
                foreach (var item in request.RequestOtherMaterials)
                {
                    body.AppendLine("<p> - " + item.OtherMaterials.OtherMaterialsCategory.OtherMaterialsCategoryName + " được nhập về kho " +
                        item.OtherMaterials.WareHouse.WarehouseName + ", số lượng: " + item.Quantity.ToString() + "</p>");
                }
            }
            body.AppendLine("<p>Người duyệt</p>");
            body.AppendLine("<p>" + approver + "</p>");
            return body.ToString();
        }

        public static string BodyEmailForAdminReceiveRequest(Request request)
        {

            StringBuilder body = new StringBuilder("<h1>Yêu cầu với tên \"" + request.RequestName + "\"</h1>\n" +
                            "<p>Loại yêu cầu: " + request.RequestCategory.RequestCategoryName + "</p>\n");

            if (request.Issue != null)
            {
                body.AppendLine("<p>Mã sụ vụ: " + request?.Issue?.IssueCode + "</p>\n");
            }
                            

            body.AppendLine("<p>Vui lòng kiểm tra chi tiết yêu cầu</p>\n");
            return body.ToString();
        }
    }
}
