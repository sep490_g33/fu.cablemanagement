﻿namespace FU.CableManagement.API.Models.UserModels
{
    public class UserLoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
