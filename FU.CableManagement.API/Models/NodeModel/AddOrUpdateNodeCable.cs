﻿using System;
using System.Collections.Generic;

namespace FU.CableManagement.API.Models.NodeModel
{
    public class AddOrUpdateNodeCable
    {
        public Guid CableId { get; set; }
        public List<Guid> ListNodeIds { get; set; }
    }
}
