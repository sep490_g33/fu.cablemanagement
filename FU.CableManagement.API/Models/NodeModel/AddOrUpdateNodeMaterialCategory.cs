﻿using System;
using System.Collections.Generic;

namespace FU.CableManagement.API.Models.NodeModel
{
    /// <summary>
    /// 
    /// </summary>
    public class AddOrUpdateNodeMaterialCategory
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid NodeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<AddNodeMaterialCategory> ListAddOrUpdateNodeMaterialCategories { get; set; }
    }
    public class AddNodeMaterialCategory
    {
        public int MaterialCategoryId { get; set; }
        public int Quantity { get; set; } = 1;
    }
}
