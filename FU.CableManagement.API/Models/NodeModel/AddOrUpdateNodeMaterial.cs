﻿using System;
using System.Collections.Generic;

namespace FU.CableManagement.API.Models.NodeModel
{
    public class AddOrUpdateNodeMaterial
    {
        public Guid NodeId { get; set; }
        public List<AddNodeMaterial> ListAddOrUpdateNodeMaterials { get; set; }
    }
    public class AddNodeMaterial
    {
        public int MaterialId { get; set; }
        public int Quantity { get; set; } = 1;
    }
}
