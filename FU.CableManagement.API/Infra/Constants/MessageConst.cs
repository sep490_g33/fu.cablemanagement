﻿namespace FU.CableManagement.API.Infra.Constants
{
    public static class MessageConst
    {
        public static class SupplierMessage
        {
            public const string Supplier_Not_Found = "Không tìm thấy nhà cung cấp";
        }

        public static class CableMessage
        {
            public const string Cable_Not_Found = "Không tìm thấy cáp";
        }

        public static class WareHouseMessage
        {
            public const string WareHouse_Not_Found = "Không tìm thấy kho hàng";
        }

        public static class OtherMaterialCategory
        {
            public const string Other_Material_Category_Not_Found = "Không tìm thấy chủng loại vật liệu";
        }

        public static class CableCategoryMessage
        {
            public const string Cable_Category_Not_Found = "Không tìm thấy chủng loại cáp";
        }

        public static class IssueMessage
        {
            public const string Issue_Not_Found = "Không tìm thấy sự vụ";
        }

        public static class OtherMaterial
        {
            public const string Other_Material_Not_Found = "Không tìm thấy vật liệu";
        }

        public static class TransactionHistory
        {
            public const string Transaction_History_Not_Found = "Không tìm thấy lịch sử giao dịch";
        }
        public static class Request
        {
            public const string Request_Not_Found = "Không tìm thấy yêu cầu";
        }

        public static class NodeMessage
        {
            public const string Node_Not_Found = "Không tìm thấy điểm";
            public const string Cannot_Parse_Double = "Lỗi khi nhập dữ liệu";
        }

        public static class RequestCategoryMessage
        {
            public const string Request_Category_Not_Found = "Không tìm thấy loại yêu cầu";
        }

        public static class RouteMessage
        {
            public const string Route_Name_Existed = "Tên tuyến đã tồn tại";
            public const string Route_Not_Found = "Không tìm thấy tuyến";
        }
    }
}
