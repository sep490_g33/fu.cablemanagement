﻿using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net;

namespace FU.CableManagement.API.Infra.ErrorHandling
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        var message = contextFeature.Error.Message;

                        if (contextFeature.Error is KeyNotFoundException keyNotFoundException)
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                        }
                        else if (contextFeature.Error is UnauthorizedAccessException unauthorizedAccessException)
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                        }
                        else if (contextFeature.Error is ApplicationException applicationException)
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        }
                        else
                        {
                            Log.Logger.Error($"Something went wrong: {contextFeature.Error}");
                            Log.Logger.Error($"Something went wrong at message: {contextFeature.Error.Message} with stacktrace = {contextFeature.Error.StackTrace}");
                        }

                        await context.Response.WriteAsync(JsonConvert.SerializeObject(new BaseResponseDTO<string>()
                        {
                            Code = context.Response.StatusCode,
                            Message = message,
                            Success = false
                        }));
                    }
                });
            });
        }
    }
}
