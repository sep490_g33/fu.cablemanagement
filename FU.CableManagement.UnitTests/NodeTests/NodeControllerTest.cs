﻿using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.NodeDTOs;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace FU.CableManagement.UnitTests.NodeTests
{
    [TestFixture]
    public class NodeControllerTests
    {
        private Mock<INodeService> _NodeService;
        private Mock<IServiceProvider> _serviceProvider;
        private NodeController _NodeController;
        private Mock<HttpContext> contextMock;
        private NodeDTO model;
        [SetUp]
        public void SetUp()
        {
            _NodeService = new Mock<INodeService>();
            _serviceProvider = new Mock<IServiceProvider>();

            contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Staff),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);
            _NodeController = new NodeController(_serviceProvider.Object, _NodeService.Object);
            model = new NodeDTO();
        }

        [Test]
        public void CreateNode_WhenCallNotByAdmin_ThrowUnauthorizedAccessException()
        {
            var createModel = new CreateNodeDTO();
            _NodeController = new NodeController(_serviceProvider.Object, _NodeService.Object);
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await _NodeController.CreateNode(createModel));
        }

        [Test]
        public async Task CreateNode_WhenCallByStaff_RetrieveService()
        {
            var createModel = new CreateNodeDTO();
            _NodeController = new NodeController(_serviceProvider.Object, _NodeService.Object);
            _NodeController.ControllerContext.HttpContext = contextMock.Object;
            _NodeService.Setup(x => x.CreateNode(createModel)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await _NodeController.CreateNode(createModel);
            _NodeService.Verify(s => s.CreateNode(createModel));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void UpdateNode_WhenCallNotByAdmin_ThrowUnauthorizedAccessException()
        {
            var updateModel = new UpdateNodeDTO();
            _NodeController = new NodeController(_serviceProvider.Object, _NodeService.Object);
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await _NodeController.UpdateNode(updateModel));
        }

        [Test]
        public async Task UpdateNode_WhenCallByStaff_RetrieveService()
        {
            var updateModel = new UpdateNodeDTO();
            _NodeController = new NodeController(_serviceProvider.Object, _NodeService.Object);
            _NodeController.ControllerContext.HttpContext = contextMock.Object;
            _NodeService.Setup(x => x.UpdateNode(updateModel)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await _NodeController.UpdateNode(updateModel);
            _NodeService.Verify(s => s.UpdateNode(updateModel));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void DeleteNode_WhenCallNotByAdmin_ThrowUnauthorizedAccessException()
        {
            _NodeController = new NodeController(_serviceProvider.Object, _NodeService.Object);
            Guid id = Guid.NewGuid();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await _NodeController.DeleteNode(id));
        }

        [Test]
        public async Task DeleteNode_WhenCallByStaff_RetrieveService()
        {
            _NodeController = new NodeController(_serviceProvider.Object, _NodeService.Object);
            Guid id = Guid.NewGuid();
            _NodeController.ControllerContext.HttpContext = contextMock.Object;
            _NodeService.Setup(x => x.DeleteNode(id)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await _NodeController.DeleteNode(id);
            _NodeService.Verify(s => s.DeleteNode(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task GetNodeId_WhenCalled_RetrieveService()
        {
            _NodeController = new NodeController(_serviceProvider.Object, _NodeService.Object);
            _NodeController.ControllerContext.HttpContext = contextMock.Object;
            Guid id = Guid.NewGuid();
            _NodeService.Setup(x => x.GetNodeById(id)).ReturnsAsync(new BaseResponseDTO<NodeDTO>(model));
            var result = await _NodeController.GetNodeById(id);
            _NodeService.Verify(s => s.GetNodeById(id));
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data.Equals(model));
        }
        [Test]
        public async Task GetAllSuplier_WhenCalled_RetriveService()
        {
            _NodeController = new NodeController(_serviceProvider.Object, _NodeService.Object);
            _NodeService.Setup(x => x.GetAllNodes("", null, 1, 12)).ReturnsAsync(new BaseResponseDTO<PagedResultDTO<NodeDTO>>(new PagedResultDTO<NodeDTO>(1, 12, 12, new List<NodeDTO>())));
            var result = await _NodeController.GetAllNodes("", null, 1, 12);
            _NodeService.Verify(s => s.GetAllNodes("", null, 1, 12));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.NotNull(result.Data);
        }

        [Test]
        public void ImportNode_WhenCallNotByAdmin_ThrowUnauthorizedAccessException()
        {
            _NodeController = new NodeController(_serviceProvider.Object, _NodeService.Object);
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await _NodeController.ImportNodes(null));
        }

        [Test]
        public async Task ImportNode_WhenCallByStaff_RetrieveService()
        {
            _NodeController = new NodeController(_serviceProvider.Object, _NodeService.Object);
            _NodeController.ControllerContext.HttpContext = contextMock.Object;
            _NodeService.Setup(x => x.ImportNodes(null)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await _NodeController.ImportNodes(null);
            _NodeService.Verify(s => s.ImportNodes(null));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }
    }
}
