﻿using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.WareHouseDTOs;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace FU.CableManagement.UnitTests.WareHouseTests
{
    [TestFixture]
    public class WareHouseControllerTest
    {
        private WareHouseController controller;
        private Mock<IWareHouseService> WareHouseService;

        [SetUp]
        public void SetUp()
        {
            var _mockServiceProvider = new Mock<IServiceProvider>();
            WareHouseService = new Mock<IWareHouseService>();
            controller = new WareHouseController(_mockServiceProvider.Object, WareHouseService.Object);
        }
        [Test]
        public async Task GetAllWareHouses_WhenCall_RetriveService()
        {
            var data = new BaseResponseDTO<PagedResultDTO<WareHouseDTO>>();
            WareHouseService.Setup(x => x.GetAllWareHouse("", 12, 1)).ReturnsAsync(data);
            var result = await controller.GetAllWareHouse("");
            WareHouseService.Verify(s => s.GetAllWareHouse("", 12, 1));
            Assert.NotNull(result);
        }

        [Test]
        public async Task GetWareHouseById_WhenCall_RetriveService()
        {
            var data = new BaseResponseDTO<WareHouseDTO>(new WareHouseDTO());
            int id = 5;
            WareHouseService.Setup(x => x.GetWareHouseById(id)).ReturnsAsync(data);
            var result = await controller.GetWareHouseById(id);
            WareHouseService.Verify(s => s.GetWareHouseById(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.NotNull(result.Data);
        }
        [Test]
        public void CreateWareHouse_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = new WareHouseDTO();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.CreateWareHouse(null));
        }

        [Test]
        public async Task CreateWareHouse_WhenCallByAdmin_RetrieveService()
        {

            var model = new WareHouseDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            WareHouseService.Setup(x => x.CreateWareHouse(model, "")).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.CreateWareHouse(model);
            WareHouseService.Verify(s => s.CreateWareHouse(model, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }



        [Test]
        public void UpdateWareHouse_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = new WareHouseDTO();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.UpdateWareHouse(model));
        }

        [Test]
        public async Task UpdateWareHouse_WhenCallByAdmin_RetrieveService()
        {

            var model = new WareHouseDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            WareHouseService.Setup(x => x.UpdateWareHouse(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.UpdateWareHouse(model);
            WareHouseService.Verify(s => s.UpdateWareHouse(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }



        [Test]
        public void DeleteWareHouse_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            int id = 5;
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.DeleteWareHouse(id));
        }

        [Test]
        public async Task DeleteWareHouse_WhenCallByAdmin_RetrieveService()
        {
            int id = 5;
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            WareHouseService.Setup(x => x.DeleteWareHouse(id)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.DeleteWareHouse(id);
            WareHouseService.Verify(s => s.DeleteWareHouse(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }


        [Test]
        public void ImportExcelWareHouse_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            Guid id = Guid.NewGuid();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.ImportExcelWarehouse(null));
        }

        [Test]
        public async Task ImportExcelWareHouse_WhenCallByAdmin_RetrieveService()
        {
            var id = Guid.NewGuid();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            WareHouseService.Setup(x => x.ImportExcelWarehouse(null, "")).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.ImportExcelWarehouse(null);
            WareHouseService.Verify(s => s.ImportExcelWarehouse(null, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }
    }
}
