﻿using FU.CableManagement.API.Models.UserModels;
using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.Data.Entities;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.EmailDTOs;
using FU.CableManagement.DataAccess.DTO.UserDTOs;
using Microsoft.AspNetCore.Http;
using Moq;
using System.Security.Claims;

namespace FU.CableManagement.UnitTests.UserTests
{
    [TestFixture]
    public class UserControllerTest
    {
        private UserController controller;
        private Mock<IUserService> userService;
        private Mock<IEmailService> _emailService;

        [SetUp]
        public void SetUp()
        {
            var _mockServiceProvider = new Mock<IServiceProvider>();
            userService = new Mock<IUserService>();
            _emailService = new Mock<IEmailService>();
            controller = new UserController(_mockServiceProvider.Object, userService.Object, _emailService.Object);
        }
        [Test]
        public async Task GetAccessToken_WhenCall_RetriveService()
        {
            var data = new BaseResponseDTO<TokenResponse>(new TokenResponse());
            userService.Setup(x => x.Get_AccessToken(null)).ReturnsAsync(data);
            var result = await controller.GetAccessToken(null);
            userService.Verify(s => s.Get_AccessToken(null));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.NotNull(result.Data);
        }

        [Test]
        public void GetListUser_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.GetListUser(""));
        }

        [Test]
        public async Task GetListUser_WhenCallByAdmin_RetrieveService()
        {
            var data = new BaseResponseDTO<PagedResultDTO<UserDTO>>();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            userService.Setup(x => x.GetUsers("", 12, 1)).ReturnsAsync(data);
            var result = await controller.GetListUser("");
            userService.Verify(s => s.GetUsers("", 12, 1));
            Assert.NotNull(result);
        }

        [Test]
        public void UpdateUser_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = new UpdateUserDTO();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.UpdateUser(model));
        }

        [Test]
        public async Task UpdateUser_WhenCallByAdmin_RetrieveService()
        {
            var model = new UpdateUserDTO();
            var data = new BaseResponseDTO<bool>(true);
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            userService.Setup(x => x.UpdateUser(model)).ReturnsAsync(data);
            var result = await controller.UpdateUser(model);
            userService.Verify(s => s.UpdateUser(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void DeleteUser_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = Guid.NewGuid();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.DeleteUser(model));
        }

        [Test]
        public async Task DeleteUser_WhenCallByAdmin_RetrieveService()
        {
            var model = Guid.NewGuid();
            var data = new BaseResponseDTO<bool>(true);
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            userService.Setup(x => x.DeleteUser(model)).ReturnsAsync(data);
            var result = await controller.DeleteUser(model);
            userService.Verify(s => s.DeleteUser(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task RegisterUser_WhenCall_RetriveService()
        {
            var model = new RegisterUserDTO();
            var data = new BaseResponseDTO<bool>(true);
            userService.Setup(x => x.RegisterUser(It.IsAny<User>())).ReturnsAsync(data);
            var result = await controller.RegisterUser(model);
            userService.Verify(s => s.RegisterUser(It.IsAny<User>()));
            _emailService.Verify(s => s.sendEmail(It.IsAny<EmailDTO>()));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task ForgetPassword_WhenCall_RetriveService()
        {
            var model = "";
            var data = new BaseResponseDTO<bool>(true);
            var result = await controller.ForgetPassword(model);
            userService.Verify(s => s.ChangePassword("", It.IsAny<string>()));
            _emailService.Verify(s => s.sendEmail(It.IsAny<EmailDTO>()));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void ImportExcelUser_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.ImportExcelUser(null));
        }

        [Test]
        public async Task ImportExcelUser_WhenCallByAdmin_RetrieveService()
        {
            var data = new BaseResponseDTO<bool>(true);
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            userService.Setup(x => x.ImportExcelUser(null)).ReturnsAsync(data);
            var result = await controller.ImportExcelUser(null);
            userService.Verify(s => s.ImportExcelUser(null));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }
    }
}
