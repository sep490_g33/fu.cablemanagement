﻿using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialCategoryDTOs;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace FU.CableManagement.UnitTests.OtherMaterialCategoryTests
{
    [TestFixture]
    public class OtherMaterialCategoryControllerTest
    {
        private OtherMaterialCategoryController controller;
        private Mock<IOtherMaterialCateogryService> OtherMaterialCategoryService;

        [SetUp]
        public void SetUp()
        {
            var _mockServiceProvider = new Mock<IServiceProvider>();
            OtherMaterialCategoryService = new Mock<IOtherMaterialCateogryService>();
            controller = new OtherMaterialCategoryController(_mockServiceProvider.Object, OtherMaterialCategoryService.Object);
        }
        [Test]
        public async Task GetAllOtherMaterialCategorys_WhenCall_RetriveService()
        {
            var data = new BaseResponseDTO<PagedResultDTO<OtherMaterialCategoryDTO>>();
            OtherMaterialCategoryService.Setup(x => x.GetAllOtherMaterialCategories("", 12, 1)).ReturnsAsync(data);
            var result = await controller.GetAllMaterialCategories("");
            OtherMaterialCategoryService.Verify(s => s.GetAllOtherMaterialCategories("", 12, 1));
            Assert.NotNull(result);
        }

        [Test]
        public async Task GetOtherMaterialCategoryById_WhenCall_RetriveService()
        {
            var data = new BaseResponseDTO<OtherMaterialCategoryDTO>(new OtherMaterialCategoryDTO());
            //Guid id = Guid.NewGuid();
            int id = 5;
            OtherMaterialCategoryService.Setup(x => x.GetOtherMaterialCategoryById(id)).ReturnsAsync(data);
            var result = await controller.GetMaterialCategoryById(id);
            OtherMaterialCategoryService.Verify(s => s.GetOtherMaterialCategoryById(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.NotNull(result.Data);
        }
        [Test]
        public void AddMaterialCategory_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = new OtherMaterialCategoryDTO();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.AddMaterialCategory(null));
        }

        //[Test]
        //public async Task AddMaterialCategory_WhenCallByAdmin_RetrieveService()
        //{

        //    var model = new OtherMaterialCategoryDTO();
        //    var contextMock = new Mock<HttpContext>();
        //    var user = new ClaimsPrincipal();
        //    var listClaim = new List<Claim>
        //    {
        //        new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
        //        new Claim("userid","")
        //    };
        //    user.AddIdentity(new ClaimsIdentity(listClaim));
        //    contextMock.Setup(x => x.User).Returns(user);

        //    controller.ControllerContext.HttpContext = contextMock.Object;
        //    OtherMaterialCategoryService.Setup(x => x.AddOtherMaterialCategory("")).ReturnsAsync(new BaseResponseDTO<bool>(true));
        //    var result = await controller.AddMaterialCategory("");
        //    OtherMaterialCategoryService.Verify(s => s.AddOtherMaterialCategory(""));
        //    Assert.NotNull(result);
        //    Assert.That(result.Success, Is.True);
        //    Assert.That(result.Data, Is.True);
        //}

        //[Test]
        //public async Task ImportOtherMaterialCategory_WhenCallByWarehouseKeeper_RetrieveService()
        //{

        //    var model = new OtherMaterialCategoryDTO();
        //    var contextMock = new Mock<HttpContext>();
        //    var user = new ClaimsPrincipal();
        //    var listClaim = new List<Claim>
        //    {
        //        new Claim(ClaimTypes.Role,UserRoleConstant.WarehouseKeeper),
        //        new Claim("userid","")
        //    };
        //    user.AddIdentity(new ClaimsIdentity(listClaim));
        //    contextMock.Setup(x => x.User).Returns(user);

        //    controller.ControllerContext.HttpContext = contextMock.Object;
        //    OtherMaterialCategoryService.Setup(x => x.AddOtherMaterialCategory("")).ReturnsAsync(new BaseResponseDTO<bool>(true));
        //    var result = await controller.AddMaterialCategory("");
        //    OtherMaterialCategoryService.Verify(s => s.AddOtherMaterialCategory(""));
        //    Assert.NotNull(result);
        //    Assert.That(result.Success, Is.True);
        //    Assert.That(result.Data, Is.True);
        //}

        [Test]
        public void UpdateOtherMaterialCategory_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = new OtherMaterialCategoryDTO();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.UpdateMaterialCategory(model));
        }

        [Test]
        public async Task UpdateOtherMaterialCategory_WhenCallByAdmin_RetrieveService()
        {

            var model = new OtherMaterialCategoryDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            OtherMaterialCategoryService.Setup(x => x.UpdateOtherMaterialCategory(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.UpdateMaterialCategory(model);
            OtherMaterialCategoryService.Verify(s => s.UpdateOtherMaterialCategory(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task UpdateOtherMaterialCategory_WhenCallByWarehouseKeeper_RetrieveService()
        {

            var model = new OtherMaterialCategoryDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.WarehouseKeeper),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            OtherMaterialCategoryService.Setup(x => x.UpdateOtherMaterialCategory(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.UpdateMaterialCategory(model);
            OtherMaterialCategoryService.Verify(s => s.UpdateOtherMaterialCategory(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void DeleteOtherMaterialCategory_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            int id = 5;
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.DeleteMaterialCategory(id));
        }

        [Test]
        public async Task DeleteOtherMaterialCategory_WhenCallByAdmin_RetrieveService()
        {
            int id = 5;
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            OtherMaterialCategoryService.Setup(x => x.DeleteOtherMaterialCategory(id)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.DeleteMaterialCategory(id);
            OtherMaterialCategoryService.Verify(s => s.DeleteOtherMaterialCategory(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task DeleteOtherMaterialCategory_WhenCallByWarehouseKeeper_RetrieveService()
        {
            int id = 5;
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.WarehouseKeeper),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            OtherMaterialCategoryService.Setup(x => x.DeleteOtherMaterialCategory(id)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.DeleteMaterialCategory(id);
            OtherMaterialCategoryService.Verify(s => s.DeleteOtherMaterialCategory(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }



        //[Test]
        //public void ImportExcelOtherMaterialCategory_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        //{
        //    Guid id = Guid.NewGuid();
        //    Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.ImportExcelMaterialCategories(null));
        //}

        //[Test]
        //public async Task ImportExcelOtherMaterialCategory_WhenCallByAdmin_RetrieveService()
        //{
        //    var id = Guid.NewGuid();
        //    var contextMock = new Mock<HttpContext>();
        //    var user = new ClaimsPrincipal();
        //    var listClaim = new List<Claim>
        //    {
        //        new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
        //        new Claim("userid","")
        //    };
        //    user.AddIdentity(new ClaimsIdentity(listClaim));
        //    contextMock.Setup(x => x.User).Returns(user);

        //    controller.ControllerContext.HttpContext = contextMock.Object;
        //    OtherMaterialCategoryService.Setup(x => x.ImportExcelOtherMaterialCategories(null)).ReturnsAsync(new BaseResponseDTO<bool>(true));
        //    var result = await controller.ImportExcelMaterialCategories(null);
        //    OtherMaterialCategoryService.Verify(s => s.ImportExcelOtherMaterialCategories(null));
        //    Assert.NotNull(result);
        //    Assert.That(result.Success, Is.True);
        //    Assert.That(result.Data, Is.True);
        //}

        //[Test]
        //public async Task ImportExcelOtherMaterialCategory_WhenCallByWarehouseKeeper_RetrieveService()
        //{
        //    var id = Guid.NewGuid();
        //    var contextMock = new Mock<HttpContext>();
        //    var user = new ClaimsPrincipal();
        //    var listClaim = new List<Claim>
        //    {
        //        new Claim(ClaimTypes.Role,UserRoleConstant.WarehouseKeeper),
        //        new Claim("userid","")
        //    };
        //    user.AddIdentity(new ClaimsIdentity(listClaim));
        //    contextMock.Setup(x => x.User).Returns(user);

        //    controller.ControllerContext.HttpContext = contextMock.Object;
        //    OtherMaterialCategoryService.Setup(x => x.ImportExcelOtherMaterialCategories(null)).ReturnsAsync(new BaseResponseDTO<bool>(true));
        //    var result = await controller.ImportExcelMaterialCategories(null);
        //    OtherMaterialCategoryService.Verify(s => s.ImportExcelOtherMaterialCategories(null));
        //    Assert.NotNull(result);
        //    Assert.That(result.Success, Is.True);
        //    Assert.That(result.Data, Is.True);
        //}
    }
}
