﻿using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.SupplierDTOs;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace FU.CableManagement.UnitTests.SupplierTests
{
    [TestFixture]
    public class SupplierControllerTests
    {
        private Mock<ISupplierService> _supplierService;
        private Mock<IServiceProvider> _serviceProvider;
        private SupplierController _supplierController;
        private Mock<HttpContext> contextMock;
        private SupplierDTO model;
        [SetUp]
        public void SetUp()
        {
            _supplierService = new Mock<ISupplierService>();
            _serviceProvider = new Mock<IServiceProvider>();

            contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);
            _supplierController = new SupplierController(_serviceProvider.Object, _supplierService.Object);
            model = new SupplierDTO();
        }

        [Test]
        public void CreateSupplier_WhenCallNotByAdmin_ThrowUnauthorizedAccessException()
        {
            _supplierController = new SupplierController(_serviceProvider.Object, _supplierService.Object);
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await _supplierController.CreateSupplier(model));
        }

        [Test]
        public async Task CreateSupplier_WhenCallByAdmin_RetrieveService()
        {
            _supplierController = new SupplierController(_serviceProvider.Object, _supplierService.Object);
            _supplierController.ControllerContext.HttpContext = contextMock.Object;
            _supplierService.Setup(x => x.CreateSupplier(model, "")).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await _supplierController.CreateSupplier(model);
            _supplierService.Verify(s => s.CreateSupplier(model, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void UpdateSupplier_WhenCallNotByAdmin_ThrowUnauthorizedAccessException()
        {
            _supplierController = new SupplierController(_serviceProvider.Object, _supplierService.Object);
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await _supplierController.UpdateSupplier(model));
        }

        [Test]
        public async Task UpdateSupplier_WhenCallByAdmin_RetrieveService()
        {
            _supplierController = new SupplierController(_serviceProvider.Object, _supplierService.Object);
            _supplierController.ControllerContext.HttpContext = contextMock.Object;
            _supplierService.Setup(x => x.UpdateSupplier(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await _supplierController.UpdateSupplier(model);
            _supplierService.Verify(s => s.UpdateSupplier(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void DeleteSupplier_WhenCallNotByAdmin_ThrowUnauthorizedAccessException()
        {
            _supplierController = new SupplierController(_serviceProvider.Object, _supplierService.Object);
            int id = 0;
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await _supplierController.DeleteSupplier(id));
        }

        [Test]
        public async Task DeleteSupplier_WhenCallByAdmin_RetrieveService()
        {
            _supplierController = new SupplierController(_serviceProvider.Object, _supplierService.Object);
            int id = 0;
            _supplierController.ControllerContext.HttpContext = contextMock.Object;
            _supplierService.Setup(x => x.DeleteSupplier(id)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await _supplierController.DeleteSupplier(id);
            _supplierService.Verify(s => s.DeleteSupplier(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task GetSupplierId_WhenCalled_RetrieveService()
        {
            _supplierController = new SupplierController(_serviceProvider.Object, _supplierService.Object);
            _supplierController.ControllerContext.HttpContext = contextMock.Object;
            int id = 5;
            _supplierService.Setup(x => x.GetSupplierById(id)).ReturnsAsync(new BaseResponseDTO<SupplierDTO>(model));
            var result = await _supplierController.GetSupplierById(id);
            _supplierService.Verify(s => s.GetSupplierById(id));
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data.Equals(model));
        }
        [Test]
        public async Task GetAllSuplier_WhenCalled_RetriveService()
        {
            _supplierController = new SupplierController(_serviceProvider.Object, _supplierService.Object);
            _supplierService.Setup(x => x.GetAllSupplier("", 1, 12)).ReturnsAsync(new BaseResponseDTO<PagedResultDTO<SupplierDTO>>(new PagedResultDTO<SupplierDTO>(1, 12, 12, new List<SupplierDTO>())));
            var result = await _supplierController.GetAllSupplier("", 1, 12);
            _supplierService.Verify(s => s.GetAllSupplier("", 1, 12));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.NotNull(result.Data);
        }

        [Test]
        public void ImportSupplier_WhenCallNotByAdmin_ThrowUnauthorizedAccessException()
        {
            _supplierController = new SupplierController(_serviceProvider.Object, _supplierService.Object);
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await _supplierController.ImportSuppliers(null));
        }

        [Test]
        public async Task ImportSupplier_WhenCallByAdmin_RetrieveService()
        {
            _supplierController = new SupplierController(_serviceProvider.Object, _supplierService.Object);
            _supplierController.ControllerContext.HttpContext = contextMock.Object;
            _supplierService.Setup(x => x.ImportSupplier(null, "")).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await _supplierController.ImportSuppliers(null);
            _supplierService.Verify(s => s.ImportSupplier(null, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }


        [Test]
        public void ExportSupplier_WhenCallNotByAdmin_ThrowUnauthorizedAccessException()
        {
            _supplierController = new SupplierController(_serviceProvider.Object, _supplierService.Object);
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await _supplierController.ImportSuppliers(null));
        }

        [Test]
        public async Task ExportSupplier_WhenCallByAdmin_RetrieveService()
        {
            _supplierController = new SupplierController(_serviceProvider.Object, _supplierService.Object);
            _supplierController.ControllerContext.HttpContext = contextMock.Object;
            _supplierService.Setup(x => x.ExportSupplier()).ReturnsAsync(new BaseResponseDTO<FileResponseDTO>(new FileResponseDTO()));
            var result = await _supplierController.ExportSuppliers();
            _supplierService.Verify(s => s.ExportSupplier());
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.NotNull(result.Data);
        }
    }
}
