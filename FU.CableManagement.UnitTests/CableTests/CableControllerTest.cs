﻿using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.DTO.CableDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace FU.CableManagement.UnitTests.CableTests
{
    [TestFixture]
    public class CableControllerTest
    {
        private CableController controller;
        private Mock<ICableService> cableService;

        [SetUp]
        public void SetUp()
        {
            var _mockServiceProvider = new Mock<IServiceProvider>();
            cableService = new Mock<ICableService>();
            controller = new CableController(_mockServiceProvider.Object, cableService.Object);
        }
        [Test]
        public async Task GetAllCables_WhenCall_RetriveService()
        {
            var data = new BaseResponseDTO<PagedResultDTO<CableDTO>>();
            cableService.Setup(x => x.GetAllCables("", 12, 1, null,null)).ReturnsAsync(data);
            var result = await controller.GetAllCables("");
            cableService.Verify(s => s.GetAllCables("", 12, 1, null, null));
            Assert.NotNull(result);
        }

        [Test]
        public async Task GetCableById_WhenCall_RetriveService()
        {
            var data = new BaseResponseDTO<CableDTO>(new CableDTO());
            Guid id = Guid.NewGuid();
            cableService.Setup(x => x.GetCableById(id)).ReturnsAsync(data);
            var result = await controller.GetCableById(id);
            cableService.Verify(s => s.GetCableById(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.NotNull(result.Data);
        }
        [Test]
        public void ImportCable_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = new CableImportDTO();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.ImportCable(null));
        }

        [Test]
        public async Task ImportCable_WhenCallByAdmin_RetrieveService()
        {

            var model = new CableImportDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            cableService.Setup(x => x.AddCable(model, "")).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.ImportCable(model);
            cableService.Verify(s => s.AddCable(model, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task ImportCable_WhenCallByWarehouseKeeper_RetrieveService()
        {

            var model = new CableImportDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.WarehouseKeeper),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            cableService.Setup(x => x.AddCable(model, "")).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.ImportCable(model);
            cableService.Verify(s => s.AddCable(model, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void UpdateCable_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = new CableImportDTO();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.UpdateCable(null));
        }

        [Test]
        public async Task UpdateCable_WhenCallByAdmin_RetrieveService()
        {

            var model = new CableUpdateDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            cableService.Setup(x => x.UpdateCable(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.UpdateCable(model);
            cableService.Verify(s => s.UpdateCable(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task UpdateCable_WhenCallByWarehouseKeeper_RetrieveService()
        {

            var model = new CableUpdateDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.WarehouseKeeper),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            cableService.Setup(x => x.UpdateCable(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.UpdateCable(model);
            cableService.Verify(s => s.UpdateCable(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void DeleteCable_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            Guid id = Guid.NewGuid();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.DeleteCable(id));
        }

        [Test]
        public async Task DeleteCable_WhenCallByAdmin_RetrieveService()
        {
            var id = Guid.NewGuid();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            cableService.Setup(x => x.DeleteCable(id)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.DeleteCable(id);
            cableService.Verify(s => s.DeleteCable(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task DeleteCable_WhenCallByWarehouseKeeper_RetrieveService()
        {
            var id = Guid.NewGuid();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.WarehouseKeeper),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            cableService.Setup(x => x.DeleteCable(id)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.DeleteCable(id);
            cableService.Verify(s => s.DeleteCable(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task GetCablesInAWarehouse()
        {
            var warehouseId = 5;
            var data = new BaseResponseDTO<PagedResultDTO<CableDTO>>();
            cableService.Setup(x => x.GetCablesInAWarehouse(warehouseId, 12, 1)).ReturnsAsync(data);
            var result = await controller.GetCablesInAWarehouse(warehouseId);
            cableService.Verify(s => s.GetCablesInAWarehouse(warehouseId, 12, 1));
            Assert.NotNull(result);
        }

        [Test]
        public void ImportExcelCable_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            Guid id = Guid.NewGuid();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.ImportExcelCable(null));
        }

        [Test]
        public async Task ImportExcelCable_WhenCallByAdmin_RetrieveService()
        {
            var id = Guid.NewGuid();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            cableService.Setup(x => x.ImportExcelCables(null, "")).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.ImportExcelCable(null);
            cableService.Verify(s => s.ImportExcelCables(null, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task ImportExcelCable_WhenCallByWarehouseKeeper_RetrieveService()
        {
            var id = Guid.NewGuid();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.WarehouseKeeper),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            cableService.Setup(x => x.ImportExcelCables(null, "")).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.ImportExcelCable(null);
            cableService.Verify(s => s.ImportExcelCables(null, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }
    }
}
