﻿using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.RequestDTOs;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace FU.CableManagement.UnitTests.RequestTests
{
    [TestFixture]
    public class RequestControllerTest
    {
        private RequestController controller;
        private Mock<IRequestService> requestService;

        [SetUp]
        public void SetUp()
        {
            var _mockServiceProvider = new Mock<IServiceProvider>();
            requestService = new Mock<IRequestService>();
            controller = new RequestController(_mockServiceProvider.Object, requestService.Object);
        }

        [Test]
        public void CreateRequest_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = new RequestCreateDTO();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.CreateRequestExport(model));
        }

        [Test]
        public async Task CreateRequest_WhenCallByStaff_RetrieveService()
        {

            var model = new RequestCreateDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Staff),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            requestService.Setup(x => x.CreateRequest(model, "")).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.CreateRequestExport(model);
            requestService.Verify(s => s.CreateRequest(model, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void UpdateRequest_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = new RequestUpdateDTO();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.UpdateRequest(model));
        }

        [Test]
        public async Task UpdateRequest_WhenCallByStaff_RetrieveService()
        {

            var model = new RequestUpdateDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Staff),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            requestService.Setup(x => x.UpdateRequest(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.UpdateRequest(model);
            requestService.Verify(s => s.UpdateRequest(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void DeleteRequest_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var requestId = Guid.NewGuid();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.DeleteRequest(requestId));
        }

        [Test]
        public async Task DeleteRequest_WhenCallByStaff_RetrieveService()
        {
            var requestId = Guid.NewGuid();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Staff),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            requestService.Setup(x => x.DeleteRequest(requestId)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.DeleteRequest(requestId);
            requestService.Verify(s => s.DeleteRequest(requestId));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }


        [Test]
        public void GetRequestById_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var requestId = Guid.NewGuid();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.GetRequestById(requestId));
        }

        [Test]
        public async Task GetRequestById_WhenCallByStaff_RetrieveService()
        {
            var requestId = Guid.NewGuid();
            var data = new BaseResponseDTO<RequestDTO>(new RequestDTO());
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Staff),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            requestService.Setup(x => x.GetRequestById(requestId)).ReturnsAsync(data);
            var result = await controller.GetRequestById(requestId);
            requestService.Verify(s => s.GetRequestById(requestId));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.NotNull(result.Data);
        }

        [Test]
        public async Task GetRequestById_WhenCallByLeader_RetrieveService()
        {
            var requestId = Guid.NewGuid();
            var data = new BaseResponseDTO<RequestDTO>(new RequestDTO());
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Leader),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            requestService.Setup(x => x.GetRequestById(requestId)).ReturnsAsync(data);
            var result = await controller.GetRequestById(requestId);
            requestService.Verify(s => s.GetRequestById(requestId));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.NotNull(result.Data);
        }

        [Test]
        public void GetAllRequest_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.GetAllRequest(""));
        }

        [Test]
        public async Task GetAllRequest_WhenCallByLeader_RetrieveService()
        {
            var data = new BaseResponseDTO<PagedResultDTO<RequestDTO>>();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Leader),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            requestService.Setup(x => x.GetAllRequest("", 12, 1, null)).ReturnsAsync(data);
            var result = await controller.GetAllRequest("");
            requestService.Verify(s => s.GetAllRequest("", 12, 1, null));
            Assert.NotNull(result);
        }


        [Test]
        public void GetRequestByStaff_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.GetRequestByStaff(""));
        }

        [Test]
        public async Task GetRequestByStaff_WhenCallByStaff_RetrieveService()
        {
            var data = new BaseResponseDTO<PagedResultDTO<RequestDTO>>();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Staff),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            requestService.Setup(x => x.GetRequestByStaff("", 12, 1, "", null)).ReturnsAsync(data);
            var result = await controller.GetRequestByStaff("");
            requestService.Verify(s => s.GetRequestByStaff("", 12, 1, "", null));
            Assert.NotNull(result);
        }

        [Test]
        public void ApproveRequest_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var requestId = Guid.NewGuid();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.ApproveRequestExport(requestId));
        }

        [Test]
        public async Task ApproveRequest_WhenCallByLeader_RetrieveService()
        {
            var requestId = Guid.NewGuid();
            var data = new BaseResponseDTO<bool>(true);
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Leader),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            requestService.Setup(x => x.ApproveRequestExport(requestId, "")).ReturnsAsync(data);
            var result = await controller.ApproveRequestExport(requestId);
            requestService.Verify(s => s.ApproveRequestExport(requestId, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void RejectRequest_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var requestId = Guid.NewGuid();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.RejectRequest(requestId));
        }

        [Test]
        public async Task RejectRequest_WhenCallByLeader_RetrieveService()
        {
            var requestId = Guid.NewGuid();
            var data = new BaseResponseDTO<bool>(true);
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Leader),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            requestService.Setup(x => x.RejectRequest(requestId, "")).ReturnsAsync(data);
            var result = await controller.RejectRequest(requestId);
            requestService.Verify(s => s.RejectRequest(requestId, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

    }
}
