﻿using FU.CableManagement.API.Services.Services;
using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.DTO.CableDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using FU.CableManagement.DataAccess.DTO.OtherMaterialsDTOs;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace FU.CableManagement.UnitTests.OtherMaterialTests
{
    [TestFixture]
    public class OtherMaterialControllerTest
    {
        private OtherMaterialsController controller;
        private Mock<IOtherMaterialsService> OtherMaterialService;

        [SetUp]
        public void SetUp()
        {
            var _mockServiceProvider = new Mock<IServiceProvider>();
            OtherMaterialService = new Mock<IOtherMaterialsService>();
            controller = new OtherMaterialsController(_mockServiceProvider.Object, OtherMaterialService.Object);
        }
        [Test]
        public async Task GetAllOtherMaterials_WhenCall_RetriveService()
        {
            var data = new BaseResponseDTO<PagedResultDTO<OtherMaterialDTO>>();
            OtherMaterialService.Setup(x => x.GetAllOtherMaterials("", 12, 1, null)).ReturnsAsync(data);
            var result = await controller.GetAllMaterials("");
            OtherMaterialService.Verify(s => s.GetAllOtherMaterials("", 12, 1, null));
            Assert.NotNull(result);
        }

        [Test]
        public async Task GetOtherMaterialById_WhenCall_RetriveService()
        {
            var data = new BaseResponseDTO<OtherMaterialDTO>(new OtherMaterialDTO());
            //Guid id = Guid.NewGuid();
            int id = 5;
            OtherMaterialService.Setup(x => x.GetOtherMaterialById(id)).ReturnsAsync(data);
            var result = await controller.GetMaterialsById(id);
            OtherMaterialService.Verify(s => s.GetOtherMaterialById(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.NotNull(result.Data);
        }
        [Test]
        public void AddMaterialCategory_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = new OtherMaterialDTO();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.AddMaterials(null));
        }

        [Test]
        public async Task AddMaterialCategory_WhenCallByAdmin_RetrieveService()
        {

            var model = new OtherMaterialAddDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            OtherMaterialService.Setup(x => x.AddOtherMaterial(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.AddMaterials(model);
            OtherMaterialService.Verify(s => s.AddOtherMaterial(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task ImportOtherMaterial_WhenCallByWarehouseKeeper_RetrieveService()
        {

            var model = new OtherMaterialAddDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.WarehouseKeeper),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            OtherMaterialService.Setup(x => x.AddOtherMaterial(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.AddMaterials(model);
            OtherMaterialService.Verify(s => s.AddOtherMaterial(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void UpdateOtherMaterial_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = new OtherMaterialUpdateDTO();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.UpdateMaterialsById(model));
        }

        [Test]
        public async Task UpdateOtherMaterial_WhenCallByAdmin_RetrieveService()
        {

            var model = new OtherMaterialUpdateDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            OtherMaterialService.Setup(x => x.UpdateOtherMaterial(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.UpdateMaterialsById(model);
            OtherMaterialService.Verify(s => s.UpdateOtherMaterial(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task UpdateOtherMaterial_WhenCallByWarehouseKeeper_RetrieveService()
        {

            var model = new OtherMaterialUpdateDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.WarehouseKeeper),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            OtherMaterialService.Setup(x => x.UpdateOtherMaterial(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.UpdateMaterialsById(model);
            OtherMaterialService.Verify(s => s.UpdateOtherMaterial(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void DeleteOtherMaterial_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            int id = 5;
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.DeleteMaterialsById(id));
        }

        [Test]
        public async Task DeleteOtherMaterial_WhenCallByAdmin_RetrieveService()
        {
            int id = 5;
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            OtherMaterialService.Setup(x => x.DeleteOtherMaterial(id)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.DeleteMaterialsById(id);
            OtherMaterialService.Verify(s => s.DeleteOtherMaterial(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task DeleteOtherMaterial_WhenCallByWarehouseKeeper_RetrieveService()
        {
            int id = 5;
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.WarehouseKeeper),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            OtherMaterialService.Setup(x => x.DeleteOtherMaterial(id)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.DeleteMaterialsById(id);
            OtherMaterialService.Verify(s => s.DeleteOtherMaterial(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }



        [Test]
        public void ImportExcelOtherMaterial_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            Guid id = Guid.NewGuid();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.ImportExcelMaterials(null));
        }

        [Test]
        public async Task ImportExcelOtherMaterial_WhenCallByAdmin_RetrieveService()
        {
            var id = Guid.NewGuid();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            OtherMaterialService.Setup(x => x.ImportExcelMaterials(null)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.ImportExcelMaterials(null);
            OtherMaterialService.Verify(s => s.ImportExcelMaterials(null));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task ImportExcelOtherMaterial_WhenCallByWarehouseKeeper_RetrieveService()
        {
            var id = Guid.NewGuid();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.WarehouseKeeper),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            OtherMaterialService.Setup(x => x.ImportExcelMaterials(null)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.ImportExcelMaterials(null);
            OtherMaterialService.Verify(s => s.ImportExcelMaterials(null));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

    }
}
