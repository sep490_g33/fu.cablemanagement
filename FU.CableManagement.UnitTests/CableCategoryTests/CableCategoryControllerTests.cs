﻿using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.DTO.CableCategoryDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace FU.CableManagement.UnitTests.CableCategoryTests
{
    [TestFixture]
    public class CableCategoryControllerTests
    {
        private CableCategoryController controller;
        private Mock<ICableCategoryService> cableCategoryService;
        private Mock<HttpContext> contextMock;
        private CableCategoryDTO model = new CableCategoryDTO();
        [SetUp]
        public void SetUp()
        {
            var serviceProvider = new Mock<IServiceProvider>();
            cableCategoryService = new Mock<ICableCategoryService>();
            contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);
            controller = new CableCategoryController(serviceProvider.Object, cableCategoryService.Object);
        }

        [Test]
        public async Task GetCableCategoryById_WhenCall_RetrieveService()
        {
            int id = 0;
            cableCategoryService.Setup(x => x.GetCableCategoryById(id)).ReturnsAsync(new BaseResponseDTO<CableCategoryDTO>(new CableCategoryDTO()));
            var result = await controller.GetCableCategoryById(id);
            cableCategoryService.Verify(s => s.GetCableCategoryById(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.NotNull(result.Data);
        }
        [Test]

        public async Task GetAllMaterialCategories_WhenCall_RetrieveService()
        {
            var data = new PagedResultDTO<CableCategoryDTO>(1, 1, 12, new List<CableCategoryDTO>());
            var resultSV = new BaseResponseDTO<PagedResultDTO<CableCategoryDTO>>(data);
            cableCategoryService.Setup(x => x.GetCableCategories("", 1, 12)).ReturnsAsync(resultSV);
            var result = await controller.GetAllCableCategories("", 1, 12);
            cableCategoryService.Verify(s => s.GetCableCategories("", 1, 12));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.NotNull(result.Data);
        }

        [Test]
        public void AddCableCategory_WhenCallNotByAdmin_ThrowUnauthorizedAccessException()
        {

            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.AddCableCategory(null));
        }


        [Test]
        public void UpdateCableCategory_WhenCallNotByAdmin_ThrowUnauthorizedAccessException()
        {
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.UpdateCableCategory(null));
        }

        [Test]
        public async Task UpdateCableCategory_WhenCallByAdmin_RetrieveService()
        {
            controller.ControllerContext.HttpContext = contextMock.Object;
            cableCategoryService.Setup(x => x.UpdateCableCategory(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.UpdateCableCategory(model);
            cableCategoryService.Verify(s => s.UpdateCableCategory(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void DeleteCableCategory_WhenCallNotByAdmin_ThrowUnauthorizedAccessException()
        {
            int id = 5;
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.DeleteCableCategory(5));
        }

        [Test]
        public async Task DeleteCableCategory_WhenCallByAdmin_RetrieveService()
        {
            int id = 5;
            controller.ControllerContext.HttpContext = contextMock.Object;
            cableCategoryService.Setup(x => x.DeleteCableCategory(id)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.DeleteCableCategory(id);
            cableCategoryService.Verify(s => s.DeleteCableCategory(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

    }
}
