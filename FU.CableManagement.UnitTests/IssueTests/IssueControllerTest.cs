﻿using FU.CableManagement.DataAccess.Common.Constants;
using FU.CableManagement.DataAccess.DTO.IssueDTOs;
using FU.CableManagement.DataAccess.DTO.CommonDTOs;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace FU.CableManagement.UnitTests.IssueTests
{
    [TestFixture]
    public class IssueControllerTest
    {
        private IssueController controller;
        private Mock<IIssueService> IssueService;

        [SetUp]
        public void SetUp()
        {
            var _mockServiceProvider = new Mock<IServiceProvider>();
            IssueService = new Mock<IIssueService>();
            controller = new IssueController(_mockServiceProvider.Object, IssueService.Object);
        }
        [Test]
        public async Task GetAllIssues_WhenCall_RetriveService()
        {
            var data = new BaseResponseDTO<PagedResultDTO<IssueDTO>>();
            IssueService.Setup(x => x.GetAllIssue("", 12, 1)).ReturnsAsync(data);
            var result = await controller.GetAllIssue("");
            IssueService.Verify(s => s.GetAllIssue("", 12, 1));
            Assert.NotNull(result);
        }

        [Test]
        public async Task GetIssueById_WhenCall_RetriveService()
        {
            var data = new BaseResponseDTO<IssueDTO>(new IssueDTO());
            Guid id = Guid.NewGuid();
            IssueService.Setup(x => x.GetIssueById(id)).ReturnsAsync(data);
            var result = await controller.GetIssueById(id);
            IssueService.Verify(s => s.GetIssueById(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.NotNull(result.Data);
        }
        [Test]
        public void CreateIssue_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = new IssueDTO();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.CreateIssue(null));
        }

        [Test]
        public async Task CreateIssue_WhenCallByAdmin_RetrieveService()
        {

            var model = new IssueDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            IssueService.Setup(x => x.CreateIssue(model, "")).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.CreateIssue(model);
            IssueService.Verify(s => s.CreateIssue(model, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task ImportIssue_WhenCallByStaff_RetrieveService()
        {

            var model = new IssueDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Staff),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            IssueService.Setup(x => x.CreateIssue(model, "")).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.CreateIssue(model);
            IssueService.Verify(s => s.CreateIssue(model, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void UpdateIssue_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            var model = new IssueDTO();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.UpdateIssue(model));
        }

        [Test]
        public async Task UpdateIssue_WhenCallByAdmin_RetrieveService()
        {

            var model = new IssueDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            IssueService.Setup(x => x.UpdateIssue(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.UpdateIssue(model);
            IssueService.Verify(s => s.UpdateIssue(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task UpdateIssue_WhenCallByWarehouseKeeper_RetrieveService()
        {

            var model = new IssueDTO();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Staff),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            IssueService.Setup(x => x.UpdateIssue(model)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.UpdateIssue(model);
            IssueService.Verify(s => s.UpdateIssue(model));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public void DeleteIssue_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            Guid id = Guid.NewGuid();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.DeleteIssue(id));
        }

        [Test]
        public async Task DeleteIssue_WhenCallByAdmin_RetrieveService()
        {
            var id = Guid.NewGuid();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            IssueService.Setup(x => x.DeleteIssue(id)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.DeleteIssue(id);
            IssueService.Verify(s => s.DeleteIssue(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task DeleteIssue_WhenCallByWarehouseKeeper_RetrieveService()
        {
            var id = Guid.NewGuid();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Staff),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            IssueService.Setup(x => x.DeleteIssue(id)).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.DeleteIssue(id);
            IssueService.Verify(s => s.DeleteIssue(id));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }



        [Test]
        public void ImportExcelIssue_WhenCallUnAuthorize_ThrowUnauthorizedAccessException()
        {
            Guid id = Guid.NewGuid();
            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await controller.ImportExcelIssue(null));
        }

        [Test]
        public async Task ImportExcelIssue_WhenCallByAdmin_RetrieveService()
        {
            var id = Guid.NewGuid();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Admin),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            IssueService.Setup(x => x.ImportExcelIssue(null, "")).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.ImportExcelIssue(null);
            IssueService.Verify(s => s.ImportExcelIssue(null, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }

        [Test]
        public async Task ImportExcelIssue_WhenCallByWarehouseKeeper_RetrieveService()
        {
            var id = Guid.NewGuid();
            var contextMock = new Mock<HttpContext>();
            var user = new ClaimsPrincipal();
            var listClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Role,UserRoleConstant.Staff),
                new Claim("userid","")
            };
            user.AddIdentity(new ClaimsIdentity(listClaim));
            contextMock.Setup(x => x.User).Returns(user);

            controller.ControllerContext.HttpContext = contextMock.Object;
            IssueService.Setup(x => x.ImportExcelIssue(null, "")).ReturnsAsync(new BaseResponseDTO<bool>(true));
            var result = await controller.ImportExcelIssue(null);
            IssueService.Verify(s => s.ImportExcelIssue(null, ""));
            Assert.NotNull(result);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Data, Is.True);
        }
    }
}
